package com.expenseondemand.eod.webservice.model.approval.detail;


import com.expenseondemand.eod.webservice.model.MasterRequest;

/**
 * Created by ITDIVISION\maximess087 on 4/1/17.
 */

public class ApprovalRejectedExpenseRequest extends MasterRequest {
    private String ExpenseDetailId;
    private String ExpenseHeaderId;
    private String RejectionReasonID;
    private String RejectionReason;
    private String Status;
    private String EntryBy;
    private String CompanyID;
    private String ApproverNote;
    private String CounterApproverNote;
    private String Receiptprovided;

    public String getExpenseDetailId() {
        return ExpenseDetailId;
    }

    public void setExpenseDetailId(String expenseDetailId) {
        ExpenseDetailId = expenseDetailId;
    }

    public String getExpenseHeaderId() {
        return ExpenseHeaderId;
    }

    public void setExpenseHeaderId(String expenseHeaderId) {
        ExpenseHeaderId = expenseHeaderId;
    }

    public String getRejectionReasonID() {
        return RejectionReasonID;
    }

    public void setRejectionReasonID(String rejectionReasonID) {
        RejectionReasonID = rejectionReasonID;
    }

    public String getRejectionReason() {
        return RejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        RejectionReason = rejectionReason;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getEntryBy() {
        return EntryBy;
    }

    public void setEntryBy(String entryBy) {
        EntryBy = entryBy;
    }

    public String getCompanyID() {
        return CompanyID;
    }

    public void setCompanyID(String companyID) {
        CompanyID = companyID;
    }

    public String getApproverNote() {
        return ApproverNote;
    }

    public void setApproverNote(String approverNote) {
        ApproverNote = approverNote;
    }

    public String getCounterApproverNote() {
        return CounterApproverNote;
    }

    public void setCounterApproverNote(String counterApproverNote) {
        CounterApproverNote = counterApproverNote;
    }

    public String getReceiptprovided() {
        return Receiptprovided;
    }

    public void setReceiptprovided(String receiptprovided) {
        Receiptprovided = receiptprovided;
    }
}
