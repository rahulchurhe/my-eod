package com.expenseondemand.eod.webservice.model.approval.detail;

import com.expenseondemand.eod.webservice.model.MasterRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ITDIVISION\maximess087 on 10/1/17.
 */

public class ApprovalRejectedExpensesRequestData extends MasterRequest {

    List<ApprovalRejectedExpenseRequest> requests;

    public List<ApprovalRejectedExpenseRequest> getRequests() {
        return requests;
    }

    public void setRequests(List<ApprovalRejectedExpenseRequest> requests) {
        this.requests = requests;
    }
}
