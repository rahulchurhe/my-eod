package com.expenseondemand.eod.model;

public class ValidationError 
{
	private int validationErrorCode;
	private String validationMessage;

	public ValidationError() 
	{
	}

	public ValidationError(String validationMessage) 
	{
		super();
		this.validationMessage = validationMessage;
	}

	public ValidationError(int validationErrorCode, String validationMessage) 
	{
		super();
		this.validationErrorCode = validationErrorCode;
		this.validationMessage = validationMessage;
	}

	public int getValidationErrorCode() 
	{
		return validationErrorCode;
	}

	public void setValidationErrorCode(int validationErrorCode) 
	{
		this.validationErrorCode = validationErrorCode;
	}

	public String getValidationMessage() 
	{
		return validationMessage;
	}

	public void setValidationMessage(String validationMessage) 
	{
		this.validationMessage = validationMessage;
	}
}
