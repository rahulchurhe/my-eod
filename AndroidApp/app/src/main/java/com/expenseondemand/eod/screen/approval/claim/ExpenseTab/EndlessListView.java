package com.expenseondemand.eod.screen.approval.claim.ExpenseTab;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.model.approval.claim.ApprovalClaimListExpenseItem;
import com.expenseondemand.eod.util.AppConstant;

import java.util.ArrayList;

public class EndlessListView extends ListView implements OnScrollListener {
	
	private Context context;
	private View footer;
	private boolean isLoading;
	private EndLessListener listener;
	private ExpenseListTabAdapter adapter;

	public EndlessListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.setOnScrollListener(this);
		
		this.context = context;
	}

	public EndlessListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.setOnScrollListener(this);
		
		this.context = context;
	}

	public EndlessListView(Context context) {
		super(context);		
		this.setOnScrollListener(this);
		
		this.context = context;
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {

	}	

	//	4
	public void addNewData( ArrayList<ApprovalClaimListExpenseItem> data) {
		if(data.size()==0){

		}
		this.removeFooterView(footer);
		adapter.addAll(data);
		adapter.notifyDataSetChanged();
		isLoading = false;
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		
		if(getAdapter() == null)
			return;
		
		if(getAdapter().getCount() == 0)
			return;
		
		int l = visibleItemCount + firstVisibleItem;
		
		if(l >= totalItemCount && !isLoading){

			if(SharePreference.getBoolean(context, AppConstant.LOAD_MORE_CLAIM)){
				//	add footer layout
				this.addFooterView(footer);

				//	set progress boolean
				isLoading = true;

				//	call interface method to load new data
				listener.loadData();
			}



		}
	}
	
	//	Calling order from MainActivity
	//	1
	public void setLoadingView(int resId) {		
		footer = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(resId, null);		//		footer = (View)inflater.inflate(resId, null);
		this.addFooterView(footer);
	}	

	//	2
	public void setListener(EndLessListener listener) {
		this.listener = listener;
	}	

	//	3
	public void setAdapter(ExpenseListTabAdapter adapter) {
		super.setAdapter(adapter);
		this.adapter = adapter;
		this.removeFooterView(footer);
	}

	public void stopFooter() {
		this.removeFooterView(footer);
	}

	public void LoadMore(){
		listener.loadData();
	}



	//	interface
	public interface EndLessListener{
		public void loadData();
	}
}