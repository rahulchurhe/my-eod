package com.expenseondemand.eod.activity;

import android.os.Bundle;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.util.AppConstant;

public class WhatsNewActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whats_new);
        //Initialize views
        initializeView();
    }

    //initialize
    void initializeView() {
        handleToolBar(getResources().getString(R.string.title_whats_new), AppConstant.DEFAULT_COLOR, AppConstant.DEFAULT_COLOR, true);
    }
}
