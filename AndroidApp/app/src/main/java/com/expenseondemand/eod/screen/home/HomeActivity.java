package com.expenseondemand.eod.screen.home;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.activity.BaseActivity;
import com.expenseondemand.eod.activity.WhatsNewActivity;
import com.expenseondemand.eod.dialogFragment.ApplicationAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationErrorAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationNetworkDialog;
import com.expenseondemand.eod.exception.EODBusinessException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.AuthenticationManager;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.DbManager;
import com.expenseondemand.eod.manager.DeviceManager;
import com.expenseondemand.eod.manager.ExpenseManager;
import com.expenseondemand.eod.manager.PersistentManager;
import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.model.HomeItem;
import com.expenseondemand.eod.model.Statistics;
import com.expenseondemand.eod.model.UserCredential;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.screen.approval.claim.ClaimListActivity;
import com.expenseondemand.eod.screen.authentication.LoginActivity;
import com.expenseondemand.eod.screen.claimant.category.LoadCategoryService;
import com.expenseondemand.eod.screen.claimant.create.CreateEditExpenseActivity;
import com.expenseondemand.eod.screen.claimant.upload.UploadExpenseActivity;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.util.UIUtil;

import java.util.ArrayList;

public class HomeActivity extends BaseActivity implements OnClickListener, HomeItemAdapter.HomeItemEventListener, ApplicationAlertDialog.AlertDialogActionListener, ApplicationErrorAlertDialog.ErrorDialogActionListener {
    public static final int TYPE_LOGOUT = 0;
    public static final int TYPE_ERROR = -1;
    public static final int TYPE_UPGRADE = 1;
    public static final int TYPE_OFFLINE_LOGOUT = 2;
    private RecyclerView recyclerViewHome;
    private ImageView imageViewLogo;
    private TextView textViewClaimantName;
    private ImageButton imageButtonWhatsNew;
    private ImageButton imageButtonHelp;

    private ArrayList<HomeItem> homeData;
    private HomeItemAdapter homeItemAdapter;
    boolean isUpgradeMandatory;
    //set instance alert dialog
    private ApplicationAlertDialog applicationAlertDialog;
    private ApplicationErrorAlertDialog applicationErrorAlertDialog;
    private ApplicationNetworkDialog applicationNetworkDialogl;
    private LoginUserAsyncTask loginUserAsyncTask;

    @Override
    protected void onResume() {
        super.onResume();
        if (SharePreference.getBoolean(getApplicationContext(), AppConstant.BACK_NETWORK_SETTING_STAT)) {
            SharePreference.putBoolean(getApplicationContext(), AppConstant.BACK_NETWORK_SETTING_STAT, false);
        }
        loadCompanyLogo();
        updateStatisticsOnResume();
        populateData();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity_layout2);
        initializeView();
        //  populateData();
        startLoadCategoryService();
    }

    //Initialize View
    private void initializeView() {
        recyclerViewHome = (RecyclerView) findViewById(R.id.recyclerViewHome);
        imageViewLogo = (ImageView) findViewById(R.id.imageViewLogo);
        textViewClaimantName = (TextView) findViewById(R.id.textViewClaimantName);
        imageButtonWhatsNew = (ImageButton) findViewById(R.id.imageButtonWhatsNew);
        imageButtonHelp = (ImageButton) findViewById(R.id.imageButtonHelp);
    }

    private void populateData() {
        String user_name = "";
        UserInfo userInfo = CachingManager.getUserInfo();
        if (userInfo != null) {
            try {
                user_name = userInfo.getDisplayName().substring(0, userInfo.getDisplayName().indexOf(" "));
            } catch (Exception e) {
                user_name = userInfo.getDisplayName();
            }
            textViewClaimantName.setText("Hi, " + user_name);
        }

        final LinearLayoutManager homeItemLinerLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewHome.setLayoutManager(homeItemLinerLayoutManager);

        homeData = new ArrayList<>();

        //CAPTURE - header
        HomeItem homeItem = new HomeItem();
        homeItem.setName(getResources().getString(R.string.home_item_capture));
        homeItem.setType(HomeItem.TYPE_HEADER);
        homeItem.setCount(0);
        homeItem.setImageBackgroundColor(0);
        homeItem.setImageBackgroundSelectedColor(0);
        homeItem.setImageResourceId(0);
        homeItem.setAction(HomeItem.NO_ACTION);
        homeData.add(homeItem);

        //CREATE EXPENSE
        homeItem = new HomeItem();
        homeItem.setName(getResources().getString(R.string.home_item_create_expense));
        homeItem.setType(HomeItem.TYPE_ITEM);
        homeItem.setCount(0);
        homeItem.setImageBackgroundColor(R.color.home_item_create_expense);
        homeItem.setImageBackgroundSelectedColor(R.color.home_item_create_expense_selected);
        homeItem.setImageResourceId(R.drawable.enter_expense_home);
        homeItem.setAction(HomeItem.CREATE_EXPENSE_ITEM_ACTION);
        homeData.add(homeItem);

        //CAMERA
        homeItem = new HomeItem();
        homeItem.setName(getResources().getString(R.string.home_item_camera));
        homeItem.setType(HomeItem.TYPE_ITEM);
        homeItem.setCount(0);
        homeItem.setImageBackgroundColor(R.color.home_item_camera);
        homeItem.setImageBackgroundSelectedColor(R.color.home_item_camera_selected);
        homeItem.setImageResourceId(R.drawable.camera_icon_home);
        homeItem.setAction(HomeItem.CAMERA_ITEM_ACTION);
        homeData.add(homeItem);

        //DUTY OF CARE
        homeItem = new HomeItem();
        homeItem.setName(getResources().getString(R.string.home_item_duty_of_care));
        homeItem.setType(HomeItem.TYPE_ITEM);
        homeItem.setCount(0);
        homeItem.setImageBackgroundColor(R.color.home_item_duty_of_care);
        homeItem.setImageBackgroundSelectedColor(R.color.home_item_duty_of_care_selected);
        homeItem.setImageResourceId(R.drawable.duty_of_care_home);
        homeItem.setAction(HomeItem.DUTY_OF_CARE_EXPENSE_ITEM_ACTION);
        homeData.add(homeItem);

        //ORGANISE - header
        homeItem = new HomeItem();
        homeItem.setName(getResources().getString(R.string.home_item_organise));
        homeItem.setType(HomeItem.TYPE_HEADER);
        homeItem.setCount(0);
        homeItem.setImageBackgroundColor(0);
        homeItem.setImageBackgroundSelectedColor(0);
        homeItem.setImageResourceId(0);
        homeItem.setAction(HomeItem.NO_ACTION);
        homeData.add(homeItem);

        //UPLOAD EXPENSE
        homeItem = new HomeItem();
        homeItem.setName(getResources().getString(R.string.home_item_upload_expense));
        homeItem.setType(HomeItem.TYPE_ITEM);
        homeItem.setCount(0);
        homeItem.setImageBackgroundColor(R.color.home_item_upload_expense);
        homeItem.setImageBackgroundSelectedColor(R.color.home_item_upload_expense_selected);
        homeItem.setImageResourceId(R.drawable.open_item_icon_home);
        homeItem.setAction(HomeItem.UPLOAD_EXPENSE_ITEM_ACTION);
        homeData.add(homeItem);

        //OPEN ITEMS
        homeItem = new HomeItem();
        homeItem.setName(getResources().getString(R.string.home_item_open_item));
        homeItem.setType(HomeItem.TYPE_ITEM);
        homeItem.setCount(0);
        homeItem.setImageBackgroundColor(R.color.home_item_open_item);
        homeItem.setImageBackgroundSelectedColor(R.color.home_item_open_item_selected);
        homeItem.setImageResourceId(R.drawable.open_item_icon_home);
        homeItem.setAction(HomeItem.OPEN_ITEM_ACTION);
        homeData.add(homeItem);

        //VIEW CLAIM STATUS
        homeItem = new HomeItem();
        homeItem.setName(getResources().getString(R.string.home_item_view_claim_status));
        homeItem.setType(HomeItem.TYPE_ITEM);
        homeItem.setCount(0);
        homeItem.setImageBackgroundColor(R.color.home_item_view_claim_status);
        homeItem.setImageBackgroundSelectedColor(R.color.home_item_view_claim_status_selected);
        homeItem.setImageResourceId(R.drawable.view_claim_status_icon_home);
        homeItem.setAction(HomeItem.VIEW_CLAIM_STATUS_ITEM_ACTION);
        homeData.add(homeItem);


        if (userInfo != null) {
            if (userInfo.isApprover()) {
                //APPROVE - header
                homeItem = new HomeItem();
                homeItem.setName(getResources().getString(R.string.home_item_approve));
                homeItem.setType(HomeItem.TYPE_HEADER);
                homeItem.setCount(0);
                homeItem.setImageBackgroundColor(0);
                homeItem.setImageBackgroundSelectedColor(0);
                homeItem.setImageResourceId(0);
                homeItem.setAction(HomeItem.NO_ACTION);
                homeData.add(homeItem);

                //APPROVALS
                homeItem = new HomeItem();
                homeItem.setName(getResources().getString(R.string.home_item_approvals));
                homeItem.setType(HomeItem.TYPE_ITEM);
                homeItem.setCount(0);
                homeItem.setImageBackgroundColor(R.color.home_item_approvals);
                homeItem.setImageBackgroundSelectedColor(R.color.home_item_approvals_selected);
                homeItem.setImageResourceId(R.drawable.approval_icon_home);
                homeItem.setAction(HomeItem.APPROVALS_ITEM_ACTION);
                homeData.add(homeItem);
            }
        }


        homeItemAdapter = new HomeItemAdapter(this, this, homeData);
        recyclerViewHome.setAdapter(homeItemAdapter);

        //Start Handle Top Layout
        imageButtonHelp.setOnClickListener(this);

        imageButtonWhatsNew.setOnClickListener(this);

        //Handle Bottom bar
        LinearLayout linearLayoutAdvances = (LinearLayout) findViewById(R.id.linearLayoutAdvances);
        linearLayoutAdvances.setOnClickListener(this);

        LinearLayout linearLayoutInvite = (LinearLayout) findViewById(R.id.linearLayoutInvite);
        linearLayoutInvite.setOnClickListener(this);

        LinearLayout linearLayoutAnalytics = (LinearLayout) findViewById(R.id.linearLayoutAnalytics);
        linearLayoutAnalytics.setOnClickListener(this);

        LinearLayout linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutProfile.setOnClickListener(this);

        LinearLayout linearLayoutLogout = (LinearLayout) findViewById(R.id.linearLayoutLogout);
        linearLayoutLogout.setOnClickListener(this);
        //End Handle Bottom bar

        loadCompanyLogo();
        updateStatistics();
    }

    private void loadCompanyLogo() {
        LoadImageAsyncTask loadImageAsyncTask = new LoadImageAsyncTask();
        loadImageAsyncTask.execute();
    }

    private void startLoadCategoryService() {
        //Check if to Sync Categories
        if (true) {
            Intent intent = new Intent(this, LoadCategoryService.class);
            startService(intent);
        }
    }

    @Override
    public void handleHomeItemClick(int position, int x, int y) {
        switch (position) {
            case HomeItem.CREATE_EXPENSE_ITEM_ACTION: {
                handleCreateExpense();
                break;
            }

            case HomeItem.CAMERA_ITEM_ACTION: {
                handleCamera();
                toastMessage(x, y);
                break;
            }

            case HomeItem.DUTY_OF_CARE_EXPENSE_ITEM_ACTION: {
                handleDutyOfCare();
                toastMessage(x, y);
                break;
            }

            case HomeItem.UPLOAD_EXPENSE_ITEM_ACTION: {
                handleUploadExpense();
                break;
            }

            case HomeItem.OPEN_ITEM_ACTION: {
                handleOpenItem();
                toastMessage(x, y);
                break;
            }

            case HomeItem.VIEW_CLAIM_STATUS_ITEM_ACTION: {
                handleViewClaimStatus();
                toastMessage(x, y);
                break;
            }


            case HomeItem.APPROVALS_ITEM_ACTION: {
                handleApproval();
                break;
            }
        }
    }

    private void toastMessage(int x, int y) {

        Toast toast = Toast.makeText(getApplicationContext(), "Coming Soon", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP | Gravity.RIGHT, Math.round(x), Math.round(y + 170));
        toast.show();
    }

    @Override
    public void onClick(View view) {
        //Get View Id
        int viewId = view.getId();

        switch (viewId) {
            case R.id.imageButtonHelp: {
                handleHelp();
                break;
            }

            case R.id.imageButtonWhatsNew: {
                handleWhatsNew();
                break;
            }
            case R.id.linearLayoutAdvances: {
                handleAdvances();
                break;
            }
            case R.id.linearLayoutInvite: {
                handleInvite();
                break;
            }

            case R.id.linearLayoutAnalytics: {
                handleAnalytics();
                break;
            }

            case R.id.linearLayoutProfile: {
                handleProfile();
                break;
            }

            case R.id.linearLayoutLogout: {
                handleLogout();
                break;
            }
        }
    }

    //Start handle top layout item event
    //Handle Help
    private void handleHelp() {
        Toast toast = Toast.makeText(getApplicationContext(), "Coming Soon", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP | Gravity.RIGHT, 0, 0);
        toast.show();
    }

    //Handle Message
    private void handleWhatsNew() {
        //Navigate to whats new screen
        navigateToWhatsNewActivity();
    }
    //End handle top layout item event


    //Start Handle Home menu item event
    //Handle Create Expense
    private void handleCreateExpense() {
        navigateToCreateExpense();
    }

    //Handle Camera
    private void handleCamera() {
        //  Toast.makeText(HomeActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();
    }

    //Handle Duty of Care
    private void handleDutyOfCare() {
        // Toast.makeText(HomeActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();
    }

    //Handle Upload Expense
    private void handleUploadExpense() {
        navigateToExpenseList();
    }

    //Handle Open Item
    private void handleOpenItem() {
        // Toast.makeText(HomeActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();
    }

    //Handle View Claim Status
    private void handleViewClaimStatus() {
        // Toast.makeText(HomeActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();
    }

    //Handle Approval
    private void handleApproval() {
        navigateToApprovalClaimList();
    }

    //Handle Expense List
    private void handleExpenseList() {
        navigateToExpenseList();
    }
    //End Handle Home menu item envet

    //Start Handle Bottom bar events
    //Handle Advances
    private void handleAdvances() {
        Toast.makeText(HomeActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();
    }

    //Handle Invite
    private void handleInvite() {
        Toast.makeText(HomeActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();
    }

    //handle Analytics
    private void handleAnalytics() {
        Toast.makeText(HomeActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();
    }

    //handle Profile
    private void handleProfile() {
        Toast.makeText(HomeActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();
    }

    //Handle Logout
    private void handleLogout() {
        //Check if Network Available
        if (DeviceManager.isOnline()) {
            //Show Confirmation dialog
            showAlertDialog(false, getString(R.string.text_logout_message), getString(R.string.text_logout), getString(R.string.button_yes), getString(R.string.button_no), TYPE_LOGOUT);

        } else {
            //Show Network Error Dialog
            showAlertDialog(false, getString(R.string.text_logout_message), getString(R.string.text_logout), getString(R.string.button_yes), getString(R.string.button_no), TYPE_OFFLINE_LOGOUT);
        }

    }
    //End Handle Bottom bar events

    //Logout
    private void logout() {
        //Logout AsyncTask
        LogoutAsyncTask logoutAsyncTask = new LogoutAsyncTask();
        logoutAsyncTask.execute();
    }


    //Navigate to Create Expense
    private void navigateToCreateExpense() {
        //Create Intent
        Intent intent = new Intent(this, CreateEditExpenseActivity.class);
        startActivity(intent);
    }

    //Navigate to Expense List
    private void navigateToExpenseList() {
        //Create Intent
        Intent intent = new Intent(this, UploadExpenseActivity.class);
        startActivity(intent);
    }

    //Navigate to Approval Claim List
    private void navigateToApprovalClaimList() {
        Intent intent = new Intent(this, ClaimListActivity.class);
        startActivity(intent);
    }

    //Navigate to Whats New Screen
    private void navigateToWhatsNewActivity() {
        Intent intent = new Intent(this, WhatsNewActivity.class);
        startActivity(intent);
//		showApplicationUpdateDialog(true);
    }

    //Navigate to Login Screen
    private void navigateToLogin() {
        //Get user is remembered or not
        boolean isUserRemembered = false;
        //Persist isUserRemembered
        PersistentManager.persistIsUserRemembered(isUserRemembered);
        //Create Intent
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        this.finish();
    }


    //Logout AsyncTask
    private class LogoutAsyncTask extends AsyncTask<Void, Void, Void> {
        private Dialog dialog = null;
        private Exception exception = null;

        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(HomeActivity.this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                //Logout User
                AuthenticationManager.logoutUser();
            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);

            if (exception == null) {
                //OnSucess
                onSuccessLogoutUser();
            } else if (exception instanceof EODException) {
                //OnError
                onErrorLogoutUser((EODException) exception);
            }
        }
    }

    //On Success Logout User
    private void onSuccessLogoutUser() {
        //Navigate to Login Screen
        navigateToLogin();
    }

    //On Error Logout
    private void onErrorLogoutUser(EODException eodException) {
        //Show Alert Dialog
        if (eodException == null) {
            showErrorAlertDialog(null, null, null, TYPE_LOGOUT);
        }
    }


    //Alert Dialogs
    void showAlertDialog(boolean neutralButton, String message, String title, String okButtonTitle, String cancelButtonTitle, int type) {
        applicationAlertDialog = new ApplicationAlertDialog().getInstance(this, neutralButton, message, title, okButtonTitle, cancelButtonTitle, type);
        applicationAlertDialog.show(getSupportFragmentManager(), "dialog");
    }

    void showErrorAlertDialog(String message, String title, String okButtonTitle, int type) {
        applicationErrorAlertDialog = new ApplicationErrorAlertDialog().getInstance(this, message, title, okButtonTitle, type);
        applicationErrorAlertDialog.show(getSupportFragmentManager(), "dialog");
    }

    void enableNetwork() {
        applicationNetworkDialogl = new ApplicationNetworkDialog().getInstance(HomeActivity.this);
        applicationNetworkDialogl.show(getSupportFragmentManager(), "dialog");
    }

    //upgrade dialog
    void showApplicationUpdateDialog() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        String okButtonTitle = null;
        if (isUpgradeMandatory) {
            okButtonTitle = getString(R.string.label_button_download);
        } else {
            okButtonTitle = getString(R.string.label_button_alert_dialog_remind_me_later);
        }
        showAlertDialog(false, getString(R.string.label_application_update_dialog_information), getString(R.string.label_application_update_dialog_title), okButtonTitle, null, TYPE_UPGRADE);
    }

    @Override
    public void onRetryButtonClick(int type) {
        updateStatistics();
    }

    @Override
    public void onCancelButtonClick() {
    }

    @Override
    public void onPositiveButtonClick(int type) {
        switch (type) {
            case TYPE_LOGOUT:
                //Logout
                // logout();
                finish();
                navigateToLogin();
                break;
            case TYPE_OFFLINE_LOGOUT:
                //Logout
                finish();
                navigateToLogin();
                break;
            case TYPE_UPGRADE:
                if (isUpgradeMandatory) {
                    finish();
                    overridePendingTransition(R.anim.slide_in_back, R.anim.slide_out_back);
                } else {
                    //TODO markUserAppUpdateStatus
                }
                break;
        }
    }

    @Override
    public void onNegativeButtonClick() {

    }


    private void updateStatisticsOnResume() {
        if (DeviceManager.isOnline()) {
            StatisticsAsyncTask statisticsAsyncTask = new StatisticsAsyncTask();
            statisticsAsyncTask.execute();
        } else {

        }
    }

    private void updateStatistics() {
        if (DeviceManager.isOnline()) {
            StatisticsAsyncTask statisticsAsyncTask = new StatisticsAsyncTask();
            statisticsAsyncTask.execute();
        } else {
            enableNetwork();
        }
    }

    //Statistics AsyncTask
    private class StatisticsAsyncTask extends AsyncTask<Void, Void, Statistics> {
        private Exception exception = null;
        private Dialog dialog = null;

        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(HomeActivity.this);
        }

        @Override
        protected Statistics doInBackground(Void... params) {
            Statistics statistics = null;

            try {
                statistics = HomeManager.getStatistics();
            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return statistics;
        }

        @Override
        protected void onPostExecute(Statistics statistics) {
            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);

            if (exception == null) {
                //OnSuccess
                onSuccessGetStatistics(statistics);

            } else if (exception instanceof EODBusinessException) {
                //OnFail
                onFailGetStatistics((EODException) exception);
            } else if (exception instanceof EODException) {
                //OnError
                onErrorGetStatistics(exception);
            }
        }
    }

    private void onSuccessGetStatistics(Statistics statistics) {
        if (statistics != null) {
            //Set Approvals count
            for (HomeItem homeItem : homeData) {
                if (homeItem.getAction() == HomeItem.APPROVALS_ITEM_ACTION) {
                    homeItem.setCount(Integer.parseInt(statistics.getPendingApprovalCount()));
                    homeItemAdapter.notifyDataSetChanged();
                    break;
                }
            }
            //Set Upload expense count
            for (HomeItem homeItem : homeData) {
                if (homeItem.getAction() == HomeItem.UPLOAD_EXPENSE_ITEM_ACTION) {
                    UserInfo userInfo = CachingManager.getUserInfo();
                    if (userInfo != null) {
                        try {
                            int count = ExpenseManager.getExpenseListCount(userInfo.getUserId());
                            if (count != 0) {
                                homeItem.setCount(Integer.parseInt("" + count));
                                homeItemAdapter.notifyDataSetChanged();
                            } else {
                                homeItem.setCount(0);
                                homeItemAdapter.notifyDataSetChanged();
                            }
                        } catch (EODException eodException) {
                        }
                    }
                    break;
                }
            }

            //Save App rejected setting.
            CachingManager.saveAppRejectedSetting(statistics.getAppRejSettings());
        }
        UserCredential userCredential = CachingManager.getUserInfo().getUserCredential();
        if (DeviceManager.isOnline() && userCredential != null) {
            //Async Task
            loginUserAsyncTask = new LoginUserAsyncTask();
            loginUserAsyncTask.execute(userCredential);
        }

    }

    private void onFailGetStatistics(EODException exception) {
        if (exception == null) {
            showErrorAlertDialog(null, null, null, TYPE_LOGOUT);
        }

        //showResponseErrorAlertDialog(false, null, null, null);
    }

    private void onErrorGetStatistics(Exception exception) {
        if (exception == null) {
            showErrorAlertDialog(null, null, null, TYPE_LOGOUT);
        } else {
            //when user credential change on web app
            try {
                UserCredential userCredential = CachingManager.getUserInfo().getUserCredential();
            } catch (Exception e) {
                finish();
                navigateToLogin();
            }

        }

        //showResponseErrorAlertDialog(false, null, null, null);
    }

    //Statistics AsyncTask
    private class LoadImageAsyncTask extends AsyncTask<Void, Void, Bitmap> {
        private Exception exception = null;

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            Bitmap imageData = null;

            try {
                imageData = HomeManager.getImageData();
            } catch (EODException eodException) {
                this.exception = eodException;
            } catch (Exception Exception) {

            }

            return imageData;
        }

        @Override
        protected void onPostExecute(Bitmap imageData) {
            try {
                //Dismiss progress dialog.
                if (exception == null) {
                    //OnSuccess
                    onSuccessLoadImage(imageData);
                } else if (exception instanceof EODBusinessException) {
                    //OnFail
                    onFailLoadImage((EODException) exception);
                } else if (exception instanceof EODException) {
                    //OnError
                    onErrorLoadImage(exception);
                }
            } catch (Exception e) {
                // Toast.makeText(HomeActivity.this,"Insufficient Memory",Toast.LENGTH_LONG).show();
            }
        }
    }

    private void onSuccessLoadImage(Bitmap bitmap) {
        if (bitmap != null)

            bitmap.setHasAlpha(true);
        imageViewLogo.setImageBitmap(bitmap);
    }

    private void onFailLoadImage(EODException exception) {
    }

    private void onErrorLoadImage(Exception exception) {
    }

    //Login User AsyncTask
    private class LoginUserAsyncTask extends AsyncTask<UserCredential, Void, UserInfo> {
        private Exception exception = null;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected UserInfo doInBackground(UserCredential... params) {
            UserInfo userInfo = null;
            SharePreference.putBoolean(getApplicationContext(), "IS_LOGIN", true);
            try {
                //Authenticate User
                userInfo = AuthenticationManager.authenticateUser(params[0]);
                DbManager.saveUserInformation(userInfo);

            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return userInfo;
        }

        @Override
        protected void onPostExecute(UserInfo user) {
            //Dismiss progress dialog.


            if (exception == null) {
                //OnSuccess
                if (user != null) {
                    String user_name="";
                    try {
                        user_name = user.getDisplayName().substring(0, user.getDisplayName().indexOf(" "));
                    } catch (Exception e) {
                        user_name = user.getDisplayName();
                    }
                    textViewClaimantName.setText("Hi, " + user_name);

                }
                onSuccessAuthenticateUser(user);
            } else if (exception instanceof EODBusinessException) {
                //OnFail

            } else if (exception instanceof EODException) {
                //OnError
            }
        }
    }

    public void onSuccessAuthenticateUser(UserInfo user) {
        if (user != null) {
            //Get user is remembered or not
            boolean isUserRemembered = true;

            //Persist isUserRemembered
            PersistentManager.persistIsUserRemembered(isUserRemembered);

            //Persist Last LoggedIn UserId
            PersistentManager.persistUserId(user.getUserId());
        }
    }
}
