package com.expenseondemand.eod.webservice.model.approval.detail.expense.receipt;

import com.expenseondemand.eod.webservice.model.MasterResponse;

/**
 * Created by ITDIVISION\maximess087 on 28/1/17.
 */

public class ExpenseDetailReceiptResponse extends MasterResponse{
    private ExpenseDetailsReceiptData data;

    public ExpenseDetailsReceiptData getData() {
        return data;
    }

    public void setData(ExpenseDetailsReceiptData data) {
        this.data = data;
    }
}
