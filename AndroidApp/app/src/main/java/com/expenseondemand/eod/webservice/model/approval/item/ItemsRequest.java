package com.expenseondemand.eod.webservice.model.approval.item;

import com.expenseondemand.eod.webservice.model.MasterRequest;

/**
 * Created by Nandani Gupta on 2016-10-20.
 */
public class ItemsRequest extends MasterRequest
{
    String  approverId;
    String  approverCompanyId;
    String  claimId;
    String  approverFlag;
    int NoOfRecords;
    int PageNo;

    public int getNoOfRecords() {
        return NoOfRecords;
    }

    public void setNoOfRecords(int noOfRecords) {
        NoOfRecords = noOfRecords;
    }

    public int getPageNo() {
        return PageNo;
    }

    public void setPageNo(int pageNo) {
        PageNo = pageNo;
    }

    public String getApproverId() {
        return approverId;
    }

    public void setApproverId(String approverId) {
        this.approverId = approverId;
    }

    public String getApproverCompanyId() {
        return approverCompanyId;
    }

    public void setApproverCompanyId(String approverCompanyId) {
        this.approverCompanyId = approverCompanyId;
    }

    public String getClaimId() {
        return claimId;
    }

    public void setClaimId(String claimId) {
        this.claimId = claimId;
    }

    public String getApproverFlag() {
        return approverFlag;
    }

    public void setApproverFlag(String approverFlag) {
        this.approverFlag = approverFlag;
    }
}
