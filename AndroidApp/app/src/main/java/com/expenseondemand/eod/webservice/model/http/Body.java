package com.expenseondemand.eod.webservice.model.http;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 26-09-2016.
 */
public class Body
{
    String jsonRequestString;
    ArrayList<String> multipartFileName;

    public String getJsonRequestString()
    {
        return jsonRequestString;
    }

    public void setJsonRequestString(String jsonRequestString)
    {
        this.jsonRequestString = jsonRequestString;
    }

    public ArrayList<String> getMultipartFileName()
    {
        return multipartFileName;
    }

    public void setMultipartFileName(ArrayList<String> multipartFileName)
    {
        this.multipartFileName = multipartFileName;
    }
}
