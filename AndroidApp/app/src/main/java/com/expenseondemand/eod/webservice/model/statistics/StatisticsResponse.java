package com.expenseondemand.eod.webservice.model.statistics;

import com.expenseondemand.eod.webservice.model.MasterResponse;

/**
 * Created by Ramit Yadav on 14-10-2016.
 */

public class StatisticsResponse extends MasterResponse {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
