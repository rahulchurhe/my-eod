package com.expenseondemand.eod.model;

/**
 * Created by ITDIVISION\maximess087 on 24/1/17.
 */

public class AppRejectionSetting {
    private boolean AppRejectSettingsRule1;
    private boolean AppRejectSettingsRule2;
    private boolean AppRejectSettingsRule3;
    private boolean AppRejectSettingsRule4;


    public boolean isAppRejectSettingsRule1() {
        return AppRejectSettingsRule1;
    }

    public void setAppRejectSettingsRule1(String appRejectSettingsRule1) {
        this.AppRejectSettingsRule1 = (appRejectSettingsRule1 != null) && (appRejectSettingsRule1.equals("Y"));
    }

    public boolean isAppRejectSettingsRule2() {
        return AppRejectSettingsRule2;
    }

    public void setAppRejectSettingsRule2(String appRejectSettingsRule2) {
        this.AppRejectSettingsRule2 = (appRejectSettingsRule2 != null) && (appRejectSettingsRule2.equals("Y"));
    }

    public boolean isAppRejectSettingsRule3() {
        return AppRejectSettingsRule3;
    }

    public void setAppRejectSettingsRule3(String appRejectSettingsRule3) {
        this.AppRejectSettingsRule3 = (appRejectSettingsRule3 != null) && (appRejectSettingsRule3.equals("Y"));
    }

    public boolean isAppRejectSettingsRule4() {
        return AppRejectSettingsRule4;
    }

    public void setAppRejectSettingsRule4(String appRejectSettingsRule4) {
        this.AppRejectSettingsRule4 = (appRejectSettingsRule4 != null) && (appRejectSettingsRule4.equals("Y"));
    }
}
