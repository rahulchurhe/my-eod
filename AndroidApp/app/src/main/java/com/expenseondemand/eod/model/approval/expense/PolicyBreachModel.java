package com.expenseondemand.eod.model.approval.expense;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Nandani Gupta on 2016-10-20.
 */
public class PolicyBreachModel implements Parcelable
{
    private String expenseDetailId;
    private String rowId;
    private boolean isDailyCap;
    private boolean isExpenseLimit;
    private boolean isMaxSpendBreach;
    private boolean isPreApproval;
    private String amountSetLimit;
    private String amountBreach;
    private String status;
    private String expenseLimit;
    private String MaxSpendamt;
    private String DailCapAmt;
    private ArrayList<Notes> notes;

    public String getDailCapAmt() {
        return DailCapAmt;
    }

    public void setDailCapAmt(String dailCapAmt) {
        DailCapAmt = dailCapAmt;
    }

    /**
     * Standard basic constructor for non-parcel
     * object creation
     */
    public PolicyBreachModel() { }

    /**
     *
     * Constructor to use when re-constructing object
     * from a parcel
     *
     * @param in a parcel from which to read this object
     */
    public PolicyBreachModel(Parcel in) {
        ApproverExpensePolicyBreechModel(in);
    }
    /**
     *
     * Called from the constructor to create this
     * object from a parcel.
     *
     * @param in parcel from which to re-create object
     */
    private void ApproverExpensePolicyBreechModel(Parcel in) {
        expenseDetailId = in.readString();
        rowId = in.readString();

        isDailyCap = in.readByte() != 0;     //isDailyCap == true if byte != 0
        isExpenseLimit = in.readByte() != 0; //isExpenseLimit == true if byte != 0
        isMaxSpendBreach = in.readByte() != 0; //isMaxSpendBreach == true if byte != 0
        isPreApproval = in.readByte() != 0; //isPreApproval == true if byte != 0
        amountSetLimit = in.readString();
        amountBreach = in.readString();
        status = in.readString();
        expenseLimit = in.readString();
        MaxSpendamt = in.readString();
        DailCapAmt = in.readString();
        notes = in.readArrayList(Notes.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(expenseDetailId);
        dest.writeString(rowId);
        dest.writeByte((byte) (isDailyCap ? 1 : 0));   //if isDailyCap == true, byte == 1
        dest.writeByte((byte) (isExpenseLimit ? 1 : 0));   //if isExpenseLimit == true, byte == 1
        dest.writeByte((byte) (isMaxSpendBreach ? 1 : 0));   //if isMaxSpendBreach == true, byte == 1
        dest.writeByte((byte) (isPreApproval ? 1 : 0));   //if isPreApproval == true, byte == 1
        dest.writeString(amountSetLimit);
        dest.writeString(amountBreach);
        dest.writeString(status);
        dest.writeString(expenseLimit);
        dest.writeString(MaxSpendamt);
        dest.writeString(DailCapAmt);
        dest.writeList(notes);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PolicyBreachModel> CREATOR = new Creator<PolicyBreachModel>() {
        @Override
        public PolicyBreachModel createFromParcel(Parcel in) {
            return new PolicyBreachModel(in);
        }

        @Override
        public PolicyBreachModel[] newArray(int size) {
            return new PolicyBreachModel[size];
        }
    };

    public ArrayList<Notes> getNotes()
    {
        return notes;
    }

    public void setNotes(ArrayList<Notes> notes) {
        this.notes = notes;
    }

    public boolean isExpenseLimit() {
        return isExpenseLimit;
    }

    public void setIsExpenseLimit(String isExpenseLimit) {
        this.isExpenseLimit =  (isExpenseLimit != null) && (isExpenseLimit.equals("Y"));
    }

    public String getExpenseDetailId() {
        return expenseDetailId;
    }

    public void setExpenseDetailId(String expenseDetailId) {
        this.expenseDetailId = expenseDetailId;
    }

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public boolean isDailyCap() {
        return isDailyCap;
    }

    public void setIsDailyCap(String isDailyCap) {
        this.isDailyCap = (isDailyCap != null) && (isDailyCap.equals("Y"));
    }

    public boolean isMaxSpendBreach() {
        return isMaxSpendBreach;
    }

    public void setIsMaxSpendBreach(String isMaxSpendBreach) {
        this.isMaxSpendBreach = (isMaxSpendBreach != null) && (isMaxSpendBreach.equals("Y"));
    }

    public boolean isPreApproval() {
        return isPreApproval;
    }

    public void setIsPreApproval(String isPreApproval) {
        this.isPreApproval = (isPreApproval != null) && (isPreApproval.equals("Y"));
    }

    public String getAmountSetLimit() {
        return amountSetLimit;
    }

    public void setAmountSetLimit(String amountSetLimit) {
        this.amountSetLimit = amountSetLimit;
    }

    public String getAmountBreach() {
        return amountBreach;
    }

    public void setAmountBreach(String amountBreach) {
        this.amountBreach = amountBreach;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isCounterApproval()
    {
        return (status != null) && (status.equals("B"));
    }

    public String getExpenseLimit() {
        return expenseLimit;
    }

    public void setExpenseLimit(String expenseLimit) {
        this.expenseLimit = expenseLimit;
    }

    public String getMaxSpendamt() {
        return MaxSpendamt;
    }

    public void setMaxSpendamt(String maxSpendamt) {
        MaxSpendamt = maxSpendamt;
    }
}
