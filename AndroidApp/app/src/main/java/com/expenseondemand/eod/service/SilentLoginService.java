package com.expenseondemand.eod.service;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.AuthenticationManager;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.DbManager;
import com.expenseondemand.eod.manager.DeviceManager;
import com.expenseondemand.eod.manager.PersistentManager;
import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.model.UserCredential;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.receiver.ConnectivityChangeReceiver;

/**
 * Created by priya on 21-02-2017.
 */

public class SilentLoginService extends IntentService {
    public final static int SILENT_LOGIN_INPROGRESS = 1;
    public final static int SILENT_LOGIN_SUCCESS = 2;
    public final static int SILENT_LOGIN_FAIL = 0;

    public SilentLoginService() {
        super("SilentLoginService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (DeviceManager.isNetworkAvailable() && PersistentManager.isUserRemembered()) {
            UserInfo userInfo = CachingManager.getUserInfo();
            if (userInfo != null) {
                UserCredential userCredential = userInfo.getUserCredential();
                if (userCredential != null) {
                    handleSilentLogin(userCredential);
                } else {
                    PersistentManager.persistSilentLoginStatus(SilentLoginService.SILENT_LOGIN_FAIL);
                }
            } else {
                PersistentManager.persistSilentLoginStatus(SilentLoginService.SILENT_LOGIN_FAIL);
            }
        } else {
            PersistentManager.persistSilentLoginStatus(SilentLoginService.SILENT_LOGIN_FAIL);
        }
        ConnectivityChangeReceiver.completeWakefulIntent(intent);
    }

    private void handleSilentLogin(UserCredential userCredential) {
        UserInfo userInfo = null;

        SharePreference.putBoolean(getApplicationContext(), "IS_LOGIN", true);
        try {
            //Authenticate User
            userInfo = AuthenticationManager.authenticateUser(userCredential);
            DbManager.saveUserInformation(userInfo);
            PersistentManager.persistSilentLoginStatus(SilentLoginService.SILENT_LOGIN_SUCCESS);
        } catch (EODException eodException) {
            PersistentManager.persistSilentLoginStatus(SilentLoginService.SILENT_LOGIN_FAIL);
        }
    }
}


