package com.expenseondemand.eod.util;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URLConnection;

/**
 * Created by Ramit Yadav on 26-09-2016.
 */
public class MultipartUtility
{
    private String boundary;
    private String charset;
    private OutputStream outputStream;
    private PrintWriter printWriter;

    /**
     * This Constructor
     * @param outputStream
     * @param printWriter
     * @param charset
     * @param boundary
     */
    public MultipartUtility(OutputStream outputStream, PrintWriter printWriter, String charset, String boundary)
    {
        this.outputStream = outputStream;
        this.printWriter = printWriter;
        this.charset = charset;
        this.boundary = boundary;
    }

    /**
     * Adds a form field to the request
     * @param name field name
     * @param value field value
     */
    public void addFormField(String name, String value)
    {
        printWriter.append(AppConstant.TWO_HYPHENS + boundary).append(AppConstant.LINE_FEED);
        printWriter.append(AppConstant.REQUEST_CONTENT_DISPOSITION_FORM_DATA_NAME + name + "\"").append(AppConstant.LINE_FEED);
        printWriter.append(AppConstant.REQUEST_HEADER_CONTENT_TYPE_KEY + " : " + AppConstant.CONTENT_TYPE_JSON_MULTIPART_DATA + "; charset=" + charset).append(AppConstant.LINE_FEED);
        printWriter.append(AppConstant.LINE_FEED);
        printWriter.append(value).append(AppConstant.LINE_FEED);
        printWriter.flush();
    }

    /**
     * Adds a upload file section to the request
     * @param fieldName name attribute in <input type="file" name="..." />
     * @param uploadFile a File to be uploaded
     * @throws IOException
     */
    public void addFilePart(String fieldName, File uploadFile) throws IOException
    {
        String fileName = uploadFile.getName();
        printWriter.append(AppConstant.TWO_HYPHENS + boundary).append(AppConstant.LINE_FEED);
        printWriter.append(AppConstant.REQUEST_CONTENT_DISPOSITION_FORM_DATA_NAME + fieldName + AppConstant.FILE_NAME + fileName + "\"").append(AppConstant.LINE_FEED);
        printWriter.append(AppConstant.REQUEST_HEADER_CONTENT_TYPE_KEY + " : " + URLConnection.guessContentTypeFromName(fileName)).append(AppConstant.LINE_FEED);
        printWriter.append(AppConstant.REQUEST_CONTENT_TRANSFER_ENCODING).append(AppConstant.LINE_FEED);
        printWriter.append(AppConstant.LINE_FEED);
        printWriter.flush();

        FileInputStream inputStream = new FileInputStream(uploadFile);
        byte[] buffer = new byte[4096];
        int bytesRead = -1;

        while ((bytesRead = inputStream.read(buffer)) != -1)
        {
            outputStream.write(buffer, 0, bytesRead);
        }

        outputStream.flush();
        inputStream.close();
        printWriter.append(AppConstant.LINE_FEED);
        printWriter.flush();
    }
}
