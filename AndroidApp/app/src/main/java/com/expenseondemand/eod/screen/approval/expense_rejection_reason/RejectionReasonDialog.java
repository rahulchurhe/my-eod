package com.expenseondemand.eod.screen.approval.expense_rejection_reason;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.RejectionReasonManger;
import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.model.AppRejectionSetting;
import com.expenseondemand.eod.model.approval.detail.ExpenseDetail;
import com.expenseondemand.eod.screen.approval.detail.ApproveRejectedInterface;
import com.expenseondemand.eod.screen.approval.detail.ExpenseDetailActivity;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.UIUtil;
import com.expenseondemand.eod.webservice.model.RejectionReasonModel.RejectionReasonModel;

import java.util.ArrayList;

/**
 * Created by Nandani Gupta on 2016-09-19.
 */
public class RejectionReasonDialog extends DialogFragment implements View.OnClickListener, RejectionReasonListAdapter.RejectionReasonDialogClickListener {
    RecyclerView recyclerViewReasonList;
    ArrayList<RejectionReasonModel> rejectionReasonList;
    TextView textViewCancel;
    TextView textViewOk;
    TextView textViewOther;
    EditText editTextRejectNotes;
    TextView textViewNotesCount;
    RejectionReasonListAdapter rejectionReasonListAdapter;
    int selectedReasonPos=-1;
    ScrollView scrollViewReasonDialog;
    private static ApproveRejectedInterface listener;
    private String str_RejectionReasonID,str_RejectioxnReason;
    AppRejectionSetting appRejectionSetting;
    private TextView textViewNoticeForAll;

    public static RejectionReasonDialog newInstance(ArrayList<RejectionReasonModel> rejectionReasonList,ApproveRejectedInterface listener1) {
        RejectionReasonDialog frag = new RejectionReasonDialog();
        frag.rejectionReasonList = rejectionReasonList;
        listener=listener1;
        return frag;
    }


    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout( ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
        //initialize dialog
        Dialog dialog = getDialog();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View view = inflater.inflate(R.layout.rejection_reason_dialog, container, false);

        initialization(view);
        setNoteAsPerRejectionSettingRule();
        return view;

    }

    //Initialize
    void initialization(View view) {
        appRejectionSetting= CachingManager.getAppRejectedSetting();
        scrollViewReasonDialog = (ScrollView) view.findViewById(R.id.scrollViewReasonDialog);


        recyclerViewReasonList = (RecyclerView) view.findViewById(R.id.recyclerViewReasonList);
        //set adapter
        rejectionReasonListAdapter = new RejectionReasonListAdapter(getActivity(), rejectionReasonList, this);
        recyclerViewReasonList.setAdapter(rejectionReasonListAdapter);


        textViewCancel = (TextView) view.findViewById(R.id.textViewCancel);
        textViewCancel.setOnClickListener(this);
        textViewOk = (TextView) view.findViewById(R.id.textViewOk);
        textViewOk.setOnClickListener(this);


        textViewOther = (TextView) view.findViewById(R.id.textViewOther);
        textViewOther.setOnClickListener(this);

        editTextRejectNotes = (EditText) view.findViewById(R.id.editTextRejectNotes);
        textViewNotesCount = (TextView) view.findViewById(R.id.textViewNotesCount);

        editTextRejectNotes.addTextChangedListener(new RejectionNotesTextWatcher());
        textViewNoticeForAll=(TextView)view.findViewById(R.id.textViewNoticeForAll);

        //prepare reason list
        initializeReasonData();

    }

    private void setNoteAsPerRejectionSettingRule() {
        if(SharePreference.getBoolean(getActivity(),"IsMultiRejection")){
            textViewNoticeForAll.setVisibility(View.VISIBLE);
        }else{
            textViewNoticeForAll.setVisibility(View.GONE);
        }
    }


    void initializeReasonData() {
        if (rejectionReasonList.size() > 0) {

            rejectionReasonListAdapter.notifyDataSetChanged();

        } else {
            GetRejectionReasonListAsyncTask getRejectionReasonList = new GetRejectionReasonListAsyncTask();
            getRejectionReasonList.execute();
        }
    }


    //Rejection Reason  User AsyncTask
    private class GetRejectionReasonListAsyncTask extends AsyncTask<Void, Void, ArrayList<RejectionReasonModel>> {
        private Dialog progressDialog;
        private Exception exception = null;

        @Override
        protected void onPreExecute() {
            scrollViewReasonDialog.setVisibility(View.GONE);
            progressDialog = UIUtil.showProgressDialog(getActivity());
        }

        @Override
        protected ArrayList<RejectionReasonModel> doInBackground(Void... params) {

            ArrayList<RejectionReasonModel> rejectionReasonLocalList = null;

            try {
                rejectionReasonLocalList = RejectionReasonManger.getRejectionReasonList();

            } catch (EODException exception) {

                this.exception = exception;
            }


            return rejectionReasonLocalList;
        }

        @Override
        protected void onPostExecute(ArrayList<RejectionReasonModel> rejectionReasonLocalList) {
            scrollViewReasonDialog.setVisibility(View.VISIBLE);

            if (exception == null) {
                if (rejectionReasonLocalList != null) {
                    //Dismiss progress dialog.
                    UIUtil.dismissDialog(progressDialog);
                    //On Success
                    onSuccessGetRejectionReasonList(rejectionReasonLocalList);
                }

            } else {
                //Dismiss progress dialog.
                UIUtil.dismissDialog(progressDialog);
                //On Fail
                onFailGetExpenseCategoryList();
            }

        }
    }

    //handle category sync first time


    //On Success
    public void onSuccessGetRejectionReasonList(ArrayList<RejectionReasonModel> rejectionReasonLocalList) {
        // clear for refresh reason list
        rejectionReasonList.clear();
        rejectionReasonList.addAll(rejectionReasonLocalList);

        // refresh adapter
        rejectionReasonListAdapter.notifyDataSetChanged();

        //    rejectionReasonDialogUpload.notifyReasonListUploaded(rejectionReasonList);

    }


    //On Fail
    public void onFailGetExpenseCategoryList() {
        // handle later by nandini

    }


    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        switch (viewId) {
            case R.id.textViewCancel:
                dismiss();
                break;
            case R.id.textViewOk:

                if(selectedReasonPos==-1){
                    str_RejectionReasonID="0";
                    if(editTextRejectNotes.getText().toString().equals("")){
                        str_RejectioxnReason="null";

                    }else {
                        str_RejectioxnReason=editTextRejectNotes.getText().toString();
                    }
                }else {
                    str_RejectioxnReason=rejectionReasonList.get(selectedReasonPos).getName();
                    str_RejectionReasonID=rejectionReasonList.get(selectedReasonPos).getRejectionReasonID();
                }

                if(str_RejectioxnReason.equals("null")){
                    Toast.makeText(getContext(),getString(R.string.rejectionReason),Toast.LENGTH_SHORT).show();
                }else{
                    listener.rejectClickOk(str_RejectioxnReason,str_RejectionReasonID);
                    dismiss();
                }

                break;
            case R.id.textViewOther:
                //handle other reason option
                handleOtherOption();
                break;

        }
    }

    @Override
    public void handleReasonClick(int position) {
        //update selected Reason
       /*if -1 then other
       * otherwise reason from reason list
       * */
        editTextRejectNotes.setText("");
        editTextRejectNotes.setVisibility(View.GONE);

        selectedReasonPos = position;
        InvisibleRejectionNotes();
    }


    /**
     * Text Watcher
     */

    private class RejectionNotesTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence text, int start, int before, int count) {

        }

        @Override
        public void onTextChanged(CharSequence text, int start, int before, int count) {
            updateNotesTextCount(text.length());
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }

    public void handleOtherOption() {
        selectedReasonPos=-1;
        editTextRejectNotes.setVisibility(View.VISIBLE);
        textViewNotesCount.setVisibility(View.VISIBLE);

        rejectionReasonListAdapter.prepareSelectListFalse();
        rejectionReasonListAdapter.notifyDataSetChanged();

    }

    void InvisibleRejectionNotes() {
        editTextRejectNotes.setVisibility(View.GONE);
        textViewNotesCount.setVisibility(View.GONE);
    }

    void updateNotesTextCount(int count) {
        int updatedValue = getResources().getInteger(R.integer.approval_expense_screen_rejection_notes_max_length) - count;
        textViewNotesCount.setText(String.valueOf(updatedValue));
    }

    @Override
    public void onResume() {
        super.onResume();

        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_BACK) && (event.getAction() == KeyEvent.ACTION_UP))

                    dismiss();

                return false;
            }
        });
    }


}



