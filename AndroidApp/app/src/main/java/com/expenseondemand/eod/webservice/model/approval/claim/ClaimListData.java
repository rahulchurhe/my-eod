package com.expenseondemand.eod.webservice.model.approval.claim;

import java.util.ArrayList;

/**
 * Created by ITDIVISION\maximess087 on 2/3/17.
 */

public class ClaimListData {

    private ArrayList<ClaimData> data;
    private String HasMorePage;

    public String getHasMorePage() {
        return HasMorePage;
    }

    public void setHasMorePage(String hasMorePage) {
        HasMorePage = hasMorePage;
    }

    public ArrayList<ClaimData> getData() {
        return data;
    }

    public void setData(ArrayList<ClaimData> data) {
        this.data = data;
    }
}
