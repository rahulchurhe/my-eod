package com.expenseondemand.eod.dialogFragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.expenseondemand.eod.R;

/**
 * Created by Nandani Gupta on 2016-09-19.
 */
public class PhotoPickerDialog extends DialogFragment
{
    PhotoPickerClickListener photoPickerClickListener;

    public interface PhotoPickerClickListener
    {
        void handleGallery();
        void handleCamera();
        void handleCancel();
    }

    public static PhotoPickerDialog newInstance(PhotoPickerClickListener photoPickerClickListener)
    {
        PhotoPickerDialog frag = new PhotoPickerDialog();
        frag.photoPickerClickListener = photoPickerClickListener;
        return frag;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if ((keyCode == android.view.KeyEvent.KEYCODE_BACK) && (event.getAction() == KeyEvent.ACTION_UP))
                    handleCancel();
                return false;
            }
        });
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Window window = getDialog().getWindow();
        window.setLayout( ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
        window.getAttributes().windowAnimations = R.style.DialogAnimation;
        window.setGravity(Gravity.BOTTOM);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.attach_receipt_dialog_layout, container, false);

        setCancelable(false);


        //Get TextView Photos
        ImageView imageViewPhotoGallery = (ImageView) view.findViewById(R.id.imageViewPhotoGallery);
        ImageView imageViewCamera = (ImageView) view.findViewById(R.id.imageViewCamera);
        TextView textViewCancel = (TextView) view.findViewById(R.id.textViewCancel);

        //Set OnClick Listeners
        imageViewPhotoGallery.setOnClickListener(new PhotoPickerDialogListener());
        imageViewCamera.setOnClickListener(new PhotoPickerDialogListener());
        textViewCancel.setOnClickListener(new PhotoPickerDialogListener());

        return view;
    }

    private class PhotoPickerDialogListener implements View.OnClickListener
    {

        @Override
        public void onClick(View view)
        {
            switch (view.getId())
            {
                case R.id.imageViewPhotoGallery:
                {
                    handleGallery();
                    break;
                }
                case R.id.imageViewCamera:
                {
                    handleCamera();

                    break;
                }
                case R.id.textViewCancel:
                {
                    handleCancel();
                    break;
                }
            }
        }
    }

    private void handleGallery()
    {
        dismiss();
        photoPickerClickListener.handleGallery();
    }

    private void handleCamera()
    {
        dismiss();
        photoPickerClickListener.handleCamera();
    }
    private void handleCancel()
    {
        dismiss();
        photoPickerClickListener.handleCancel();
    }
}

