package com.expenseondemand.eod.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Build;
import android.provider.Settings;
import android.text.Html;
import android.text.Spanned;

import java.io.ByteArrayOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

public class AppUtil {
    //Is String Empty
    public static boolean isStringEmpty(String stringRes) {
        if (stringRes == null || stringRes.trim().isEmpty()) {
            return true;
        }

        return false;
    }

    //Check Collection for Empty
    public static boolean isCollectionEmpty(Collection<? extends Object> collection) {
        if (collection == null || collection.isEmpty()) {
            return true;
        }

        return false;
    }

    //Set String Not NULL
    public static String setStringNotNull(String stringRes) {
        return ((stringRes == null) ? "" : stringRes.trim());
    }

    //Check for NULL - Integer
    public static Integer setIntegerNotNull(Integer value) {
        return ((value == null) ? 0 : value);
    }

    //Prepare Title
    public static String prepareTitle(String beforeStr, String afterStr) {
        StringBuilder title = new StringBuilder("");

        title.append(beforeStr);
        title.append(" - ");
        title.append(afterStr);

        return title.toString();
    }

    /*
     * Convert String(Word) to CamelCase
     */
    public static String convertToCamelCase(String string) {
        if (!AppUtil.isStringEmpty(string)) {
            String word = null;
            int wordLength = 0;
            char firstLetter = '\u0000';
            String restOfWord = "";

            string = string.trim().toLowerCase(Locale.getDefault()); //remove extra spaces
            final StringBuilder resultantString = new StringBuilder(string.length()); //make new string builder
            String[] words = string.split("\\s+"); //If spaces then split
            int wordsLength = words.length;

            for (int index = 0; index < wordsLength; index++) {
                word = words[index];
                if (index > 0) {
                    resultantString.append(" ");
                }

                if (word != null) {
                    word = word.trim();
                    wordLength = word.length();
                    if (wordLength > 0) {
                        firstLetter = Character.toUpperCase(word.charAt(0));
                        resultantString.append(firstLetter);
                        if (wordLength > 1) {
                            restOfWord = word.substring(1);
                            resultantString.append(restOfWord);
                        }
                    }
                }
            }

            return resultantString.toString();
        }

        return string;
    }

    public static byte[] getByteArray(Bitmap bitmap) {
        byte[] byteArray = null;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        boolean isConversionSuccess = false;

        try {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byteArray = stream.toByteArray();

            if (byteArray != null && byteArray.length > 0) {
                isConversionSuccess = true;
            }
        } catch (Exception e) {
            //Do Nothing
        }

        if (!isConversionSuccess) //Attempt for PNG
        {
            stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byteArray = stream.toByteArray();
        }

        return byteArray;
    }

    public static String formatToDouble(String data) {
        DecimalFormat decimalFormat = new DecimalFormat(AppConstant.FORMAT_AMOUNT, new DecimalFormatSymbols(Locale.ENGLISH));
        double valueInDouble = 0.0;
        String valueInString = null;

        try {
            valueInDouble = Double.parseDouble(data);
        } catch (NumberFormatException numberFormatException) {
            //Consume it
        }

        valueInString = decimalFormat.format(valueInDouble);

        return valueInString;
    }

    /**
     * Get Current Date/Time (Date Object)
     *
     * @return date - Date Object
     */
    private static Date getCurrentDateTime() {
        //Get Calender Instance
        Calendar calendar = Calendar.getInstance();

        //Get Date/Time
        Date date = calendar.getTime();

        return date;
    }

    /**
     * Get Formatted Date from Date Object
     *
     * @param dateFormat - Format of Date String
     * @param date       - Date object
     * @return Formatted Date String
     */
    public static String getFormattedDate(String dateFormat, Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.getDefault());
        String formattedDateString = simpleDateFormat.format(date);

        return formattedDateString;
    }

    /**
     * Get Current Formatted Date
     *
     * @param dateFormat - Format of Date String
     * @return Formatted Date String
     */
    public static String getCurrentFormattedDate(String dateFormat) {
        //Get Current Date Object
        Date date = getCurrentDateTime();

        String formattedDateString = getFormattedDate(dateFormat, date);

        return formattedDateString;
    }

    public static Date getDate(int date, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.DAY_OF_MONTH, date);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);

        Date dateObject = calendar.getTime();

        return dateObject;
    }

    /**
     * Convert formatted date to milliseconds.
     *
     * @param formattedDate
     * @return
     */
    public static long getLongDate(String dateFormat, String formattedDate) {
        long longDate = 0;
        Date date;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.getDefault());

        try {
            if (!formattedDate.equals("")) {
                date = simpleDateFormat.parse(formattedDate);
                longDate = date.getTime();
            } else {
                longDate = System.currentTimeMillis();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return longDate;
    }

    /**
     * Get Formatted Date from Long Date (in Milliseconds)
     *
     * @param milliseconds
     * @return formatted date
     */
    public static String getFormattedDateString(String dateFormat, long longDate) {
        String formattedDateString = null;
        Date date = null;

        try {
            if (longDate != 0L) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.getDefault());
                date = new Date(longDate);
                formattedDateString = simpleDateFormat.format(date);
            } else {
                formattedDateString = "";
            }
        } catch (Exception e) {
            formattedDateString = "";
        }

        return formattedDateString;
    }

    public static int convertToInt(String data) {
        int convertedData = 0;

        try {
            convertedData = Integer.parseInt(data);
        } catch (NumberFormatException e) {
            //
        }

        return convertedData;
    }

    /**
     * To check if Android version is 4.0 and above
     *
     * @return True if Android version is 4.0 and above else False
     */
    public static boolean isCurrentVersionICSAndAbove() {
        try {
            int currentapiVersion = android.os.Build.VERSION.SDK_INT;

            if (currentapiVersion >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    /***
     * Close cursor
     *
     * @param cursor
     */
    public static void closeCursor(Cursor cursor) {
        if (cursor != null && !cursor.isClosed()) {
            try {
                cursor.close();
            } catch (Exception e) {
                //Do Nothing
                e.printStackTrace();
            }
        }
    }

    public static int getDateDiffences(long newerDate, long olderDate) {
        int diffInDays = (int) ((newerDate - olderDate) / (1000 * 60 * 60 * 24));

        return diffInDays;
    }

    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String string) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            return Html.fromHtml(string, Html.FROM_HTML_MODE_LEGACY);
        else
            return Html.fromHtml(string);
    }

    public static boolean isLollipopAndAbove() {
        boolean isAbove = false;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            isAbove = true;
        }
        return isAbove;
    }

    public static String getCurrentTimeStamp() {
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
            String currentDateTime = dateFormat.format(new Date()); // Find today's date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getDeviceID(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static String getApplicationVersion(Context context) {
        String version = null;

        try {
            String packageName = context.getPackageName();

            PackageInfo pInfo = context.getPackageManager().getPackageInfo(packageName, 0);
            version = String.valueOf(pInfo.versionCode);
        } catch (PackageManager.NameNotFoundException exception) {
        }

        return version;
    }

    public static String getMD5String(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    //Check for NULL - Object
    public static Object checkForNull(Object value) {
        return ((value == null) ? "" : value);
    }

    //Check for NULL - Integer
    public static Integer checkForNull(Integer value) {
        return ((value == null) ? 0 : value);
    }

    //Check for NULL - Long
    public static Long checkForNull(Long value) {
        return ((value == null) ? 0 : value);
    }

    //Check for NULL - String
    public static String checkForNull(String value) {
        return ((value == null) ? "" : value.trim());
    }
}
