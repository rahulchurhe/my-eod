package com.expenseondemand.eod.model;

import com.expenseondemand.eod.webservice.model.approval.detail.ApprovalRejectedExpenseRequest;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 14-10-2016.
 */

public class Statistics
{
    private String approvedCount;
    private String pendingCount;
    private String inCompleteCount;
    private String rejectedCount;
    private String passedForPayment;
    private String rejectedAmount;
    private String pendingAmount;
    private String approvedAmount;
    private String inCompleteAmount;
    private String paidAmount;
    private String costCentre;
    private String expenseApproverName;
    private String preApprovalCount;
    private String approvePreApprovalCount;
    private String submitDOCCount;
    private String approveDOCCount;
    private String mobileExpenseCount;
    private String pendingApprovalCount;
    private ArrayList<MobileExpense> firstMobileExpenses;
    private AppRejectionSetting AppRejSettings;

    public AppRejectionSetting getAppRejSettings() {
        return AppRejSettings;
    }

    public void setAppRejSettings(AppRejectionSetting appRejSettings) {
        AppRejSettings = appRejSettings;
    }

    public String getApprovedCount()
    {
        return approvedCount;
    }

    public void setApprovedCount(String approvedCount)
    {
        this.approvedCount = approvedCount;
    }

    public String getPendingCount()
    {
        return pendingCount;
    }

    public void setPendingCount(String pendingCount)
    {
        this.pendingCount = pendingCount;
    }

    public String getInCompleteCount()
    {
        return inCompleteCount;
    }

    public void setInCompleteCount(String inCompleteCount)
    {
        this.inCompleteCount = inCompleteCount;
    }

    public String getRejectedCount()
    {
        return rejectedCount;
    }

    public void setRejectedCount(String rejectedCount)
    {
        this.rejectedCount = rejectedCount;
    }

    public String getPassedForPayment()
    {
        return passedForPayment;
    }

    public void setPassedForPayment(String passedForPayment)
    {
        this.passedForPayment = passedForPayment;
    }

    public String getRejectedAmount()
    {
        return rejectedAmount;
    }

    public void setRejectedAmount(String rejectedAmount)
    {
        this.rejectedAmount = rejectedAmount;
    }

    public String getPendingAmount()
    {
        return pendingAmount;
    }

    public void setPendingAmount(String pendingAmount)
    {
        this.pendingAmount = pendingAmount;
    }

    public String getApprovedAmount()
    {
        return approvedAmount;
    }

    public void setApprovedAmount(String approvedAmount)
    {
        this.approvedAmount = approvedAmount;
    }

    public String getInCompleteAmount()
    {
        return inCompleteAmount;
    }

    public void setInCompleteAmount(String inCompleteAmount)
    {
        this.inCompleteAmount = inCompleteAmount;
    }

    public String getPaidAmount()
    {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount)
    {
        this.paidAmount = paidAmount;
    }

    public String getCostCentre()
    {
        return costCentre;
    }

    public void setCostCentre(String costCentre)
    {
        this.costCentre = costCentre;
    }

    public String getExpenseApproverName()
    {
        return expenseApproverName;
    }

    public void setExpenseApproverName(String expenseApproverName)
    {
        this.expenseApproverName = expenseApproverName;
    }

    public String getPreApprovalCount()
    {
        return preApprovalCount;
    }

    public void setPreApprovalCount(String preApprovalCount)
    {
        this.preApprovalCount = preApprovalCount;
    }

    public String getApprovePreApprovalCount()
    {
        return approvePreApprovalCount;
    }

    public void setApprovePreApprovalCount(String approvePreApprovalCount)
    {
        this.approvePreApprovalCount = approvePreApprovalCount;
    }

    public String getSubmitDOCCount()
    {
        return submitDOCCount;
    }

    public void setSubmitDOCCount(String submitDOCCount)
    {
        this.submitDOCCount = submitDOCCount;
    }

    public String getApproveDOCCount()
    {
        return approveDOCCount;
    }

    public void setApproveDOCCount(String approveDOCCount)
    {
        this.approveDOCCount = approveDOCCount;
    }

    public String getMobileExpenseCount()
    {
        return mobileExpenseCount;
    }

    public void setMobileExpenseCount(String mobileExpenseCount)
    {
        this.mobileExpenseCount = mobileExpenseCount;
    }

    public String getPendingApprovalCount()
    {
        return pendingApprovalCount;
    }

    public void setPendingApprovalCount(String pendingApprovalCount)
    {
        this.pendingApprovalCount = pendingApprovalCount;
    }

    public ArrayList<MobileExpense> getFirstMobileExpenses()
    {
        return firstMobileExpenses;
    }

    public void setFirstMobileExpenses(ArrayList<MobileExpense> firstMobileExpenses)
    {
        this.firstMobileExpenses = firstMobileExpenses;
    }
}
