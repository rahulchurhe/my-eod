package com.expenseondemand.eod.webservice.model.approval.detail;

import com.expenseondemand.eod.webservice.model.MasterRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ITDIVISION\maximess087 on 10/1/17.
 */

public class ApprovalRejectedExpenseContainer extends MasterRequest {

   ApprovalRejectedExpensesRequestData requestData;

    public ApprovalRejectedExpensesRequestData getRequestData() {
        return requestData;
    }

    public void setRequestData(ApprovalRejectedExpensesRequestData requestData) {
        this.requestData = requestData;
    }

}
