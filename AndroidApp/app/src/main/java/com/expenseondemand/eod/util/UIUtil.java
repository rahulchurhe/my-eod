package com.expenseondemand.eod.util;

import android.annotation.SuppressLint;
import android.app.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.MediaManager;

import java.io.File;

public class UIUtil
{
	private static Toast toast;
	private static boolean isShowingGotoSetting;
	private static Bitmap bitmap;

	//Show Soft Keyboard
	public static void showSoftKeyboard(EditText editText)
	{
		//Request Focus
		editText.requestFocus();

		InputMethodManager inputMethodManager = (InputMethodManager)editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
	}

	//Hide Soft Keyboard
	public static void hideSoftKeyboard(Activity activity)
	{
		//Get View
		View view = activity.getWindow().getDecorView();

		//Hide Soft Keyboard
		hideSoftKeyboard(view);
	}

	//Hide Soft Keyboard
	public static void hideSoftKeyboard(View view)
	{
		if(view == null)
		{
			return;
		}

		//Get Window Token
		IBinder token = view.getWindowToken();

		//Get InputMethodManager
		InputMethodManager inputMethodManager = (InputMethodManager)view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

		//Gain Focus on Current View
		view.requestFocus();

		//Hide Soft Input
		inputMethodManager.hideSoftInputFromWindow(token, InputMethodManager.RESULT_UNCHANGED_SHOWN);
	}

	//Show Toast
	public static void showToast(int messageId)
	{
		//Get Application Context
		Context appContext = CachingManager.getAppContext();

		showToast(appContext.getString(messageId));
	}

	//Show Toast
	public static void showToast(String message)
	{
		//Check Toast
		if(toast != null)
		{
			//Get View of Toast
			View view = toast.getView();

			if(view.isShown())
			{
				return;
			}
		}

		//Get Application Context
		Context appContext = CachingManager.getAppContext();

		//Create Toast
		toast = Toast.makeText(appContext, message, Toast.LENGTH_SHORT);

		//Show toast
		toast.show();
	}

	/**
	 * Shows Progress Bar Dialog
	 * @param activityContext
	 * @return
	 */
	public static Dialog showProgressDialog(Context activityContext)
	{
		Dialog dialog = null;

		try 
		{
			dialog = new Dialog(activityContext);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
			dialog.setContentView(R.layout.async_progress_dialog_layout);
			dialog.setCancelable(false);
			dialog.show();
		} 
		catch (Exception exception) 
		{
			//Consume it
			exception.printStackTrace();
		}

		return dialog;
	}

	public static void dismissDialog(Dialog dialog)
	{
		try 
		{
			if (dialog != null)
			{
				dialog.dismiss();
			}
		} 
		catch (Exception exception) 
		{
			//Consume it
		}
	}

	/**
	 * 
	 * @param activity
	 * @param message
	 */
	/*public static void showAlertDialog(Activity activity, String message)
	{
		showAlertDialog(activity, message, false);		
	}

	*//**
	 * 
	 * @param activity
	 * @param message
	 * @param finishActivity
	 *//*
	public static void showAlertDialog(final Activity activity, String message, final boolean finishActivity)
	{
		String alertDialogButtonTextOk = activity.getString(R.string.button_ok);

		final AlertDialog alertDialog = new AlertDialog.Builder(activity).create();

		// Setting Dialog Message
		alertDialog.setMessage(message);

		// Setting OK Button
		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, alertDialogButtonTextOk, new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int which)
			{
				UIUtil.dismissDialog(alertDialog);

				if (finishActivity)
				{
					activity.finish();
					activity.overridePendingTransition(R.anim.slide_in_back, R.anim.slide_out_back);
				}
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}

	*//**
	 * Prepares and shows the Alert Dialog with a pre-defined error message.
	 * @param activity
	 *//*
	public static void showErrorAlertDialog(Activity activity)
	{
		showErrorAlertDialog(activity, null, null, false);
	}

	*//**
	 * Show  Error Alert Dialog with Customized Title (ExceptionCode+Title) and Message
	 * @param activity
	 * @param title
	 * @param message
	 *//*
	public static void showErrorAlertDialog(Activity activity, int exceptionCode, String title, String message)
	{
		showErrorAlertDialog(activity, ExceptionManager.prepareTitle(exceptionCode, title), message, false);
	}
	
	*//**
	 * Prepares and shows the Alert Dialog
	 * @param activity
	 * @param finishActivity - If true then will close the activity else not
	 *//*
	public static void showErrorAlertDialog(final Activity activity, String title, String message, final boolean finishActivity)
	{
		String alertDialogButtonTextOk = activity.getString(R.string.button_ok);
		
		if(AppUtil.isStringEmpty(title))
		{
			title = activity.getString(R.string.text_error);
		}
		
		if(AppUtil.isStringEmpty(message))
		{
			message = activity.getString(R.string.text_error_processing);
		}

		final AlertDialog alertDialog = new AlertDialog.Builder(activity).create();

		//Set Title;
		alertDialog.setTitle(title);

		// Setting Dialog Message
		alertDialog.setMessage(message);

		// Setting OK Button
		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, alertDialogButtonTextOk, new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int which)
			{
				UIUtil.dismissDialog(alertDialog);
				if (finishActivity)
				{
					activity.finish();
					activity.overridePendingTransition(R.anim.slide_in_back, R.anim.slide_out_back);
				}
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}

	//Create AlertDialog.Builder
	public static AlertDialog.Builder getAlertDialogBuilder(Context context, Object title, Object message)
	{
		//Get AlertDialog
		AlertDialog.Builder builder = getAlertDialogBuilder(context, title, 0, null, message);

		return builder;
	}

	*//**
	 * Get Alert Dialog, with ability to give custom title, message and buttons.
	 * 
	 * @param context
	 * @param title
	 * @param positiveButton
	 * @param negativeButton
	 * @param message
	 * @return
	 *//*
	public static AlertDialog.Builder getAlertDialogBuilder(Context context, Object title, Integer positiveButton, Integer negativeButton, Object message)
	{
		//Prepare AlertDialog.Builder
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle((title instanceof Integer) ? context.getString(((Integer)title).intValue()) : title.toString());
		builder.setMessage((message instanceof Integer) ? (((AppUtil.setIntegerNotNull((Integer)message)) == 0) ? context.getString(R.string.text_error_processing) : context.getString((Integer)message)) : ((AppUtil.isStringEmpty((String)message) ? context.getString(R.string.text_error_processing) : ((String)message)).trim()));
		builder.setCancelable(false);

		if(positiveButton != null)
		{
			builder.setPositiveButton(((positiveButton == 0) ? R.string.button_ok : positiveButton), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					//Dismiss Dialog
					dialog.dismiss();
				}
			});
		}

		if(negativeButton != null)
		{
			builder.setNegativeButton(((negativeButton == 0) ? R.string.button_dismiss : negativeButton), new DialogInterface.OnClickListener()
			{	
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					//Dismiss Dialog
					dialog.dismiss();
				}
			});
		}

		return builder;
	}

	//Show AlertDialog : Network
	public static AlertDialog enableNetwork(final Context context)
	{
		if(isShowingGotoSetting)
		{
			return null;
		}

		//Set Flag
		isShowingGotoSetting = true;

		//Show AlertDialog
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(R.string.text_network);
		builder.setMessage(R.string.text_network_not_available);
		builder.setCancelable(false);

		builder.setPositiveButton(R.string.button_settings, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				//Dismiss Dialog
				dialog.dismiss();

				//Goto WiFi Settings
				Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
				context.startActivity(intent);

				//Set Flag
				isShowingGotoSetting = false;
			}
		});

		builder.setNegativeButton(R.string.button_dismiss, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				//Dismiss Dialog
				dialog.dismiss();

				//Set Flag
				isShowingGotoSetting = false;
			}
		});

		//Get AlertDialog
		AlertDialog alertDialog = builder.create();

		//Show AlertDialog
		alertDialog.show();

		return alertDialog;
	}
*/
    //set animation
	public static Animation getAnimationInstanse(int animationName)
	{

		//Get Application Context
		Context appContext = CachingManager.getAppContext();

		Animation animation = AnimationUtils.loadAnimation(appContext, animationName);
		animation.setInterpolator((new AccelerateDecelerateInterpolator()));
		animation.setFillAfter(true);


		return  animation;
	}

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	public static void setBackground(View view, Drawable drawable)
	{
		if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
			view.setBackground(drawable);
		else
			view.setBackgroundDrawable(drawable);
	}

	/**
	 * Show Custom Confirm Dialog
	 * @param context
	 * @param msg
	 * @param positiveBtnCaption
	 * @param negativeBtnCaption
	 * @param alertDialogBoxClickListener
	 */
	public static void showCustomConfirmDialog(Activity context, String msg, String positiveBtnCaption, String negativeBtnCaption,final AlertDialogPermissionBoxClickInterface alertDialogBoxClickListener)
	{
		TextView textViewTitle;
		TextView textViewMsg;
		TextView buttonPositive;
		TextView buttonNegative;
		AlertDialog alertDialog;
		try
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			alertDialog = builder.create();
//			alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			ViewGroup parent = null;
			View dialogView = inflater.inflate(R.layout.app_upgrade_dialog_fragment, parent);

			textViewTitle = (TextView) dialogView.findViewById(R.id.textViewTitle);
			textViewTitle.setVisibility(View.GONE);

			textViewMsg = (TextView) dialogView.findViewById(R.id.textViewMessage);
			textViewMsg.setText(msg);

			CustomConfirmAlertButtonClickListener customConfirmAlertButtonClickListener = new CustomConfirmAlertButtonClickListener(alertDialog, alertDialogBoxClickListener);

			buttonPositive = (TextView) dialogView.findViewById(R.id.textViewOk);
			buttonPositive.setText(positiveBtnCaption);
			buttonPositive.setOnClickListener(customConfirmAlertButtonClickListener);

			buttonNegative = (TextView) dialogView.findViewById(R.id.textViewCancel);
			buttonNegative.setText(negativeBtnCaption);
			buttonNegative.setOnClickListener(customConfirmAlertButtonClickListener);

			alertDialog.show();
			alertDialog.setContentView(dialogView);
		}
		catch (Exception exception)
		{

		}
	}

	private static class CustomConfirmAlertButtonClickListener implements android.view.View.OnClickListener
	{
		private AlertDialogPermissionBoxClickInterface	alertDialogClickListener;
		private AlertDialog					alertDialog;

		public CustomConfirmAlertButtonClickListener(AlertDialog alertDialog, AlertDialogPermissionBoxClickInterface alertDialogClickListener)
		{
			this.alertDialogClickListener = alertDialogClickListener;
			this.alertDialog = alertDialog;
		}

		@Override
		public void onClick(View view)
		{
			int id = view.getId();
			boolean isPositiveButtonClicked;
			switch (id)
			{
				case R.id.textViewOk:
					isPositiveButtonClicked = true;
					alertDialogClickListener.onButtonClicked(isPositiveButtonClicked);
					break;
				case R.id.textViewCancel:
					isPositiveButtonClicked = false;
					alertDialogClickListener.onButtonClicked(isPositiveButtonClicked);
					break;
				default:
					break;
			}

			dismissDialog(alertDialog);
		}
	}

	public static void showToastNotification(Context context, String message, String title, boolean isLongDuration)
	{
		int duration = 0;

		if (isLongDuration)
		{
			duration = Toast.LENGTH_LONG;
		}
		else
		{
			duration = Toast.LENGTH_SHORT;
		}

		if (message!=null && !message.isEmpty())
		{
			if (title != null)
			{
				String toastMessage = title + "\n\n" + message;
				Toast.makeText(context, toastMessage, duration).show();
			}
			else
			{
				String toastMessage = message;
				Toast.makeText(context, toastMessage, duration).show();
			}
		}
	}


	public static Dialog showReceiptDialog(String file_path,String receiptName, final Activity activity) {
		final Dialog dialog = new Dialog(activity);
		if (file_path != null) {
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
			dialog.setContentView(R.layout.project_receipt_dailog);

			DisplayMetrics dm = new DisplayMetrics();
			activity.getWindowManager().getDefaultDisplay().getMetrics(dm);

			int screenWidth = dm.widthPixels;
			int screenHeight = dm.heightPixels;
			int dialogWidth = (screenWidth * 95) / 100;
			int dialogHeight = (screenHeight * 95) / 100;

			WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
			Window window = dialog.getWindow();
			layoutParams.copyFrom(window.getAttributes());
			layoutParams.width = dialogWidth;
			layoutParams.height = dialogHeight;

			window.setAttributes(layoutParams);

			ImageView imageReceipt = (ImageView) dialog.findViewById(R.id.imageViewReceipt);
			ImageView imageViewCancel = (ImageView) dialog.findViewById(R.id.imageViewCancel);
			TextView textPDF = (TextView) dialog.findViewById(R.id.textPDF);

			LinearLayout layout_pdf = (LinearLayout) dialog.findViewById(R.id.layout_pdf);
			Button buttonViewPDF = (Button) dialog.findViewById(R.id.buttonViewPDF);

			imageViewCancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			try{
				if (receiptName.contains("pdf")) {
					textPDF.setText(receiptName);
					imageReceipt.setVisibility(View.GONE);
					layout_pdf.setVisibility(View.VISIBLE);

					final Uri pdfFile = Uri.fromFile(new File(file_path));
					buttonViewPDF.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setDataAndType(pdfFile, "application/pdf");
							intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
							activity.startActivity(intent);
						}
					});
				} else {
					imageReceipt.setVisibility(View.VISIBLE);
					layout_pdf.setVisibility(View.GONE);
					bitmap = MediaManager.getScaledDownBitmapFromFilePath(file_path);
					imageReceipt.setImageBitmap(bitmap);
				}
			}catch (Exception e){
				e.printStackTrace();
				Toast.makeText(activity,"Pdf viewer not available",Toast.LENGTH_SHORT).show();
			}

			dialog.show();
		}

		return dialog;
	}

	public static Dialog showAttachedReceiptDialog(Bitmap receiptName, final Activity activity) {
		final Dialog dialog = new Dialog(activity);
		if (receiptName != null) {
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
			dialog.setContentView(R.layout.project_receipt_dailog);

			DisplayMetrics dm = new DisplayMetrics();
			activity.getWindowManager().getDefaultDisplay().getMetrics(dm);

			int screenWidth = dm.widthPixels;
			int screenHeight = dm.heightPixels;
			int dialogWidth = (screenWidth * 95) / 100;
			int dialogHeight = (screenHeight * 95) / 100;

			WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
			Window window = dialog.getWindow();
			layoutParams.copyFrom(window.getAttributes());
			layoutParams.width = dialogWidth;
			layoutParams.height = dialogHeight;

			window.setAttributes(layoutParams);

			ImageView imageReceipt = (ImageView) dialog.findViewById(R.id.imageViewReceipt);
			ImageView imageViewCancel = (ImageView) dialog.findViewById(R.id.imageViewCancel);
			TextView textPDF = (TextView) dialog.findViewById(R.id.textPDF);

			LinearLayout layout_pdf = (LinearLayout) dialog.findViewById(R.id.layout_pdf);
			Button buttonViewPDF = (Button) dialog.findViewById(R.id.buttonViewPDF);

			imageViewCancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			try{

				imageReceipt.setVisibility(View.VISIBLE);
				layout_pdf.setVisibility(View.GONE);
				imageReceipt.setImageBitmap(receiptName);
			}catch (Exception e){
				e.printStackTrace();
				Toast.makeText(activity,"Pdf viewer not available",Toast.LENGTH_SHORT).show();
			}

			dialog.show();
		}

		return dialog;
	}
}
