package com.expenseondemand.eod.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Expense implements Parcelable, Expenseable
{
	private int expenseId;
	private String userId;
	private long ExpenseDate;
	private int expenseCategoryId;
	private int expenseBaseCategoryId;
	private String expenseCategoryName;
	private String currencyCode;
	private String expenseAmount;
	private String expenseNotes;
	private boolean hasReceipt;
	//private String receiptImage; Removed, Introduced "ReceiptImage"
	private boolean isSelected; //Only used in UploadExpenseActivity for Tracking Selection of Expenses
	private ReceiptImage receiptImage;

	public int getExpenseBaseCategoryId() {
		return expenseBaseCategoryId;
	}

	public void setExpenseBaseCategoryId(int expenseBaseCategoryId) {
		this.expenseBaseCategoryId = expenseBaseCategoryId;
	}

	public static Creator<Expense> getCREATOR() {
		return CREATOR;
	}

	public Expense()

	{
	}

	public int getExpenseId() 
	{
		return expenseId;
	}

	public void setExpenseId(int expenseId) 
	{
		this.expenseId = expenseId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public long getExpenseDate() {
		return ExpenseDate;
	}

	public void setExpenseDate(long expenseDate) {
		ExpenseDate = expenseDate;
	}

	/*public long getExpenseDate()
	{
		return expenseDate;
	}

	public void setExpenseDate(long expenseDate) 
	{
		this.expenseDate = expenseDate;
	}*/

	public int getExpenseCategoryId() 
	{
		return expenseCategoryId;
	}

	public void setExpenseCategoryId(int expenseCategoryId) 
	{
		this.expenseCategoryId = expenseCategoryId;
	}

	public String getExpenseCategoryName() 
	{
		return expenseCategoryName;
	}

	public void setExpenseCategoryName(String expenseCategoryName) 
	{
		this.expenseCategoryName = expenseCategoryName;
	}

	public String getCurrencyCode() 
	{
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) 
	{
		this.currencyCode = currencyCode;
	}

	public String getExpenseAmount() 
	{
		return expenseAmount;
	}

	public void setExpenseAmount(String expenseAmount) 
	{
		this.expenseAmount = expenseAmount;
	}

	public String getExpenseNotes() 
	{
		return expenseNotes;
	}

	public void setExpenseNotes(String expenseNotes) 
	{
		this.expenseNotes = expenseNotes;
	}

	public boolean isHasReceipt() {
		return hasReceipt;
	}

	public void setHasReceipt(boolean hasReceipt) {
		this.hasReceipt = hasReceipt;
	}

	public ReceiptImage getReceiptImage() {
		return receiptImage;
	}

	public void setReceiptImage(ReceiptImage receiptImage) {
		this.receiptImage = receiptImage;
	}

	public boolean isSelected() 
	{
		return isSelected;
	}

	public void setSelected(boolean isSelected) 
	{
		this.isSelected = isSelected;
	}

	@Override
	public int describeContents() 
	{
		//Do Nothing...

		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) 
	{
		dest.writeInt(expenseId);
		dest.writeString(userId);
		dest.writeLong(ExpenseDate);
		dest.writeInt(expenseCategoryId);
		dest.writeInt(expenseBaseCategoryId);
		dest.writeString(expenseCategoryName);
		dest.writeString(currencyCode);
		dest.writeString(expenseAmount);
		dest.writeString(expenseNotes);
		dest.writeInt((hasReceipt) ? 1 : 0);
		dest.writeInt((isSelected) ? 1 : 0);
		dest.writeParcelable(receiptImage, flags);
	}

	public Expense(Parcel source) 
	{
		this.expenseId = source.readInt();
		this.userId = source.readString();
		this.ExpenseDate = source.readLong();
		this.expenseCategoryId = source.readInt();
		this.expenseBaseCategoryId = source.readInt();
		this.expenseCategoryName = source.readString();
		this.currencyCode = source.readString();
		this.expenseAmount = source.readString();
		this.expenseNotes = source.readString();
		this.hasReceipt = ((source.readInt() == 1) ? true : false);
		this.isSelected = ((source.readInt() == 1) ? true : false);
		this.receiptImage = source.readParcelable(ReceiptImage.class.getClassLoader());
	}

	public static final Parcelable.Creator<Expense> CREATOR = new  Creator<Expense>() {

		@Override
		public Expense[] newArray(int size) 
		{
			//Do Nothing...

			return null;
		}

		@Override
		public Expense createFromParcel(Parcel source) 
		{
			return new Expense(source);
		}
	};
}
