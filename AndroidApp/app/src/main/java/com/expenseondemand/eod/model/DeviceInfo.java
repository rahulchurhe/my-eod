package com.expenseondemand.eod.model;

/**
 * Created by Ramit Yadav on 13-10-2016.
 */

public class DeviceInfo
{
    private String deviceId;
    private String appVersion;

    public String getDeviceId()
    {
        return deviceId;
    }

    public void setDeviceId(String deviceId)
    {
        this.deviceId = deviceId;
    }

    public String getAppVersion()
    {
        return appVersion;
    }

    public void setAppVersion(String appVersion)
    {
        this.appVersion = appVersion;
    }
}
