package com.expenseondemand.eod.receiver;


import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.expenseondemand.eod.manager.DeviceManager;
import com.expenseondemand.eod.manager.PersistentManager;
import com.expenseondemand.eod.service.SilentLoginService;

/**
 * Created by priya on 21-02-2017.
 */

public class ConnectivityChangeReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        boolean isDeviceConnected = DeviceManager.isNetworkAvailable();
        int silentLoginStatus = PersistentManager.getSilentLoginStatus();
        if (isDeviceConnected && (silentLoginStatus == SilentLoginService.SILENT_LOGIN_FAIL || silentLoginStatus != SilentLoginService.SILENT_LOGIN_INPROGRESS)) {
            doSilentAuthorization(context);
        } else {
            //TODO: Enable Network settings or show no network dialog
        }
    }

    private void doSilentAuthorization(Context context) {
        Intent intent = new Intent(context, SilentLoginService.class);
        context.startService(intent);
        PersistentManager.persistSilentLoginStatus(SilentLoginService.SILENT_LOGIN_INPROGRESS);
    }

}
