package com.expenseondemand.eod.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.UIUtil;

public class BaseActivity extends AppCompatActivity {
    /**
     *
     *
     * This method handle the action bar and show icon in Action Bar
     */
    public void handleActionBar(Activity activity, int customLayout, String title) {
//		android.support.v7.app.ActionBar  actionBar = getSupportActionBar();
//		actionBar.setTitle(title);
//		actionBar.setDisplayHomeAsUpEnabled(true);
//		actionBar.setDisplayUseLogoEnabled(false);
//		actionBar.setDisplayShowHomeEnabled(false);
//		actionBar.setIcon(null);
//		actionBar.setLogo(null);
//
//		if(customLayout != 0)
//		{
//			actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//			actionBar.setCustomView(R.layout.custom_action_bar_layout);
//		}
    }

    /**
     * This method handle the tool bar and set title
     */
    private void handleToolBar(Toolbar toolbar, String title, boolean showBack) {
        setSupportActionBar(toolbar);
        toolbar.setTitle(title);

        getSupportActionBar().setDisplayHomeAsUpEnabled(showBack);
        getSupportActionBar().setDisplayShowHomeEnabled(showBack);
    }

    public void handleToolBar(String title, int backgroundColorId, int statusBarColorId, boolean showBack) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_layout);
        if (toolbar != null) {
            handleToolBar(toolbar, title, showBack);

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && statusBarColorId != AppConstant.DEFAULT_COLOR)
                getWindow().setStatusBarColor(ContextCompat.getColor(this, statusBarColorId));
            if (backgroundColorId != AppConstant.DEFAULT_COLOR)
                toolbar.setBackgroundColor(ContextCompat.getColor(this, backgroundColorId));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Get Menu Inflater
        MenuInflater inflater = getMenuInflater();

        //Inflate Menu
        inflater.inflate(R.menu.menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuItemId = item.getItemId();

        switch (menuItemId) {
            case android.R.id.home:
                //Handle Home Up Navigation
                handleHomeUpNavigation();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    //Handle Home Up Navigation
    public void handleHomeUpNavigation() {
        this.finish();
        overridePendingTransition(R.anim.slide_in_back, R.anim.slide_out_back);
    }

    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.slide_in_forward, R.anim.slide_out_forward);
    }

    @Override
    public void onBackPressed() {
        //this.finish();
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_back, R.anim.slide_out_back);
    }
          /* show error screen
		  * set image for screen
		  * or
		  * Alert dialog

		   @param
		 * @internetOrServerConnection    yes then show  internet otherwise  ServerConnecion icon
		 * @alertOrScreen   yse  then show  alert otherwise  full screen
		 * Message as depend screen
		 * */

    public void handleErrorOrNodataScreen(boolean noDataScreen, boolean internetOrServerConnection, String messageForNodata) {
        // Error screen
        LinearLayout linearErrorScreen = (LinearLayout) findViewById(R.id.linearErrorScreen);
        ImageView imageViewErrorIcon = (ImageView) findViewById(R.id.imageViewErrorIcon);
        TextView textViewErrorMessage = (TextView) findViewById(R.id.textViewErrorMessage);


        // No data screen
        LinearLayout linearNodataScreen = (LinearLayout) findViewById(R.id.linearNodataScreen);
        TextView textViewNodataMessage = (TextView) findViewById(R.id.textViewNodataMessage);

        if (noDataScreen) {
            if (linearNodataScreen != null) {
                linearErrorScreen.setVisibility(View.GONE);
                linearNodataScreen.setVisibility(View.VISIBLE);

                textViewNodataMessage.setText(messageForNodata);
            }
        } else {
            if (linearErrorScreen != null) {

                linearErrorScreen.setVisibility(View.VISIBLE);
                linearNodataScreen.setVisibility(View.GONE);

                if (internetOrServerConnection) {
                    imageViewErrorIcon.setImageResource(R.drawable.no_internet_connection);
                    textViewErrorMessage.setText(getResources().getString(R.string.no_internet_connection));
                } else {
                    imageViewErrorIcon.setImageResource(R.drawable.cant_connect_server_icon);
                    textViewErrorMessage.setText(getResources().getString(R.string.can_not_connect_server));
                }
            }


        }

    }


}
