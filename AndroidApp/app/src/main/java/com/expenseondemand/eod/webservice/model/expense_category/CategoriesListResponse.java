package com.expenseondemand.eod.webservice.model.expense_category;

import com.expenseondemand.eod.webservice.model.MasterResponse;

import java.util.ArrayList;


public class CategoriesListResponse extends MasterResponse
{
	private ArrayList<CategoryInfo> categoryList;

	public ArrayList<CategoryInfo> getCategoryList()
	{
		return categoryList;
	}

	public void setCategoryList(ArrayList<CategoryInfo> categoryList)
	{
		this.categoryList = categoryList;
	}
}
