package com.expenseondemand.eod.manager;

import com.expenseondemand.eod.model.DeviceInfo;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.util.AppUtil;

import android.app.Application;

public class ApplicationManager
{
	//Prepare Application
	public static void prepareApplication(Application appContext)
	{
		//Save Application Context in Cache
		CachingManager.saveAppContext(appContext);
		
		//Cache User Info Basedg on Last Stored UserId
		
		//Get Last Stored UserId
		String userId = PersistentManager.getLastStoredUserId();
		
		try
		{
			//Get UserIfo from DB
			UserInfo userInfo = DbManager.getUserInfo(userId);
			
			//Store in Cache
			CachingManager.saveUserInfo(userInfo);

			String deviceId = AppUtil.getDeviceID(appContext);
			String applicationVersion = AppUtil.getApplicationVersion(appContext);

			DeviceInfo deviceInfo = new DeviceInfo();
			deviceInfo.setDeviceId(deviceId);
			deviceInfo.setAppVersion(applicationVersion);

			CachingManager.saveDeviceInfo(deviceInfo);
		}
		catch(Exception exception)
		{
			//Do Nothing...
		}
	}
}
