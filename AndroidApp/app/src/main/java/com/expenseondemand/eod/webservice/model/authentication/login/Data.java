package com.expenseondemand.eod.webservice.model.authentication.login;

/**
 * Created by Ramit Yadav on 13-10-2016.
 */

public class Data
{
    private boolean validUser; //true/false
    private String message;
    private String token;
    private String defaultCurrencyCode;
    private int defaultCategoryId;
    private String userId;
    private String resourceId;
    private String costCenterId;
    private String companyLogo;
    private String branchId;
    private String companyId;
    private String departmentId;
    private String costCenterName;
    private String companyName;
    private String displayName;
    private String isApprover;
    private String isFinance;
    private String isBoss;
    private String isDelegate;
    private String gradeId;
    private String DeputyAssigned;

    public String getDeputyAssigned() {
        return DeputyAssigned;
    }

    public void setDeputyAssigned(String deputyAssigned) {
        DeputyAssigned = deputyAssigned;
    }

    public boolean isValidUser() {
        return validUser;
    }

    public void setValidUser(boolean validUser) {
        this.validUser = validUser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDefaultCurrencyCode() {
        return defaultCurrencyCode;
    }

    public void setDefaultCurrencyCode(String defaultCurrencyCode) {
        this.defaultCurrencyCode = defaultCurrencyCode;
    }

    public int getDefaultCategoryId() {
        return defaultCategoryId;
    }

    public void setDefaultCategoryId(int defaultCategoryId) {
        this.defaultCategoryId = defaultCategoryId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getCostCenterId() {
        return costCenterId;
    }

    public void setCostCenterId(String costCenterId) {
        this.costCenterId = costCenterId;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getCostCenterName() {
        return costCenterName;
    }

    public void setCostCenterName(String costCenterName) {
        this.costCenterName = costCenterName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getIsApprover() {
        return isApprover;
    }

    public void setIsApprover(String isApprover) {
        this.isApprover = isApprover;
    }

    public String getIsFinance() {
        return isFinance;
    }

    public void setIsFinance(String isFinance) {
        this.isFinance = isFinance;
    }

    public String getIsBoss() {
        return isBoss;
    }

    public void setIsBoss(String isBoss) {
        this.isBoss = isBoss;
    }

    public String getIsDelegate() {
        return isDelegate;
    }

    public void setIsDelegate(String isDelegate) {
        this.isDelegate = isDelegate;
    }

    public String getGradeId() {
        return gradeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }
}
