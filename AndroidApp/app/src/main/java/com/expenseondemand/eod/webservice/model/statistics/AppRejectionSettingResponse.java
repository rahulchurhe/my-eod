package com.expenseondemand.eod.webservice.model.statistics;

/**
 * Created by ITDIVISION\maximess087 on 24/1/17.
 */

public class AppRejectionSettingResponse {
    private String AppRejectSettingsRule1;
    private String AppRejectSettingsRule2;
    private String AppRejectSettingsRule3;
    private String AppRejectSettingsRule4;

    public String getAppRejectSettingsRule1() {
        return AppRejectSettingsRule1;
    }

    public void setAppRejectSettingsRule1(String appRejectSettingsRule1) {
        AppRejectSettingsRule1 = appRejectSettingsRule1;
    }

    public String getAppRejectSettingsRule2() {
        return AppRejectSettingsRule2;
    }

    public void setAppRejectSettingsRule2(String appRejectSettingsRule2) {
        AppRejectSettingsRule2 = appRejectSettingsRule2;
    }

    public String getAppRejectSettingsRule3() {
        return AppRejectSettingsRule3;
    }

    public void setAppRejectSettingsRule3(String appRejectSettingsRule3) {
        AppRejectSettingsRule3 = appRejectSettingsRule3;
    }

    public String getAppRejectSettingsRule4() {
        return AppRejectSettingsRule4;
    }

    public void setAppRejectSettingsRule4(String appRejectSettingsRule4) {
        AppRejectSettingsRule4 = appRejectSettingsRule4;
    }
}
