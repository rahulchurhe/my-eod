package com.expenseondemand.eod.screen.approval.expense.duplicate;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.callbackInterface.SharedClickListenerInteface;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;

/**
 * Created by Nandani Gupta on 2016-10-03.
 */
public class DuplicateEntryAdapter extends RecyclerView.Adapter<DuplicateEntryAdapter.MyViewHolder> {

    private JSONArray json;
    private Context activityContext;
    private SharedClickListenerInteface sharedClickListenerInteface;

    public class MyViewHolder extends RecyclerView.ViewHolder

    {
        TextView textViewSerialNumber;
        TextView txt_fieldTitleSource, txt_fieldValueSource;
        TextView txt_fieldTitleDate, txt_fieldValueDate;
        TextView txt_fieldTitleDestination, txt_fieldValueDestination;
        TextView txt_fieldTitleMile, txt_fieldValueMile;

        public MyViewHolder(View view) {
            super(view);
            textViewSerialNumber = (TextView) view.findViewById(R.id.textViewSerialNumber);

            txt_fieldTitleSource = (TextView) view.findViewById(R.id.txt_fieldTitleSource);
            txt_fieldValueSource = (TextView) view.findViewById(R.id.txt_fieldValueSource);

            txt_fieldTitleDestination = (TextView) view.findViewById(R.id.txt_fieldTitleDestination);
            txt_fieldValueDestination = (TextView) view.findViewById(R.id.txt_fieldValueDestination);

            txt_fieldTitleDate = (TextView) view.findViewById(R.id.txt_fieldTitleDate);
            txt_fieldValueDate = (TextView) view.findViewById(R.id.txt_fieldValueDate);

            txt_fieldTitleMile = (TextView) view.findViewById(R.id.txt_fieldTitleMile);
            txt_fieldValueMile = (TextView) view.findViewById(R.id.txt_fieldValueMile);
        }
    }


    public DuplicateEntryAdapter(Context activityContext, JSONArray jsonArray) {
        this.activityContext = activityContext;
        this.json = jsonArray;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.duplicate_entry_list_item1, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder viewHolder, int position) {
        try {
            int serialNum = position + 1;
            viewHolder.textViewSerialNumber.setText(String.valueOf(serialNum));

            JSONObject jsonObject1 = json.getJSONObject(position);

            viewHolder.txt_fieldTitleSource.setText(jsonObject1.getString("FieldTitle1")+"- ");
            viewHolder.txt_fieldValueSource.setText(jsonObject1.getString("FieldValue1"));

            viewHolder.txt_fieldTitleDestination.setText(jsonObject1.getString("FieldTitle2")+"- ");
            viewHolder.txt_fieldValueDestination.setText(jsonObject1.getString("FieldValue2"));

            viewHolder.txt_fieldTitleDate.setText(jsonObject1.getString("FieldTitle3")+"- ");
            long date = AppUtil.getLongDate(AppConstant.FORMAT_DATE_WEB_SERVICE, jsonObject1.getString("FieldValue3"));
            String formattedDate = AppUtil.getFormattedDateString(AppConstant.FORMAT_DATE_DISPLAY, date);
            viewHolder.txt_fieldValueDate.setText(formattedDate);

            viewHolder.txt_fieldTitleMile.setText(jsonObject1.getString("FieldTitle4")+"- ");
            //viewHolder.txt_fieldValueMile.setText(jsonObject1.getString("FieldValue4"));
            viewHolder.txt_fieldValueMile.setText(new DecimalFormat("0.00").format(Double.parseDouble(jsonObject1.getString("FieldValue4"))));

            if(jsonObject1.getString("ColourIndicator").toString().equals("Blue")){
                viewHolder.txt_fieldTitleSource.setTextColor(activityContext.getResources().getColor(R.color.text_color_violet));
                viewHolder.txt_fieldValueSource.setTextColor(activityContext.getResources().getColor(R.color.text_color_violet));

                viewHolder.txt_fieldTitleDestination.setTextColor(activityContext.getResources().getColor(R.color.text_color_violet));
                viewHolder.txt_fieldValueDestination.setTextColor(activityContext.getResources().getColor(R.color.text_color_violet));

                viewHolder.txt_fieldTitleDate.setTextColor(activityContext.getResources().getColor(R.color.text_color_violet));
                viewHolder.txt_fieldValueDate.setTextColor(activityContext.getResources().getColor(R.color.text_color_violet));

                viewHolder.txt_fieldTitleMile.setTextColor(activityContext.getResources().getColor(R.color.text_color_violet));
                viewHolder.txt_fieldValueMile.setTextColor(activityContext.getResources().getColor(R.color.text_color_violet));

            }else if(jsonObject1.getString("ColourIndicator").toString().equals("Red")){
                viewHolder.txt_fieldTitleSource.setTextColor(activityContext.getResources().getColor(R.color.text_color_red));
                viewHolder.txt_fieldValueSource.setTextColor(activityContext.getResources().getColor(R.color.text_color_red));

                viewHolder.txt_fieldTitleDestination.setTextColor(activityContext.getResources().getColor(R.color.text_color_red));
                viewHolder.txt_fieldValueDestination.setTextColor(activityContext.getResources().getColor(R.color.text_color_red));

                viewHolder.txt_fieldTitleDate.setTextColor(activityContext.getResources().getColor(R.color.text_color_red));
                viewHolder.txt_fieldValueDate.setTextColor(activityContext.getResources().getColor(R.color.dark_red));

                viewHolder.txt_fieldTitleMile.setTextColor(activityContext.getResources().getColor(R.color.text_color_red));
                viewHolder.txt_fieldValueMile.setTextColor(activityContext.getResources().getColor(R.color.text_color_red));
            }

        } catch (Exception e) {
        }
    }

    @Override
    public int getItemCount() {
        // return 10;
        return json.length();
    }


    private class DuplicateExpenseClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            int clickPosition = (int) view.getTag();
            sharedClickListenerInteface.ShareClicked(clickPosition);


        }
    }

    //set instance of callback interface click listener


    public void setSharedClickListener(SharedClickListenerInteface sharedClickListenerInteface) {
        this.sharedClickListenerInteface = sharedClickListenerInteface;
    }
}