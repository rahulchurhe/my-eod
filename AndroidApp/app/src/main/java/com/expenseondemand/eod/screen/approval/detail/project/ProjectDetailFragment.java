package com.expenseondemand.eod.screen.approval.detail.project;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.expenseondemand.eod.BaseFragment;
import com.expenseondemand.eod.R;
import com.expenseondemand.eod.dialogFragment.ApplicationAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationNetworkDialog;
import com.expenseondemand.eod.exception.EODBusinessException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.DeviceManager;
import com.expenseondemand.eod.manager.MediaManager;
import com.expenseondemand.eod.model.approval.detail.project.AdditionalDetailsItem;
import com.expenseondemand.eod.model.approval.detail.project.BusinessPurposeItem;
import com.expenseondemand.eod.model.approval.detail.project.PaymentTypeItem;
import com.expenseondemand.eod.model.approval.detail.project.PreApprovedDetailsItem;
import com.expenseondemand.eod.model.approval.detail.project.ProjectDetailsItem;
import com.expenseondemand.eod.screen.approval.detail.ExpenseDetailManager;
import com.expenseondemand.eod.screen.approval.detail.notes.NotePreApprovalAdapter;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.util.UIUtil;
import com.expenseondemand.eod.webservice.model.approval.detail.expense.receipt.ExpenseDetailsReceipt;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 16-09-2016.
 */
public class ProjectDetailFragment extends BaseFragment implements View.OnClickListener, ProjectDetailAdapter.ProjectDetailInterface {
    private static String expDetailID;
    private static String expHeaderID;
    private static ArrayList<AdditionalDetailsItem> additionalDetailData1;
    private static PaymentTypeItem paymentTypeItemData;
    private static BusinessPurposeItem businessPurposeItemData;
    private static ArrayList<PreApprovedDetailsItem> preApprovedDetailsItems;
    private final String PROJECT_DETAIL_KEY = "PROJECT_DETAIL_KEY";

    private RecyclerView recyclerViewProjectDetail;
    private ProjectDetailAdapter projectDetailAdapter;
    private ArrayList<ProjectDetailsItem> projectDetailData;
    ApplicationAlertDialog applicationAlertDialog;
    public static final int TYPE_ALERT = 0;

    private String filePath;
    private File file;
    private FileOutputStream os;
    private ApplicationNetworkDialog applicationNetworkDialog;

    private boolean isExpandedReason = false;
    private boolean isExpandedAdditionalDetails = false;
    private boolean isExpandedPreApprovalDetails = false;
    private ImageView imageViewPlus;
    private RecyclerView recyclerViewAdditionalField;
    private AdditionalProjectDetailAdapter additionalProjectDetailAdapter;
    private ImageView img_plusAdditionalField;
    private TextView txt_labelPaymentType, txt_valuePaymentType, txt_labelBusinessPurpose, txt_valueBusinessPurpose;
    private LinearLayout linear_projectDetails, linear_additionalField;
    private LinearLayout linear_paymentType, linear_businessPurpose;
    private LinearLayout linear_preApproval;
    private NotePreApprovalAdapter notePreApprovalAdapter;
    private RecyclerView recyclerPreApprovalNote;
    private ImageView img_plusPreApproval;


    public static ProjectDetailFragment getInstance(ArrayList<PreApprovedDetailsItem> preApprovedDetailsData, ArrayList<ProjectDetailsItem> projectDetailData, ArrayList<AdditionalDetailsItem> additionalDetailData, PaymentTypeItem paymentTypeItem, BusinessPurposeItem businessPurposeItem, String expenseDetailID, String expenseHeaderID) {
        ProjectDetailFragment projectDetailFragment = new ProjectDetailFragment();
        projectDetailFragment.projectDetailData = projectDetailData;
        preApprovedDetailsItems = preApprovedDetailsData;
        additionalDetailData1 = additionalDetailData;
        paymentTypeItemData = paymentTypeItem;
        businessPurposeItemData = businessPurposeItem;
        expDetailID = expenseDetailID;
        expHeaderID = expenseHeaderID;

        return projectDetailFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.project_detail_fragment, container, false);

        initializeView(rootView);

        if (savedInstanceState != null)
            projectDetailData = savedInstanceState.getParcelableArrayList(PROJECT_DETAIL_KEY);

        populateView();

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(PROJECT_DETAIL_KEY, projectDetailData);
        super.onSaveInstanceState(outState);
    }

    private void initializeView(View rootView) {
        recyclerViewProjectDetail = (RecyclerView) rootView.findViewById(R.id.recyclerViewProjectDetail);
        final LinearLayoutManager projectDetailItemLinerLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerViewProjectDetail.setLayoutManager(projectDetailItemLinerLayoutManager);

        recyclerViewAdditionalField = (RecyclerView) rootView.findViewById(R.id.recyclerViewAdditionalField);
        final LinearLayoutManager projectDetailItemLinerLayoutManager1 = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerViewAdditionalField.setLayoutManager(projectDetailItemLinerLayoutManager1);


        recyclerPreApprovalNote = (RecyclerView) rootView.findViewById(R.id.recyclerPreApprovalNote);
        final LinearLayoutManager projectDetailItemLinerLayoutManager2 = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerPreApprovalNote.setLayoutManager(projectDetailItemLinerLayoutManager2);

        recyclerViewProjectDetail.setNestedScrollingEnabled(false);
        recyclerViewAdditionalField.setNestedScrollingEnabled(false);
        recyclerPreApprovalNote.setNestedScrollingEnabled(false);

        imageViewPlus = (ImageView) rootView.findViewById(R.id.imageViewPlus);
        imageViewPlus.setOnClickListener(this);

        img_plusAdditionalField = (ImageView) rootView.findViewById(R.id.img_plusAdditionalField);
        img_plusAdditionalField.setOnClickListener(this);


        img_plusPreApproval = (ImageView) rootView.findViewById(R.id.img_plusPreApproval);
        img_plusPreApproval.setOnClickListener(this);


        txt_labelPaymentType = (TextView) rootView.findViewById(R.id.txt_labelPaymentType);
        txt_valuePaymentType = (TextView) rootView.findViewById(R.id.txt_valuePaymentType);
        txt_labelBusinessPurpose = (TextView) rootView.findViewById(R.id.txt_labelBusinessPurpose);
        txt_valueBusinessPurpose = (TextView) rootView.findViewById(R.id.txt_valueBusinessPurpose);

        linear_projectDetails = (LinearLayout) rootView.findViewById(R.id.linear_projectDetails);
        linear_additionalField = (LinearLayout) rootView.findViewById(R.id.linear_additionalField);
        linear_paymentType = (LinearLayout) rootView.findViewById(R.id.linear_paymentType);
        linear_businessPurpose = (LinearLayout) rootView.findViewById(R.id.linear_businessPurpose);
        linear_preApproval = (LinearLayout) rootView.findViewById(R.id.linear_preApproval);

        if (paymentTypeItemData != null) {
            linear_paymentType.setVisibility(View.VISIBLE);
            txt_labelPaymentType.setText(paymentTypeItemData.getLabelName());
            txt_valuePaymentType.setText(paymentTypeItemData.getValue());
        } else {
            linear_paymentType.setVisibility(View.GONE);
        }
        if (businessPurposeItemData != null) {
            linear_businessPurpose.setVisibility(View.VISIBLE);
            txt_labelBusinessPurpose.setText(businessPurposeItemData.getLabelName());
            txt_valueBusinessPurpose.setText(businessPurposeItemData.getValue());
        } else {
            linear_businessPurpose.setVisibility(View.GONE);
        }

        preApprovalExpandable(preApprovedDetailsItems);
    }

    private void populateView() {
        if (!AppUtil.isCollectionEmpty(projectDetailData)) {
            linear_projectDetails.setVisibility(View.VISIBLE);
            projectDetailAdapter = new ProjectDetailAdapter(getActivity(), projectDetailData, this);
            recyclerViewProjectDetail.setAdapter(projectDetailAdapter);
        } else {
            linear_projectDetails.setVisibility(View.GONE);
        }

        if (!AppUtil.isCollectionEmpty(additionalDetailData1)) {
            linear_additionalField.setVisibility(View.VISIBLE);
            additionalProjectDetailAdapter = new AdditionalProjectDetailAdapter(getActivity(), additionalDetailData1);
            recyclerViewAdditionalField.setAdapter(additionalProjectDetailAdapter);
        } else {
            linear_additionalField.setVisibility(View.GONE);
        }
    }

    private void preApprovalExpandable(ArrayList<PreApprovedDetailsItem> preApprovedDetails) {
        if (!AppUtil.isCollectionEmpty(preApprovedDetails)) {
            linear_preApproval.setVisibility(View.VISIBLE);
            notePreApprovalAdapter = new NotePreApprovalAdapter(getActivity(), preApprovedDetails);
            recyclerPreApprovalNote.setAdapter(notePreApprovalAdapter);
        } else {
            linear_preApproval.setVisibility(View.GONE);
        }
    }

    @Override
    public void handleReceipt() {
        if (DeviceManager.isOnline()) {
            new ReceiptAsyncTask().execute(expDetailID, expHeaderID);
        } else {
            enableNetwork();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewPlus: {
                handlePlusMinusClick();
                break;
            }
            case R.id.img_plusAdditionalField: {
                handlePlusMinusAdditionalDetailsClick();
                break;
            }
            case R.id.img_plusPreApproval: {
                handlePlusMinusPreApprovalDetailsClick();
                break;
            }

        }
    }

    private class ReceiptAsyncTask extends AsyncTask<String, Void, ExpenseDetailsReceipt> {
        private Exception exception = null;
        private Dialog dialog = null;


        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(getActivity());
        }

        @Override
        protected ExpenseDetailsReceipt doInBackground(String... param) {
            ExpenseDetailsReceipt expenseDetailsReceipt = null;
            try {
                expenseDetailsReceipt = ExpenseDetailManager.getExpenseDetailReceipt(param);
            } catch (EODException e) {
                e.printStackTrace();
            }
            return expenseDetailsReceipt;
        }


        @Override
        protected void onPostExecute(ExpenseDetailsReceipt expenseDetailsReceipt) {
            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);

            if (exception == null) {
                //OnSucess
                onSuccessGetExpenseDetail(expenseDetailsReceipt);
            } else if (exception instanceof EODBusinessException) {
                //OnFail
                onFailGetExpenseDetail((EODException) exception);
            } else if (exception instanceof EODException) {
                //OnError
                onErrorGetExpenseDetail(exception);
            }
        }


    }

    private void onErrorGetExpenseDetail(Exception exception) {
        showAlertDialog(getResources().getString(R.string.text_error));
    }

    private void onFailGetExpenseDetail(EODException exception) {
        showAlertDialog(getResources().getString(R.string.text_error));
    }

    private void onSuccessGetExpenseDetail(ExpenseDetailsReceipt expenseDetailsReceipt) {
        //Download Receipts
        if (expenseDetailsReceipt != null) {
            String receiptName = expenseDetailsReceipt.getDescription();
            String base64ReceiptString = expenseDetailsReceipt.getDocument();

            if (receiptName.contains(".pdf")) {
                file = MediaManager.getReceiptPathFormPermanentStorage(receiptName);
                try {
                    os = new FileOutputStream(file, false);
                    os.write(Base64.decode(base64ReceiptString, Base64.NO_WRAP));
                    os.flush();
                    os.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                file = MediaManager.getReceiptPathFormPermanentStorage(receiptName);
                Bitmap bitmap = MediaManager.getBitmapFromString(base64ReceiptString, CachingManager.getAppContext());
                MediaManager.writeBitmapTofFile(file, bitmap);
            }

            //Create filepath of downloaded receipt
            filePath = file.getAbsolutePath();

            //Show Receipt popup dialog
            UIUtil.showReceiptDialog(filePath, receiptName, getActivity());
        }
    }

    //API Response Error
    private void showAlertDialog(String msg) {
        applicationAlertDialog = new ApplicationAlertDialog().getInstance((ApplicationAlertDialog.AlertDialogActionListener) getActivity(), true, getString(R.string.text_error_processing), msg, getString(R.string.button_ok), TYPE_ALERT);
        applicationAlertDialog.show(getFragmentManager(), "dialog");
    }

    //Network enable dialog
    void enableNetwork() {
        applicationNetworkDialog = new ApplicationNetworkDialog().getInstance(getActivity());
        applicationNetworkDialog.show(getFragmentManager(), "dialog");
    }

    private void handlePlusMinusClick() {
        if (isExpandedReason) {
            recyclerViewProjectDetail.setVisibility(View.GONE);
            imageViewPlus.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.approval_expense_detail_policy_violation_plus_selector));
        } else {
            recyclerViewProjectDetail.setVisibility(View.VISIBLE);
            imageViewPlus.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.approval_expense_detail_policy_violation_minus_selector));
        }
        isExpandedReason = !isExpandedReason;
    }

    private void handlePlusMinusAdditionalDetailsClick() {
        if (isExpandedAdditionalDetails) {
            recyclerViewAdditionalField.setVisibility(View.GONE);
            img_plusAdditionalField.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.approval_expense_detail_policy_violation_plus_selector));
        } else {
            recyclerViewAdditionalField.setVisibility(View.VISIBLE);
            img_plusAdditionalField.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.approval_expense_detail_policy_violation_minus_selector));
        }
        isExpandedAdditionalDetails = !isExpandedAdditionalDetails;
    }

    private void handlePlusMinusPreApprovalDetailsClick() {
        if (isExpandedPreApprovalDetails) {
            recyclerPreApprovalNote.setVisibility(View.GONE);
            img_plusPreApproval.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.approval_expense_detail_policy_violation_plus_selector));
        } else {
            recyclerPreApprovalNote.setVisibility(View.VISIBLE);
            img_plusPreApproval.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.approval_expense_detail_policy_violation_minus_selector));
        }
        isExpandedPreApprovalDetails = !isExpandedPreApprovalDetails;
    }
}
