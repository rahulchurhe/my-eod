package com.expenseondemand.eod.model.approval.expense.multiplecategory;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ITDIVISION\maximess087 on 16/1/17.
 */

public class MultiCategoryExpenseParam implements Parcelable {
    private String ExpenseHeaderID;
    private String ExpenseDetailID;
    private String CompanyId;
    private String IsMultiPayment;
    private String Date;

    public MultiCategoryExpenseParam(){}
    protected MultiCategoryExpenseParam(Parcel in) {
        ExpenseHeaderID = in.readString();
        ExpenseDetailID = in.readString();
        CompanyId = in.readString();
        IsMultiPayment = in.readString();
        Date = in.readString();
    }
    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeString(ExpenseHeaderID);
        dest.writeString(ExpenseDetailID);
        dest.writeString(CompanyId);
        dest.writeString(IsMultiPayment);
        dest.writeString(Date);
    }

    public static final Creator<MultiCategoryExpenseParam> CREATOR = new Creator<MultiCategoryExpenseParam>() {
        @Override
        public MultiCategoryExpenseParam createFromParcel(Parcel in) {
            return new MultiCategoryExpenseParam(in);
        }

        @Override
        public MultiCategoryExpenseParam[] newArray(int size) {
            return new MultiCategoryExpenseParam[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public String getExpenseHeaderID() {
        return ExpenseHeaderID;
    }

    public void setExpenseHeaderID(String expenseHeaderID) {
        ExpenseHeaderID = expenseHeaderID;
    }

    public String getExpenseDetailID() {
        return ExpenseDetailID;
    }

    public void setExpenseDetailID(String expenseDetailID) {
        ExpenseDetailID = expenseDetailID;
    }

    public String getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(String companyId) {
        CompanyId = companyId;
    }

    public String getIsMultiPayment() {
        return IsMultiPayment;
    }

    public void setIsMultiPayment(String isMultiPayment) {
        IsMultiPayment = isMultiPayment;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
}
