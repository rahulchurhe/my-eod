package com.expenseondemand.eod.model.approval.detail.project;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ramit Yadav on 16-09-2016.
 */
public class ProjectDetailItem implements Parcelable
{
    private String key;
    private String value;
    private int receiptCount;

    public ProjectDetailItem(Parcel in)
    {
        key = in.readString();
        value = in.readString();
        receiptCount = in.readInt();
    }

    public ProjectDetailItem()
    {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(key);
        dest.writeString(value);
        dest.writeInt(receiptCount);
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    public static final Creator<ProjectDetailItem> CREATOR = new Creator<ProjectDetailItem>() {
        @Override
        public ProjectDetailItem createFromParcel(Parcel in) {
            return new ProjectDetailItem(in);
        }

        @Override
        public ProjectDetailItem[] newArray(int size) {
            return new ProjectDetailItem[size];
        }
    };

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public int getReceiptCount()
    {
        return receiptCount;
    }

    public void setReceiptCount(int receiptCount)
    {
        this.receiptCount = receiptCount;
    }

    public void setReceiptCount(String receiptCount)
    {
        this.receiptCount = Integer.parseInt(receiptCount);
    }
}
