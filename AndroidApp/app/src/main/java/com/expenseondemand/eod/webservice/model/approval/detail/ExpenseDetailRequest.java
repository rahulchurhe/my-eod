package com.expenseondemand.eod.webservice.model.approval.detail;

import com.expenseondemand.eod.webservice.model.MasterRequest;

/**
 * Created by Ramit Yadav on 20-10-2016.
 */

public class ExpenseDetailRequest extends MasterRequest
{
    private String expenseHeaderID;
    private String expenseDetailID;
    private String approverId;
    private String approverCompanyId;
    private String isApprover;

    public String getExpenseHeaderID()
    {
        return expenseHeaderID;
    }

    public void setExpenseHeaderID(String expenseHeaderID)
    {
        this.expenseHeaderID = expenseHeaderID;
    }

    public String getExpenseDetailID()
    {
        return expenseDetailID;
    }

    public void setExpenseDetailID(String expenseDetailID)
    {
        this.expenseDetailID = expenseDetailID;
    }

    public String getApproverId()
    {
        return approverId;
    }

    public void setApproverId(String approverId)
    {
        this.approverId = approverId;
    }

    public String getApproverCompanyId()
    {
        return approverCompanyId;
    }

    public void setApproverCompanyId(String approverCompanyId)
    {
        this.approverCompanyId = approverCompanyId;
    }

    public String getIsApprover() {
        return isApprover;
    }

    public void setIsApprover(String isApprover)
    {
        this.isApprover = isApprover;
    }

    public void setIsApprover(boolean isApprover)
    {
        this.isApprover = isApprover ? "1" : "0";
    }
}
