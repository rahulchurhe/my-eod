package com.expenseondemand.eod.screen.approval.detail.project;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.model.approval.detail.project.ProjectDetailItem;
import com.expenseondemand.eod.model.approval.detail.project.ProjectDetailsItem;
import com.expenseondemand.eod.util.AppUtil;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 16-09-2016.
 */
public class ProjectDetailAdapter extends RecyclerView.Adapter {
    private final ProjectDetailInterface projectDetailFragment;
    private Context context;
    private ArrayList<ProjectDetailsItem> projectDetailData;

    public interface ProjectDetailInterface{
        void handleReceipt();
    }

    public ProjectDetailAdapter(Context context, ArrayList<ProjectDetailsItem> projectDetailData, ProjectDetailInterface projectDetailFragment) {
        this.context = context;
        this.projectDetailData = projectDetailData;
        this.projectDetailFragment=projectDetailFragment;
    }

    @Override
    public int getItemCount() {
        return AppUtil.isCollectionEmpty(projectDetailData) ? 0 : projectDetailData.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.project_detail_item, parent, false);

        return new ProjectDetailItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ProjectDetailsItem projectDetailItem = projectDetailData.get(position);

        ProjectDetailItemHolder projectDetailItemHolder = (ProjectDetailItemHolder) holder;
        projectDetailItemHolder.textViewLeft.setText(projectDetailItem.getLabelName());
        projectDetailItemHolder.textViewRight.setText(projectDetailItem.getValue());
        /*int receiptCount = projectDetailItem.getReceiptCount();
        if (receiptCount > 0) {
            projectDetailItemHolder.imageViewReceipt.setVisibility(View.VISIBLE);
            projectDetailItemHolder.textViewCount.setVisibility(View.VISIBLE);
            projectDetailItemHolder.textViewCount.setText(String.valueOf(projectDetailItem.getReceiptCount()));
        } else {
            projectDetailItemHolder.imageViewReceipt.setVisibility(View.INVISIBLE);
            projectDetailItemHolder.textViewCount.setVisibility(View.GONE);
        }*/

//        projectDetailItemHolder.imageViewReceipt.setOnClickListener(new ProjectDetailClickListener());


    }

    protected static class ProjectDetailItemHolder extends RecyclerView.ViewHolder {
        TextView textViewLeft;
        TextView textViewRight;
        TextView textViewCount;
        // ImageView imageViewReceipt;

        public ProjectDetailItemHolder(View itemView) {
            super(itemView);

            textViewLeft = (TextView) itemView.findViewById(R.id.textViewLeft);
            textViewRight = (TextView) itemView.findViewById(R.id.textViewRight);
            textViewCount = (TextView) itemView.findViewById(R.id.textViewCount);
            //  imageViewReceipt = (ImageView) itemView.findViewById(R.id.imageViewReceipt);
        }
    }


    private class ProjectDetailClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            int viewId = view.getId();
            switch (viewId) {
                case R.id.imageViewReceipt:
                    projectDetailFragment.handleReceipt();
                    break;
            }
        }
    }
}
