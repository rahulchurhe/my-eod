package com.expenseondemand.eod.screen.claimant.upload;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.activity.BaseActivity;
import com.expenseondemand.eod.dialogFragment.ApplicationAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationErrorAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationNetworkDialog;
import com.expenseondemand.eod.exception.EODBusinessException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.AuthenticationManager;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.DbManager;
import com.expenseondemand.eod.manager.DeviceManager;
import com.expenseondemand.eod.manager.ExpenseManager;
import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.manager.ValidationManager;
import com.expenseondemand.eod.model.Expense;
import com.expenseondemand.eod.model.UserCredential;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.model.ValidationError;
import com.expenseondemand.eod.screen.claimant.create.CreateEditExpenseActivity;
import com.expenseondemand.eod.screen.claimant.detail.ClaimantExpenseDetailActivity;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.util.UIUtil;

import java.util.ArrayList;
import java.util.Collections;

public class UploadExpenseActivity extends BaseActivity implements OnClickListener, UploadExpenseAdapter.UploadExpenseRowClickListener, ApplicationAlertDialog.AlertDialogActionListener, ApplicationErrorAlertDialog.ErrorDialogActionListener {

    public static final int TYPE_LOGIN = 0;
    public static final int TYPE_DELETE = 1;
    public static final int TYPE_ERROR = -1;
    public static final int TYPE_SUCCESS = 200;
    public static final int TYPE_UPLOAD = 2;
    public static final int TYPE_EDIT = 4;

    private RecyclerView recyclerViewExpenseList;
    private CheckBox checkBoxSelectAll;
    private RelativeLayout relativeRootLayout;
    private ArrayList<Expense> expenses;
    private UploadExpenseAdapter expenseListAdapter;
    private CheckBoxListener checkBoxListener;
    private LinearLayout linearErrorScreen;
    private LinearLayout linearNodataScreen;
    private Button buttonRetry;
    private boolean isAuthenticationRetried;
    private ArrayList<Integer> expenseIdList;

    //set instance alert dialog
    private ApplicationAlertDialog applicationAlertDialog;
    private ApplicationErrorAlertDialog applicationErrorAlertDialog;
    private ApplicationNetworkDialog applicationNetworkDialogl;

    @Override
    protected void onResume() {
        super.onResume();
        //Refresh expense list after click save dialog yes button.
        if (SharePreference.getBoolean(getApplicationContext(), AppConstant.BACK_CREATE_EXPENSE)) {
            SharePreference.putBoolean(getApplicationContext(), AppConstant.BACK_CREATE_EXPENSE, false);
            refreshExpenseList();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_expense_list_activity_layout);

        //Initialize View
        initializeView();

        //Initialize Data
        initializeData();
    }

    //Initialize View
    private void initializeView() {
        //Handle toolbar
        handleToolBar(getResources().getString(R.string.text_upload_expense), R.color.approval_claim_list_toolbar, R.color.approval_claim_list_status_bar, true);

        //initialize expenses list
        expenses = new ArrayList<Expense>();

        //Get List View
        recyclerViewExpenseList = (RecyclerView) findViewById(R.id.recyclerViewExpenseList);
        //Get Adapter
        expenseListAdapter = new UploadExpenseAdapter(UploadExpenseActivity.this, expenses, this);
        recyclerViewExpenseList.setAdapter(expenseListAdapter);

        //Get CheckBox Listener
        checkBoxListener = new CheckBoxListener();

        //Get error screen
        linearErrorScreen = (LinearLayout) findViewById(R.id.linearErrorScreen);
        //Get No data screen
        linearNodataScreen = (LinearLayout) findViewById(R.id.linearNodataScreen);
        //Root Layout
        relativeRootLayout = (RelativeLayout) findViewById(R.id.relativeRootLayout);

        //Get CheckBox
        checkBoxSelectAll = (CheckBox) findViewById(R.id.checkBoxSelectAll);
        checkBoxSelectAll.setOnClickListener(checkBoxListener);


        //Delete Button
        ImageView imageViewDelete = (ImageView) findViewById(R.id.imageViewDelete);
        imageViewDelete.setOnClickListener(this);

        //Edit Button
        ImageView imageViewEdit = (ImageView) findViewById(R.id.imageViewEdit);
        imageViewEdit.setOnClickListener(this);

        //Upload Button
        ImageView imageViewUpload = (ImageView) findViewById(R.id.imageViewUpload);
        imageViewUpload.setOnClickListener(this);

    }

    //Initialize Data
    private void initializeData() {
        //Refresh Expense List
        refreshExpenseList();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        //Initialize Data
        initializeData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Get Menu Inflater
        MenuInflater inflater = getMenuInflater();

        //Inflate Menu
        inflater.inflate(R.menu.expense_list_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuItemId = item.getItemId();
        switch (menuItemId) {
            case R.id.menuCreateExpense:
                //Handle Create Expense
                handleCreateExpense();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        switch (viewId) {
            case R.id.imageViewDelete:
                handleDeleteExpense();
                break;

            case R.id.imageViewEdit:
                handleEditExpense();
                break;

            case R.id.imageViewUpload:
                handleUploadExpense();
                break;

            case R.id.buttonRetry:
                refreshExpenseList();
                break;
        }
    }

    //Refresh Expense List
    private void refreshExpenseList() {
        //Get Expenses from DB
        getExpenseListFromDb();

        //Reset Select All Checkbox
        checkBoxSelectAll.setChecked(false);
    }

    //Handle Create Expense
    private void handleCreateExpense() {
        navigateToCreateExpense();
    }


    //Handle Edit Expense
    private void handleEditExpense() {
        //Get Selected Expense List
        ArrayList<Expense> expenseList = getSelectedExpenseList();

        //Validate
        ValidationError validationError = ValidationManager.validateEditExpense(expenseList);

        if (validationError != null) {
            //Show Alert Dialog
            showAlertDialog(true, validationError.getValidationMessage(), null, null, TYPE_EDIT);

        } else {
            Expense expense = null;

            //Iterate Expense List to get Selected Expense
            for (Expense tempExpense : expenseList) {
                if (tempExpense != null) {
                    if (tempExpense.isSelected()) {
                        expense = tempExpense;

                        break;
                    }
                }
            }

            if (expense != null) {
                //Navigate to Edit Expense
                navigateToEditExpense(expense);
            }
        }
    }

    //Handle Delete Expense
    private void handleDeleteExpense() {
        //Get Selected Expense List
        ArrayList<Expense> expenseList = getSelectedExpenseList();

        ValidationError validationError = ValidationManager.validateDeleteExpense(expenseList);

        if (validationError != null) {
            //Show Alert Dialog
            showAlertDialog(true, validationError.getValidationMessage(), null, null, TYPE_ERROR);
        } else {
            //Create List of Expense-Ids to Delete
            expenseIdList = new ArrayList<Integer>();

            //Iterate Expense List to Get Ids of Expenses to delete
            for (Expense expense : expenseList) {
                if (expense != null) {
                    if (expense.isSelected()) {
                        expenseIdList.add(expense.getExpenseId());
                    }
                }
            }
            //show Alert Dialog
            showAlertDialog(false, getString(R.string.text_delete_expense), null, getString(R.string.button_yes), getString(R.string.button_no), TYPE_DELETE);
        }
    }

    private void deleteExpense(ArrayList<Integer> expenseIdList) {
        //Async task
        DeleteExpenseAsyncTask asyncTask = new DeleteExpenseAsyncTask(expenseIdList);
        asyncTask.execute();
    }

    //Handle Upload Expense
    private void handleUploadExpense() {
        //Get Selected Expense List
        ArrayList<Expense> expenseList = getSelectedExpenseList();

        ValidationError validationError = ValidationManager.validateUploadExpense(expenseList);

        if (validationError != null) {
            //Show Alert Dialog
            showAlertDialog(true, validationError.getValidationMessage(), null, null, TYPE_ERROR);
        } else {
            //Check if Network Available
            if (DeviceManager.isOnline()) {
                UploadExpenseAsyncTask asyncTask = new UploadExpenseAsyncTask(expenseList);
                asyncTask.execute();
            } else {
                //Show Network Error Dialog
                enableNetwork();
            }
        }
    }

    //Navigate to Create Expense
    private void navigateToCreateExpense() {
        Intent intent = new Intent(this, CreateEditExpenseActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_forward, R.anim.slide_out_forward);
    }

    //Navigate to Create Expense
    private void navigateToEditExpense(Expense expense) {
        Intent intent = new Intent(this, CreateEditExpenseActivity.class);
        intent.putExtra(AppConstant.KEY_IS_EDIT_EXPENSE, true);
        intent.putExtra(AppConstant.KEY_EXPENSE_OBJECT, expense);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_forward, R.anim.slide_out_forward);
    }


    //Get Selected Expense List
    private ArrayList<Expense> getSelectedExpenseList() {
        ArrayList<Expense> expenseList = new ArrayList<Expense>();

        if (!AppUtil.isCollectionEmpty(expenses)) {
            for (Expense expense : expenses) {
                if (expense != null) {
                    if (expense.isSelected()) {
                        expenseList.add(expense);
                    }
                }
            }
        }

        return expenseList;
    }

    //Get Expenses from DB
    private void getExpenseListFromDb() {
        String userId = null;
        //Get UserInfo from Cache
        UserInfo userInfo = CachingManager.getUserInfo();
        if (userInfo != null) {
            userId = userInfo.getUserId();
        }

        //Run AsyncTask
        GetExpenseListAsyncTask asyncTask = new GetExpenseListAsyncTask();
        asyncTask.execute(userId);
    }

    //get click on list  checkbox expense item

    @Override
    public void handleCheckBoxClick(CheckBox checkBox) {
        handleSelectExpense(checkBox);
    }

    //get click on list row expense item
    @Override
    public void handleExpenseRowClick(int position) {
        //Get Expense Item
        Expense expense = expenses.get(position);

        //Navigate to Expense Detail Activity
        navigateToExpenseDetail(expense);
    }


    private class CheckBoxListener implements OnClickListener {
        @Override
        public void onClick(View view) {
            int viewId = view.getId();

            //Cast
            CheckBox checkBox = (CheckBox) view;

            switch (viewId) {
                case R.id.checkBoxSelectAll:
                    handleSelectAll(checkBox);
                    break;
            }
        }

        //Select All Expenses
        private void handleSelectAll(CheckBox checkBox) {
            //Get Checkbox state
            boolean isChecked = checkBox.isChecked();

            if (!AppUtil.isCollectionEmpty(expenses)) {
                //Iterate Display Expense List
                for (Expense expense : expenses) {
                    expense.setSelected(isChecked);
                }

                //Notify Adapter
                expenseListAdapter.notifyDataSetChanged();
            } else {
                checkBox.setChecked(false);
            }
        }
    }

    //Select Single Expense
    private void handleSelectExpense(CheckBox checkBox) {
        //Get Expense from Tag
        Expense expense = (Expense) checkBox.getTag();

        boolean isChecked = checkBox.isChecked();
        expense.setSelected(isChecked);

        if (isChecked == false) {
            //UnCheck Select All
            checkBoxSelectAll.setChecked(false);
        }
    }

    private void navigateToExpenseDetail(Expense expense) {
        Intent intent = new Intent(this, ClaimantExpenseDetailActivity.class);
        intent.putExtra(AppConstant.KEY_EXPENSE_OBJECT, expense);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_forward, R.anim.slide_out_forward);
    }

    /**
     * Async Task
     */
    private class GetExpenseListAsyncTask extends AsyncTask<String, Void, Void> {
        private ArrayList<Expense> expensesDisplayList;
        private Dialog dialog = null;
        private Exception exception = null;

        @Override
        protected void onPreExecute() {
            expensesDisplayList = new ArrayList<Expense>();
            dialog = UIUtil.showProgressDialog(UploadExpenseActivity.this);
        }

        @Override
        protected Void doInBackground(String... params) {
            String userId = params[0];

            try {
                //Get Expense List
                expensesDisplayList = ExpenseManager.getExpenseList(userId);
            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //Dismiss Dialog
            dialog.dismiss();

            if (exception == null) {
                onSuccessGetExpenseList(expensesDisplayList);

            } else {
                //On Error
                onErrorGetExpenseList();
            }
        }
    }

    //On Success
    private void onSuccessGetExpenseList(ArrayList<Expense> expensesDisplayList) {
        expenses.clear();
        if (expensesDisplayList != null) {
            Collections.reverse(expensesDisplayList);
            expenses.addAll(expensesDisplayList);
            //Hide Select All Layout
        }

        //Notify Adapter
        expenseListAdapter.notifyDataSetChanged();
        handleVisibilityEmptyView();
    }

    //On Error
    private void onErrorGetExpenseList() {
        //Show Error screen
        handleErrorOrNodataScreen(false, false, null);
        relativeRootLayout.setVisibility(View.GONE);
    }

    //Delete Expense AsyncTask
    private class DeleteExpenseAsyncTask extends AsyncTask<Void, Void, Void> {
        private Dialog dialog = null;
        private Exception exception = null;

        ArrayList<Integer> expenseIdList = null;

        public DeleteExpenseAsyncTask(ArrayList<Integer> expenseIdList) {
            this.expenseIdList = expenseIdList;
        }

        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(UploadExpenseActivity.this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                //Delete Expense
                ExpenseManager.deleteExpenses(expenseIdList);
            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);

            if (exception == null) {
                //On Success
                onSuccessDeleteExpense();
            } else {
                //On Fail
                onErrorDeleteExpense();
            }
        }
    }

    private void onSuccessDeleteExpense() {
        //Referesh List
        refreshExpenseList();
    }

    private void onErrorDeleteExpense() {
        //Show Error Alert Dialog
        showErrorAlertDialog(false, null, null, null, TYPE_DELETE);
        //showResponseErrorAlertDialog(false, null, null, null);
    }

    //Upload Expense Async Task
    private class UploadExpenseAsyncTask extends AsyncTask<Void, Void, Void> {
        private Dialog dialog = null;
        private Exception exception = null;
        private ArrayList<Expense> uploadExpenseList = null;

        public UploadExpenseAsyncTask(ArrayList<Expense> expenseList) {
            this.uploadExpenseList = expenseList;
        }

        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(UploadExpenseActivity.this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                //Iterate List and Upload one by one
                for (Expense expense : uploadExpenseList) {
                    //Upload Expense
                    ExpenseManager.uploadExpense(expense);
                }
            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);

            if (exception == null) {
                //On Success
                onSuccessUploadExpense(uploadExpenseList);
            } else {
                //On Fail
                onErrorUploadExpense();
            }
        }
    }

    private void onSuccessUploadExpense(ArrayList<Expense> uploadExpenseList) {
        //Referesh Expense List
        refreshExpenseList();
        //Prepare Success Message
        String successMessage = (uploadExpenseList.size() > 1) ? getString(R.string.text_expense_items_uploaded) : getString(R.string.text_expense_item_uploaded);
        //Get Dialog
        showAlertDialog(true, successMessage, null, null, TYPE_SUCCESS);
    }

    private void onErrorUploadExpense() {
        if (isAuthenticationRetried) //Already Retried
        {
            //Show Error Alert Dialog
            showErrorAlertDialog(false, null, null, null, TYPE_UPLOAD);
            //showResponseErrorAlertDialog(false, null, null, null);
            //Referesh Expense List
            refreshExpenseList();
        } else //Not yet Retried
        {
            //Authenticate User Again to obtain new Token
            authenticateUser();
        }
    }

    //Authenticate User
    private void authenticateUser() {
        //UserCredentials
        UserCredential userCredential = null;

        //Get UserInfo
        UserInfo userInfo = CachingManager.getUserInfo();

        if (userInfo != null) {
            //Get User Credentials
            userCredential = userInfo.getUserCredential();
        }

        //Autheticate User AsyncTask
        AuthenticateUserAsyncTask asyncTask = new AuthenticateUserAsyncTask();
        asyncTask.execute(userCredential);
    }

    //Authenticate User AsyncTask
    private class AuthenticateUserAsyncTask extends AsyncTask<UserCredential, Void, UserInfo> {
        private Dialog dialog = null;
        private Exception exception = null;

        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(UploadExpenseActivity.this);
        }

        @Override
        protected UserInfo doInBackground(UserCredential... params) {
            UserInfo userInfo = null;

            try {
                //Authenticate User
                userInfo = AuthenticationManager.authenticateUser(params[0]);

                //Insert User Information In Database
                DbManager.saveUserInformation(userInfo);
            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return userInfo;
        }

        @Override
        protected void onPostExecute(UserInfo user) {
            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);

            if (exception == null) {
                //OnSucess
                onSuccessAuthenticateUser(user);
            } else if (exception instanceof EODBusinessException) {
                //OnFail
                onFailLogin((EODException) exception);
            } else if (exception instanceof EODException) {
                //OnError
                onErrorLogin(exception);
            }
        }
    }

    //OnSucess
    private void onSuccessAuthenticateUser(UserInfo user) {
        if (user != null) {
            //Retry Upload AsyncTask
            handleUploadExpense();

            //Set Flag True - Async task is retried
            isAuthenticationRetried = true;
        }
    }

    //OnFail
    private void onFailLogin(EODException exception) {
        //Show Error Alert Dialog
        showErrorAlertDialog(false, null, null, null, TYPE_LOGIN);
        //showResponseErrorAlertDialog(false, null, null, null);

        //Referesh Expense List
        refreshExpenseList();
    }

    //OnError
    private void onErrorLogin(Exception exception) {
        //Show Error Alert Dialog
        showErrorAlertDialog(false, null, null, null, TYPE_LOGIN);
        //showResponseErrorAlertDialog(false, null, null, null);
        //Referesh Expense List
        refreshExpenseList();
    }

    //handle recyclerview emptyview
    private void handleVisibilityEmptyView() {
        if (expenses.size() == 0) {
            //Show Empty screen
            handleErrorOrNodataScreen(true, false, getResources().getString(R.string.claimant_text_empty_expense_list));
            relativeRootLayout.setVisibility(RelativeLayout.GONE);
        } else {
            linearErrorScreen.setVisibility(TextView.GONE);
            linearNodataScreen.setVisibility(TextView.GONE);
            relativeRootLayout.setVisibility(RelativeLayout.VISIBLE);
        }
    }

    private void showAlertDialog(boolean neutralButton, String message, String title, String okButtonTitle, int type) {
        applicationAlertDialog = new ApplicationAlertDialog().getInstance(this, neutralButton, message, title, okButtonTitle, type);
        applicationAlertDialog.show(getSupportFragmentManager(), "dialog");
    }

    private void showAlertDialog(boolean neutralButton, String message, String title, String okButtonTitle, String cancelButtonTitle, int type) {
        applicationAlertDialog = new ApplicationAlertDialog().getInstance(this, neutralButton, message, title, okButtonTitle, cancelButtonTitle, type);
        applicationAlertDialog.show(getSupportFragmentManager(), "dialog");
    }

    private void showErrorAlertDialog(boolean neutralButton, String message, String title, String okButtonTitle, int type) {
        applicationErrorAlertDialog = new ApplicationErrorAlertDialog().getInstance(this, message, title, okButtonTitle, type);
        applicationErrorAlertDialog.show(getSupportFragmentManager(), "dialog");
    }


    private void enableNetwork() {
        applicationNetworkDialogl = new ApplicationNetworkDialog().getInstance(UploadExpenseActivity.this);
        applicationNetworkDialogl.show(getSupportFragmentManager(), "dialog");
    }

    // alert dialog action listener
    @Override
    public void onPositiveButtonClick(int type) {
        switch (type) {
            case TYPE_UPLOAD:

                break;
            case TYPE_DELETE:

                //Delete Expense
                deleteExpense(expenseIdList);
                break;
        }
    }

    @Override
    public void onNegativeButtonClick() {

    }

    //error dialog action listener
    @Override
    public void onRetryButtonClick(int type) {
        handleUploadExpense();
    }

    @Override
    public void onCancelButtonClick() {

    }

}
