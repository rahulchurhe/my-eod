package com.expenseondemand.eod.screen.approval.detail.receipt;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.expenseondemand.eod.BaseFragment;
import com.expenseondemand.eod.R;
import com.expenseondemand.eod.dialogFragment.ApplicationAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationNetworkDialog;
import com.expenseondemand.eod.dialogFragment.CameraAlertDialog;
import com.expenseondemand.eod.exception.EODBusinessException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.DeviceManager;
import com.expenseondemand.eod.manager.MediaManager;
import com.expenseondemand.eod.model.approval.detail.project.ProjectDetailsItem;
import com.expenseondemand.eod.model.approval.detail.project.ReceiptItem;
import com.expenseondemand.eod.screen.approval.detail.ExpenseDetailManager;
import com.expenseondemand.eod.util.UIUtil;
import com.expenseondemand.eod.webservice.model.approval.detail.expense.receipt.ExpenseDetailsReceipt;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import static com.expenseondemand.eod.screen.claimant.create.CreateEditExpenseActivity.TYPE_CREATE;

/**
 * Created by Ramit Yadav on 16-09-2016.
 */
public class ReceiptFragment extends BaseFragment implements ReceiptAdapter.ProjectDetailInterface, CameraAlertDialog.CameraAlertDialogActionListener {
    private static String expDetailID;
    private static String expHeaderID;
    private static ReceiptItem receiptItem;
    private final String PROJECT_DETAIL_KEY = "PROJECT_DETAIL_KEY";

    private RecyclerView recyclerViewProjectDetail;
    private ArrayList<ProjectDetailsItem> projectDetailData;
    ApplicationAlertDialog applicationAlertDialog;
    public static final int TYPE_ALERT = 0;

    private String filePath;
    private File file;
    private FileOutputStream os;
    private ApplicationNetworkDialog applicationNetworkDialog;
    private ImageView imageViewReceipt;
    private TextView textViewLeft, textViewRight, textViewCount;
    private ReceiptAdapter projectDetailAdapter;
    private LinearLayout linear_noReceipt;
    private CameraAlertDialog cameraAlertDialog;


    public static ReceiptFragment getInstance(ArrayList<ProjectDetailsItem> projectDetailData, ReceiptItem receiptItemData, String expenseDetailID, String expenseHeaderID) {
        ReceiptFragment projectDetailFragment = new ReceiptFragment();
        projectDetailFragment.projectDetailData = projectDetailData;
        receiptItem = receiptItemData;
        expDetailID = expenseDetailID;
        expHeaderID = expenseHeaderID;

        return projectDetailFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.receipt_fragment, container, false);

        initializeView(rootView);

        if (savedInstanceState != null)
            projectDetailData = savedInstanceState.getParcelableArrayList(PROJECT_DETAIL_KEY);

        // populateView();

        imageViewReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (DeviceManager.isOnline()) {
                    new ReceiptAsyncTask().execute(expDetailID, expHeaderID);
                } else {
                    enableNetwork();
                }
            }
        });


        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(PROJECT_DETAIL_KEY, projectDetailData);
        super.onSaveInstanceState(outState);
    }

    private void initializeView(View rootView) {
        textViewLeft = (TextView) rootView.findViewById(R.id.textViewLeft);
        textViewRight = (TextView) rootView.findViewById(R.id.textViewRight);
        textViewCount = (TextView) rootView.findViewById(R.id.textViewCount);
        linear_noReceipt = (LinearLayout) rootView.findViewById(R.id.linear_noReceipt);
        imageViewReceipt = (ImageView) rootView.findViewById(R.id.imageViewReceipt);

        if (receiptItem != null) {
            textViewLeft.setText(receiptItem.getLabelName());

            if (Integer.parseInt(receiptItem.getCount()) > 0) {
                imageViewReceipt.setVisibility(View.VISIBLE);
                linear_noReceipt.setVisibility(View.VISIBLE);
                textViewCount.setText(receiptItem.getCount());
                textViewRight.setText(receiptItem.getValue());
            } else {
                imageViewReceipt.setVisibility(View.INVISIBLE);
                linear_noReceipt.setVisibility(View.GONE);
                textViewRight.setTextColor(getResources().getColor(R.color.dark_red));
                textViewRight.setText("No Receipt");
            }

        }
    }

    private void populateView() {
        projectDetailAdapter = new ReceiptAdapter(getActivity(), projectDetailData, this);
        recyclerViewProjectDetail.setAdapter(projectDetailAdapter);
    }

    @Override
    public void handleReceipt() {
        if (DeviceManager.isOnline()) {
            new ReceiptAsyncTask().execute(expDetailID, expHeaderID);
        } else {
            enableNetwork();
        }
    }

    @Override
    public void onPositiveCamBtnClick(int type) {

    }

    @Override
    public void onNegativeButtonClick() {

    }

    /*Receipt Async Task*/
    private class ReceiptAsyncTask extends AsyncTask<String, Void, ExpenseDetailsReceipt> {
        private Exception exception = null;
        private Dialog dialog = null;


        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(getActivity());
        }

        @Override
        protected ExpenseDetailsReceipt doInBackground(String... param) {
            ExpenseDetailsReceipt expenseDetailsReceipt = null;
            try {
                expenseDetailsReceipt = ExpenseDetailManager.getExpenseDetailReceipt(param);
            } catch (EODException e) {
                e.printStackTrace();
            }
            return expenseDetailsReceipt;
        }


        @Override
        protected void onPostExecute(ExpenseDetailsReceipt expenseDetailsReceipt) {
            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);

            if (exception == null) {
                //OnSucess
                onSuccessGetExpenseDetail(expenseDetailsReceipt);
            } else if (exception instanceof EODBusinessException) {
                //OnFail
                onFailGetExpenseDetail((EODException) exception);
            } else if (exception instanceof EODException) {
                //OnError
                onErrorGetExpenseDetail(exception);
            }
        }


    }

    private void onErrorGetExpenseDetail(Exception exception) {
        showAlertDialog(getResources().getString(R.string.text_error));
    }

    private void onFailGetExpenseDetail(EODException exception) {
        showAlertDialog(getResources().getString(R.string.text_error));
    }

    private void onSuccessGetExpenseDetail(ExpenseDetailsReceipt expenseDetailsReceipt) {
        //Download Receipts
        try {
            if (expenseDetailsReceipt != null) {
                String receiptName = expenseDetailsReceipt.getDescription();
                String base64ReceiptString = expenseDetailsReceipt.getDocument();

                if (receiptName.contains(".pdf")) {
                    file = MediaManager.getReceiptPathFormPermanentStorage(receiptName);
                    try {
                        os = new FileOutputStream(file, false);
                        os.write(Base64.decode(base64ReceiptString, Base64.NO_WRAP));
                        os.flush();
                        os.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    file = MediaManager.getReceiptPathFormPermanentStorage(receiptName);
                    Bitmap bitmap = MediaManager.getBitmapFromString(base64ReceiptString, CachingManager.getAppContext());
                    MediaManager.writeBitmapTofFile(file, bitmap);
                }

                //Create filepath of downloaded receipt
                filePath = file.getAbsolutePath();

                //Show Receipt popup dialog
                UIUtil.showReceiptDialog(filePath, receiptName, getActivity());

            }
        } catch (Exception e) {
            //alert dialog for handle exception on 4.2.2 device
            showCameraAlertDialog(false, getString(R.string.text_camera_not_working), getString(R.string.text_camera_not_working_Alert), getString(R.string.button_ok), getString(R.string.button_no), TYPE_CREATE);
        }
    }

    //API Response Error
    private void showAlertDialog(String msg) {
        applicationAlertDialog = new ApplicationAlertDialog().getInstance((ApplicationAlertDialog.AlertDialogActionListener) getActivity(), true, getString(R.string.text_error_processing), msg, getString(R.string.button_ok), TYPE_ALERT);
        applicationAlertDialog.show(getFragmentManager(), "dialog");
    }

    //Network enable dialog
    void enableNetwork() {
        applicationNetworkDialog = new ApplicationNetworkDialog().getInstance(getActivity());
        applicationNetworkDialog.show(getFragmentManager(), "dialog");
    }

    private void showCameraAlertDialog(boolean neutralButton, String message, String title, String okButtonTitle, String cancelButtonTitle, int type) {
        cameraAlertDialog = new CameraAlertDialog().getInstance(this, neutralButton, message, title, okButtonTitle, cancelButtonTitle, type);
        cameraAlertDialog.show(getFragmentManager(), "dialog");
    }

}
