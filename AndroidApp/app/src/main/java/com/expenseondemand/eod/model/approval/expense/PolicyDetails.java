package com.expenseondemand.eod.model.approval.expense;

import android.os.Parcel;
import android.os.Parcelable;

import com.expenseondemand.eod.model.approval.detail.dynamic.DynamicDetail;
import com.expenseondemand.eod.webservice.model.approval.item.DuplicateItemsModel;

import java.util.ArrayList;

/**
 * Created by ITDIVISION\maximess087 on 16/1/17.
 */

public class PolicyDetails implements Parcelable {
    /*Y - If expense date overdue
N - Date lies in number of days*/
    private boolean LateSubmission;//
    private String NormalSubmissionDays;
    private boolean IsResubmitted;//
    private boolean IsEscalated;//
    private boolean IsDailyLimitBreached;//
    private String DailyLimitAmount;
    private boolean IsMaxSpentBreach;//
    private String MaxSpentAmount;
    private boolean PreApprovalBreach;
    private String PreAprovalAmount;
    private boolean IsCaRequired;//
    private ArrayList<Notes>  Notes;

    public PolicyDetails(){}

    protected PolicyDetails(Parcel in) {
        LateSubmission = in.readByte() != 0;
        NormalSubmissionDays = in.readString();
        IsResubmitted = in.readByte() != 0;
        IsEscalated = in.readByte() != 0;
        IsDailyLimitBreached = in.readByte() != 0;
        DailyLimitAmount = in.readString();
        IsMaxSpentBreach = in.readByte() != 0;
        MaxSpentAmount = in.readString();
        PreApprovalBreach = in.readByte() != 0;
        PreAprovalAmount = in.readString();
        IsCaRequired= in.readByte() != 0;
        Notes = in.readArrayList(Notes.class.getClassLoader());
    }

    public static final Creator<PolicyDetails> CREATOR = new Creator<PolicyDetails>() {
        @Override
        public PolicyDetails createFromParcel(Parcel in) {
            return new PolicyDetails(in);
        }

        @Override
        public PolicyDetails[] newArray(int size) {
            return new PolicyDetails[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) (LateSubmission ? 1 : 0));
        parcel.writeString(NormalSubmissionDays);
        parcel.writeByte((byte) (IsResubmitted ? 1 : 0));
        parcel.writeByte((byte) (IsEscalated ? 1 : 0));
        parcel.writeByte((byte) (IsDailyLimitBreached ? 1 : 0));
        parcel.writeString(DailyLimitAmount);
        parcel.writeByte((byte) (IsMaxSpentBreach ? 1 : 0));
        parcel.writeString(MaxSpentAmount);
        parcel.writeByte((byte) (PreApprovalBreach ? 1 : 0));
        parcel.writeString(PreAprovalAmount);
        parcel.writeByte((byte) (IsCaRequired ? 1 : 0));
        parcel.writeList(Notes);
    }

    public ArrayList<com.expenseondemand.eod.model.approval.expense.Notes> getNotes() {
        return Notes;
    }

    public void setNotes(ArrayList<com.expenseondemand.eod.model.approval.expense.Notes> notes) {
        Notes = notes;
    }

    public String getNormalSubmissionDays() {
        return NormalSubmissionDays;
    }

    public void setNormalSubmissionDays(String normalSubmissionDays) {
        NormalSubmissionDays = normalSubmissionDays;
    }

    public boolean isResubmitted() {
        return IsResubmitted;
    }

    public void setResubmitted(String resubmitted) {
        this.IsResubmitted = (resubmitted != null) && (resubmitted.equals("R"));
    }

    public boolean isLateSubmission() {
        return LateSubmission;
    }

    public void setLateSubmission(String lateSubmission) {
        this.LateSubmission = (lateSubmission != null) && (lateSubmission.equals("Y"));
    }

    public boolean isEscalated() {
        return IsEscalated;
    }

    public void setEscalated(String escalated) {

        this.IsEscalated = (escalated != null) && (escalated.equals("Y"));
    }

    public boolean isDailyLimitBreached() {
        return IsDailyLimitBreached;
    }

    public void setDailyLimitBreached(String dailyLimitBreached) {
        this.IsDailyLimitBreached = (dailyLimitBreached != null) && (dailyLimitBreached.equals("Y"));
    }

    public String getDailyLimitAmount() {
        return DailyLimitAmount;
    }

    public void setDailyLimitAmount(String dailyLimitAmount) {
        DailyLimitAmount = dailyLimitAmount;
    }

    public boolean isMaxSpentBreach() {
        return IsMaxSpentBreach;
    }

    public void setMaxSpentBreach(String maxSpentBreach) {
        this.IsMaxSpentBreach = (maxSpentBreach != null) && (maxSpentBreach.equals("Y"));
    }

    public String getMaxSpentAmount() {
        return MaxSpentAmount;
    }

    public void setMaxSpentAmount(String maxSpentAmount) {
        MaxSpentAmount = maxSpentAmount;
    }

    public boolean isPreApprovalBreach() {
        return PreApprovalBreach;
    }

    public void setPreApprovalBreach(String preApprovalBreach) {
        this.PreApprovalBreach = (preApprovalBreach != null) && (preApprovalBreach.equals("Y"));
    }

    public String getPreAprovalAmount() {
        return PreAprovalAmount;
    }

    public void setPreAprovalAmount(String preAprovalAmount) {
        PreAprovalAmount = preAprovalAmount;
    }

    public boolean isCaRequired() {
        return IsCaRequired;
    }

    public void setCaRequired(String caRequired) {
        this.IsCaRequired = (caRequired != null) && (caRequired.equals("Y"));
    }
}
