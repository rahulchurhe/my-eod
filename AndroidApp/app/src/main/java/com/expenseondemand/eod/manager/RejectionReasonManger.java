package com.expenseondemand.eod.manager;

import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.webservice.model.RejectionReasonModel.RejectionReasonResponseList;
import com.expenseondemand.eod.webservice.model.RejectionReasonModel.RejectionReasonModel;
import com.expenseondemand.eod.webservice.model.http.HTTPData;

import java.util.ArrayList;

/**
 * Created by Nandani Gupta on 2016-10-17.
 */
public class RejectionReasonManger
{
    //Get Rejection Reason
    public static ArrayList<RejectionReasonModel> getRejectionReasonList() throws EODException
    {
        RejectionReasonResponseList response = null;

        //Prepare http request
        HTTPData httpData = new HTTPData();
        httpData.setType(AppConstant.HTTP_METHOD_GET);

         String url = prepareRejectionReasonUrl();


        //Call to Web Service
        String jsonResponseString = (String) WebServiceManager.callWebService(AppConstant.BASE_URL, url, httpData);

        System.out.println("json Response--"+jsonResponseString);

        //Prepare Response Object
        response = (RejectionReasonResponseList) ParseManager.prepareWebServiceResponseObject(RejectionReasonResponseList.class, jsonResponseString);

        //Process Response Status
        WebServiceManager.processResponseStatus(response);

        //Prepare Rejection Reason List
        ArrayList<RejectionReasonModel> rejectionReasonResponseLists = response.getData();

        return rejectionReasonResponseLists;
    }


    // prepare url
      private static String prepareRejectionReasonUrl()
      {
          String url = null;

          //get UserInfo
          UserInfo userInfo = CachingManager.getUserInfo();

          //get compony Id
          String companyId = userInfo.getCompanyId();

          url = AppConstant.REJECTION_REASONS_URL  + "/" + companyId ;

          return url;
      }



}
