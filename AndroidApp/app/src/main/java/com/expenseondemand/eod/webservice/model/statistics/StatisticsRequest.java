package com.expenseondemand.eod.webservice.model.statistics;

import com.expenseondemand.eod.webservice.model.MasterRequest;

/**
 * Created by Ramit Yadav on 14-10-2016.
 */

public class StatisticsRequest extends MasterRequest
{
    private String branchId;
    private String companyId;
    private String costCenterId;
    private String id;
    private String departmentId;
    private String editionCode;

    public String getBranchId()
    {
        return branchId;
    }

    public void setBranchId(String branchId)
    {
        this.branchId = branchId;
    }

    public String getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(String companyId)
    {
        this.companyId = companyId;
    }

    public String getCostCenterId() {
        return costCenterId;
    }

    public void setCostCenterId(String costCenterId)
    {
        this.costCenterId = costCenterId;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getDepartmentId()
    {
        return departmentId;
    }

    public void setDepartmentId(String departmentId)
    {
        this.departmentId = departmentId;
    }

    public String getEditionCode() {
        return editionCode;
    }

    public void setEditionCode(String editionCode)
    {
        this.editionCode = editionCode;
    }
}
