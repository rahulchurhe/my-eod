package com.expenseondemand.eod.screen.approval.expense_rejection_reason;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.webservice.model.RejectionReasonModel.RejectionReasonModel;

import java.util.ArrayList;

/**
 * Created by Nandani Gupta on 2016-09-13.
 */
public class RejectionReasonListAdapter extends RecyclerView.Adapter<RejectionReasonListAdapter.MyViewHolder> {

    private Context activityContext;
    private boolean onBind;
    private  ArrayList<RejectionReasonModel> rejectionReasonList;
    boolean[] positionTrack;
    RejectionReasonDialogClickListener rejectionReasonDialogClickListener;
    public class MyViewHolder extends RecyclerView.ViewHolder
    {

          TextView textViewReason;

        public MyViewHolder(View view)
        {
            super(view);

            textViewReason = (TextView)view.findViewById(R.id.textViewReason);

        }
    }

    public interface RejectionReasonDialogClickListener
    {
         void handleReasonClick(int position);

      }

    public RejectionReasonListAdapter(Context activityContext, ArrayList<RejectionReasonModel> rejectionReasonList, RejectionReasonDialogClickListener rejectionReasonDialogClickListener)
    {
        this.activityContext = activityContext;
        this.rejectionReasonList = rejectionReasonList;
        this.rejectionReasonDialogClickListener =  rejectionReasonDialogClickListener;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rejection_reason_dialog_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder viewHolder, int position)
    {
        if(positionTrack == null)
        {
            positionTrack = new boolean[rejectionReasonList.size()];
        }
          viewHolder.textViewReason.setText(rejectionReasonList.get(position).getName());
          viewHolder.textViewReason.setTag(position);
          viewHolder.textViewReason.setOnClickListener(new RejectionReasonListClickListener());

            if (positionTrack[position])
            {
                viewHolder.textViewReason.setBackgroundResource(R.color.create_expenses_bg_color_pressed);
            } else
            {
                viewHolder.textViewReason.setBackgroundResource(R.color.white);
            }

    }

    @Override
    public int getItemCount() {
        return rejectionReasonList.size();
    }


    private class RejectionReasonListClickListener implements View.OnClickListener
    {


        @Override
        public void onClick(View view)
        {


            prepareSelectListFalse();

            int position = (int) view.getTag();

            positionTrack[position] = true;

            rejectionReasonDialogClickListener.handleReasonClick(position);


            //set notify changed for update allyDataSetChanged();
            notifyDataSetChanged();


        }
    }


  public void prepareSelectListFalse()
  {
      try{
          for(int i=0;i< positionTrack.length;i++)
          {
              positionTrack[i] = false;
          }
      }catch (Exception e){
          e.printStackTrace();
      }

  }

}