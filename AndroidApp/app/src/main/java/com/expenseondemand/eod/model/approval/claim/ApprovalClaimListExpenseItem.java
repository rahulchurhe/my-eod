package com.expenseondemand.eod.model.approval.claim;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 12-09-2016.
 */
public class ApprovalClaimListExpenseItem
{
    private String claimantName;
    private String claimType;
    private String claimId;
    private String noOfItems;
    private String recordCount;
    private String employeeId;
    private String claimDate;
    private String rowId;
    private String resourceId;
    private String amount;
    private String pendingSince;
    private boolean rejected;
    private boolean resubmitted;
    private boolean breachedItem;
    private boolean status;
    private boolean breachedClaim;
    private boolean dailyLimitBreached;
    //private boolean preApprovalBreach;
    private String preApprovalBreach;
    private String preApprovalAmount;
    private String approverNote;
    private String dailyCapAmount;
    private boolean isMaxSpendBreach;
    private String maxSpendAmount;
    private String approverId;
    private ArrayList<PreApprovedDetails> PreApprovedDetails;

    public ArrayList<com.expenseondemand.eod.model.approval.claim.PreApprovedDetails> getPreApprovedDetails() {
        return PreApprovedDetails;
    }

    public void setPreApprovedDetails(ArrayList<com.expenseondemand.eod.model.approval.claim.PreApprovedDetails> preApprovedDetails) {
        PreApprovedDetails = preApprovedDetails;
    }

    public String getPreApprovalBreach() {
        return preApprovalBreach;
    }

    public void setPreApprovalBreach(String preApprovalBreach) {
        this.preApprovalBreach = preApprovalBreach;
    }

    public String getClaimantName()
    {
        return claimantName;
    }

    public void setClaimantName(String claimantName)
    {
        this.claimantName = claimantName;
    }

    public String getClaimType()
    {
        return claimType;
    }

    public void setClaimType(String claimType)
    {
        this.claimType = claimType;
    }

    public String getClaimId()
    {
        return claimId;
    }

    public void setClaimId(String claimId)
    {
        this.claimId = claimId;
    }

    public String getNoOfItems()
    {
        return noOfItems;
    }

    public void setNoOfItems(String noOfItems)
    {
        this.noOfItems = noOfItems;
    }

    public String getRecordCount()
    {
        return recordCount;
    }

    public void setRecordCount(String recordCount)
    {
        this.recordCount = recordCount;
    }

    public String getEmployeeId()
    {
        return employeeId;
    }

    public void setEmployeeId(String employeeId)
    {
        this.employeeId = employeeId;
    }

    public String getClaimDate()
    {
        return claimDate;
    }

    public void setClaimDate(String claimDate)
    {
        this.claimDate = claimDate;
    }

    public String getRowId()
    {
        return rowId;
    }

    public void setRowId(String rowId)
    {
        this.rowId = rowId;
    }

    public String getResourceId()
    {
        return resourceId;
    }

    public void setResourceId(String resourceId)
    {
        this.resourceId = resourceId;
    }

    public String getAmount()
    {
        return amount;
    }

    public void setAmount(String amount)
    {
        this.amount = amount;
    }

    public String getPendingSince()
    {
        return pendingSince;
    }

    public void setPendingSince(String pendingSince)
    {
        this.pendingSince = pendingSince;
    }

    public boolean isRejected()
    {
        return rejected;
    }

    public void setRejected(boolean rejected)
    {
        this.rejected = rejected;
    }

    public void setRejected(String rejected)
    {
        this.rejected = (rejected != null) && (rejected.equals("R"));
    }

    public boolean isResubmitted()
    {
        return resubmitted;
    }

    public void setResubmitted(boolean resubmitted)
    {
        this.resubmitted = resubmitted;
    }

    public void setResubmitted(String resubmitted)
    {
        this.resubmitted = (resubmitted != null) && resubmitted.equals("Y");
    }

    public boolean isBreachedItem()
    {
        return breachedItem;
    }

    public void setBreachedItem(boolean breachedItem)
    {
        this.breachedItem = breachedItem;
    }

    public void setBreachedItem(String breachedItem)
    {
        this.breachedItem = (breachedItem != null) && breachedItem.equals("1");
    }

    public boolean isStatus()
    {
        return status;
    }

    public void setStatus(boolean status)
    {
        this.status = status;
    }

    public void setStatus(String status)
    {
        this.status = (status != null) && status.equals("B");
    }

    public boolean isBreachedClaim()
    {
        return breachedClaim;
    }

    public void setBreachedClaim(boolean breachedClaim)
    {
        this.breachedClaim = breachedClaim;
    }

    public void setBreachedClaim(String breachedClaim)
    {
        this.breachedClaim = (breachedClaim != null) && breachedClaim.equals("1");
    }

    public boolean isDailyLimitBreached()
    {
        return dailyLimitBreached;
    }

    public void setDailyLimitBreached(boolean dailyLimitBreached)
    {
        this.dailyLimitBreached = dailyLimitBreached;
    }

    public void setDailyLimitBreached(String dailyLimitBreached)
    {
        this.dailyLimitBreached = (dailyLimitBreached != null) && dailyLimitBreached.equals("Y");
    }

    /*public boolean isPreApprovalBreach()
    {
        return preApprovalBreach;
    }

    public void setPreApprovalBreach(boolean preApprovalBreach)
    {
        this.preApprovalBreach = preApprovalBreach;
    }

    public void setPreApprovalBreach(String preApprovalBreach)
    {
        this.preApprovalBreach = (preApprovalBreach != null) && preApprovalBreach.equals("Y");
    }*/

    public String getPreApprovalAmount()
    {
        return preApprovalAmount;
    }

    public void setPreApprovalAmount(String preApprovalAmount)
    {
        this.preApprovalAmount = preApprovalAmount;
    }

    public String getApproverNote()
    {
        return approverNote;
    }

    public void setApproverNote(String approverNote)
    {
        this.approverNote = approverNote;
    }

    public String getDailyCapAmount()
    {
        return dailyCapAmount;
    }

    public void setDailyCapAmount(String dailyCapAmount)
    {
        this.dailyCapAmount = dailyCapAmount;
    }

    public boolean isMaxSpendBreach()
    {
        return isMaxSpendBreach;
    }

    public void setMaxSpendBreach(boolean maxSpendBreach)
    {
        isMaxSpendBreach = maxSpendBreach;
    }

    public void setMaxSpendBreach(String maxSpendBreach)
    {
        isMaxSpendBreach = (maxSpendBreach != null) && maxSpendBreach.equals("Y");
    }

    public String getMaxSpendAmount()
    {
        return maxSpendAmount;
    }

    public void setMaxSpendAmount(String maxSpendAmount)
    {
        this.maxSpendAmount = maxSpendAmount;
    }

    public String getApproverId()
    {
        return approverId;
    }

    public void setApproverId(String approverId)
    {
        this.approverId = approverId;
    }
}
