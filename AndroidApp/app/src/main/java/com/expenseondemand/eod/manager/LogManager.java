package com.expenseondemand.eod.manager;

public class LogManager
{
	//Singleton Class
	public static LogManager logManager;
	
	//Constructor
	private LogManager()
	{
		
	}
	
	//Get Instance
	public static LogManager getInstance()
	{
		if(logManager == null)
		{
			logManager = new LogManager();
		}
		
		return logManager;
	}
	
	public void addLog(int exceptionCode, String exceptionMessage, Exception exceptionObject)
	{
		printLog(exceptionCode, exceptionMessage, exceptionObject);
	}
	
	private void printLog(int exceptionCode, String exceptionMessage, Exception exceptionObject)
	{
		StringBuilder builder = new StringBuilder();
		
		builder.append("\nException Occurred : "+exceptionCode);
		builder.append("\nMessage : "+exceptionMessage+"\n");
		
		System.out.println(builder);
		
		exceptionObject.printStackTrace();
	}
}
