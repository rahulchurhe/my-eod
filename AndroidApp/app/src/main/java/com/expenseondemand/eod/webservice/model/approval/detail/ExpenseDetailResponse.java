package com.expenseondemand.eod.webservice.model.approval.detail;

import com.expenseondemand.eod.webservice.model.MasterResponse;

/**
 * Created by Ramit Yadav on 20-10-2016.
 */

public class ExpenseDetailResponse extends MasterResponse
{
    private ExpenseDetailData data;

    public ExpenseDetailData getData()
    {
        return data;
    }

    public void setData(ExpenseDetailData data)
    {
        this.data = data;
    }
}
