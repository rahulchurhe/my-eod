package com.expenseondemand.eod.screen.approval.detail.notes;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.expenseondemand.eod.BaseFragment;
import com.expenseondemand.eod.R;
import com.expenseondemand.eod.model.approval.detail.notes.Notes;
import com.expenseondemand.eod.model.approval.detail.project.PreApprovedDetailsItem;
import com.expenseondemand.eod.util.AppUtil;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 16-09-2016.
 */
public class NotesFragment extends BaseFragment implements View.OnClickListener {
    private static ArrayList<com.expenseondemand.eod.model.approval.expense.Notes> policyBreechNote;
    private Notes notesData;
    private TextView textViewNotes;
    private TextView textViewCCNotes;
    private TextView textViewCCNotesValue;
    private TextView textViewMobileNotes;
    private TextView textViewMobileNotesValue;
    private LinearLayout linearLayoutReason;
    private LinearLayout linearLayoutExpandable;
    private ImageView imageViewPlus;
    private boolean isExpandedReason = false;

    public static NotesFragment getInstance(Notes notesData, ArrayList<com.expenseondemand.eod.model.approval.expense.Notes> policyBreechNoteArray) {
        NotesFragment projectDetailFragment = new NotesFragment();
        projectDetailFragment.notesData = notesData;
        policyBreechNote = policyBreechNoteArray;

        return projectDetailFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.notes_fragment, container, false);
        initializeView(rootView);

        populateView();

        return rootView;
    }

    private void initializeView(View rootView) {
        textViewNotes = (TextView) rootView.findViewById(R.id.textViewNotes);
        textViewCCNotes = (TextView) rootView.findViewById(R.id.textViewCCNotes);
        textViewCCNotesValue = (TextView) rootView.findViewById(R.id.textViewCCNotesValue);
        textViewMobileNotes = (TextView) rootView.findViewById(R.id.textViewMobileNotes);
        textViewMobileNotesValue = (TextView) rootView.findViewById(R.id.textViewMobileNotesValue);
        linearLayoutReason = (LinearLayout) rootView.findViewById(R.id.linearLayoutReason);
        linearLayoutExpandable = (LinearLayout) rootView.findViewById(R.id.linearLayoutExpandable);
        imageViewPlus = (ImageView) rootView.findViewById(R.id.imageViewPlus);


        imageViewPlus.setOnClickListener(this);
    }

    private void populateView() {
        String notes = notesData.getEnteredNotes();
        if (notes != null && !notes.equals("")) {
            textViewNotes.setText(notes);
        } else {
            textViewNotes.setVisibility(View.GONE);
        }


        String ccNotes = notesData.getCcNotes();
        if (ccNotes != null && !ccNotes.equals("")) {
            textViewCCNotes.setVisibility(View.VISIBLE);
            textViewCCNotesValue.setVisibility(View.VISIBLE);
            textViewCCNotesValue.setText(ccNotes);
        } else {
            textViewCCNotes.setVisibility(View.GONE);
            textViewCCNotesValue.setVisibility(View.GONE);
        }

        String mobileNotes = notesData.getMobileNotes();
        if (mobileNotes != null && !mobileNotes.equals("")) {
            textViewMobileNotes.setVisibility(View.VISIBLE);
            textViewMobileNotesValue.setVisibility(View.VISIBLE);
            textViewMobileNotesValue.setText(mobileNotes);
        } else {
            textViewMobileNotes.setVisibility(View.GONE);
            textViewMobileNotesValue.setVisibility(View.GONE);
        }


        policyBreechNote();
    }


    private void policyBreechNote() {

        if (!AppUtil.isCollectionEmpty(policyBreechNote)) {
            linearLayoutReason.setVisibility(View.VISIBLE);
            LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            for (com.expenseondemand.eod.model.approval.expense.Notes note : policyBreechNote) {
                View view = layoutInflater.inflate(R.layout.policy_violation_reason_layout, null);
                TextView textViewKey = (TextView) view.findViewById(R.id.textViewKey);
                TextView textViewValue = (TextView) view.findViewById(R.id.textViewValue);

                String reason = getResources().getString(R.string.approval_expense_detail_policy_violation_notes, note.getReason(), note.getName());
                textViewKey.setText(reason);
                textViewValue.setText(note.getNotes());

                linearLayoutExpandable.addView(view);
            }
        } else
            linearLayoutReason.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageViewPlus: {
                handlePlusMinusClick();
                break;
            }
        }
    }

    private void handlePlusMinusClick() {
        if (isExpandedReason) {
            linearLayoutExpandable.setVisibility(View.VISIBLE);
            imageViewPlus.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.approval_expense_detail_policy_violation_minus_selector));
        } else {
            linearLayoutExpandable.setVisibility(View.GONE);
            imageViewPlus.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.approval_expense_detail_policy_violation_plus_selector));
        }
        isExpandedReason = !isExpandedReason;
    }

}
