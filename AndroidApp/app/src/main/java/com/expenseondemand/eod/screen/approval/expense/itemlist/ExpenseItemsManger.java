package com.expenseondemand.eod.screen.approval.expense.itemlist;

import android.util.Log;

import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.ParseManager;
import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.manager.WebServiceManager;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.model.approval.expense.DuplicateItemModel;
import com.expenseondemand.eod.model.approval.expense.PolicyBreachModel;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.webservice.model.approval.item.DuplicateItemResponseModel;
import com.expenseondemand.eod.webservice.model.approval.item.ItemResponseData;
import com.expenseondemand.eod.webservice.model.approval.item.ItemsRequest;
import com.expenseondemand.eod.webservice.model.approval.item.ItemsResponse;
import com.expenseondemand.eod.webservice.model.approval.item.ItemsResponseModel;
import com.expenseondemand.eod.webservice.model.approval.item.PolicyBreechResponseModel;
import com.expenseondemand.eod.webservice.model.http.Body;
import com.expenseondemand.eod.webservice.model.http.HTTPData;

import java.util.ArrayList;

/**
 * Created by Nandani Gupta on 2016-10-20.
 */
public class ExpenseItemsManger {
    /*Expense List*/
    //Get Expense items
    public static ArrayList<ExpenseItemsModel> getExpenseItems(String ClaimId,int count,int pageNo) throws EODException {
        ItemsResponse response = null;

        //Prepare Request Object
        ItemsRequest request = prepareExpenseItemRequest(ClaimId,count,pageNo);

        //Prepare JSON Request String
        String jsonRequestString = ParseManager.prepareJsonRequest(request);
        Log.e("jsonRequestString","Req>>"+jsonRequestString);

        //Prepare http request
        HTTPData httpData = new HTTPData();
        httpData.setType(AppConstant.HTTP_METHOD_POST);
        Body body = new Body();
        body.setJsonRequestString(jsonRequestString);
        httpData.setBody(body);

        //Call to Web Service
        String jsonResponseString = (String) WebServiceManager.callWebService(AppConstant.BASE_URL, AppConstant.EXPENSE_ITEMS, httpData);

        System.out.println("json Response--" + jsonResponseString);

        //Prepare Response Object
        response = (ItemsResponse) ParseManager.prepareWebServiceResponseObject(ItemsResponse.class, jsonResponseString);

        //Process Response Status
        WebServiceManager.processResponseStatus(response);

        //Prepare Categories List
        ArrayList<ExpenseItemsModel> approverExpenseItemsList = prepareExpenseItemList(response.getData());

        return approverExpenseItemsList;
    }


    //Prepare Expense item Request
    private static ItemsRequest prepareExpenseItemRequest(String ClaimId,int count,int pageNo) {
        // get UserInfo
        UserInfo userInfo = CachingManager.getUserInfo();

        //Create
        ItemsRequest itemsRequest = new ItemsRequest();

        itemsRequest.setApproverCompanyId(userInfo.getCompanyId());
        //itemsRequest.setApproverFlag(userInfo.isFinance() ? "1" : "0");
        itemsRequest.setApproverFlag(AppConstant.IS_APPROVER_FLAG);
        itemsRequest.setApproverId(userInfo.getResourceId());
        itemsRequest.setClaimId(ClaimId);
        itemsRequest.setNoOfRecords(count);
        itemsRequest.setPageNo(pageNo);

        return itemsRequest;
    }

    //ArrayList<ItemsResponseModel> approverExpenseItemsResponsesList
    //Prepare Expense item Response
    private static ArrayList<ExpenseItemsModel> prepareExpenseItemList(ItemResponseData approverExpenseItemsResponsesList) {

        ArrayList<ExpenseItemsModel> approverExpenseItemsList = null;

        if (approverExpenseItemsResponsesList!=null) {
            if(approverExpenseItemsResponsesList.getHasMorePage().equals("Y")){
                SharePreference.putBoolean(CachingManager.getAppContext(),AppConstant.LOAD_MORE_EXPENSES,true);
            }else{
                SharePreference.putBoolean(CachingManager.getAppContext(),AppConstant.LOAD_MORE_EXPENSES,false);
            }
            //New List
            approverExpenseItemsList = new ArrayList<ExpenseItemsModel>();

            for (ItemsResponseModel expenseItemsResponseModel : approverExpenseItemsResponsesList.getData()) {
                //New Expense Category
                ExpenseItemsModel expenseItemsModel = new ExpenseItemsModel();


                expenseItemsModel.setRowId(expenseItemsResponseModel.getRowId());
                expenseItemsModel.setBaseCategoryId(expenseItemsResponseModel.getBaseCategoryId());    // Row Id
                expenseItemsModel.setReferenceNumber(expenseItemsResponseModel.getReferenceNumber());           // Reference Number
                expenseItemsModel.setDateExpense(expenseItemsResponseModel.getDateExpense());                   // Date Expense
                expenseItemsModel.setCustomerName(expenseItemsResponseModel.getCustomerName());                 // Customer  Name
                expenseItemsModel.setProjectName(expenseItemsResponseModel.getProjectName());                   // Project Name
                expenseItemsModel.setExpenseCategory(expenseItemsResponseModel.getExpenseCategory());           // Expense Category
                expenseItemsModel.setBillable(expenseItemsResponseModel.getBillable());                         // Billable
                expenseItemsModel.setPaymentTypeName(expenseItemsResponseModel.getPaymentTypeName());           // Payment Type Name
                expenseItemsModel.setCurrencyCode(expenseItemsResponseModel.getCurrencyCode());                 // Currency Code
                expenseItemsModel.setExpenseAmount(expenseItemsResponseModel.getExpenseAmount());               // Expense Amount
                expenseItemsModel.setAmount(expenseItemsResponseModel.getAmount());                             // Amount
                expenseItemsModel.setExpenseDetailID(expenseItemsResponseModel.getExpenseDetailID());           // Expense Detail Id
                expenseItemsModel.setExpenseHeaderID(expenseItemsResponseModel.getExpenseHeaderID());           // Expense Header ID
                expenseItemsModel.setItemizationID(expenseItemsResponseModel.getItemizationID());               // Expense Itemization ID
                expenseItemsModel.setRejected(expenseItemsResponseModel.getRejected());                         // Rejected flag
                expenseItemsModel.setPolicyBreech(preparePolicyBreech(expenseItemsResponseModel.getPolicyBreech())); // Policy Breech List
                expenseItemsModel.setNotes(expenseItemsResponseModel.getNotes());                               // Notes
                expenseItemsModel.setEscalationflag(expenseItemsResponseModel.getEscalationflag());             // Escalation flag
                expenseItemsModel.setDuplicateFlag(expenseItemsResponseModel.getDuplicateFlag());// Duplicate flag
                expenseItemsModel.setDuplicateItems(expenseItemsResponseModel.getDuplicateItems());// Duplicate Expenses
                expenseItemsModel.setReceipt(expenseItemsResponseModel.getReceipt());
                expenseItemsModel.setReceiptCount(expenseItemsResponseModel.getReceiptCount());
                approverExpenseItemsList.add(expenseItemsModel);


            }
        }

        return approverExpenseItemsList;

    }
    private static PolicyBreachModel preparePolicyBreech(PolicyBreechResponseModel policyBreechResponseModel) {
        //Policy Breech
        PolicyBreachModel policyBreachModel = new PolicyBreachModel();

        if (policyBreechResponseModel != null) {
            policyBreachModel.setIsExpenseLimit(policyBreechResponseModel.getIsExpenseLimit());
            policyBreachModel.setExpenseDetailId(policyBreechResponseModel.getExpenseDetailId());
            policyBreachModel.setRowId(policyBreechResponseModel.getRowId());
            policyBreachModel.setIsDailyCap(policyBreechResponseModel.getIsDailyCap());
            policyBreachModel.setIsMaxSpendBreach(policyBreechResponseModel.getIsMaxSpendBreach());
            policyBreachModel.setIsPreApproval(policyBreechResponseModel.getIsPreApproval());
            policyBreachModel.setAmountSetLimit(policyBreechResponseModel.getAmountSetLimmit());
            policyBreachModel.setAmountBreach(policyBreechResponseModel.getAmountBreach());
            policyBreachModel.setStatus(policyBreechResponseModel.getStatus());
            policyBreachModel.setExpenseLimit(policyBreechResponseModel.getExpenseLimit());
            policyBreachModel.setMaxSpendamt(policyBreechResponseModel.getMaxSpendamt());
            policyBreachModel.setDailCapAmt(policyBreechResponseModel.getDailCapAmt());
            policyBreachModel.setNotes(policyBreechResponseModel.getNotes());


        }

        return policyBreachModel;
    }

    /**/
}