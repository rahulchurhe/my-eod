package com.expenseondemand.eod.exception;

public class EODAppException extends EODException
{
	private static final long serialVersionUID = 1L;

	public EODAppException() 
	{
	}
	
	//Constructor
	public EODAppException(int exceptionCode)
	{
		super(exceptionCode, "");
	}
	
	//Constructor
	public EODAppException(int exceptionCode, String exceptionMessage)
	{
		super(exceptionCode, exceptionMessage);
	}
}
