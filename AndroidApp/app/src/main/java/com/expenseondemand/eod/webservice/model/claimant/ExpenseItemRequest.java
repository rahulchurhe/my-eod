package com.expenseondemand.eod.webservice.model.claimant;

import com.expenseondemand.eod.webservice.model.MasterRequest;

public class ExpenseItemRequest extends MasterRequest
{
	private String clientRefId;
	private double amount;
	private String currencyCode;
	private String ExpenseDate; //E.g "yyyymmdd" - "20140610"
	private int expenseCategoryId;
	private String hasReceipt;
	private String notes;
	private Receipt receipt;

	public String getExpenseDate() {
		return ExpenseDate;
	}

	public void setExpenseDate(String expenseDate) {
		ExpenseDate = expenseDate;
	}

	private int HasReceipt;
	
	public String getClientRefId() 
	{
		return clientRefId;
	}
	
	public void setClientRefId(String clientRefId) 
	{
		this.clientRefId = clientRefId;
	}
	
	public double getAmount() 
	{
		return amount;
	}
	
	public void setAmount(double amount) 
	{
		this.amount = amount;
	}
	
	public String getCurrencyCode() 
	{
		return currencyCode;
	}
	
	public void setCurrencyCode(String currencyCode) 
	{
		this.currencyCode = currencyCode;
	}
	
	/*public String getExpenesDate()
	{
		return expenesDate;
	}
	
	public void setExpenesDate(String expenesDate) 
	{
		this.expenesDate = expenesDate;
	}*/
	
	public int getExpenseCategoryId() 
	{
		return expenseCategoryId;
	}
	
	public void setExpenseCategoryId(int expenseCategoryId)
	{
		this.expenseCategoryId = expenseCategoryId;
	}
	
//	public String isHasReceipt()
//	{
//		return hasReceipt;
//	}
//
//	public void setHasReceipt(String hasReceipt)
//	{
//		this.hasReceipt = hasReceipt;
//	}
	
	public String getNotes() 
	{
		return notes;
	}
	
	public void setNotes(String notes)
	{
		this.notes = notes;
	}
	
//	public Receipt getReceipt()
//	{
//		return receipt;
//	}
//
//	public void setReceipt(Receipt receipt)
//	{
//		this.receipt = receipt;
//	}
	public int getHasReceipt() {
		return HasReceipt;
	}

	public void setHasReceipt(int hasReceipt) {
		HasReceipt = hasReceipt;
	}
}
