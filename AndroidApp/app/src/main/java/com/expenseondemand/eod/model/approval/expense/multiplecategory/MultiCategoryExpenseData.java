package com.expenseondemand.eod.model.approval.expense.multiplecategory;

import com.expenseondemand.eod.model.approval.expense.PolicyDetailsResponse;
import com.expenseondemand.eod.webservice.model.approval.item.DuplicateItemsModel;
import com.expenseondemand.eod.webservice.model.approval.item.PolicyBreechResponseModel;

import java.util.ArrayList;

/**
 * Created by ITDIVISION\maximess087 on 16/1/17.
 */

public class MultiCategoryExpenseData {
    private String UniqueId;
    private String date;
    private String Amount;
    private String ExpneseCategory;
    private String ExpenseCategoryId;
    private String BaseCategoryId;
    private String ExpenseItemizationId;
    private String Rejected;
    private String EscalationFlag;
    PolicyBreechResponseModel policyBreech;
    /*private PolicyDetailsResponse PolicyDetails;*/
    private String IsDuplicate;
    ArrayList<DuplicateItemsModel> DuplicateItems;

    public PolicyBreechResponseModel getPolicyBreech() {
        return policyBreech;
    }

    public void setPolicyBreech(PolicyBreechResponseModel policyBreech) {
        this.policyBreech = policyBreech;
    }

    public String getRejected() {
        return Rejected;
    }

    public void setRejected(String rejected) {
        Rejected = rejected;
    }

    public String getEscalationFlag() {
        return EscalationFlag;
    }

    public void setEscalationFlag(String escalationFlag) {
        EscalationFlag = escalationFlag;
    }

    public String getIsDuplicate() {
        return IsDuplicate;
    }

    public void setIsDuplicate(String isDuplicate) {
        IsDuplicate = isDuplicate;
    }

    public ArrayList<DuplicateItemsModel> getDuplicateItems() {
        return DuplicateItems;
    }

    public void setDuplicateItems(ArrayList<DuplicateItemsModel> duplicateItems) {
        DuplicateItems = duplicateItems;
    }

    public String getUniqueId() {
        return UniqueId;
    }

    public void setUniqueId(String uniqueId) {
        UniqueId = uniqueId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getExpneseCategory() {
        return ExpneseCategory;
    }

    public void setExpneseCategory(String expneseCategory) {
        ExpneseCategory = expneseCategory;
    }

    public String getExpenseCategoryId() {
        return ExpenseCategoryId;
    }

    public void setExpenseCategoryId(String expenseCategoryId) {
        ExpenseCategoryId = expenseCategoryId;
    }

    public String getBaseCategoryId() {
        return BaseCategoryId;
    }

    public void setBaseCategoryId(String baseCategoryId) {
        BaseCategoryId = baseCategoryId;
    }

    public String getExpenseItemizationId() {
        return ExpenseItemizationId;
    }

    public void setExpenseItemizationId(String expenseItemizationId) {
        ExpenseItemizationId = expenseItemizationId;
    }

   /* public PolicyDetailsResponse getPolicyDetails() {
        return PolicyDetails;
    }

    public void setPolicyDetails(PolicyDetailsResponse policyDetails) {
        PolicyDetails = policyDetails;
    }*/
}
