package com.expenseondemand.eod.screen.approval.detail;

/**
 * Created by ITDIVISION\maximess087 on 4/1/17.
 */

public interface ApproveRejectedInterface {
    public void rejectClickOk(String rejectionNote, String rejectionId);
    public void approveClickOk(String approverNote, String counterApproverNote);

}
