package com.expenseondemand.eod.util;

public class AppConstant {
    //String Constants
    public static final String LOG_TAG = "EODLogs";
    public static final String FORMAT_DATE_NETWORK = "yyyy-mm-dd hh:mm:ss";
    public static final String FORMAT_DATE_DISPLAY = "dd MMM yyyy";
    public static final String FORMAT_DATE_CALENDER = "MMMM yyyy";
    public static final String FORMAT_DATE_WEB_SERVICE = "yyyyMMdd";
    public static final String DIRECTORY_NAME_RECEIPT_IMAGES = "ExpenseReceipt";
    public static final String DIRECTORY_NAME_APP_LOGO = "Logo";
    public static final String DIRECTORY_NAME_TEMP = "Temp";
    public static final String FILE_NAME_APP_LOGO = "logo";
    public static final String FILE_NAME_TEMP = "temp";
    public static final String RECEIPT_IMAGE_FILE_NAME_WEBSERVICE = "content.jpeg";
    public static final String RECEIPT_IMAGE_MIME_TYPE_WEBSERVICE = "image/jpeg";
    public static final String FORMAT_AMOUNT = "0.00";
    public static final String YES = "Y";
    public static final String EDITION_CODE = "SE";
    public static final String IS_APPROVER_FLAG = "0";
    //Integer Constants
    public static final long TIME_SPLASH_DELAY_IN_MILLISEC = 2000;
    public static final int TIME_CONNECTION_TIMEOUT = 30000; //30 seconds
    public static final int REFRESH_CATEGORIES_NO_OF_DAYS = 7;
    public static final int IMAGE_STANDARD_HEIGHT = 768;
    public static final int IMAGE_STANDARD_WIDTH = 768;

    //Request Codes
    public static final int REQUEST_SELECT_CATEGORY = 1001;
    public static final int REQUEST_SELECT_DATE = 1002;
    public static final int REQUEST_SELECT_IMAGE_FROM_GALLERY = 1003;
    public static final int REQUEST_CAPTURE_IMAGE = 1004;

    //Shared Preferences
    public static final String KEY_PREF_IS_USER_REMEMBERED = "KEY_PREF_IS_USER_REMEMBERED";
    public static final String KEY_PREF_LAST_STORED_USER_ID = "KEY_PREF_LAST_STORED_USER_ID";
    public static final String KEY_PREF_AUTHORIZATION_TOKEN = "KEY_PREF_AUTHORIZATION_TOKEN";
    public static final String KEY_PREF_LAST_CATEGORIES_SYNC_DATE = "KEY_PREF_LAST_CATEGORIES_SYNC_DATE";

    //Keys
    public static final String KEY_CATEGORY_NAME = "KEY_CATEGORY_NAME";
    public static final String KEY_CATEGORY_CODE = "KEY_CATEGORY_CODE";
    public static final String KEY_BASE_CATEGORY_CODE = "KEY_BASE+CATEGORY_CODE";
    public static final String KEY_CALENDER_TAP_START = "start";
    public static final String KEY_CALENDER_TAP_END = "end";
    public static final String KEY_CALENDER_LAST_TAP_LOCATION = "lastTappedLocation";
    public static final String KEY_CALENDER_MONTH = "month";
    public static final String KEY_CALENDER_YEAR = "year";
    public static final String KEY_CALENDER_TAP_COUNT = "clickCount";
    public static final String KEY_EXPENSE_DATE = "KEY_EXPENSE_DATE";
    public static final String KEY_SAVED_SELECTED_DATES = "KEY_SAVED_SELECTED_DATES";
    public static final String KEY_SELECTED_DATES = "KEY_SELECTED_DATES";
    public static final String KEY_SELECTED_CALENDAR_DATES = "KEY_SELECTED_CALENDAR_DATES";
    public static final String KEY_MULTIPLE_DATES_ALLOWED = "KEY_MULTIPLE_DATES_ALLOWED";
    public static final String KEY_EXPENSE_RECEIPT_IMAGE_PATH = "KEY_EXPENSE_RECEIPT_IMAGE_PATH";
    public static final String KEY_EXPENSE_CATEGORY_ID = "KEY_EXPENSE_CATEGORY_ID";
    public static final String KEY_EXPENSE_BASE_CATEGORY_ID = "KEY_EXPENSE_BASE_CATEGORY_ID";
    public static final String KEY_IS_EDIT_EXPENSE = "IS_EDIT_EXPENSE";
    public static final String KEY_EXPENSE_OBJECT = "KEY_EXPENSE_OBJECT";
    public static final String KEY_SYNC_CATEGORY = "KEY_SYNC_CATEGORY";
    public static final String KEY_REASON_lIST = "KEY_REASON_lIST";
    public static final String KEY_CLAIM_ID = "KEY_CLAIM_ID";
    public static final String KEY_CLAIM_TYPE = "KEY_CLAIM_TYPE";
    public static final String KEY_CLAIM_NAME = "KEY_CLAIM_NAME";
    public static final String IS_REJECTED = "IS_REJECTED";
    public static final String KEY_EXPENSE_ITEM_POLICY_BREECH = "KEY_EXPENSE_ITEM_POLICY_BREECH";
    public static final String KEY_EXPENSE_ITEM_HEADER_ID = "KEY_EXPENSE_ITEM_HEADER_ID";
    public static final String KEY_EXPENSE_ITEM_DETAIL_ID = "KEY_EXPENSE_ITEM_DETAIL_ID";
    public static final String KEY_PARENT_DUPLICATE_EXPENSE_ITEM = "KEY_PARENT_DUPLICATE_EXPENSE_ITEM";
    public static final String KEY_MULTIPLE_CATEGORY_EXPENSE = "KEY_MULTIPLE_CATEGORY_EXPENSE";
    public static final String KEY_MULTIPLE_CATEGORY_DETAIL_EXPENSE = "KEY_MULTIPLE_CATEGORY_DETAIL_EXPENSE";
    public static final String KEY_MULTIPLE_CATEGORY_POLICY_DETAILS = "KEY_MULTIPLE_CATEGORY_POLICY_DETAILS";
    public static final String KEY_EXPENSE_DETAIL = "KEY_EXPENSE_DETAIL";

    /*Share preference key*/
    public static final String IS_DEPUTY_ASSIGNED = "IS_DEPUTY_ASSIGNED";
    public static final String BACK_NETWORK_SETTING_STAT = "BACK_NETWORK_SETTING_STAT";
    public static final String BACK_EXPENSE_DETAIL = "BACK_EXPENSE_DETAIL";
    public static final String BACK_EXPENSE_DETAIL_LIST = "BACK_EXPENSE_DETAIL_LIST";
    public static final String BACK_CLAIM_LIST = "BACK_CLAIM_LIST";
    public static final String BACK_CREATE_EXPENSE = "BACK_CREATE_EXPENSE";
    public static final String isCounterApprover = "isCounterApprover";
    public static final String LOAD_MORE_CLAIM = "LOAD_MORE_CLAIM";
    public static final String LOAD_MORE_EXPENSES = "LOAD_MORE";



    //Eppense List Item Types
    public static final int ITEM_TYPE_DATE_SECTION = 0;
    public static final int ITEM_TYPE_EXPENSE_ITEM = 1;

    //Project Detail Receipt
    public static final String DIRECTORY_NAME_PROJECT_DETAIL_RECEIPT = "ProjectDetailReceipt";
    public static final String REQUEST_TYPE_POST = "POST";

    //Http Methods
    public static int HTTP_METHOD_POST = 1;
    public static int HTTP_METHOD_GET = 2;
    public static int HTTP_METHOD_GET_IMAGE = 3;

    //Web Service URLs

    //Staging Url
	/*public static final String BASE_URL = "https://staging.expenseondemand.com/EODMobileAPI";
	public static final String BASE_IMAGE_URL = "https://staging.expenseondemand.com/TNE_M5S5_BC/Images/Misc/";*/

    //Production Url
    public static final String BASE_URL = "https://app.expenseondemand.com/EODMobileAPI";
    public static final String BASE_IMAGE_URL = "https://app.expenseondemand.com/tne/Images/Misc/";

    //staging url
    //public static final String BASE_URL = "https://staging.expenseondemand.com/MobilityService/mock-api/v1/authenticate";

    //Authenticate User
    public static final String AUTHENTICATE_USER_URL = "/api/authentication/login";
    //Expense Categories
    public static final String EXPENSE_CATEGORIES_URL = "/api/expensecategory";
    //Upload Expense
    public static final String UPLOAD_EXPENSE_URL = "/api/UploadExpense/upload-item";
    //Rejection Reason
    public static final String REJECTION_REASONS_URL = "/api/RejectionReasons";
    //Sign Off
    public static final String SIGN_OFF = "/api/authentication/logout";
    //Statistics
    public static final String STATISTICS_URL = "/api/Statistics";
    //Expense items
    public static final String EXPENSE_ITEMS = "/api/Claim";
    public static final String CLAIM_LIST = "/api/Approver/";
    public static final String EXPENSE_DETAIL = "/api/itemdetail";
    public static final String EXPENSE_DETAIL_RECEIPT = "/api/Receipt";
    public static final String APPROVE_REJECTED_EXPENSE = "/api/approverejectitem/";
    public static final String EXPENSE_MULTIPLE_CATEGORY_LIST = "/api/MultipleCategory/List";
    public static final String EXPENSE_MULTIPLE_CATEGORY_DETAILS = "/api/MultipleCategory/Detail";

    //WebService Header
    public static final String REQUEST_HEADER_CONTENT_TYPE_KEY = "Content-Type";
    public static final String REQUEST_HEADER_DEVICE_ID = "DeviceID";
    public static final String REQUEST_HEADER_HTTP_VERB = "Http-Verb";
    public static final String REQUEST_HEADER_DATE_TIME_STAMP = "DateTimeStamp";
    public static final String REQUEST_HEADER_URL = "URL";
    public static final String REQUEST_HEADER_AUTHORIZATION_KEY = "Authorization";
    public static final String REQUEST_HEADER_USER_ID_KEY = "UserId";
    public static final String REQUEST_HEADER_ACCEPT_LANGUAGE = "Accept-Language";
    public static final String REQUEST_HEADER_OS_VERSION = "OSVersion";
    public static final String REQUEST_HEADER_OS_TYPE = "OSType";
    public static final String REQUEST_HEADER_APP_VERSION = "AppVersion";
    public static final String REQUEST_HEADER_TOKEN = "Token";


    public static final String CONTENT_TYPE_JSON = "application/json;charset=utf-8";
    public static final String CONTENT_TYPE_JSON_MULTIPART_DATA = "application/json";
    public static final String CONTENT_TYPE_MULTIPART_DATA = "multipart/form-data; boundary = ";

    public static final String CONTENT_DEFAULT_LANGUAGE = "en-gb";
    public static final String CONTENT_OS_TYPE = "Android";


    public static final String REQUEST_DATA = "RequestData";
    public static final String REQUEST_CONTENT = "Content";
    public static final String RECEIPT_EXTENSION = ".jpg";

    //Multi-Part Constants
    public static final String ENCODING_UTF_8 = "UTF-8";
    public static final String LINE_FEED = "\r\n";
    public static final String TWO_HYPHENS = "--";
    public static final String REQUEST_CONTENT_DISPOSITION_FORM_DATA_NAME = "Content-Disposition: form-data; name=\"";
    public static final String REQUEST_CONTENT_TRANSFER_ENCODING = "Content-Transfer-Encoding: binary";
    public static final String FILE_NAME = "\"; filename=\"";

    //Validation Error Codes
    public static final int VALIDATION_ERROR_CODE_101 = 101;
    public static final int VALIDATION_ERROR_CODE_102 = 102;
    public static final int VALIDATION_ERROR_CODE_103 = 103;
    public static final int VALIDATION_ERROR_CODE_104 = 104;
    public static final int VALIDATION_ERROR_CODE_105 = 105;
    public static final int VALIDATION_ERROR_CODE_106 = 106;
    public static final int VALIDATION_ERROR_CODE_107 = 107;
    public static final int VALIDATION_ERROR_CODE_108 = 108;

    //Error Message
    /*Touch Image View*/
    public static final String KEY_NORMALIZED_SCALE = "KEY_NORMALIZED_SCALE";
    public static final String KEY_MATRIX_VALUES = "KEY_MATRIX_VALUES";
    public static final String KEY_MATCH_VIEW_HEIGHT = "KEY_MATCH_VIEW_HEIGHT";
    public static final String KEY_MATCH_VIEW_WIDTH = "KEY_MATCH_VIEW_WIDTH";
    public static final String KEY_VIEW_HEIGHT = "KEY_VIEW_HEIGHT";
    public static final String KEY_VIEW_WIDTH = "KEY_VIEW_WIDTH";
    public static final String KEY_IMAGE_RENDERED = "KEY_IMAGE_RENDERED";
    public static final String UNSUPPORTED_OPERATION_MESSAGE = "TouchImageView does not support FIT_START or FIT_END";
    public static final String KEY_EXPENSE_FILTER_TYPE = "KEY_EXPENSE_FILTER_TYPE";
    public static final int EXPENSE_NOTE_CHARACTER_COUNT_LIMIT = 100;

    //Error Codes
    public static final int ERROR_CODE_1001 = 1001;
    public static final int ERROR_CODE_1002 = 1002;
    public static final int ERROR_CODE_1003 = 1003;
    public static final int ERROR_CODE_1004 = 1004;
    public static final int ERROR_CODE_1005 = 1005;
    public static final int ERROR_CODE_1006 = 1006;
    public static final int ERROR_CODE_1007 = 1007;
    public static final int ERROR_CODE_1008 = 1008;
    public static final int ERROR_CODE_1009 = 1009;
    public static final int ERROR_CODE_1010 = 1010;
    public static final int ERROR_CODE_1011 = 1011;
    public static final int ERROR_CODE_1012 = 1012;
    public static final int ERROR_CODE_1013 = 1013;
    public static final int ERROR_CODE_1014 = 1014;
    public static final int ERROR_CODE_1015 = 1015;
    public static final int ERROR_CODE_1016 = 1016;
    public static final int ERROR_CODE_1017 = 1017;
    public static final int ERROR_CODE_1018 = 1018;
    public static final int ERROR_CODE_1019 = 1019;
    public static final int ERROR_CODE_1020 = 1020;
    public static final int ERROR_CODE_1021 = 1021;
    public static final int ERROR_CODE_1022 = 1022;
    public static final int ERROR_CODE_1023 = 1023;
    public static final int ERROR_CODE_1024 = 1024;
    public static final int ERROR_CODE_1025 = 1025;
    public static final int ERROR_CODE_1026 = 1026;
    public static final int ERROR_CODE_1027 = 1027;
    public static final int ERROR_CODE_1028 = 1028;
    public static final int ERROR_CODE_1029 = 1029;
    public static final int ERROR_CODE_1244 = 1244;

    public static final int ERROR_CODE_2001 = 2001;
    public static final int ERROR_CODE_2002 = 2002;
    public static final int ERROR_CODE_2003 = 2003;
    public static final int ERROR_CODE_2004 = 2004;
    public static final int ERROR_CODE_2005 = 2005;
    public static final int ERROR_CODE_2006 = 2006;


    /*no color value*/
    public static final int DEFAULT_COLOR = -1;

    //whats new html file path

    //This key is used to store current Silent login status in shared preferences
    public static final String PREF_KEY_SILENT_LOGIN_STATUS = "PREF_KEY_SILENT_LOGIN_STATUS";



}
