package com.expenseondemand.eod.model.approval.claim;

/**
 * Created by ITDIVISION\maximess087 on 22/2/17.
 */

public class PreApprovedDetails {
    private String labelName;
    private String value;

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
