package com.expenseondemand.eod.manager;

import android.graphics.Bitmap;
import android.util.Log;

import com.expenseondemand.eod.exception.EODAppException;
import com.expenseondemand.eod.exception.EODAuthenticationFailureException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.model.UserCredential;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.webservice.model.authentication.login.AuthenticateUserRequest;
import com.expenseondemand.eod.webservice.model.authentication.login.AuthenticateUserResponse;
import com.expenseondemand.eod.webservice.model.authentication.logout.SignOffResponse;
import com.expenseondemand.eod.webservice.model.authentication.login.Data;
import com.expenseondemand.eod.webservice.model.http.Body;
import com.expenseondemand.eod.webservice.model.http.HTTPData;

import java.io.File;

public class AuthenticationManager {
    //Get Last Stored UserCredentials
    public static UserCredential getLastStoredUserCredentials(String userId) throws EODException {
        //Get Last Stored User Credentails from DB
        UserCredential userCredential = DbManager.getLastStoredUserCredentials(userId);

        return userCredential;
    }

    //Authenticate User Locally
    public static UserInfo authenticateUserLocally(UserCredential userCredential) throws EODException {
        //Get UserInfo from Db
        UserInfo userInfo = DbManager.getUserInfo(userCredential);

        boolean isValidUser = isUserAuthenticated(userInfo);

        //Authenticate User from DB
        if (isValidUser) {
            //Cache UserInfo
            CachingManager.saveUserInfo(userInfo);
        } else {
            //Remove UserInfo from cache
            CachingManager.saveUserInfo(null);

            //Throw Exception
            throw (new EODAuthenticationFailureException());
        }

        return userInfo;
    }

    //Is User Authenticated
    private static boolean isUserAuthenticated(UserInfo userInfo) {
        boolean isUserValid = true;

        if (userInfo == null) {
            isUserValid = false;
        }

        return isUserValid;
    }

    //Authenticate User
    public static UserInfo authenticateUser(UserCredential userCredential) throws EODException {
        AuthenticateUserResponse response = null;
        //CachingManager.saveUserInfo();

        //Prepare Request Object
        AuthenticateUserRequest request = prepareAuthenticateUserRequest(userCredential);

        //Prepare JSON Request String
        String jsonRequestString = ParseManager.prepareJsonRequest(request);

        //Prepare http request
        HTTPData httpData = new HTTPData();
        httpData.setType(AppConstant.HTTP_METHOD_POST);
        Body body = new Body();
        body.setJsonRequestString(jsonRequestString);
        httpData.setBody(body);

        //Call to Web Service
        String jsonResponseString = (String) WebServiceManager.callWebService(AppConstant.BASE_URL, AppConstant.AUTHENTICATE_USER_URL, httpData);

        Log.e("loginResponse", ":-" + jsonResponseString);
        //Prepare Response Object
        response = (AuthenticateUserResponse) ParseManager.prepareWebServiceResponseObject(AuthenticateUserResponse.class, jsonResponseString);

        //Process Response Status
        WebServiceManager.processResponseStatus(response);

        //Process Response Status
        UserInfo user = processAuthenticateUserResponse(response, userCredential);

        try {
            downloadLogo(user.getCompanyLogo());
        } catch (Exception e) {
        }

        return user;
    }

    //Prepare Authentiication User Request
    private static AuthenticateUserRequest prepareAuthenticateUserRequest(UserCredential userCredential) {
        //Create
        AuthenticateUserRequest authenticateUserRequest = new AuthenticateUserRequest();

        authenticateUserRequest.setLoginId(userCredential.getLoginId()); //LoginId
        authenticateUserRequest.setPassword(userCredential.getPassword()); //Password

        return authenticateUserRequest;
    }

    //Check Response Status
    private static UserInfo processAuthenticateUserResponse(AuthenticateUserResponse response, UserCredential userCredential) throws EODException {
        UserInfo userInfo = null;

        if (response != null) {
            //Is Valid User
            boolean isValidUser = response.getData().isValidUser();

            if (!isValidUser) {
                //Remove UserInfo from cache
                CachingManager.saveUserInfo(null);

                //Throw Exception
                throw new EODAuthenticationFailureException(AppConstant.ERROR_CODE_1007, response.getData().getMessage());
            } else {
                //Prepare User Application Model
                userInfo = prepareUserApplicationModel(response, userCredential);

                //Cache UserInfo
                CachingManager.saveUserInfo(userInfo);

                //Persist Token
                PersistentManager.persistAuthorizationToken(userInfo.getToken());
            }
        } else {
            //Remove UserInfo from cache
            CachingManager.saveUserInfo(null);

            throw new EODAppException(AppConstant.ERROR_CODE_1008);
        }

        return userInfo;
    }

    //Prepare User Application Model
    private static UserInfo prepareUserApplicationModel(AuthenticateUserResponse response, UserCredential userCredential) {
        UserInfo user = null;

        if (response != null) {
            user = new UserInfo();

            Data data = response.getData();
            if (data != null) {
                boolean isApprover = data.getIsApprover().equalsIgnoreCase(AppConstant.YES);
                boolean isFinance = data.getIsFinance().equalsIgnoreCase(AppConstant.YES);
                boolean isBoss = data.getIsBoss().equalsIgnoreCase(AppConstant.YES);
                boolean isDelegate = data.getIsDelegate().equalsIgnoreCase(AppConstant.YES);
                boolean isDeputyAssigned=data.getDeputyAssigned().equalsIgnoreCase(AppConstant.YES);
                user.setUserId(data.getUserId()); //User Id
                user.setToken(data.getToken()); //Token
                user.setDefaultCategoryId(data.getDefaultCategoryId()); //Default Category Id
                user.setDefaultCurrencyCode(data.getDefaultCurrencyCode()); //Default Currency Code
                user.setUserCredential(userCredential);
                user.setResourceId(data.getResourceId());
                user.setCostCenterId(data.getCostCenterId());
                user.setCompanyLogo(data.getCompanyLogo());
                user.setBranchId(data.getBranchId());
                user.setCompanyId(data.getCompanyId());
                user.setBranchId(data.getBranchId());
                user.setDepartmentId(data.getDepartmentId());
                user.setCostCenterName(data.getCostCenterName());
                user.setCompanyName(data.getCompanyName());
                user.setDisplayName(data.getDisplayName());
                user.setApprover(isApprover);
                user.setFinance(isFinance);
                user.setBoss(isBoss);
                user.setDelegate(isDelegate);
                user.setGradeId(data.getGradeId());
                user.setDeputyAssigned(isDeputyAssigned);

                //Set isDeputy assigned
                SharePreference.putBoolean(CachingManager.getAppContext(),AppConstant.IS_DEPUTY_ASSIGNED,isDeputyAssigned);
            }
        }

        return user;
    }

    public static boolean downloadLogo(String imageName) throws EODException {
        //Prepare http request
        HTTPData httpData = new HTTPData();
        httpData.setType(AppConstant.HTTP_METHOD_GET_IMAGE);

        //Call to Web Service
        boolean imageLoaded = (boolean) WebServiceManager.callWebService(AppConstant.BASE_IMAGE_URL, imageName, httpData);

        if (imageLoaded) {
            File tempFile = DeviceManager.getImageFile(AppConstant.DIRECTORY_NAME_TEMP, AppConstant.FILE_NAME_TEMP);
            Bitmap selectedBitmap = MediaManager.getScaledDownBitmapFromFilePath(tempFile.getAbsolutePath());

            if (selectedBitmap != null) {
                File imageFile = DeviceManager.getImageFile(AppConstant.DIRECTORY_NAME_APP_LOGO, AppConstant.FILE_NAME_APP_LOGO);
                MediaManager.writeBitmapTofFile1(imageFile, selectedBitmap);
            }
        }

        return imageLoaded;
    }

    //Logout User
    public static void logoutUser() throws EODException {
        //Prepare http request
        HTTPData httpData = new HTTPData();
        httpData.setType(AppConstant.HTTP_METHOD_POST);

        //Call to Web Service
        String jsonResponseString = (String) WebServiceManager.callWebService(AppConstant.BASE_URL, AppConstant.SIGN_OFF, httpData);

        //Prepare Response Object
        SignOffResponse response = (SignOffResponse) ParseManager.prepareWebServiceResponseObject(SignOffResponse.class, jsonResponseString);

        //Process Response Status
        WebServiceManager.processResponseStatus(response);
    }
}
