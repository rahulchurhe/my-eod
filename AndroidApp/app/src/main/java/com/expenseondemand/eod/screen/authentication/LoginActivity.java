package com.expenseondemand.eod.screen.authentication;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.activity.BaseActivity;
import com.expenseondemand.eod.dialogFragment.ApplicationAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationErrorAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationNetworkDialog;
import com.expenseondemand.eod.exception.EODBusinessException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.AuthenticationManager;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.DbManager;
import com.expenseondemand.eod.manager.DeviceManager;
import com.expenseondemand.eod.manager.PersistentManager;
import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.model.UserCredential;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.screen.home.HomeActivity;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.util.UIUtil;

public class LoginActivity extends BaseActivity implements OnClickListener, ApplicationErrorAlertDialog.ErrorDialogActionListener, ApplicationAlertDialog.AlertDialogActionListener {

    private EditText editTextUserName;
    private EditText editTextPassword;
    private Button buttonLogin;
    private LoginUserAsyncTask loginUserAsyncTask;
    private Dialog dialog = null;
    private ApplicationAlertDialog applicationAlertDialog;
    private ApplicationErrorAlertDialog applicationErrorAlertDialog;
    private ApplicationNetworkDialog applicationNetworkDialogl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_activity_layout);

        //Initialize View
        initializeView();
    }

    //Initialize View
    private void initializeView() {
        //Get View References
        editTextUserName = (EditText) findViewById(R.id.editTextUsername);

        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        editTextPassword.setTypeface(Typeface.DEFAULT);

        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        buttonLogin.setTextColor(getResources().getColor(R.color.white));
        buttonLogin.setOnClickListener(this);

        //set toolbar title
        handleToolBar(getResources().getString(R.string.login_activity_title), AppConstant.DEFAULT_COLOR, AppConstant.DEFAULT_COLOR, false);

        //Set TextWatcher
        LoginFormTextWatcher userNameTextWatcher = new LoginFormTextWatcher();
        editTextUserName.addTextChangedListener(userNameTextWatcher);

        LoginFormTextWatcher passwordTextWatcher = new LoginFormTextWatcher();
        editTextPassword.addTextChangedListener(passwordTextWatcher);


    }


    @Override
    public void onClick(View view) {
        //Get View Id
        int viewId = view.getId();

        switch (viewId) {
            case R.id.buttonLogin:

                //Handle Login
                handleLogin();

                break;
        }
    }

    //Handle Login
    private void handleLogin() {
        //Login User
        loginUser();
    }

    //Get User Credentials From Form
    private UserCredential getUserCredentials() {
        //Get Credentials
        String loginId = editTextUserName.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        //User Credentials
        UserCredential userCredential = new UserCredential();
        userCredential.setLoginId(loginId);
        userCredential.setPassword(password);

        return userCredential;
    }

    //Login User
    private void loginUser() {
        //User Credentials
        UserCredential userCredential = getUserCredentials();

        //Check if Network Available
        if (DeviceManager.isOnline()) {

            //Async Task
            loginUserAsyncTask = new LoginUserAsyncTask();
            loginUserAsyncTask.execute(userCredential);
        } else {
            enableNetwork();

        }
    }


    //Handle Access to Login Button
    private void handleLoginButtonAccess() {
        String loginId = editTextUserName.getText().toString();
        String password = editTextPassword.getText().toString();

        if (AppUtil.isStringEmpty(loginId) || AppUtil.isStringEmpty(password)) {
            disableLoginButton();
        } else {
            enableLoginButton();
        }
    }

    //Enable Login Button
    @SuppressWarnings("deprecation")
    private void enableLoginButton() {
        buttonLogin.setEnabled(true);
    }

    //Disable Login Button
    @SuppressWarnings("deprecation")
    private void disableLoginButton() {
        buttonLogin.setEnabled(false);
    }


    //Navigate to Home Activity
    private void navigateToHomeActivity() {
        //Create Intent
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);

        //Finish this Activity
        LoginActivity.this.finish();
        overridePendingTransition(R.anim.slide_in_forward, R.anim.slide_out_forward);
    }

    @Override
    protected void onStop() {
        super.onStop();

        //Dismiss Progress Dialog
        UIUtil.dismissDialog(dialog);

        if (loginUserAsyncTask != null) {
            //Cancel Async Task If it is Executing
            loginUserAsyncTask.cancel(false);
        }

    }

    /**
     * Hide Soft Keyboard when-ever user touches the area outside the EditText
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View view = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (view instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                UIUtil.hideSoftKeyboard(w);
            }
        }

        return ret;
    }


    /**
     * Text Watcher
     */

    private class LoginFormTextWatcher implements TextWatcher {
        @Override
        public void onTextChanged(CharSequence text, int start, int before, int count) {
            //Handle Access to Login Button
            handleLoginButtonAccess();
        }

        @Override
        public void beforeTextChanged(CharSequence text, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            handleLoginButtonAccess();
        }
    }


    //Login User AsyncTask
    private class LoginUserAsyncTask extends AsyncTask<UserCredential, Void, UserInfo> {
        private Exception exception = null;

        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(LoginActivity.this);
        }

        @Override
        protected UserInfo doInBackground(UserCredential... params) {
            UserInfo userInfo = null;
            CachingManager.saveUserInfo(null);
            SharePreference.putBoolean(getApplicationContext(), "IS_LOGIN", true);
            try {
                //Authenticate User

                userInfo = AuthenticationManager.authenticateUser(params[0]);
                DbManager.saveUserInformation(userInfo);

            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return userInfo;
        }

        @Override
        protected void onPostExecute(UserInfo user) {
            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);

            if (exception == null) {
                //OnSucess
                onSuccessAuthenticateUser(user);
            } else if (exception instanceof EODBusinessException) {
                //OnFail
                onFailLogin((EODException) exception);
            } else if (exception instanceof EODException) {
                //OnError
                onErrorLogin(exception);
            }
        }
    }

    //OnSucess
    public void onSuccessAuthenticateUser(UserInfo user) {
        if (user != null) {
            //Get user is remembered or not
            boolean isUserRemembered = true;

            //Persist isUserRemembered
            PersistentManager.persistIsUserRemembered(isUserRemembered);

            //Persist Last LoggedIn UserId
            PersistentManager.persistUserId(user.getUserId());

            //Navigate to Home Activity
            navigateToHomeActivity();
        }
    }

    //OnFail
    public void onFailLogin(EODException exception) {
        //Show Alert Dialog
        showAlertDialog(true, exception.getExceptionMessage(), null, null, ApplicationAlertDialog.TYPE_ERROR);

    }

    //OnError
    public void onErrorLogin(Exception exception) {
        //Show Alert Dialog
        showErrorAlertDialog(false, null, null, null, ApplicationAlertDialog.TYPE_ERROR);
        // showResponseErrorAlertDialog(false, null, null, null);
    }

    void showAlertDialog(boolean neutralButton, String message, String title, String okButtonTitle, int type) {
        applicationAlertDialog = new ApplicationAlertDialog().getInstance(this, neutralButton, message, title, okButtonTitle, type);
        applicationAlertDialog.show(getSupportFragmentManager(), "dialog");
    }

    void showErrorAlertDialog(boolean neutralButton, String message, String title, String okButtonTitle, int type) {
        applicationErrorAlertDialog = new ApplicationErrorAlertDialog().getInstance(this, message, title, okButtonTitle, type);
        applicationErrorAlertDialog.show(getSupportFragmentManager(), "dialog");
    }


    void enableNetwork() {

        applicationNetworkDialogl = new ApplicationNetworkDialog().getInstance(LoginActivity.this);
        applicationNetworkDialogl.show(getSupportFragmentManager(), "dialog");
    }
// error dialog action handling

    @Override
    public void onRetryButtonClick(int type) {
        //Handle Login
        handleLogin();
    }

    @Override
    public void onCancelButtonClick() {

    }


    // alert dialog action handling
    @Override
    public void onPositiveButtonClick(int type) {

    }

    @Override
    public void onNegativeButtonClick() {
    }


}
