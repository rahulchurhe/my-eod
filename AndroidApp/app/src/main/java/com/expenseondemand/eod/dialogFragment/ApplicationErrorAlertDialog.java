package com.expenseondemand.eod.dialogFragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.expenseondemand.eod.R;

/**
 * Created by Ramit Yadav on 04-10-2016.
 */

public class ApplicationErrorAlertDialog extends DialogFragment implements View.OnClickListener
{
    private TextView textViewOk;
    private TextView textViewCancel;
    private TextView textViewMessage;
    private TextView textViewTitle;
    private String message;
    private String title;
    private String okButtonTitle;
    private boolean neutralButton;
    int type;
    private ErrorDialogActionListener errorDialogActionListener;

    public interface ErrorDialogActionListener
    {
        void onRetryButtonClick(int type);

        void onCancelButtonClick();
    }

    public static ApplicationErrorAlertDialog getInstance(ErrorDialogActionListener errorDialogActionListener,String message,String title,String okButtonTitle,int type)
    {
        ApplicationErrorAlertDialog applicationErrorAlertDialog = new ApplicationErrorAlertDialog();

        applicationErrorAlertDialog.errorDialogActionListener = errorDialogActionListener;

        applicationErrorAlertDialog.message = message;
        applicationErrorAlertDialog.title = title;
        applicationErrorAlertDialog.okButtonTitle = okButtonTitle;
        applicationErrorAlertDialog.type = type;
        return applicationErrorAlertDialog;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_BACK) && (event.getAction() == KeyEvent.ACTION_UP))
                    handleCancel();
                return false;
            }
        });
    }

    @Override
    public void onStart()
    {
        super.onStart();
        getDialog().getWindow().setLayout( ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);

    }

    @Override
    public void onPause()
    {
        super.onPause();
        handleCancel();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.application_error_alert_dialog, container, false);
         setCancelable(false);
        textViewOk = (TextView) view.findViewById(R.id.textViewOk);
        textViewCancel= (TextView) view.findViewById(R.id.textViewCancel);
        textViewMessage = (TextView) view.findViewById(R.id.textViewMessage);
        textViewTitle = (TextView) view.findViewById(R.id.textViewTitle);

        if(title == null)
        textViewTitle.setText(R.string.text_error_title);
        else
        textViewTitle.setText(title);

        if(message == null)
        textViewMessage.setText(R.string.text_error_processing);
        else
        textViewMessage.setText(message);

        if(okButtonTitle == null)
        textViewOk.setText(getResources().getString(R.string.button_retry));
        else
        textViewOk.setText(okButtonTitle);

        textViewOk.setOnClickListener(this);

//        if(neutralButton)
//            textViewCancel.setVisibility(View.GONE);
//        else
//        {
            textViewCancel.setText(getResources().getString(R.string.button_cancel));
            textViewCancel.setOnClickListener(this);
//        }

        return view;
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.textViewOk:
            {
                handleRetry();
                break;
            }

            case R.id.textViewCancel:
            {
                handleCancel();
                break;
            }
        }
    }

    private void handleRetry()
    {
        dismiss();
        errorDialogActionListener.onRetryButtonClick(type);
    }

    private void handleCancel()
    {
        dismiss();
        errorDialogActionListener.onCancelButtonClick();
    }
}
