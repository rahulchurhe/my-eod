package com.expenseondemand.eod.webservice.model.approval.item;

/**
 * Created by Nandani Gupta on 2016-10-20.
 */
public class DuplicateItemResponseModel
{

    String expenseCategoryFieldDetailId ;
    String expenseDetailId;
    String fieldValue;
    String dateExpense;

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public String getExpenseCategoryFieldDetailId() {
        return expenseCategoryFieldDetailId;
    }

    public void setExpenseCategoryFieldDetailId(String expenseCategoryFieldDetailId) {
        this.expenseCategoryFieldDetailId = expenseCategoryFieldDetailId;
    }

    public String getExpenseDetailId() {
        return expenseDetailId;
    }

    public void setExpenseDetailId(String expenseDetailId) {
        this.expenseDetailId = expenseDetailId;
    }

    public String getDateExpense() {
        return dateExpense;
    }

    public void setDateExpense(String dateExpense) {
        this.dateExpense = dateExpense;
    }
}
