package com.expenseondemand.eod.exception;

public class EODAuthenticationFailureException extends EODBusinessException
{
	private static final long serialVersionUID = 1L;

	public EODAuthenticationFailureException() 
	{
	}
	
	//Constructor
	public EODAuthenticationFailureException(int exceptionCode)
	{
		super(exceptionCode, "");
	}
	
	//Constructor
	public EODAuthenticationFailureException(int exceptionCode, String exceptionMessage)
	{
		super(exceptionCode, exceptionMessage);
	}
}
