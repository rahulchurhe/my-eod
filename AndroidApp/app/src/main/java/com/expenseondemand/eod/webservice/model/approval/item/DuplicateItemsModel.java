package com.expenseondemand.eod.webservice.model.approval.item;

import java.util.ArrayList;

/**
 * Created by ITDIVISION\maximess087 on 13/1/17.
 */

public class DuplicateItemsModel {

    String ColourIndicator;
    DuplicateItem DuplicateItem;

    public String getColourIndicator() {
        return ColourIndicator;
    }

    public void setColourIndicator(String colourIndicator) {
        ColourIndicator = colourIndicator;
    }

    public DuplicateItem getDuplicateItem() {
        return DuplicateItem;
    }

    public void setDuplicateItem(DuplicateItem duplicateItem) {
        DuplicateItem = duplicateItem;
    }
}
