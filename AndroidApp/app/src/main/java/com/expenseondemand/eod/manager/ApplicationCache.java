package com.expenseondemand.eod.manager;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.model.AppRejectionSetting;
import com.expenseondemand.eod.model.DeviceInfo;
import com.expenseondemand.eod.model.UserInfo;

import android.app.Application;

public class ApplicationCache
{
	/**
	 * Singleton Class
	 */
	private static ApplicationCache applicationCache = null;

	private final int MAX_BASE_CATEGORIES = 30;

	//Constructor
	private ApplicationCache()
	{

	}

	//Get Instance
	public static synchronized ApplicationCache getInstance()
	{
		if(applicationCache == null)
		{
			applicationCache = new ApplicationCache();
		}

		return applicationCache;
	}

	/**
	 * Data in Cache
	 */

	//Application Context
	private Application appContext;

	private int [] categoryResourceId;

	//User Info
	private UserInfo userInfo;
	private DeviceInfo deviceInfo;
	private AppRejectionSetting AppRejSettings;

	public AppRejectionSetting getAppRejSettings() {
		return AppRejSettings;
	}

	public void setAppRejSettings(AppRejectionSetting appRejSettings) {
		AppRejSettings = appRejSettings;
	}

	public Application getAppContext() {
		return appContext;
	}

	public void setAppContext(Application appContext) {
		this.appContext = appContext;
	}
	
	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public DeviceInfo getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(DeviceInfo deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public int getCategoryResourceId(int baseCategoryId)
	{
		if(categoryResourceId == null)
		{
			categoryResourceId = new int[MAX_BASE_CATEGORIES];

			categoryResourceId[0] = R.drawable.cat_1_books_publications;
			categoryResourceId[1] = R.drawable.cat_2_course_conferences_fees;
			categoryResourceId[2] = R.drawable.cat_3_mileage;
			categoryResourceId[3] = R.drawable.cat_4_office_supplies;
			categoryResourceId[4] = R.drawable.cat_5_comms_telephone_fax;
			categoryResourceId[5] = R.drawable.cat_6_travel_entertainment;
			categoryResourceId[6] = R.drawable.cat_7_travel_hotels;
			categoryResourceId[7] = R.drawable.cat_8_travel_meals;
			categoryResourceId[8] = R.drawable.cat_9_travel_trains;
			categoryResourceId[9] = R.drawable.cat_10_travel_taxis;
			categoryResourceId[10] = R.drawable.cat_11_car_hire_rental;
			categoryResourceId[11] = R.drawable.cat_12_petrol;
			categoryResourceId[12] = R.drawable.cat_13_travel_air;
			categoryResourceId[13] = R.drawable.cat_14_pstage_courier;
			categoryResourceId[14] = R.drawable.cat_15_tips_gratuities;
			categoryResourceId[15] = R.drawable.cat_16_software;
			categoryResourceId[16] = R.drawable.cat_17_hardware_office_equipment;
			categoryResourceId[17] = R.drawable.cat_18_subsistence;
			categoryResourceId[18] = R.drawable.cat_19_miscellaneous;
			categoryResourceId[19] = R.drawable.cat_20_travel_parking;
			categoryResourceId[20] = R.drawable.cat_21_car_repair_service;
			categoryResourceId[21] = R.drawable.cat_22_advance_received;
			categoryResourceId[22] = R.drawable.temp_category_icon;//TODO need icon
			categoryResourceId[23] = R.drawable.temp_category_icon;//TODO need icon
			categoryResourceId[24] = R.drawable.cat_25_private_mileage;
			categoryResourceId[25] = R.drawable.cat_26_personal_expenses;
			categoryResourceId[26] = R.drawable.cat_27_multiple_categories;
			categoryResourceId[27] = R.drawable.cat_28_away_from_home;
			categoryResourceId[28] = R.drawable.cat_29_toll_tax;
			categoryResourceId[29] = R.drawable.cat_30_daily_allowance;
		}

		return (baseCategoryId > 0 && baseCategoryId <= MAX_BASE_CATEGORIES) ? categoryResourceId[baseCategoryId - 1] : R.drawable.temp_category_icon;
	}

	//Releasing Resources
	public void removeApplicationCache()
	{
		applicationCache = null;
	}
}
