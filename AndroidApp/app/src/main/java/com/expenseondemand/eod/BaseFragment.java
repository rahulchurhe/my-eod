package com.expenseondemand.eod;

import android.content.Intent;
import android.support.v4.app.Fragment;

/**
 * Created by Ramit Yadav on 12-09-2016.
 */
public class BaseFragment extends Fragment
{
    public void startActivity(Intent intent)
    {
        getActivity().startActivity(intent);
    }
}
