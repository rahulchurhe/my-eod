package com.expenseondemand.eod.webservice.model.approval.claim;

import com.expenseondemand.eod.webservice.model.MasterResponse;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 19-10-2016.
 */

public class ClaimResponse extends MasterResponse {
    private ClaimListData data;

    public ClaimListData getData() {
        return data;
    }

    public void setData(ClaimListData data) {
        this.data = data;
    }
    /* private ArrayList<ClaimData> data;

    public ArrayList<ClaimData> getData()
    {
        return data;
    }

    public void setData(ArrayList<ClaimData> data)
    {
        this.data = data;
    }*/
}
