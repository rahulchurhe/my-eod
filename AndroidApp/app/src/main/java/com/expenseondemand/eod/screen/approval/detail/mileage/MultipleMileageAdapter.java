package com.expenseondemand.eod.screen.approval.detail.mileage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.model.approval.detail.mileage.MultipleMileageItem;
import com.expenseondemand.eod.util.AppUtil;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 19-09-2016.
 */
public class MultipleMileageAdapter extends RecyclerView.Adapter {
    private Context context;
    private ArrayList<MultipleMileageItem> multipleMileageItems;

    public MultipleMileageAdapter(Context context, ArrayList<MultipleMileageItem> multipleMileageItems) {
        this.context = context;
        this.multipleMileageItems = multipleMileageItems;
    }

    @Override
    public int getItemCount() {
        return AppUtil.isCollectionEmpty(multipleMileageItems) ? 0 : multipleMileageItems.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.multiple_mileage_item, parent, false);

        return new ProjectDetailItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MultipleMileageItem multipleMileageItem = multipleMileageItems.get(position);

        ProjectDetailItemHolder projectDetailItemHolder = (ProjectDetailItemHolder) holder;

        /*Source*/
        projectDetailItemHolder.textViewSource.setText(multipleMileageItem.getSourceAddress());//Source Id

        /*Destination*/
        projectDetailItemHolder.textViewDestination.setText(multipleMileageItem.getDestinationAddress());//Source Id

        /*Distance*/
        projectDetailItemHolder.textViewDistance.setText(multipleMileageItem.getDistance());

        /*Amount*/
        projectDetailItemHolder.textViewAmount.setText(new DecimalFormat("0.00").format(Double.parseDouble(multipleMileageItem.getAmount())));
    }

    protected static class ProjectDetailItemHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayoutTopRow;
        TextView textViewSource;
        TextView textViewDestination;
        TextView textViewDistance;
        TextView textViewAmount;

        public ProjectDetailItemHolder(View itemView) {
            super(itemView);

            linearLayoutTopRow = (LinearLayout) itemView.findViewById(R.id.linearLayoutTopRow);
            textViewSource = (TextView) itemView.findViewById(R.id.textViewSource);
            textViewDestination = (TextView) itemView.findViewById(R.id.textViewDestination);
            textViewDistance = (TextView) itemView.findViewById(R.id.textViewDistance);
            textViewAmount = (TextView) itemView.findViewById(R.id.textViewAmount);

            textViewSource.setSelected(true);
            textViewDestination.setSelected(true);
        }
    }
}
