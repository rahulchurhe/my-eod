package com.expenseondemand.eod.webservice.model.approval.detail;

import com.expenseondemand.eod.webservice.model.approval.detail.mileage.Mileage;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 20-10-2016.
 */

public class ExpenseDetailData
{
    private String uniqueId;
    private String date;
    private String amount;
    private String isItemised;
    private int baseCategoryId;
    private ArrayList<DynamicDetail> dynamicDetails;
    //private ArrayList<ProjectDetail> projectDetails;
    private ArrayList<ProjectDetails> ProjectDetails;
    private PaymentType PaymentType;
    private BusinessPurpose BusinessPurpose;
    private ArrayList<AdditionalDetails> AdditionalDetails;
    private Receipt Receipt;
    private Notes notes;
    private Mileage mileage;
    private ArrayList<Attendees> attendees;

    public String getUniqueId()
    {
        return uniqueId;
    }

    public int getBaseCategoryId()
    {
        return baseCategoryId;
    }

    public void setBaseCategoryId(int baseCategoryId)
    {
        this.baseCategoryId = baseCategoryId;
    }

    public void setUniqueId(String uniqueId)
    {
        this.uniqueId = uniqueId;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public String getAmount()
    {
        return amount;
    }

    public void setAmount(String amount)
    {
        this.amount = amount;
    }

    public String getIsItemised()
    {
        return isItemised;
    }

    public void setIsItemised(String isItemised)
    {
        this.isItemised = isItemised;
    }

    public ArrayList<DynamicDetail> getDynamicDetails()
    {
        return dynamicDetails;
    }

    public void setDynamicDetails(ArrayList<DynamicDetail> dynamicDetails)
    {
        this.dynamicDetails = dynamicDetails;
    }

   /* public ArrayList<ProjectDetail> getProjectDetails()
    {
        return projectDetails;
    }

    public void setProjectDetails(ArrayList<ProjectDetail> projectDetails)
    {
        this.projectDetails = projectDetails;
    }*/

    public ArrayList<ProjectDetails> getProjectDetails() {
        return ProjectDetails;
    }

    public void setProjectDetails(ArrayList<ProjectDetails> projectDetails) {
        ProjectDetails = projectDetails;
    }

    public PaymentType getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        PaymentType = paymentType;
    }

    public BusinessPurpose getBusinessPurpose() {
        return BusinessPurpose;
    }

    public void setBusinessPurpose(BusinessPurpose businessPurpose) {
        BusinessPurpose = businessPurpose;
    }

    public ArrayList<AdditionalDetails> getAdditionalDetails() {
        return AdditionalDetails;
    }

    public void setAdditionalDetails(ArrayList<AdditionalDetails> additionalDetails) {
        AdditionalDetails = additionalDetails;
    }

    public Receipt getReceipt() {
        return Receipt;
    }

    public void setReceipt(Receipt receipt) {
        Receipt = receipt;
    }

    public Notes getNotes()
    {
        return notes;
    }

    public void setNotes(Notes notes)
    {
        this.notes = notes;
    }

    public Mileage getMileage()
    {
        return mileage;
    }

    public void setMileage(Mileage mileage)
    {
        this.mileage = mileage;
    }

    public ArrayList<Attendees> getAttendees()
    {
        return attendees;
    }

    public void setAttendees(ArrayList<Attendees> attendees)
    {
        this.attendees = attendees;
    }
}
