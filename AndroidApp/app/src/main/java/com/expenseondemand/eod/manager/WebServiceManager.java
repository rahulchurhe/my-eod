package com.expenseondemand.eod.manager;

import android.content.Intent;
import android.net.ParseException;
import android.util.Log;

import com.expenseondemand.eod.exception.EODAppException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.exception.EODServiceException;
import com.expenseondemand.eod.model.UserCredential;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.service.SilentLoginService;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.util.MultipartUtility;
import com.expenseondemand.eod.util.WebServiceUtil;
import com.expenseondemand.eod.webservice.model.MasterRequest;
import com.expenseondemand.eod.webservice.model.MasterResponse;
import com.expenseondemand.eod.webservice.model.http.Body;
import com.expenseondemand.eod.webservice.model.http.HTTPData;

import org.apache.http.params.CoreProtocolPNames;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class WebServiceManager
{
	//Get Request Object
	public static MasterRequest getRequestObject(MasterRequest requestObject)
	{
		//Place common parameters

		return requestObject;
	}

	//Hit Web Service
	public static Object callWebService(String baseUrl, String targetUrl, HTTPData httpData) throws EODException
	{
		Object response = requestServer(baseUrl, targetUrl, httpData);

		return response;
	}

	//Check Response Status
	public static void processResponseStatus(MasterResponse masterResponse) throws EODException
	{
		if(masterResponse == null)
		{
			//EodApp Exception
			throw new EODAppException(AppConstant.ERROR_CODE_1007);
		}
		else
		{
			boolean result = masterResponse.isSuccess();
			String message = masterResponse.getMessage();
			message = AppUtil.setStringNotNull(message);

			if((!result))
			{
				//Throw Exception
				throw new EODAppException(AppConstant.ERROR_CODE_1008, message);
			}
		}
	}

	/**
	 * Common Functions
	 */

	private static Object requestServer(String baseUrl, String targetUrl, HTTPData httpData) throws EODException
	{
		HttpURLConnection httpURLConnection = null;
		Object response = null;
		OutputStream outputStream;
		InputStream inputStream;
		int ch;

		try
		{
			System.setProperty("http.keepAlive", "false");
			URL url = new URL(baseUrl + targetUrl);
			httpURLConnection = (HttpURLConnection) url.openConnection();

			//Set Timeout for Connection and ReadData
			httpURLConnection.setConnectTimeout( AppConstant.TIME_CONNECTION_TIMEOUT); //Timeout until a connection is established
			httpURLConnection.setReadTimeout(AppConstant.TIME_CONNECTION_TIMEOUT); //Timeout for waiting for data to come


			WebServiceUtil.initializeHeaderInRequest(httpURLConnection, targetUrl, httpData.getType());

			httpURLConnection.setDoInput(true);

			if(httpData.getType() == AppConstant.HTTP_METHOD_POST)
			{
				httpURLConnection.setDoOutput(true);
				final String boundary = "-------------" + System.currentTimeMillis();


				Body body = httpData.getBody();
				boolean isMultipartRequest = body != null && !AppUtil.isCollectionEmpty(body.getMultipartFileName());

				if(isMultipartRequest)
				{
					httpURLConnection.setRequestProperty(AppConstant.REQUEST_HEADER_CONTENT_TYPE_KEY, AppConstant.CONTENT_TYPE_MULTIPART_DATA + boundary);
				}
				else
				{
					//make some HTTP header nicety
					httpURLConnection.setRequestProperty(AppConstant.REQUEST_HEADER_CONTENT_TYPE_KEY, AppConstant.CONTENT_TYPE_JSON);
				}

				//open
				httpURLConnection.connect();
				if(body != null) //safety check
				{
					//setup send
					outputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());

					Log.i(AppConstant.LOG_TAG, "REQUEST URL : " + (baseUrl + targetUrl) + "\nJSON REQUEST : " + body.getJsonRequestString());

					if (isMultipartRequest)
					{
						PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(outputStream, AppConstant.ENCODING_UTF_8), true);

						MultipartUtility multipartUtility = new MultipartUtility(outputStream, printWriter, AppConstant.ENCODING_UTF_8, boundary);
						multipartUtility.addFormField(AppConstant.REQUEST_DATA, body.getJsonRequestString());


						ArrayList<String> fileNameList = body.getMultipartFileName();
						if(!AppUtil.isCollectionEmpty(fileNameList))
						{
							for (String fileName : fileNameList)
							{
//								File file = new File(fileName.replace(" ",""));
								File file = new File(fileName);
								multipartUtility.addFilePart(AppConstant.REQUEST_CONTENT, file);
							}
						}

						printWriter.append(AppConstant.LINE_FEED).flush();
						printWriter.append(AppConstant.TWO_HYPHENS + boundary + AppConstant.TWO_HYPHENS).append(AppConstant.LINE_FEED);
						printWriter.close();
					}
					else
					{
						outputStream.write(body.getJsonRequestString().getBytes());
						//clean up
						outputStream.flush();
					}
				}
			}
			else
			{
				Log.i(AppConstant.LOG_TAG, "REQUEST URL : " + (baseUrl + targetUrl));

				httpURLConnection.setDoOutput(false);
			}


			//Get StatusCode
			int statusCode = httpURLConnection.getResponseCode();


			Log.i(AppConstant.LOG_TAG, "status: " + statusCode + " message "+httpURLConnection.getResponseMessage());

			//Check StatusCode
//			if(statusCode == HttpURLConnection.HTTP_UNAUTHORIZED)
//			{
//				//TODO
//			}
			 if(statusCode != HttpURLConnection.HTTP_OK||statusCode==HttpURLConnection.HTTP_UNAUTHORIZED)
			{
				String responseMessage = httpURLConnection.getResponseMessage();
				if (DeviceManager.isNetworkAvailable() && PersistentManager.isUserRemembered()) {
					UserInfo userInfo = CachingManager.getUserInfo();
					if (userInfo != null) {
						UserCredential userCredential = userInfo.getUserCredential();
						if (userCredential != null) {
							 userInfo = null;
							SharePreference.putBoolean(CachingManager.getAppContext(), "IS_LOGIN", true);
							try {
								//Authenticate User
								userInfo = AuthenticationManager.authenticateUser(userCredential);
								DbManager.saveUserInformation(userInfo);
							} catch (EODException eodException) {
							}
						}
					}
				}

				//Throw Exception
				throw new EODServiceException(statusCode, responseMessage);//
			}
			else
			{
				inputStream = new BufferedInputStream(httpURLConnection.getInputStream());

				if(httpData.getType() == AppConstant.HTTP_METHOD_GET_IMAGE)
				{
					response = writeImage(inputStream);
				}
				else
				{
					StringBuffer stringBuffer = new StringBuffer();
					while ((ch = inputStream.read()) != -1)
					{
						stringBuffer.append((char) ch);
					}

					response = stringBuffer.toString();

					Log.i(AppConstant.LOG_TAG, "JSON RESPONSE: " + response);
				}
			}
		}
		catch (UnsupportedEncodingException exception)
		{
			ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_2001, exception.getMessage(), exception);
		}
		catch (IOException exception)
		{
			ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_2004, exception.getMessage(), exception);
		}
		catch (ParseException exception)
		{
			ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_2005, exception.getMessage(), exception);
		}
		catch(EODException eodException)
		{
			ExceptionManager.dispatchExceptionDetails(eodException.getExceptionCode(), eodException.getExceptionMessage(), eodException);
		}
		catch(Exception exception)
		{
			ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_2006, exception.getMessage(), exception);
		}
		finally
		{
			httpURLConnection.disconnect();
		}

		return response;
	}







	private static boolean writeImage(InputStream inputStream) throws Exception
	{
		boolean retValue = false;

		File imageFile = DeviceManager.getImageFile(AppConstant.DIRECTORY_NAME_TEMP, AppConstant.FILE_NAME_TEMP);
		OutputStream output = new FileOutputStream(imageFile);

		try
		{
			byte[] buffer = new byte[1024];
			int bytesRead = 0;
			while ((bytesRead = inputStream.read(buffer, 0, buffer.length)) >= 0)
				output.write(buffer, 0, bytesRead);
			retValue = true;
		}
		finally
		{
			output.close();
		}

		return retValue;
	}
}
