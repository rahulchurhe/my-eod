package com.expenseondemand.eod.webservice.model.authentication.login;

import com.expenseondemand.eod.webservice.model.MasterResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)

public class AuthenticateUserResponse extends MasterResponse
{
	private Data data;

	public Data getData()
	{
		return data;
	}

	public void setData(Data data)
	{
		this.data = data;
	}
}
