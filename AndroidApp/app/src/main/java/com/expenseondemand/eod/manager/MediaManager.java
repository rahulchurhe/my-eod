package com.expenseondemand.eod.manager;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MediaManager {
    public static Bitmap getScaledBitmap(boolean isCameraSelect, String filePath, Uri uri) {
        Bitmap bitmap = null;
        if (isCameraSelect) {
            //get camera scaled bitmap any android version
            bitmap = getScaledDownBitmapFromFilePath(filePath);
        } else {
            try {
                //get gallery scaled bitmap any android version
                bitmap = getScaledDownBitmapFromUri(CachingManager.getAppContext(), uri, AppConstant.IMAGE_STANDARD_WIDTH, AppConstant.IMAGE_STANDARD_HEIGHT);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return bitmap;
    }

    //Camera
    public static Bitmap getScaledDownBitmapFromFilePath(String filePath) {
        BitmapFactory.Options options = getBitmapOptions();
        BitmapFactory.decodeFile(filePath, options);

        int width = options.outWidth;
        int height = options.outHeight;

        options = getBitmapOptions(height, width, AppConstant.IMAGE_STANDARD_HEIGHT, AppConstant.IMAGE_STANDARD_WIDTH);
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

        Matrix matrix = new Matrix();
        matrix.postRotate(getImageOrientation(filePath));
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        return rotatedBitmap;
    }
    public static int getImageOrientation(String imagePath) {
        int rotate = 0;
        try {
            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rotate;
    }

    //Gallery
    public static Bitmap getScaledDownBitmapFromUri(Context activity, Uri uri, int requiredWidth, int requiredHeight) throws FileNotFoundException {
        Bitmap scaledBitmap = null;
        try {
            scaledBitmap = scaleImage(activity, uri, requiredWidth, requiredHeight);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return scaledBitmap;
    }
    public static Bitmap scaleImage(Context context, Uri photoUri, int requiredWidth, int requiredHeight) throws IOException {
        Bitmap bitmap = null;
        final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
        try {
            InputStream is = context.getContentResolver().openInputStream(photoUri);
            BitmapFactory.Options dbo = new BitmapFactory.Options();
            dbo.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(is, null, dbo);
            is.close();

            //Get Image Orientation.
            int rotatedWidth, rotatedHeight;
            int orientation = getOrientation(context, photoUri);
            if (orientation == 90 || orientation == 270) {
                rotatedWidth = dbo.outHeight;
                rotatedHeight = dbo.outWidth;
            } else {
                rotatedWidth = dbo.outWidth;
                rotatedHeight = dbo.outHeight;
            }

            //Reduce size of bitmap
            int scale = 1;
            while ((dbo.outWidth * dbo.outHeight) * (1 / Math.pow(scale, 2)) > IMAGE_MAX_SIZE) {
                scale++;
            }

            Bitmap srcBitmap;
            is = context.getContentResolver().openInputStream(photoUri);

            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                dbo = new BitmapFactory.Options();
                dbo.inSampleSize = scale;
                srcBitmap = BitmapFactory.decodeStream(is, null, dbo);

                // resize to desired dimensions
                int height = srcBitmap.getHeight();
                int width = srcBitmap.getWidth();


                double y = Math.sqrt(IMAGE_MAX_SIZE / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(srcBitmap, (int) x, (int) y, true);
                srcBitmap.recycle();
                srcBitmap = scaledBitmap;

                System.gc();
            } else {
                srcBitmap = BitmapFactory.decodeStream(is);
            }

            is.close();

        /*
         * if the orientation is not 0 (or -1, which means we don't know), we
         * have to do a rotation.
         */
            if (orientation > 0) {
                Matrix matrix = new Matrix();
                matrix.postRotate(orientation);
                srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(), srcBitmap.getHeight(), matrix, true);
            }

            String type = context.getContentResolver().getType(photoUri);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            if (type.equals("image/png")) {
                srcBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            } else if (type.equals("image/jpg") || type.equals("image/jpeg")) {
                srcBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            }

            byte[] bMapArray = baos.toByteArray();
            baos.close();
            bitmap = BitmapFactory.decodeByteArray(bMapArray, 0, bMapArray.length);

            return bitmap;
        } catch (Exception e) {
            //Some device get exception if use above try block code which handle on catch block.
            InputStream inputStream = null;
            BitmapFactory.Options options = getBitmapOptions();
            inputStream = context.getContentResolver().openInputStream(photoUri);
            BitmapFactory.decodeStream(inputStream, null, options);

            int width = options.outWidth;
            int height = options.outHeight;

            options = getBitmapOptions(height, width, requiredWidth, requiredHeight);
            inputStream = context.getContentResolver().openInputStream(photoUri);
            bitmap = BitmapFactory.decodeStream(inputStream, null, options);
        }
        return bitmap;
    }
    public static int getOrientation(Context context, Uri photoUri) {
        /* it's on the external media. */
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);

        if (cursor.getCount() != 1) {
            return -1;
        }

        cursor.moveToFirst();
        return cursor.getInt(0);
    }


    public static void writeBitmapTofFile(File file, Bitmap bitmap) {
        OutputStream out = null;
        try {
            out = new FileOutputStream(file);
//			bitmap.compress(Bitmap.CompressFormat.WEBP, 100, out);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                }
            }
        }
    }

    public static void writeBitmapTofFile1(File file, Bitmap bitmap) {
        OutputStream out = null;
        try {
            out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            //bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                }
            }
        }
    }

    public static Bitmap getScaledDownBitmapFromFilePath(String filePath, int requiredHeight, int requiredWidth) {
        BitmapFactory.Options options = getBitmapOptions();
        BitmapFactory.decodeFile(filePath, options);

        int width = options.outWidth;
        int height = options.outHeight;

        options = getBitmapOptions(height, width, requiredHeight, requiredWidth);
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

        return bitmap;
    }

    private static BitmapFactory.Options getBitmapOptions() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        return options;
    }

    private static BitmapFactory.Options getBitmapOptions(int height, int width, int requiredHeight, int requiredWidth) {

        BitmapFactory.Options options = new BitmapFactory.Options();

        int inSampleSize = 1;

        if (height > requiredHeight || width > requiredWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while (((halfHeight / inSampleSize) > requiredHeight) && ((halfWidth / inSampleSize) > requiredWidth))
                inSampleSize *= 2;
        }

        options.inSampleSize = inSampleSize;
        options.inJustDecodeBounds = false;

        return options;
    }

    public static Bitmap getBitmapFromString(String imageString, Context context) {
        Bitmap bitmap = null;

        if (!AppUtil.isStringEmpty(imageString)) {
            byte[] imageBytes = decodeImage(imageString);

            if (imageBytes != null) {
                bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
            }
        }

        return bitmap;
    }

    public static byte[] decodeImage(String imageString) {
        return Base64.decode(imageString, Base64.DEFAULT);
    }

    public static Drawable getDrawableFromString(String imageString, Context context) {
        byte[] imageBytes = decodeImage(imageString);
        Drawable drawable = null;
        if (imageBytes != null) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
            drawable = new BitmapDrawable(bitmap);
        }
        return drawable;
    }

    public static String encodeTobase64(Bitmap bitmap) {
        String base64String = null;
        if (bitmap != null) {
            byte[] byteArray = AppUtil.getByteArray(bitmap);
            base64String = Base64.encodeToString(byteArray, Base64.DEFAULT);
        } else {
            base64String = "";
        }
        return base64String;
    }

    public static File getReceiptPathFormPermanentStorage(String description) {
        File fileObject = null;
        File folderFileObject = DeviceManager.getFolderOnExternalDirectory(AppConstant.DIRECTORY_NAME_PROJECT_DETAIL_RECEIPT);

        if (folderFileObject != null) {
            fileObject = new File(folderFileObject, description);
        }

        return fileObject;
    }

    //Reduce bitmap size
    private static Bitmap getBitmap(Context context, Uri photoUri) {

        //Uri uri = getImageUri(path);
        InputStream in = null;
        try {
            final int IMAGE_MAX_SIZE = 5000000; // 1.2MP
            in = context.getContentResolver().openInputStream(photoUri);
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();

            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) > IMAGE_MAX_SIZE) {
                scale++;
            }

            Bitmap b = null;
            in = context.getContentResolver().openInputStream(photoUri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);

                // resize to desired dimensions
                int height = b.getHeight();
                int width = b.getWidth();


                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x,
                        (int) y, true);
                b.recycle();
                b = scaledBitmap;

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            in.close();


            return b;
        } catch (IOException e) {

            return null;
        }
    }
}
