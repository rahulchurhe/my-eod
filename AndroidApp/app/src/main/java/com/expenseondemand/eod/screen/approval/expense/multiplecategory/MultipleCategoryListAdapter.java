package com.expenseondemand.eod.screen.approval.expense.multiplecategory;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.model.approval.expense.ExpenseInfo;
import com.expenseondemand.eod.model.approval.expense.PolicyBreachModel;
import com.expenseondemand.eod.model.approval.expense.multiplecategory.MultiCategoryExpense;
import com.expenseondemand.eod.model.approval.expense.PolicyDetails;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Nandani Gupta on 2016-09-13.
 */
public class MultipleCategoryListAdapter extends RecyclerView.Adapter<MultipleCategoryListAdapter.MyViewHolder> {

    private final ArrayList<MultiCategoryExpense> multipleCategoryExpenseList;
    private final MultipleCategoryExpenseListInterface multipleCategoryExpenseListInterface;
    private Context activityContext;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeClaimantExpenseItem;
        ImageView imageViewExpenseCategoryIcon;
        ImageView imageViewAttachmentIcon;
        ImageView imageViewViolation;
        TextView textViewExpenseCategory;
        TextView textViewExpenseCreationDate;
        TextView textViewExpenseAmount;
        ImageView imageViewRightArrow, imageViewDuplicate, imageViewEscalation;


        public MyViewHolder(View view) {
            super(view);
            relativeClaimantExpenseItem = (RelativeLayout) view.findViewById(R.id.relativeClaimantExpenseItem);
            imageViewExpenseCategoryIcon = (ImageView) view.findViewById(R.id.imageViewExpenseCategoryIcon);
            imageViewAttachmentIcon = (ImageView) view.findViewById(R.id.imageViewAttachmentIcon);
            imageViewViolation = (ImageView) view.findViewById(R.id.imageViewViolation);
            imageViewDuplicate = (ImageView) view.findViewById(R.id.imageViewDuplicate);
            textViewExpenseCategory = (TextView) view.findViewById(R.id.textViewExpenseCategory);
            textViewExpenseCreationDate = (TextView) view.findViewById(R.id.textViewExpenseCreationDate);
            textViewExpenseAmount = (TextView) view.findViewById(R.id.textViewExpenseAmount);
            imageViewRightArrow = (ImageView) view.findViewById(R.id.imageViewRightArrow);
            imageViewEscalation = (ImageView) view.findViewById(R.id.imageViewEscalation);

            textViewExpenseCategory.setSelected(true);

        }
    }

    public MultipleCategoryListAdapter(Context activityContext, ArrayList<MultiCategoryExpense> multipleCategoryExpenseList, MultipleCategoryExpenseListInterface multipleCategoryExpenseListInterface) {
        this.activityContext = activityContext;
        this.multipleCategoryExpenseList = multipleCategoryExpenseList;
        this.multipleCategoryExpenseListInterface = multipleCategoryExpenseListInterface;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.multiple_category_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder viewHolder, int position) {

        viewHolder.relativeClaimantExpenseItem.setTag(position);
        viewHolder.imageViewDuplicate.setTag(position);


        //Expense Category
        viewHolder.textViewExpenseCategory.setText(multipleCategoryExpenseList.get(position).getExpneseCategory());

        //date
        long date = AppUtil.getLongDate(AppConstant.FORMAT_DATE_WEB_SERVICE, multipleCategoryExpenseList.get(position).getDate());
        String formattedDate = AppUtil.getFormattedDateString(AppConstant.FORMAT_DATE_DISPLAY, date);
        viewHolder.textViewExpenseCreationDate.setText(formattedDate);

        //Expense Amount
        viewHolder.textViewExpenseAmount.setText(new DecimalFormat("0.00").format(Double.parseDouble(multipleCategoryExpenseList.get(position).getAmount())));

        //Expense Category Icon
        int resourceId = CachingManager.getCategoryResourceId(Integer.parseInt(multipleCategoryExpenseList.get(position).getBaseCategoryId()));
        viewHolder.imageViewExpenseCategoryIcon.setImageResource(resourceId);

        //Clicks
        viewHolder.relativeClaimantExpenseItem.setOnClickListener(new MultipleCategoryExpenseListClickListener());
        viewHolder.imageViewDuplicate.setOnClickListener(new MultipleCategoryExpenseListClickListener());


        //Set Colors
        setPolicyBreechIcons(viewHolder, multipleCategoryExpenseList.get(position).getPolicyBreech(), multipleCategoryExpenseList.get(position));
        setPolicyBreechTextColor(viewHolder, multipleCategoryExpenseList.get(position).getPolicyBreech(), multipleCategoryExpenseList.get(position));
        setPolicyBreechBackground(viewHolder, multipleCategoryExpenseList.get(position).getPolicyBreech(), multipleCategoryExpenseList.get(position));
    }

    private void setPolicyBreechIcons(MyViewHolder viewHolder, PolicyBreachModel policyDetails, MultiCategoryExpense multiCategoryExpense) {
        if (policyDetails.isExpenseLimit()) {
            viewHolder.imageViewEscalation.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imageViewEscalation.setVisibility(View.INVISIBLE);
        }

        if (multiCategoryExpense.isDuplicate()) {
            viewHolder.imageViewDuplicate.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imageViewDuplicate.setVisibility(View.INVISIBLE);
        }


        if (policyDetails != null) {
            try {
                if (policyDetails.isDailyCap() || policyDetails.isMaxSpendBreach()) {
                    viewHolder.imageViewViolation.setVisibility(View.VISIBLE);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    void setPolicyBreechTextColor(MyViewHolder viewHolder, PolicyBreachModel policyDetails, MultiCategoryExpense multiCategoryExpense) {
        if (multiCategoryExpense.isDuplicate()) {
            viewHolder.textViewExpenseCategory.setTextColor(activityContext.getResources().getColor(R.color.text_color_violet));
            viewHolder.textViewExpenseCreationDate.setTextColor(activityContext.getResources().getColor(R.color.text_color_violet));
            viewHolder.textViewExpenseAmount.setTextColor(activityContext.getResources().getColor(R.color.text_color_violet));

        }
    }

    void setPolicyBreechBackground(MyViewHolder viewHolder, PolicyBreachModel policyBreachModel, MultiCategoryExpense multiCategoryExpense) {

        if (policyBreachModel.isExpenseLimit()) {
            viewHolder.relativeClaimantExpenseItem.setBackgroundResource(R.drawable.wheat_background_selector);
        } else if (policyBreachModel.isPreApproval()) {
            viewHolder.relativeClaimantExpenseItem.setBackgroundResource(R.drawable.cream_backbgrund_selector);
        } else if (multiCategoryExpense.isRejected()) {
            viewHolder.relativeClaimantExpenseItem.setBackgroundResource(R.drawable.light_green_background_selector);
        }
    }

    @Override
    public int getItemCount() {
        return multipleCategoryExpenseList.size();
    }

    private class MultipleCategoryExpenseListClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            int viewId = view.getId();
            int clickPosition;

            switch (viewId) {
                case R.id.relativeClaimantExpenseItem:
                    clickPosition = (int) view.getTag();
                    multipleCategoryExpenseListInterface.handleExpenseRowClick(clickPosition);
                    break;

                case R.id.imageViewDuplicate:
                    clickPosition = (int) view.getTag();
                    multipleCategoryExpenseListInterface.handleDuplicateClick(clickPosition);
                    break;
            }
        }
    }
}