package com.expenseondemand.eod.screen.claimant.create;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.FileProvider;
import android.text.InputFilter;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.expenseondemand.eod.BuildConfig;
import com.expenseondemand.eod.R;
import com.expenseondemand.eod.activity.BaseActivity;
import com.expenseondemand.eod.dialogFragment.ApplicationAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationErrorAlertDialog;
import com.expenseondemand.eod.dialogFragment.CameraAlertDialog;
import com.expenseondemand.eod.dialogFragment.PhotoPickerDialog;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.CutCopyPasteEditText;
import com.expenseondemand.eod.manager.DecimalDigitsInputFilter;
import com.expenseondemand.eod.manager.DeviceManager;
import com.expenseondemand.eod.manager.ExpenseManager;
import com.expenseondemand.eod.manager.MediaManager;
import com.expenseondemand.eod.manager.PermissionManager;
import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.manager.ValidationManager;
import com.expenseondemand.eod.model.Expense;
import com.expenseondemand.eod.model.ReceiptImage;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.model.ValidationError;
import com.expenseondemand.eod.screen.claimant.category.ExpenseCategoryActivity;
import com.expenseondemand.eod.screen.claimant.upload.UploadExpenseActivity;
import com.expenseondemand.eod.util.AlertDialogPermissionBoxClickInterface;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.util.UIUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CreateEditExpenseActivity extends BaseActivity implements CameraAlertDialog.CameraAlertDialogActionListener, OnClickListener, PhotoPickerDialog.PhotoPickerClickListener, ApplicationAlertDialog.AlertDialogActionListener, ApplicationErrorAlertDialog.ErrorDialogActionListener {

    public static final int TYPE_SAVE = 0;
    public static final int TYPE_ERROR = -1;
    public static final int TYPE_CREATE = 1;
    ApplicationErrorAlertDialog applicationErrorAlertDialog;
    private boolean isEditExpense = false;
    private TextView textViewExpenseCategory;
    private TextView textViewExpenseDate;
    private EditText editTextExpenseAmount;
    private CutCopyPasteEditText editTextExpenseNotes;
    private ImageView imageViewReceipt;
    private ProgressBar progressBar;
    private TextView textViewAttachReceipt;
    private ApplicationAlertDialog applicationAlertDialog;
    private int existingExpenseId; //When Coming from Edit Expense
    private int expenseCategoryId;
    private int expenseBaseCategoryId;
    private int resourceId;
    private String expenseReceiptFilePath;

    private final int REQUEST_CAMERA = 1001;
    private final int REQUEST_GALLERY = 1002;
    private boolean cameraSelected;
    private CameraAlertDialog cameraAlertDialog;
    private Bitmap selectedBitmap = null;
    private Bitmap checkBitmapVailability = null;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //Back to the Upload expense activity
        SharePreference.putBoolean(getApplicationContext(), AppConstant.BACK_CREATE_EXPENSE, true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (checkBitmapVailability == null) {
            imageViewReceipt.setVisibility(View.GONE);
            textViewAttachReceipt.setText(getString(R.string.button_attach_receipt));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_edit_expense_activity_layout);
        //Initialize View
        initializeView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Get Menu Inflater
        MenuInflater inflater = getMenuInflater();

        //Inflate Menu
        inflater.inflate(R.menu.create_edit_expense_menu, menu);

        return true;
    }


    //Hide Soft Keyboard when-ever user touches the area outside the EditText
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View view = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (view instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                UIUtil.hideSoftKeyboard(w);
            }
        }

        return ret;
    }

    //Initialize View
    private void initializeView() {
        //Get View References
        textViewExpenseCategory = (TextView) findViewById(R.id.textViewCategory);
        textViewExpenseDate = (TextView) findViewById(R.id.textViewExpenseDate);
        editTextExpenseNotes = (CutCopyPasteEditText) findViewById(R.id.editTextNotes);
        imageViewReceipt = (ImageView) findViewById(R.id.imageViewReceipt);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        editTextExpenseAmount = (EditText) findViewById(R.id.editTextExpenseAmount);
        editTextExpenseAmount.setFilters(new InputFilter[]{new DecimalDigitsInputFilter()});

        // View AttachReceipt
        textViewAttachReceipt = (TextView) findViewById(R.id.textViewAttachReceipt);
        ImageView imageViewAttachReceipt = (ImageView) findViewById(R.id.imageViewAttachReceipt);
        imageViewAttachReceipt.setOnClickListener(this);

        //Set OnClick Listeners to Category and Date
        textViewExpenseCategory.setOnClickListener(this);
        textViewExpenseDate.setOnClickListener(this);
        imageViewReceipt.setOnClickListener(this);

        //Get Intent
        Intent intent = getIntent();

        if (intent != null) {
            //Get isEditExpense Flag from Intent
            isEditExpense = intent.getBooleanExtra(AppConstant.KEY_IS_EDIT_EXPENSE, false);
        }

        //Get Screen Title
        String screenTitle = getScreenTitle();

        //Handle Toolbar Bar
        handleToolBar(screenTitle, R.color.home_item_create_expense_selected, R.color.home_item_create_expense, true);

        if (isEditExpense) {
            handleInitializeEditExpense();
        } else {
            handleInitializeCreateExpense();
        }

        //Paste text into the edit text
        editTextExpenseNotes.setOnCutCopyPasteListener(new CutCopyPasteEditText.OnCutCopyPasteListener() {
            @Override
            public void onCut() {
            }

            @Override
            public void onCopy() {
            }

            @Override
            public void onPaste() {
                // Do your onPaste reactions
                editTextExpenseNotes.setText(crunchNumber(editTextExpenseNotes.getText().toString()));
            }
        });
    }

    //Create string buffer for removed all invisible text which attached with paste string.
    public static String crunchNumber(final String inString) {
        StringBuffer buffer = new StringBuffer(inString.length());
        for (int i = 0; i < inString.length(); i++) {
            char c = inString.charAt(i);
            if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == ',' || c == ' ') {
                buffer.append(c);
            }
        }
        return buffer.toString();
    }

    //Handle Create Expense
    private void handleInitializeCreateExpense() {

        //Get Current Date
        String currentDateString = AppUtil.getCurrentFormattedDate(AppConstant.FORMAT_DATE_DISPLAY);

        //Set Date in textView
        textViewExpenseDate.setText(currentDateString);

        //Attach Image Text
        textViewAttachReceipt.setText(getString(R.string.button_attach_receipt));
    }

    //Handle Edit Expense
    private void handleInitializeEditExpense() {
        //Get Intent
        Intent intent = getIntent();

        //get Parcel
        Expense expense = intent.getParcelableExtra(AppConstant.KEY_EXPENSE_OBJECT);

        if (expense != null) {
            existingExpenseId = expense.getExpenseId();
            expenseCategoryId = expense.getExpenseCategoryId();
            expenseBaseCategoryId = expense.getExpenseBaseCategoryId();

            //Set Date
            String dateString = AppUtil.getFormattedDateString(AppConstant.FORMAT_DATE_DISPLAY, expense.getExpenseDate());
            textViewExpenseDate.setText(dateString);

            //Set Category Name
            textViewExpenseCategory.setText(expense.getExpenseCategoryName());

            resourceId = CachingManager.getCategoryResourceId(expenseBaseCategoryId);

            //Set Category Icon
            textViewExpenseCategory.setCompoundDrawablesWithIntrinsicBounds(resourceId, 0, R.drawable.arrow_icon_home, 0);

            //Set Amount
            editTextExpenseAmount.setText(expense.getExpenseAmount());

            //Set Notes
            editTextExpenseNotes.setText(expense.getExpenseNotes());

            //Has Image
            boolean hasReceiptImage = expense.isHasReceipt();

            if (hasReceiptImage) {
                expenseReceiptFilePath = expense.getReceiptImage().getReceiptImageFileName();
                //Display Image
                displayReceiptImage();
            }
        }

        //Refresh Options Menu
        ActivityCompat.invalidateOptionsMenu(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuItemId = item.getItemId();

        switch (menuItemId) {

            case R.id.menuSave:

                //Handle Save/Edit Expense
                handleSaveExpense();

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);

        if (expenseReceiptFilePath != null)
            bundle.putString(AppConstant.KEY_EXPENSE_RECEIPT_IMAGE_PATH, expenseReceiptFilePath);

        bundle.putInt(AppConstant.KEY_EXPENSE_CATEGORY_ID, expenseCategoryId); //Expense Category Id
        bundle.putInt(AppConstant.KEY_EXPENSE_BASE_CATEGORY_ID, expenseBaseCategoryId); //Expense Base Category Id

        bundle.putString(AppConstant.KEY_CATEGORY_NAME, textViewExpenseCategory.getText().toString()); //Save Category Name
        bundle.putString(AppConstant.KEY_EXPENSE_DATE, textViewExpenseDate.getText().toString()); //Save Expense Date
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null) {
            expenseReceiptFilePath = savedInstanceState.getString(AppConstant.KEY_EXPENSE_RECEIPT_IMAGE_PATH);
            expenseCategoryId = savedInstanceState.getInt(AppConstant.KEY_EXPENSE_CATEGORY_ID);
            expenseBaseCategoryId = savedInstanceState.getInt(AppConstant.KEY_EXPENSE_BASE_CATEGORY_ID);

            textViewExpenseCategory.setText(savedInstanceState.getString(AppConstant.KEY_CATEGORY_NAME)); //Set Category Name
            resourceId = CachingManager.getCategoryResourceId(savedInstanceState.getInt(AppConstant.KEY_EXPENSE_BASE_CATEGORY_ID));

            //Set Category Icon
            textViewExpenseCategory.setCompoundDrawablesWithIntrinsicBounds(resourceId, 0, R.drawable.arrow_icon_home, 0);

            textViewExpenseDate.setText(savedInstanceState.getString(AppConstant.KEY_EXPENSE_DATE)); //Set Expense Date

            //Display Image
            displayReceiptImage();
        }
    }

    @Override
    public void onClick(View view) {
        //Get View ID
        int viewId = view.getId();

        switch (viewId) {
            case R.id.textViewExpenseDate:
                //Expense Date
                handleExpenseDate();
                break;

            case R.id.textViewCategory:
                //Category
                handleCategory();
                break;

            case R.id.imageViewAttachReceipt:
                //Attach Receipt
                handleAttachReceipt();
                break;

            case R.id.imageViewReceipt:
                //Show Attach Receipt
                showAttachedReceipt();
                break;
        }
    }

    private void showAttachedReceipt() {
        UIUtil.showAttachedReceiptDialog(selectedBitmap, CreateEditExpenseActivity.this);
    }

    //Handle Save/Edit Expense
    private void handleSaveExpense() {
        String expenseCategoryName = textViewExpenseCategory.getText().toString().trim(); //Category Name
        String expenseAmount = editTextExpenseAmount.getText().toString(); //Expense Amount
        String expenseNote = editTextExpenseNotes.getText().toString(); //Expense Amount

        ValidationError validationError = ValidationManager.validateExpenseDetails(expenseCategoryName, expenseAmount, expenseNote);

        if (validationError != null) {
            //Show Alert Dialog
            showAlertDialog(true, validationError.getValidationMessage(), null, null, TYPE_ERROR);

        } else {
            //Prepare Expense Object
            Expense expense = prepareExpenseObject();

            //Save Expense
            SaveExpenseAsyncTask ayncTask = new SaveExpenseAsyncTask();
            ayncTask.execute(expense);
        }
    }

    //Prepare Expense Object
    private Expense prepareExpenseObject() {
        boolean hasReceipt;
        String expenseCategoryName = textViewExpenseCategory.getText().toString().trim(); //Category Name
        String expenseDateString = textViewExpenseDate.getText().toString();
        long expenseDate = AppUtil.getLongDate(AppConstant.FORMAT_DATE_DISPLAY, expenseDateString); //Expense Date
        String expenseAmount = getExpenseAmount(); //Expense Amount

        String expenseNotes = editTextExpenseNotes.getText().toString().trim(); //Expense Amount


        if (checkBitmapVailability != null) {
            hasReceipt = !AppUtil.isStringEmpty(expenseReceiptFilePath); //has Receipt
        } else {
            hasReceipt = false;
        }


        //Receipt Image
        ReceiptImage receiptImage = new ReceiptImage();
        receiptImage.setReceiptImageFileName(expenseReceiptFilePath);

        String userId = null;
        String currencyCode = null;

        //Get UserInfo from cache
        UserInfo userInfo = CachingManager.getUserInfo();

        if (userInfo != null) {
            userId = userInfo.getUserId();
            currencyCode = userInfo.getDefaultCurrencyCode().substring(userInfo.getDefaultCurrencyCode().indexOf("-") + 1, userInfo.getDefaultCurrencyCode().length()).replace(" ", "");
        }

        //Create Expense
        Expense tempExpense = new Expense();

        if (isEditExpense) {
            tempExpense.setExpenseId(existingExpenseId); //Existing ExpenseID
        }

        tempExpense.setUserId(userId); //UserId
        tempExpense.setExpenseCategoryId(expenseCategoryId); //Category Id
        tempExpense.setExpenseBaseCategoryId(expenseBaseCategoryId); //Category Id
        tempExpense.setExpenseCategoryName(expenseCategoryName); //Category Name
        tempExpense.setExpenseDate(expenseDate); //Expense Date
        tempExpense.setExpenseAmount(expenseAmount); //Expense Amount
        tempExpense.setCurrencyCode(currencyCode); //Currency Code
        tempExpense.setExpenseNotes(expenseNotes); //Expense Notes
        tempExpense.setHasReceipt(hasReceipt); //Has Receipt
        tempExpense.setReceiptImage(receiptImage); //Receipt Image

        return tempExpense;
    }

    protected String getExpenseAmount() {
        String expenseAmount = null;
        expenseAmount = AppUtil.formatToDouble(editTextExpenseAmount.getText().toString());

        return expenseAmount;
    }

    //Expense Date
    private void handleExpenseDate() {
        //Get Date String from TextView
        String dateString = textViewExpenseDate.getText().toString();

        //Get Long Date
        long longDate = AppUtil.getLongDate(AppConstant.FORMAT_DATE_DISPLAY, dateString);

        //Get Calender
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTimeInMillis(longDate);

        //Create Intent for CalenderActivity
        Intent intent = new Intent(CreateEditExpenseActivity.this, CalendarActivity.class);
        intent.putExtra(AppConstant.KEY_EXPENSE_DATE, calendar);

        startActivityForResult(intent, AppConstant.REQUEST_SELECT_DATE);
        overridePendingTransition(R.anim.slide_in_forward, R.anim.slide_out_forward);
    }

    //Category
    private void handleCategory() {
        //Navigate to Category Activity
        navigateToSelectCategoryActivity();
    }

    //Handle Gallery
    private void handleAttachReceipt() {
        //Show Dialog
        showAttachReceiptDialog();
    }

    //Handle Gallery
    @Override
    public void handleGallery() {
        boolean handleRequestForPermission = PermissionManager.handleRequestForPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE,
                REQUEST_GALLERY,
                getResources().getString(R.string.label_permission_gallery_cancelled),
                getResources().getString(R.string.label_permission_dialog));
        if (handleRequestForPermission) {
            checkBitmapVailability = null;
            openGallery();

        }

    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, AppConstant.REQUEST_SELECT_IMAGE_FROM_GALLERY);
    }

    //Handle Gallery
    @Override
    public void handleCamera() {
        if (PermissionManager.handleRequestForPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE, REQUEST_CAMERA,
                getResources().getString(R.string.label_permission_dialog),
                getResources().getString(R.string.label_permission_dialog))) {
            checkBitmapVailability = null;
            openCamera();

        }
    }

    private void openCamera() {

        try {
            Uri expenseReceiptUri;
            File expenseReceiptFile = createOrEditFile();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                expenseReceiptUri = FileProvider.getUriForFile(CreateEditExpenseActivity.this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        expenseReceiptFile);
            } else {
                expenseReceiptUri = Uri.fromFile(expenseReceiptFile);
            }
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, expenseReceiptUri);
            startActivityForResult(intent, AppConstant.REQUEST_CAPTURE_IMAGE);
            overridePendingTransition(R.anim.slide_in_forward, R.anim.slide_out_forward);
        } catch (Exception e) {
            //alert dialog for handle exception on 4.2.2 device
            showCameraAlertDialog(false, getString(R.string.text_camera_not_working), getString(R.string.text_camera_not_working_Alert), getString(R.string.button_ok), getString(R.string.button_no), TYPE_CREATE);
        }

    }

    //Handle Gallery
    @Override
    public void handleCancel() {
    }

    private File createOrEditFile() {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        File expenseReceiptFile = DeviceManager.getImageFile(AppConstant.DIRECTORY_NAME_RECEIPT_IMAGES, timeStamp + AppConstant.RECEIPT_EXTENSION);
        expenseReceiptFilePath = expenseReceiptFile.getAbsolutePath();
        expenseReceiptFile.deleteOnExit();
        return expenseReceiptFile;
    }

    private void removeFile() {
        if (!AppUtil.isStringEmpty(expenseReceiptFilePath)) {
            File file = new File(expenseReceiptFilePath);
            try {
                file.delete();
            } catch (SecurityException e) {
            }
        }
    }

    //Navigate to Category Activity
    private void navigateToSelectCategoryActivity() {
        //Create Intent
        Intent intent = new Intent(this, ExpenseCategoryActivity.class);

        //Start Activity for Result
        startActivityForResult(intent, AppConstant.REQUEST_SELECT_CATEGORY);

        overridePendingTransition(R.anim.slide_in_forward, R.anim.slide_out_forward);
    }

    //Gets the screen Title for Create and Edit.
    private String getScreenTitle() {
        String title = (isEditExpense) ? getString(R.string.title_edit_expense) : getString(R.string.text_create_expense);
        return title;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case AppConstant.REQUEST_SELECT_CATEGORY: {
                    updateViewWithExpenseCategory(data);
                    break;
                }

                //get selected date result from calendar activity
                case AppConstant.REQUEST_SELECT_DATE: {
                    updateViewWithExpenseDate(data);
                    break;
                }

                case AppConstant.REQUEST_SELECT_IMAGE_FROM_GALLERY: {
                    onGalleryPhotoSelected(data);
                    break;
                }
                case AppConstant.REQUEST_CAPTURE_IMAGE: {
                    onCameraPhotoSelected();
                    break;
                }
            }
        } else//Failure
        {
            if (requestCode == AppConstant.REQUEST_CAPTURE_IMAGE && !isEditExpense)
                removeFile();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        handlePermissionResult(requestCode, permissions[0], grantResults[0]);
    }

    /**
     * Handle Permission Result
     * Method to perform task
     *
     * @param requestCode
     * @param grantResults
     */
    private void handlePermissionResult(int requestCode, String permission, int grantResults) {
        if (grantResults == PackageManager.PERMISSION_DENIED) {
            boolean shouldShowRequestPermissionRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
            if (!shouldShowRequestPermissionRationale)    //It means user has denied the permission request with "Never ask again".
            {
                //if permission mandatory then Show dialog for redirecting to settings because last time user has denied the request by checking "Never ask again" check box.
                OnPermissionRationaleClickListener alertDialogBoxClickListener = new OnPermissionRationaleClickListener();
                UIUtil.showCustomConfirmDialog(this, getString(R.string.label_permission_dialog), getString(R.string.button_settings), getString(R.string.text_cancel), alertDialogBoxClickListener);
                //else show warning that you have denied the particular permission so we can't access this feature.
            } else
                UIUtil.showToastNotification(this, getResources().getString(R.string.label_permission_dialog), null, true);

        } else {
            //Do whatever u want to perform action when permission is granted
            switch (requestCode) {
                case REQUEST_CAMERA:
                    openCamera();
                    break;

                case REQUEST_GALLERY:
                    openGallery();
                    break;
            }
        }
    }

    //Populate Expense Category in TextView
    private void updateViewWithExpenseCategory(Intent data) {
        if (data != null) {
            //Get Category Name
            String expenseCatgoryName = data.getStringExtra(AppConstant.KEY_CATEGORY_NAME);
            expenseCatgoryName = AppUtil.setStringNotNull(expenseCatgoryName);

            //Set in TextView
            textViewExpenseCategory.setText(expenseCatgoryName);

            //Get Category Code
            expenseCategoryId = data.getIntExtra(AppConstant.KEY_CATEGORY_CODE, 0);
            //Get Base Category Code
            expenseBaseCategoryId = data.getIntExtra(AppConstant.KEY_BASE_CATEGORY_CODE, 0);
            resourceId = CachingManager.getCategoryResourceId(expenseBaseCategoryId);
            //While displaying Category Icon also add right navigation arrow
            textViewExpenseCategory.setCompoundDrawablesWithIntrinsicBounds(resourceId, 0, R.drawable.arrow_icon_home, 0);
        }
    }

    //Populate Expense Date in TextView
    private void updateViewWithExpenseDate(Intent data) {
        @SuppressWarnings("unchecked")
        ArrayList<Date> selectedCalendarDates = (ArrayList<Date>) data.getSerializableExtra(AppConstant.KEY_SELECTED_CALENDAR_DATES);
        if (selectedCalendarDates != null && selectedCalendarDates.size() == 1) {
            Date date = selectedCalendarDates.get(0);
            if (date != null) {
                //Get Formatted Date String
                String dateString = AppUtil.getFormattedDate(AppConstant.FORMAT_DATE_DISPLAY, date);

                //Set in  TextView
                textViewExpenseDate.setText(dateString);
            }
        }
    }

    //On Gallery Photo Selected
    private void onGalleryPhotoSelected(Intent data) {
        cameraSelected = false;
        Uri imageUri = data.getData();
        String absoluteGalleryPath = imageUri.toString();
        runUriToBitmapAsyncTask(absoluteGalleryPath);
    }

    //On Camera Photo Selected
    private void onCameraPhotoSelected() {
        cameraSelected = true;
        runUriToBitmapAsyncTask(expenseReceiptFilePath);
    }

    //AsyncTask of Create bitmap of selected image.
    private void runUriToBitmapAsyncTask(String absoluteImagePath) {
        UriToBitmapAsyncTask uriToBitmapAsyncTask = new UriToBitmapAsyncTask();
        uriToBitmapAsyncTask.execute(absoluteImagePath);
    }

    private class UriToBitmapAsyncTask extends AsyncTask<String, Void, String> {
        private Exception exception;

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String imagePath = params[0];
                if (imagePath != null) {
                    Uri uri = Uri.parse(imagePath);
                    selectedBitmap = MediaManager.getScaledBitmap(cameraSelected, imagePath, uri);
                    if (selectedBitmap != null) {
                        File file = createOrEditFile();
                        MediaManager.writeBitmapTofFile(file, selectedBitmap);
                    }
                }
            } catch (Exception exception) {

                this.exception = exception;
            }
            return expenseReceiptFilePath;
        }

        @Override
        protected void onPostExecute(String expenseReceiptFilePath) {
            progressBar.setVisibility(View.GONE);
            if (exception == null && expenseReceiptFilePath != null) {
                updateReceiptImage(selectedBitmap);
            }
        }
    }

    private void updateReceiptImage(Bitmap bitmap) {
        if (bitmap != null) {
            imageViewReceipt.setVisibility(View.VISIBLE);
            imageViewReceipt.setImageBitmap(bitmap);

            selectedBitmap = bitmap;//set bitmap for show in receipt view.
            checkBitmapVailability = bitmap;//add bitmap for check bitmap available or not when user not select image from gallery or camera.

            //Update Attach Receipt text
            textViewAttachReceipt.setText(getString(R.string.text_change_receipt));
        }
    }

    private void updateViewWithReceiptImage(String imagePath) {
        if (!AppUtil.isStringEmpty(imagePath)) {
            File imgFile = new File(imagePath);
            if (imgFile.exists()) {
                imageViewReceipt.setVisibility(View.VISIBLE);
                imageViewReceipt.setImageURI(Uri.fromFile(imgFile));

                selectedBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());//set bitmap for show in receipt view.
                checkBitmapVailability = selectedBitmap;//add bitmap for check bitmap available or not when user not select image from gallery or camera.

                //Update Attach Receipt text
                textViewAttachReceipt.setText(getString(R.string.text_change_receipt));
            }
        }
    }

    private void displayReceiptImage() {
        if (!AppUtil.isStringEmpty(expenseReceiptFilePath)) {
            updateViewWithReceiptImage(expenseReceiptFilePath);
        }
    }

    //Show Attach Receipt Dialog
    void showAttachReceiptDialog() {
        PhotoPickerDialog attachReceiptDialogFragment = PhotoPickerDialog.newInstance(this);
        attachReceiptDialogFragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.Dialog_NoTitle);
        attachReceiptDialogFragment.show(getSupportFragmentManager(), "dialog");
    }

    //Save Expense Async Task
    private class SaveExpenseAsyncTask extends AsyncTask<Expense, Void, Void> {
        private Dialog dialog = null;
        private Exception exception = null;

        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(CreateEditExpenseActivity.this);
        }

        @Override
        protected Void doInBackground(Expense... params) {
            Expense expense = params[0];

            try {
                //Save Expense
                ExpenseManager.saveExpense(expense, isEditExpense);
            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);

            if (exception == null) {
                //On Success
                onSuccessSaveExpense();
            } else {
                //On Fail
                onErrorSaveExpense();
            }
        }
    }

    public void onSuccessSaveExpense() {
        if (isEditExpense) {
            //Show Alert Dialog
            showAlertDialog(true, getString(R.string.text_expense_updated), null, null, TYPE_SAVE);
        } else {
            //show Alert Dialog
            showAlertDialog(false, getString(R.string.text_expense_saved_message), getString(R.string.text_expense_saved_title), getString(R.string.button_yes), getString(R.string.button_no), TYPE_CREATE);
        }
    }

    public void onErrorSaveExpense() {
        //Show Error Alert Dialog
        showErrorAlertDialog(null, null, null, 0);
    }

    //Reset Expense Form
    protected void resetExpenseForm() {
        //Get Current Date
        String currentDateString = AppUtil.getCurrentFormattedDate(AppConstant.FORMAT_DATE_DISPLAY);

        textViewExpenseCategory.setText("");
        textViewExpenseCategory.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_icon_home, 0);
        textViewExpenseDate.setText(currentDateString);
        editTextExpenseAmount.setText("");
        editTextExpenseNotes.setText("");

        //visibility gone because of its background shows after set null
        imageViewReceipt.setVisibility(View.GONE);
        imageViewReceipt.setImageDrawable(null);//Another Sol- transparent background

        existingExpenseId = -1;
        expenseCategoryId = -1;
        expenseBaseCategoryId = -1;
        expenseReceiptFilePath = null;

        //Reset Focus
        editTextExpenseAmount.requestFocus();

        //Attach Image Text
        textViewAttachReceipt.setText(getString(R.string.button_attach_receipt));

        SharePreference.putBoolean(getApplicationContext(), AppConstant.BACK_CREATE_EXPENSE, true);
    }

    // Navigate to Expense List
    protected void navigateToExpenseList() {
        Intent intent = new Intent(this, UploadExpenseActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        this.finish();
        overridePendingTransition(R.anim.slide_in_forward, R.anim.slide_out_forward);
    }

    private class OnPermissionRationaleClickListener implements AlertDialogPermissionBoxClickInterface {
        @Override
        public void onButtonClicked(boolean isPositiveButtonClicked) {
            if (isPositiveButtonClicked) {
                DeviceManager.openSettings(CreateEditExpenseActivity.this, 1010);
            } else {
                ActivityCompat.finishAffinity(CreateEditExpenseActivity.this);
            }
        }
    }

    //Alert Dialog
    private void showAlertDialog(boolean neutralButton, String message, String title, String okButtonTitle, int type) {
        applicationAlertDialog = new ApplicationAlertDialog().getInstance(this, neutralButton, message, title, okButtonTitle, type);
        applicationAlertDialog.show(getSupportFragmentManager(), "dialog");
    }

    private void showAlertDialog(boolean neutralButton, String message, String title, String okButtonTitle, String cancelButtonTitle, int type) {
        applicationAlertDialog = new ApplicationAlertDialog().getInstance(this, neutralButton, message, title, okButtonTitle, cancelButtonTitle, type);
        applicationAlertDialog.show(getSupportFragmentManager(), "dialog");
    }

    private void showErrorAlertDialog(String message, String title, String okButtonTitle, int type) {
        applicationErrorAlertDialog = new ApplicationErrorAlertDialog().getInstance(this, message, title, okButtonTitle, type);
        applicationErrorAlertDialog.show(getSupportFragmentManager(), "dialog");
    }

    private void showCameraAlertDialog(boolean neutralButton, String message, String title, String okButtonTitle, String cancelButtonTitle, int type) {
        cameraAlertDialog = new CameraAlertDialog().getInstance(this, neutralButton, message, title, okButtonTitle, cancelButtonTitle, type);
        cameraAlertDialog.show(getSupportFragmentManager(), "dialog");
    }

    //alert dialog action listener button
    @Override
    public void onPositiveButtonClick(int type) {
        switch (type) {
            case TYPE_SAVE:
                //Navigate to Expense List
                navigateToExpenseList();
                break;
            case TYPE_CREATE:
                //reset Expense form
                resetExpenseForm();
                break;
        }
    }

    @Override
    public void onPositiveCamBtnClick(int type) {

    }

    @Override
    public void onNegativeButtonClick() {
        //Navigate to Expense List
        navigateToExpenseList();
    }


    @Override
    public void onRetryButtonClick(int type) {
    }

    @Override
    public void onCancelButtonClick() {

    }


}