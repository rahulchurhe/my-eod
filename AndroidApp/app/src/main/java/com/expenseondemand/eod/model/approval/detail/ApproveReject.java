package com.expenseondemand.eod.model.approval.detail;

import com.expenseondemand.eod.model.approval.detail.project.LstApproverRejectItem;

import java.util.ArrayList;

/**
 * Created by ITDIVISION\maximess087 on 23/2/17.
 */

public class ApproveReject {
    private ArrayList<LstApproverRejectItem> LstApproverReject;

    public ArrayList<LstApproverRejectItem> getLstApproverReject() {
        return LstApproverReject;
    }

    public void setLstApproverReject(ArrayList<LstApproverRejectItem> lstApproverReject) {
        LstApproverReject = lstApproverReject;
    }
}
