package com.expenseondemand.eod.manager;

import android.support.v7.app.AlertDialog;
import android.content.Context;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.exception.EODAppException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.util.UIUtil;

public class ExceptionManager
{
	//Singleton Class
	public static ExceptionManager exceptionManager;
	private boolean isTaskCompleted = true;
	
	//Constructor
	private ExceptionManager()
	{
		
	}
	
	public static ExceptionManager getInstance()
	{
		if(exceptionManager == null)
		{
			exceptionManager = new ExceptionManager();
		}
		
		return exceptionManager;
	}
	
	public static void logException(int exceptionCode, String exceptionMessage, Exception exceptionObject)
	{
		LogManager logManager = LogManager.getInstance();
		logManager.addLog(exceptionCode, exceptionMessage, exceptionObject);
	}
	
	public static void dispatchExceptionDetails(int exceptionCode, String exceptionMessage, Exception exceptionObject) throws EODException
	{
		if(exceptionObject instanceof EODAppException)
		{
			throw (EODAppException) exceptionObject;
		}
		else
		{
			//Log Exception details
			logException(exceptionCode, exceptionMessage, exceptionObject);
			
			//Throw Exception
			throw new EODAppException(exceptionCode, exceptionMessage);	
		}
	}
	
	public void setTaskCompletionFailed()
	{
		isTaskCompleted = false;
	}
	
	public boolean isTaskCompleted()
	{
		return isTaskCompleted;
	}
	
	/*public static void notifyException(int exceptionCode, Context context, int messageTitle, int messageDescription)
	{
		notifyException(exceptionCode, context, context.getString(messageTitle), context.getString(messageDescription));
	}*/
	
	/*public static void notifyException(int exceptionCode, Context context, String messageTitle, String messageDescription)
	{
		//Show AlertDialog
		AlertDialog.Builder builder = UIUtil.getAlertDialogBuilder(context, (AppUtil.prepareTitle(Integer.toString(exceptionCode), messageTitle)), messageDescription);
		builder.show();
	}*/
	
	//Prepare AlertDialog Title
	public static String prepareTitle(int exceptionCode, String title)
	{
		//Get Application Context
		Context appContext = CachingManager.getAppContext();
		
		return ((exceptionCode == 0) ? title : (appContext.getString(R.string.text_error) + "(" + exceptionCode + ") - " + title));
	}
	
	//Prepare AlertDialog Title
	public static String prepareErrorList(int exceptionCode, String title)
	{
		return ((exceptionCode == 0) ? title : (AppUtil.prepareTitle(Integer.toString(exceptionCode), title)));
	}

	//Return Error Message
	public static String isRequiredError(int fieldName)
	{
		//Get Application Context
		Context appContext = CachingManager.getAppContext();
		
		return (String.format(appContext.getString(R.string.field_is_required), appContext.getString(fieldName)));
	}

	//Return Error Message
	public static String isInvalidError(int fieldName)
	{
		//Get Application Context
		Context appContext = CachingManager.getAppContext();
		
		return (String.format(appContext.getString(R.string.field_is_invalid), appContext.getString(fieldName)));
	}
}
