package com.expenseondemand.eod.screen.approval.claim;

import android.util.Log;

import com.expenseondemand.eod.exception.EODAppException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.ParseManager;
import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.manager.WebServiceManager;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.model.approval.claim.ApprovalClaimListExpenseItem;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.webservice.model.approval.claim.ClaimData;
import com.expenseondemand.eod.webservice.model.approval.claim.ClaimListData;
import com.expenseondemand.eod.webservice.model.approval.claim.ClaimRequest;
import com.expenseondemand.eod.webservice.model.approval.claim.ClaimResponse;
import com.expenseondemand.eod.webservice.model.approval.claim.PreApprovedDetails;
import com.expenseondemand.eod.webservice.model.approval.detail.DynamicDetail;
import com.expenseondemand.eod.webservice.model.http.Body;
import com.expenseondemand.eod.webservice.model.http.HTTPData;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 19-10-2016.
 */

public class ClaimManager
{
    //getClaims
    public static ArrayList<ApprovalClaimListExpenseItem> getClaims(int count,int pageNo) throws EODException
    {
        ClaimResponse response;

        //Prepare Request Object
        ClaimRequest request = prepareClaimRequest(count,pageNo);

        //Prepare JSON Request String
        String jsonRequestString = ParseManager.prepareJsonRequest(request);

        //Prepare http request
        HTTPData httpData = new HTTPData();
        httpData.setType(AppConstant.HTTP_METHOD_POST);
        Body body = new Body();
        body.setJsonRequestString(jsonRequestString);
        httpData.setBody(body);

        //Call to Web Service
        String jsonResponseString = (String) WebServiceManager.callWebService(AppConstant.BASE_URL, AppConstant.CLAIM_LIST, httpData);

        //Prepare Response Object
        response = (ClaimResponse) ParseManager.prepareWebServiceResponseObject(ClaimResponse.class, jsonResponseString);

        //Process Response Status
        WebServiceManager.processResponseStatus(response);

        //Process Response Status
        ArrayList<ApprovalClaimListExpenseItem> claimList = processClaimResponse(response);

        return claimList;
    }

    //Prepare Claim Request
    private static ClaimRequest prepareClaimRequest(int count,int pageNo)
    {
        ClaimRequest claimRequest = new ClaimRequest();

        UserInfo userInfo = CachingManager.getUserInfo();

        if (userInfo != null)
        {
            claimRequest.setCompanyId(userInfo.getCompanyId());
            claimRequest.setApproverId(userInfo.getResourceId());
            claimRequest.setNoOfRecords(count);
            claimRequest.setPageNo(pageNo);
            //statics
        }

        return claimRequest;
    }

    //Check Response Status
    private static ArrayList<ApprovalClaimListExpenseItem> processClaimResponse(ClaimResponse response) throws EODException
    {
        ArrayList<ApprovalClaimListExpenseItem> claimListExpenseItems = null;

        if(response != null)
        {
            claimListExpenseItems = prepareClaimModel(response);
        }
        else
        {
            throw new EODAppException(AppConstant.ERROR_CODE_1008);
        }

        return claimListExpenseItems;
    }

    private static ArrayList<ApprovalClaimListExpenseItem> prepareClaimModel(ClaimResponse response)
    {
        ArrayList<ApprovalClaimListExpenseItem> claimList = null;

        ClaimListData data = response.getData();

        if(response.getData().getHasMorePage().equals("Y")){
            SharePreference.putBoolean(CachingManager.getAppContext(),AppConstant.LOAD_MORE_CLAIM,true);
        }else{
            SharePreference.putBoolean(CachingManager.getAppContext(),AppConstant.LOAD_MORE_CLAIM,false);
        }
        if(data!=null)
        {
            claimList = new ArrayList<>();
            ApprovalClaimListExpenseItem approvalClaimListExpenseItem;
            for (ClaimData claimData : data.getData())
            {
                approvalClaimListExpenseItem = new ApprovalClaimListExpenseItem();
                approvalClaimListExpenseItem.setClaimantName(claimData.getResourceName());
                approvalClaimListExpenseItem.setClaimId(claimData.getExpenseHeaderId());
                approvalClaimListExpenseItem.setClaimType(claimData.getTitle());
                approvalClaimListExpenseItem.setRecordCount(claimData.getRecordCount());
                approvalClaimListExpenseItem.setNoOfItems(claimData.getNoOfItems());
                approvalClaimListExpenseItem.setBreachedClaim(claimData.getBreachedClaim());
                approvalClaimListExpenseItem.setBreachedItem(claimData.getBreachedItem());
                approvalClaimListExpenseItem.setDailyLimitBreached(claimData.getDailyLimitBreached());
                approvalClaimListExpenseItem.setMaxSpendBreach(claimData.getIsMaxSpendBreach());
                approvalClaimListExpenseItem.setPreApprovalBreach(claimData.getPreApprovalBreach());
                approvalClaimListExpenseItem.setStatus(claimData.getStatus());
                approvalClaimListExpenseItem.setRejected(claimData.getRejected());

                ArrayList<PreApprovedDetails> preApprovedDetailse=claimData.getPreApprovedDetails();
                if(!AppUtil.isCollectionEmpty(preApprovedDetailse)){
                    ArrayList<com.expenseondemand.eod.model.approval.claim.PreApprovedDetails> preApprovedDetailsesList=new ArrayList<>();
                    for(PreApprovedDetails preApprovedDetails:preApprovedDetailse){
                        com.expenseondemand.eod.model.approval.claim.PreApprovedDetails dynamicDetailItem = new com.expenseondemand.eod.model.approval.claim.PreApprovedDetails();
                        dynamicDetailItem.setValue(preApprovedDetails.getValue());
                        dynamicDetailItem.setLabelName(preApprovedDetails.getLabelName());
                        preApprovedDetailsesList.add(dynamicDetailItem);
                    }
                    approvalClaimListExpenseItem.setPreApprovedDetails(preApprovedDetailsesList);
                }


                claimList.add(approvalClaimListExpenseItem);
            }
        }
        return claimList;
    }
}
