package com.expenseondemand.eod.webservice.model.authentication.logout;

/**
 * Created by Ramit Yadav on 13-10-2016.
 */

public class Data
{
    private boolean deleted;

    public boolean isDeleted()
    {
        return deleted;
    }

    public void setDeleted(boolean deleted)
    {
        this.deleted = deleted;
    }
}
