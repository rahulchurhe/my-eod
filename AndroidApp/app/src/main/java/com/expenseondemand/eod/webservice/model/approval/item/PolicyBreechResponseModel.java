package com.expenseondemand.eod.webservice.model.approval.item;

import com.expenseondemand.eod.model.approval.expense.Notes;

import java.util.ArrayList;

/**
 * Created by Nandani Gupta on 2016-10-20.
 */
public class PolicyBreechResponseModel
{
    private String     expenseDetailId;
    private String     rowId;
    private String     isDailyCap;
    private String     isExpenseLimit;
    private String     isMaxSpendBreach;
    private String     isPreApproval;
    private String     amountSetLimmit;
    private String     amountBreach;
    private String     status;
    private String     expenseLimit;
    private String     MaxSpendamt;
    private String     DailCapAmt;
    private ArrayList<Notes>     Notes;

    public String getDailCapAmt() {
        return DailCapAmt;
    }

    public void setDailCapAmt(String dailCapAmt) {
        DailCapAmt = dailCapAmt;
    }

    public String getIsExpenseLimit() {
        return isExpenseLimit;
    }

    public void setIsExpenseLimit(String isExpenseLimit) {
        this.isExpenseLimit = isExpenseLimit;
    }

    public String getExpenseDetailId() {
        return expenseDetailId;
    }

    public void setExpenseDetailId(String expenseDetailId) {
        this.expenseDetailId = expenseDetailId;
    }

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getIsDailyCap() {
        return isDailyCap;
    }

    public void setIsDailyCap(String isDailyCap) {
        this.isDailyCap = isDailyCap;
    }

    public String getIsMaxSpendBreach() {
        return isMaxSpendBreach;
    }

    public void setIsMaxSpendBreach(String isMaxSpendBreach) {
        this.isMaxSpendBreach = isMaxSpendBreach;
    }

    public String getIsPreApproval() {
        return isPreApproval;
    }

    public void setIsPreApproval(String isPreApproval) {
        this.isPreApproval = isPreApproval;
    }

    public String getAmountSetLimmit() {
        return amountSetLimmit;
    }

    public void setAmountSetLimmit(String amountSetLimmit) {
        this.amountSetLimmit = amountSetLimmit;
    }

    public String getAmountBreach() {
        return amountBreach;
    }

    public void setAmountBreach(String amountBreach) {
        this.amountBreach = amountBreach;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExpenseLimit() {
        return expenseLimit;
    }

    public void setExpenseLimit(String expenseLimit) {
        this.expenseLimit = expenseLimit;
    }


    public String getMaxSpendamt() {
        return MaxSpendamt;
    }

    public void setMaxSpendamt(String maxSpendamt) {
        MaxSpendamt = maxSpendamt;
    }

    public ArrayList<Notes> getNotes() {
        return Notes;
    }

    public void setNotes(ArrayList<Notes> notes) {
        Notes = notes;
    }
}
