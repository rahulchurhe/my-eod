package com.expenseondemand.eod.screen.approval.claim.ExpenseTab;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.model.approval.claim.ApprovalClaimListExpenseItem;

import java.util.ArrayList;

public class ExpenseListTabAdapter extends ArrayAdapter<ApprovalClaimListExpenseItem> {

    private final ExpenseItemEventListener expenseItemEventListener;
    private final ArrayList<ApprovalClaimListExpenseItem> claimData;
    private Context context;

    public interface ExpenseItemEventListener {
        void handleExpenseItemClick(int position);

        void handlePreApprovedDetails(int position);
    }

    public ExpenseListTabAdapter(Context context, ArrayList<ApprovalClaimListExpenseItem> items, ExpenseItemEventListener expenseItemEventListener) {
        super(context, 0, items);
        this.context = context;
        this.claimData = items;
        this.expenseItemEventListener = expenseItemEventListener;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = convertView;
        final ViewHolder v;
        if (view == null) {
            LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(R.layout.approval_claim_expense_tab_item, null);
        } else {
            view = convertView;
        }
        v = new ViewHolder();
        /**/
        v.relativeLayoutClaimExpenseTabItem = (RelativeLayout) view.findViewById(R.id.relativeLayoutClaimExpenseTabItem);
        v.textViewClaimantName = (TextView) view.findViewById(R.id.textViewClaimantName);
        v.textViewClaimType = (TextView) view.findViewById(R.id.textViewClaimType);
        v.textViewClaimId = (TextView) view.findViewById(R.id.textViewClaimId);
        v.textViewItemCount = (TextView) view.findViewById(R.id.textViewItemCount);
        v.imageViewPolicyViolation = (ImageView) view.findViewById(R.id.imageViewPolicyViolation);
        v.imageViewPreApprovalBreach = (ImageView) view.findViewById(R.id.imageViewPreApprovalBreach);
        /**/
        v.textViewClaimType.setSelected(true);
        v.textViewClaimantName.setSelected(true);
        v.textViewClaimId.setSelected(true);
        /**/
        ApprovalClaimListExpenseItem approvalClaimListExpenseItem = claimData.get(position);

        v.textViewClaimantName.setText(approvalClaimListExpenseItem.getClaimantName());
        v.textViewClaimType.setText(approvalClaimListExpenseItem.getClaimType());
        v.textViewItemCount.setText(approvalClaimListExpenseItem.getNoOfItems());

        String claimId = context.getResources().getString(R.string.approval_claim_expense_tab_claim_id, approvalClaimListExpenseItem.getClaimId());
        v.textViewClaimId.setText(claimId);

        String count = context.getResources().getString(R.string.approval_claim_expense_tab_item_count, approvalClaimListExpenseItem.getNoOfItems());
        v.textViewItemCount.setText(approvalClaimListExpenseItem.getNoOfItems());

        //v.relativeLayoutClaimExpenseTabItem.setTag(position);
        v.relativeLayoutClaimExpenseTabItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expenseItemEventListener.handleExpenseItemClick(position);
            }
        });

        if (approvalClaimListExpenseItem.getPreApprovalBreach().equals("Y")) {
            v.relativeLayoutClaimExpenseTabItem.setBackgroundResource(R.drawable.wheat_background_selector);
        } else if (approvalClaimListExpenseItem.getPreApprovalBreach().equals("")) {
            if (approvalClaimListExpenseItem.isRejected()) {
                v.relativeLayoutClaimExpenseTabItem.setBackgroundResource(R.drawable.light_green_background_selector);

            } else {
                v.relativeLayoutClaimExpenseTabItem.setBackgroundResource(R.drawable.default_listitem_background_selector);

            }
        } else {
            v.relativeLayoutClaimExpenseTabItem.setBackgroundResource(R.drawable.cream_backbgrund_selector);
        }

        if (approvalClaimListExpenseItem.getPreApprovalBreach().equals("N") || approvalClaimListExpenseItem.getPreApprovalBreach().equals("Y")) {
            v.imageViewPreApprovalBreach.setVisibility(View.VISIBLE);
        } else {
            v.imageViewPreApprovalBreach.setVisibility(View.GONE);
        }


        //Setting text color
        if (approvalClaimListExpenseItem.isBreachedClaim() && !approvalClaimListExpenseItem.isStatus()) {
            v.textViewClaimantName.setTextColor(ContextCompat.getColor(context, R.color.text_color_red));
            v.textViewClaimType.setTextColor(ContextCompat.getColor(context, R.color.text_color_red));
            v.textViewClaimId.setTextColor(ContextCompat.getColor(context, R.color.text_color_red));
            v.textViewItemCount.setTextColor(ContextCompat.getColor(context, R.color.text_color_red));
        } else if (approvalClaimListExpenseItem.isStatus()) {
            v.textViewClaimantName.setTextColor(ContextCompat.getColor(context, R.color.text_color_violet));
            v.textViewClaimType.setTextColor(ContextCompat.getColor(context, R.color.text_color_violet));
            v.textViewClaimId.setTextColor(ContextCompat.getColor(context, R.color.text_color_violet));
            v.textViewItemCount.setTextColor(ContextCompat.getColor(context, R.color.text_color_violet));
        } else {
            v.textViewClaimantName.setTextColor(ContextCompat.getColor(context, R.color.text_color_black));
            v.textViewClaimType.setTextColor(ContextCompat.getColor(context, R.color.text_color_light_grey));
            v.textViewClaimId.setTextColor(ContextCompat.getColor(context, R.color.text_color_light_grey));
            v.textViewItemCount.setTextColor(ContextCompat.getColor(context, R.color.text_color_black));
        }

        if (approvalClaimListExpenseItem.isMaxSpendBreach() || approvalClaimListExpenseItem.isDailyLimitBreached())
            v.imageViewPolicyViolation.setVisibility(View.VISIBLE);
        else
            v.imageViewPolicyViolation.setVisibility(View.GONE);


        v.imageViewPreApprovalBreach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expenseItemEventListener.handlePreApprovedDetails(position);
            }
        });

        view.setTag(v);
        return view;
    }

    public class ViewHolder {
        RelativeLayout relativeLayoutClaimExpenseTabItem;
        TextView textViewClaimantName;
        TextView textViewClaimType;
        TextView textViewClaimId;
        TextView textViewItemCount;
        ImageView imageViewPolicyViolation, imageViewPreApprovalBreach;
    }
}