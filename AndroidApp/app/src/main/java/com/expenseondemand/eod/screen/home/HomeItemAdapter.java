package com.expenseondemand.eod.screen.home;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.model.HomeItem;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.util.UIUtil;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 08-09-2016.
 */
public class HomeItemAdapter extends RecyclerView.Adapter {
    private ArrayList<HomeItem> homeData;
    private Context context;
    private HomeItemEventListener homeItemEventListener;
    private HomeItemOnClickListener homeItemOnClickListener;

    public interface HomeItemEventListener {
        void handleHomeItemClick(int position, int x, int y);
    }

    public HomeItemAdapter(Context context, HomeItemEventListener homeItemEventListener, ArrayList<HomeItem> homeItem) {
        this.context = context;
        this.homeItemEventListener = homeItemEventListener;
        homeData = homeItem;
        homeItemOnClickListener = new HomeItemOnClickListener();
    }

    @Override
    public int getItemCount() {
        return AppUtil.isCollectionEmpty(homeData) ? 0 : homeData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return homeData.get(position).getType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HomeItem.TYPE_HEADER) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_list_header_item, parent, false);

            return new HomeListHeaderItemHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_list_item, parent, false);

            return new HomeListItemHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        HomeItem homeItem = homeData.get(position);
        if (holder instanceof HomeListHeaderItemHolder) {
            HomeListHeaderItemHolder homeListHeaderItemHolder = (HomeListHeaderItemHolder) holder;
            homeListHeaderItemHolder.textViewHomeHeaderItem.setText(homeItem.getName());
        } else if (holder instanceof HomeListItemHolder) {
            HomeListItemHolder homeListItemHolder = (HomeListItemHolder) holder;
            homeListItemHolder.imageViewLeft.setImageResource(homeItem.getImageResourceId());


            StateListDrawable stateListDrawable = new StateListDrawable();
            stateListDrawable.addState(new int[]{android.R.attr.state_pressed}, new ColorDrawable(ContextCompat.getColor(context, homeItem.getImageBackgroundSelectedColor())));
            stateListDrawable.addState(new int[]{}, new ColorDrawable(ContextCompat.getColor(context, homeItem.getImageBackgroundColor())));
            UIUtil.setBackground(homeListItemHolder.imageViewLeft, stateListDrawable);
            homeListItemHolder.textViewHomeItem.setText(homeItem.getName());
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) homeListItemHolder.textViewHomeItem.getLayoutParams();

            int count = homeItem.getCount();
            if (count > 0) {
                layoutParams.leftMargin = 0;

                homeListItemHolder.textViewOverlay.setVisibility(View.VISIBLE);
                homeListItemHolder.textViewOverlay.setText(String.valueOf(count));
                homeListItemHolder.viewInvisible.setVisibility(View.VISIBLE);
            } else {
                layoutParams.leftMargin = (int) context.getResources().getDimension(R.dimen.home_shape_circle_overlay_outer_half);

                homeListItemHolder.textViewOverlay.setVisibility(View.GONE);
                homeListItemHolder.viewInvisible.setVisibility(View.GONE);
            }
            homeListItemHolder.textViewHomeItem.setLayoutParams(layoutParams);

            homeListItemHolder.relativeLayoutHomeListItem.setTag(homeItem.getAction());
            homeListItemHolder.relativeLayoutHomeListItem.setOnClickListener(homeItemOnClickListener);
        }

    }

    protected static class HomeListHeaderItemHolder extends RecyclerView.ViewHolder {
        TextView textViewHomeHeaderItem;

        public HomeListHeaderItemHolder(View itemView) {
            super(itemView);

            textViewHomeHeaderItem = (TextView) itemView;//.findViewById(R.id.textViewHomeItem);
        }
    }

    protected static class HomeListItemHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayoutHomeListItem;
        TextView textViewHomeItem;
        ImageView imageViewLeft;
        View viewInvisible;
        TextView textViewOverlay;

        public HomeListItemHolder(View itemView) {
            super(itemView);

            relativeLayoutHomeListItem = (RelativeLayout) itemView.findViewById(R.id.relativeLayoutHomeListItem);
            textViewHomeItem = (TextView) itemView.findViewById(R.id.textViewHomeItem);
            imageViewLeft = (ImageView) itemView.findViewById(R.id.imageViewLeft);
            viewInvisible = itemView.findViewById(R.id.viewInvisible);
            textViewOverlay = (TextView) itemView.findViewById(R.id.textViewOverlay);

        }
    }

    private class HomeItemOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            int action = (int) view.getTag();
            homeItemEventListener.handleHomeItemClick(action, Math.round(view.getX()), Math.round(view.getY()));

        }
    }
}
