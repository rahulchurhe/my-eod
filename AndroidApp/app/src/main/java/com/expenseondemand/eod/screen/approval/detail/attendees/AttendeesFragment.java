package com.expenseondemand.eod.screen.approval.detail.attendees;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.expenseondemand.eod.BaseFragment;
import com.expenseondemand.eod.R;
import com.expenseondemand.eod.model.approval.detail.attendees.AttendeesItem;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 16-09-2016.
 */
public class AttendeesFragment extends BaseFragment
{
    private RecyclerView recyclerViewAttendees;
    private AttendeesAdapter attendeesAdapter;
    private ArrayList<AttendeesItem> attendeesItems;

    public static AttendeesFragment getInstance(ArrayList<AttendeesItem> attendeesItems)
    {
        AttendeesFragment attendeesFragment = new AttendeesFragment();
        attendeesFragment.attendeesItems = attendeesItems;

        return attendeesFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.attendees_fragment, container, false);

        initializeView(rootView);

        return rootView;
    }

    private void initializeView(View rootView)
    {
        recyclerViewAttendees = (RecyclerView)rootView.findViewById(R.id.recyclerViewAttendees);
        final LinearLayoutManager projectDetailItemLinerLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerViewAttendees.setLayoutManager(projectDetailItemLinerLayoutManager);

        attendeesAdapter = new AttendeesAdapter(getActivity(), attendeesItems);
        recyclerViewAttendees.setAdapter(attendeesAdapter);
    }
}
