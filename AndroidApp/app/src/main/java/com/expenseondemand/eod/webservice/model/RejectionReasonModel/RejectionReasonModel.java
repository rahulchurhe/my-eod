package com.expenseondemand.eod.webservice.model.RejectionReasonModel;

/**
 * Created by Nandani Gupta on 2016-10-17.
 */
public class RejectionReasonModel
{
     private String  rejectionReasonID;
     private String  name;

    public String getRejectionReasonID() {
        return rejectionReasonID;
    }

    public void setRejectionReasonID(String rejectionReasonID) {
        this.rejectionReasonID = rejectionReasonID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
