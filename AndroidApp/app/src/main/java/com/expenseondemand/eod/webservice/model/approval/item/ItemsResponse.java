package com.expenseondemand.eod.webservice.model.approval.item;

import com.expenseondemand.eod.webservice.model.MasterResponse;

import java.util.ArrayList;

/**
 * Created by Nandani Gupta on 2016-10-20.
 */
public class ItemsResponse extends MasterResponse {

    private ItemResponseData data;

    public ItemResponseData getData() {
        return data;
    }

    public void setData(ItemResponseData data) {
        this.data = data;
    }
    /* ArrayList<ItemsResponseModel> data;

    public ArrayList<ItemsResponseModel> getData() {
        return data;
    }

    public void setData(ArrayList<ItemsResponseModel> data) {
        this.data = data;
    }*/
}
