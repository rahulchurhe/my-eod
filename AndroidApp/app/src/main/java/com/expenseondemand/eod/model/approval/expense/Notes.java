package com.expenseondemand.eod.model.approval.expense;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ramit Yadav on 25-10-2016.
 */

public class Notes implements Parcelable
{
    private String reason;
    private String name;
    private String notes;

    public Notes(){}

    public Notes(Parcel in)
    {
        reason = in.readString();
        name = in.readString();
        notes = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(reason);
        dest.writeString(name);
        dest.writeString(notes);
    }

    public String getReason()
    {
        return reason;
    }

    public void setReason(String reason)
    {
        this.reason = reason;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getNotes()
    {
        return notes;
    }

    public void setNotes(String notes)
    {
        this.notes = notes;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    public static final Creator<Notes> CREATOR = new Creator<Notes>() {
        @Override
        public Notes createFromParcel(Parcel in) {
            return new Notes(in);
        }

        @Override
        public Notes[] newArray(int size) {
            return new Notes[size];
        }
    };
}
