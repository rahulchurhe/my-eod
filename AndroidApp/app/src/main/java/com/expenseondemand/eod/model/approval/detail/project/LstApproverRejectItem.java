package com.expenseondemand.eod.model.approval.detail.project;

/**
 * Created by ITDIVISION\maximess087 on 23/2/17.
 */

public class LstApproverRejectItem {
    private String DetailId;
    private String StrApprovalAllowed;

    public String getDetailId() {
        return DetailId;
    }

    public void setDetailId(String detailId) {
        DetailId = detailId;
    }

    public String getStrApprovalAllowed() {
        return StrApprovalAllowed;
    }

    public void setStrApprovalAllowed(String strApprovalAllowed) {
        StrApprovalAllowed = strApprovalAllowed;
    }
}
