package com.expenseondemand.eod.screen.approval.detail.project;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.model.approval.detail.project.AdditionalDetailsItem;
import com.expenseondemand.eod.model.approval.detail.project.ProjectDetailItem;
import com.expenseondemand.eod.util.AppUtil;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 16-09-2016.
 */
public class AdditionalProjectDetailAdapter extends RecyclerView.Adapter {
    private Context context;
    private ArrayList<AdditionalDetailsItem> projectDetailData;


    public AdditionalProjectDetailAdapter(Context context, ArrayList<AdditionalDetailsItem> projectDetailData) {
        this.context = context;
        this.projectDetailData = projectDetailData;
    }

    @Override
    public int getItemCount() {
        return AppUtil.isCollectionEmpty(projectDetailData) ? 0 : projectDetailData.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.additional_project_detail_item, parent, false);

        return new ProjectDetailItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        AdditionalDetailsItem projectDetailItem = projectDetailData.get(position);

        ProjectDetailItemHolder projectDetailItemHolder = (ProjectDetailItemHolder) holder;
        projectDetailItemHolder.textViewLeft.setText(projectDetailItem.getLabelName());
        projectDetailItemHolder.textViewRight.setText(projectDetailItem.getValue());
    }

    protected static class ProjectDetailItemHolder extends RecyclerView.ViewHolder {
        TextView textViewLeft,textViewRight;

        public ProjectDetailItemHolder(View itemView) {
            super(itemView);
            textViewLeft = (TextView) itemView.findViewById(R.id.textViewLeft);
            textViewRight = (TextView) itemView.findViewById(R.id.textViewRight);
        }
    }

}
