package com.expenseondemand.eod.webservice.model.approval.detail.mileage;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 20-10-2016.
 */

public class Mileage
{
    /*private String vehicleID;*/
    private String VehicleId;
    private ArrayList<MileageLeg> mileageLegs;

    public String getVehicleId() {
        return VehicleId;
    }

    public void setVehicleId(String vehicleId) {
        VehicleId = vehicleId;
    }
/* public String getVehicleID()
    {
        return vehicleID;
    }

    public void setVehicleID(String vehicleID)
    {
        this.vehicleID = vehicleID;
    }*/

    public ArrayList<MileageLeg> getMileageLegs()
    {
        return mileageLegs;
    }

    public void setMileageLegs(ArrayList<MileageLeg> mileageLegs)
    {
        this.mileageLegs = mileageLegs;
    }
}
