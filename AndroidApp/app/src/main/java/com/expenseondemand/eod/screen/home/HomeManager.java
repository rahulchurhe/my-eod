package com.expenseondemand.eod.screen.home;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.expenseondemand.eod.exception.EODAppException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.DeviceManager;
import com.expenseondemand.eod.manager.ParseManager;
import com.expenseondemand.eod.manager.WebServiceManager;
import com.expenseondemand.eod.model.AppRejectionSetting;
import com.expenseondemand.eod.model.MobileExpense;
import com.expenseondemand.eod.model.Statistics;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.webservice.model.http.Body;
import com.expenseondemand.eod.webservice.model.http.HTTPData;
import com.expenseondemand.eod.webservice.model.statistics.AppRejectionSettingResponse;
import com.expenseondemand.eod.webservice.model.statistics.Data;
import com.expenseondemand.eod.webservice.model.statistics.MobileExpensesResponseData;
import com.expenseondemand.eod.webservice.model.statistics.StatisticsRequest;
import com.expenseondemand.eod.webservice.model.statistics.StatisticsResponse;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 17-10-2016.
 */

public class HomeManager {

    public static Bitmap getImageData() throws EODException {
        File imageFile = DeviceManager.getImageFile(AppConstant.DIRECTORY_NAME_APP_LOGO, AppConstant.FILE_NAME_APP_LOGO);
        Bitmap imageData = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
        return imageData;
    }

    //getStatistics
    public static Statistics getStatistics() throws EODException {
        StatisticsResponse response = null;

        //Prepare Request Object
        StatisticsRequest request = prepareStatisticsRequest();

        //Prepare JSON Request String
        String jsonRequestString = ParseManager.prepareJsonRequest(request);

        //Prepare http request
        HTTPData httpData = new HTTPData();
        httpData.setType(AppConstant.HTTP_METHOD_POST);
        Body body = new Body();
        body.setJsonRequestString(jsonRequestString);
        httpData.setBody(body);

        //Call to Web Service
        String jsonResponseString = (String) WebServiceManager.callWebService(AppConstant.BASE_URL, AppConstant.STATISTICS_URL, httpData);


        //Prepare Response Object
        response = (StatisticsResponse) ParseManager.prepareWebServiceResponseObject(StatisticsResponse.class, jsonResponseString);

        //Process Response Status
        WebServiceManager.processResponseStatus(response);

        //Process Response Status
        Statistics statistics = processStatisticsResponse(response);

        return statistics;
    }

    //Prepare Statistics Request
    private static StatisticsRequest prepareStatisticsRequest() {
        StatisticsRequest statisticsRequest = new StatisticsRequest();

        UserInfo userInfo = CachingManager.getUserInfo();

        if (userInfo != null) {
            statisticsRequest.setId(userInfo.getResourceId());
            statisticsRequest.setBranchId(userInfo.getBranchId());
            statisticsRequest.setCompanyId(userInfo.getCompanyId());
            statisticsRequest.setCostCenterId(userInfo.getCostCenterId());
            statisticsRequest.setDepartmentId(userInfo.getDepartmentId());
            statisticsRequest.setEditionCode(AppConstant.EDITION_CODE);
        }

        return statisticsRequest;
    }

    //Check Response Status
    private static Statistics processStatisticsResponse(StatisticsResponse response) throws EODException {
        Statistics statistics = null;

        if (response != null) {
            statistics = prepareStaticsModel(response);
        } else {
            throw new EODAppException(AppConstant.ERROR_CODE_1008);
        }

        return statistics;
    }

    private static Statistics prepareStaticsModel(StatisticsResponse response) {
        Statistics statistics = null;
        MobileExpense mobileExpense = null;
        ArrayList<MobileExpense> mobileExpenseList = null;
        Data data = response.getData();
        if (data != null) {
            statistics = new Statistics();
            statistics.setApprovedCount(data.getApprovedCount());
            statistics.setPendingCount(data.getPendingCount());
            statistics.setInCompleteCount(data.getInCompleteCount());
            statistics.setRejectedCount(data.getRejectedCount());
            statistics.setPassedForPayment(data.getPassedForPayment());
            statistics.setRejectedAmount(data.getRejectedAmount());
            statistics.setPendingAmount(data.getPendingAmount());
            statistics.setApprovedAmount(data.getApprovedAmount());
            statistics.setInCompleteAmount(data.getInCompleteAmount());
            statistics.setPaidAmount(data.getPaidAmount());
            statistics.setCostCentre(data.getCostCentre());
            statistics.setExpenseApproverName(data.getExpenseApproverName());
            statistics.setPreApprovalCount(data.getPreApprovalCount());
            statistics.setApprovePreApprovalCount(data.getApprovePreApprovalCount());
            statistics.setSubmitDOCCount(data.getSubmitDOCCount());
            statistics.setApproveDOCCount(data.getApproveDOCCount());
            statistics.setMobileExpenseCount(data.getMobileExpenseCount());
            statistics.setPendingApprovalCount(data.getPendingApprovalCount());
            ArrayList<MobileExpensesResponseData> firstMobileExpenses = data.getFirstMobileExpenses();
            if (!AppUtil.isCollectionEmpty(firstMobileExpenses)) {
                mobileExpenseList = new ArrayList<MobileExpense>();
                for (MobileExpensesResponseData mobileExpensesResponseData : firstMobileExpenses) {
                    mobileExpense = new MobileExpense();
                    mobileExpense.setMobileUploadExpenseId(AppUtil.checkForNull(mobileExpensesResponseData.getMobileUploadExpenseId()));
                    mobileExpense.setClientItemId(AppUtil.checkForNull(mobileExpensesResponseData.getClientItemId()));
                    mobileExpense.setDateExpense(AppUtil.checkForNull(mobileExpensesResponseData.getDateExpense()));
                    mobileExpense.setAmount(mobileExpensesResponseData.getAmount());
                    mobileExpense.setCurrencyCode(AppUtil.checkForNull(mobileExpensesResponseData.getCurrencyCode()));
                    mobileExpense.setExpenseCategoryId(AppUtil.checkForNull(mobileExpensesResponseData.getExpenseCategoryId()));
                    mobileExpense.setDateUpload(AppUtil.checkForNull(mobileExpensesResponseData.getDateUpload()));
                    mobileExpense.setDescription(AppUtil.checkForNull(mobileExpensesResponseData.getDescription()));
                    mobileExpense.setResourceId(AppUtil.checkForNull(mobileExpensesResponseData.getResourceId()));
                    mobileExpense.setRecieptImagePath(AppUtil.checkForNull(mobileExpensesResponseData.getRecieptImagePath()));
                    mobileExpense.setFlag(AppUtil.checkForNull(mobileExpensesResponseData.getFlag()));
                    mobileExpense.setExpenseCategoryName(AppUtil.checkForNull(mobileExpensesResponseData.getExpenseCategoryName()));
                    mobileExpense.setConversionRate(AppUtil.checkForNull(mobileExpensesResponseData.getConversionRate()));
                    mobileExpenseList.add(mobileExpense);
                }
            }
            statistics.setFirstMobileExpenses(mobileExpenseList);
            statistics.setAppRejSettings(prepareRejectionSetting(data.getAppRejSettings()));
        }
        return statistics;
    }

    private static AppRejectionSetting prepareRejectionSetting(AppRejectionSettingResponse appRejSettings) {
        AppRejectionSetting appRejectionSetting=new AppRejectionSetting();

        appRejectionSetting.setAppRejectSettingsRule1(appRejSettings.getAppRejectSettingsRule1());
        appRejectionSetting.setAppRejectSettingsRule2(appRejSettings.getAppRejectSettingsRule2());
        appRejectionSetting.setAppRejectSettingsRule3(appRejSettings.getAppRejectSettingsRule3());
        appRejectionSetting.setAppRejectSettingsRule4(appRejSettings.getAppRejectSettingsRule4());

        /*appRejectionSetting.setAppRejectSettingsRule1("Y");
        appRejectionSetting.setAppRejectSettingsRule2("N");
        appRejectionSetting.setAppRejectSettingsRule3("Y");
        appRejectionSetting.setAppRejectSettingsRule4("N");*/

        /*appRejectionSetting.setAppRejectSettingsRule1("Y");
        appRejectionSetting.setAppRejectSettingsRule2("Y");
        appRejectionSetting.setAppRejectSettingsRule3("N");
        appRejectionSetting.setAppRejectSettingsRule4("N");*/

        return appRejectionSetting;
    }

}
