package com.expenseondemand.eod.manager;

import java.io.IOException;

import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.webservice.model.MasterRequest;
import com.expenseondemand.eod.webservice.model.MasterResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;

public class JacksonHandler
{
	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	public static String createJson(MasterRequest requestObject) throws EODException
	{
		String jsonString = "";

		//Set the Property Naming Strategy
		OBJECT_MAPPER.setPropertyNamingStrategy(new CustomizedPropertyNamingStrategy());
				
		try
		{
			jsonString = OBJECT_MAPPER.writeValueAsString(requestObject);
		}
		catch (JsonProcessingException exception)
		{
			ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1001, exception.getMessage(), exception);
		}
		catch (Exception exception)
		{
			ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1002, exception.getMessage(), exception);
		}

		return jsonString;
	}

	public static MasterResponse createResponse(String jsonResponseString, Class<? extends Object> classInstance) throws EODException
	{
		MasterResponse masterResponse = null;

		//Set the Property Naming Strategy
		OBJECT_MAPPER.setPropertyNamingStrategy(new CustomizedPropertyNamingStrategy());
		OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		try
		{
			masterResponse = (MasterResponse)OBJECT_MAPPER.readValue(jsonResponseString, classInstance);
		}
		catch (JsonParseException exception)
		{
			ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1003, exception.getMessage(), exception);
		}
		catch (JsonMappingException exception)
		{
			ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1004, exception.getMessage(), exception);
		}
		catch (IOException exception)
		{
			ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1005, exception.getMessage(), exception);
		}
		catch (Exception exception)
		{
			ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1006, exception.getMessage(), exception);
		}

		return masterResponse;
	}
	
	private static class CustomizedPropertyNamingStrategy extends PropertyNamingStrategy
	{
		/**
		 * Auto Generated serialVersionUID
		 */
		private static final long serialVersionUID = 837479668977910450L;

		@Override
		public String nameForSetterMethod(MapperConfig<?> config, AnnotatedMethod method, String defaultName)
		{
			return convertName(defaultName);
		}
		
		@Override
		public String nameForGetterMethod(MapperConfig<?> config, AnnotatedMethod method, String defaultName)
		{
			return convertName(defaultName);
		}
		
		private String convertName(String defaultName)
		{
			return (Character.toUpperCase(defaultName.charAt(0)) + defaultName.substring(1));
		}
	}
}
