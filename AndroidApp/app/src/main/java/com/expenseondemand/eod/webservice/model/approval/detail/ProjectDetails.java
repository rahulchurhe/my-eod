package com.expenseondemand.eod.webservice.model.approval.detail;

/**
 * Created by ITDIVISION\maximess087 on 22/2/17.
 */

public class ProjectDetails  {
    private String LabelName;
    private String Value;

    public String getLabelName() {
        return LabelName;
    }

    public void setLabelName(String labelName) {
        LabelName = labelName;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
