package com.expenseondemand.eod.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.concurrent.atomic.AtomicInteger;

public class EodDatabase 
{
	private static final int DATABASE_VERSION_1 = 1;

	private Context context;
	private AtomicInteger mOpenCounter = new AtomicInteger();
	private SQLiteDatabase sqliteDatabase;
	private static EodDatabase databaseInstance;
	private static SQLiteOpenHelper sqliteOpenHelper;

	public static final String TAG = "DatabaseHelper";
	private static final String DATABASE_NAME = "expenseondemand.db";
	private static final int DATABASE_VERSION = 1;

	//Table UserInfo Table
	public static final String TABLE_NAME_EOD_USERINFO_TABLE = "EOD_UserInfo";

	//Table UserInfo_Table Columns
	public static final String COLUMN_NAME_ID = "Id";
	public static final String COLUMN_NAME_USER_ID = "UserId";
	public static final String COLUMN_NAME_LOGIN_ID = "LoginId";
	public static final String COLUMN_NAME_PASSWORD = "Password";
	public static final String COLUMN_NAME_DEFAULT_CATEGORY_ID = "DefaultCategoryId";
	public static final String COLUMN_NAME_DEFAULT_CURRENCY_CODE = "DefaultCurrencyCode";
	public static final String COLUMN_NAME_RESOURCE_ID = "ResourceId";
	public static final String COLUMN_NAME_COST_CENTER_ID = "CostCenterId";
	public static final String COLUMN_NAME_COMPANY_LOGO = "CompanyLogo";
	public static final String COLUMN_NAME_BRANCH_ID = "BranchId";
	public static final String COLUMN_NAME_COMPANY_ID = "CompanyId";
	public static final String COLUMN_NAME_DEPARTMENT_ID = "DepartmentId";
	public static final String COLUMN_NAME_COST_CENTER_NAME = "CostCenterName";
	public static final String COLUMN_NAME_COMPANY_NAME = "CompanyName";
	public static final String COLUMN_NAME_DISPLAY_NAME = "DisplayName";
	public static final String COLUMN_NAME_IS_APPROVER = "IsApprover";
	public static final String COLUMN_NAME_IS_FINANCE = "IsFinance";
	public static final String COLUMN_NAME_IS_BOSS = "IsBoss";
	public static final String COLUMN_NAME_IS_DELEGATE = "IsDelegate";
	public static final String COLUMN_NAME_GRADE_ID = "GradeId";
	public static final String COLUMN_NAME_IS_DEPUTY_ASSIGNED="DeputyAssigned";

	//Table ExpenseCategory
	public static final String TABLE_NAME_EOD_EXPENSE_CATEGORY_TABLE = "EOD_ExpenseCategory";

	//Table Expense Category Columns
	public static final String COLUMN_NAME_EXPENSE_CATEGORY_ID = "ExpenseCategoryId";
	public static final String COLUMN_NAME_EXPENSE_BASE_CATEGORY_ID = "ExpenseBaseCategoryId";
	public static final String COLUMN_NAME_EXPENSE_CATEGORY_NAME = "ExpenseCategoryName";

	//Table Expense
	public static final String TABLE_NAME_EOD_EXPENSE_TABLE = "EOD_Expense";

	//Table Expense Columns
	public static final String COLUMN_NAME_EXPENSE_ID = "ExpenseId";
	public static final String COLUMN_NAME_EXPENSE_DATE = "ExpenseDate";
	public static final String COLUMN_NAME_CURRENCY_CODE = "CurrencyCode";
	public static final String COLUMN_NAME_EXPENSE_AMOUNT = "ExpenseAmount";
	public static final String COLUMN_NAME_EXPENSE_NOTES = "ExpenseNotes";
	public static final String COLUMN_NAME_HAS_RECEIPT = "HasReceipt";
	public static final String COLUMN_NAME_RECEIPT_IMAGE = "ReceiptImage";

	//Create Table UserInfo SQL
	private static final String CREATE_TABLE_SQL_EOD_USER_INFO = "CREATE TABLE IF NOT EXISTS "  + TABLE_NAME_EOD_USERINFO_TABLE
			+ "("
			+ COLUMN_NAME_ID + " INTEGER PRIMARY KEY, " 
			+ COLUMN_NAME_LOGIN_ID + " TEXT NOT NULL, " 
			+ COLUMN_NAME_USER_ID + " TEXT NOT NULL, " 
			+ COLUMN_NAME_PASSWORD + " TEXT NOT NULL, "
			+ COLUMN_NAME_DEFAULT_CATEGORY_ID + " INTEGER, "
			+ COLUMN_NAME_DEFAULT_CURRENCY_CODE + " TEXT NOT NULL, "
			+ COLUMN_NAME_RESOURCE_ID + " TEXT NOT NULL, "
			+ COLUMN_NAME_COST_CENTER_ID + " TEXT NOT NULL, "
			+ COLUMN_NAME_COMPANY_LOGO + " TEXT NOT NULL, "
			+ COLUMN_NAME_BRANCH_ID + " TEXT NOT NULL, "
			+ COLUMN_NAME_COMPANY_ID + " TEXT NOT NULL, "
			+ COLUMN_NAME_DEPARTMENT_ID + " TEXT NOT NULL, "
			+ COLUMN_NAME_COST_CENTER_NAME + " TEXT NOT NULL, "
			+ COLUMN_NAME_COMPANY_NAME + " TEXT NOT NULL, "
			+ COLUMN_NAME_DISPLAY_NAME + " TEXT NOT NULL, "
			+ COLUMN_NAME_IS_APPROVER + " INTEGER, "
			+ COLUMN_NAME_IS_FINANCE + " INTEGER, "
			+ COLUMN_NAME_IS_BOSS + " INTEGER, "
			+ COLUMN_NAME_IS_DEPUTY_ASSIGNED + " INTEGER, "
			+ COLUMN_NAME_IS_DELEGATE + " INTEGER, "
			+ COLUMN_NAME_GRADE_ID + " TEXT NOT NULL"
			+ ")";


	//Create Table ExpenseCategory SQL
	public static final String CREATE_TABLE_SQL_EOD_EXPENSE_CATEGORY = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_EOD_EXPENSE_CATEGORY_TABLE
			+ "("
			+ COLUMN_NAME_ID + " INTEGER PRIMARY KEY, " 
			+ COLUMN_NAME_USER_ID + " TEXT NOT NULL, " 
			+ COLUMN_NAME_EXPENSE_CATEGORY_ID + " INTEGER  NOT NULL, "
			+ COLUMN_NAME_EXPENSE_BASE_CATEGORY_ID + " INTEGER  NOT NULL, "
			+ COLUMN_NAME_EXPENSE_CATEGORY_NAME + " TEXT NOT NULL"
			+ ")";

	//Create Table Expense SQL
	public static final String CREATE_TABLE_SQL_EOD_EXPENSE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_EOD_EXPENSE_TABLE
			+ "("
			+ COLUMN_NAME_EXPENSE_ID + " INTEGER PRIMARY KEY, " 
			+ COLUMN_NAME_USER_ID + " TEXT NOT NULL, " 
			+ COLUMN_NAME_EXPENSE_DATE + " INTEGER NOT NULL, "
			+ COLUMN_NAME_EXPENSE_CATEGORY_ID + " INTEGER NOT NULL, "
			+ COLUMN_NAME_EXPENSE_BASE_CATEGORY_ID + " INTEGER NOT NULL, "
			+ COLUMN_NAME_EXPENSE_CATEGORY_NAME + " TEXT NOT NULL, "
			+ COLUMN_NAME_CURRENCY_CODE + " TEXT NOT NULL, "
			+ COLUMN_NAME_EXPENSE_AMOUNT + " TEXT  NOT NULL, " 
			+ COLUMN_NAME_EXPENSE_NOTES + " TEXT, "
			+ COLUMN_NAME_HAS_RECEIPT + " INTEGER NOT NULL, "
			+ COLUMN_NAME_RECEIPT_IMAGE + " TEXT"
			+ ")";

	public static final String ALTER_TABLE_NAME_EOD_USER_INFO_TABLE_FOR_VERSION_2 = "ALTER TABLE " + TABLE_NAME_EOD_USERINFO_TABLE
			+" ADD "
			+ COLUMN_NAME_RESOURCE_ID + " TEXT_NOT_NULL";

	public static final String ALTER_TABLE_NAME_EOD_EXPENSE_CATEGORY_TABLE_FOR_BASE_CATEGORY_ID = "ALTER TABLE " + TABLE_NAME_EOD_EXPENSE_CATEGORY_TABLE
			+" ADD "
			+ COLUMN_NAME_RESOURCE_ID + " TEXT_NOT_NULL";

	//Constructor
	private EodDatabase()
	{
	}

	/**
	 * Singleton SqLite Object Approach
	 * <p>Initialize Instance, Singleton Factory Method. Call this method from once to initialize the "EodDatabase"
	 * <br>and "SqliteOpenHelper" instance throughout the application. If you subclass the Application Class, you can call
	 * <br>it from there.
	 * @param context
	 */
	public static synchronized void initializeInstance(Context context)
	{
		if (databaseInstance == null)
		{
			databaseInstance = new EodDatabase();
			sqliteOpenHelper = new OpenHelper(context);
		}
	}

	/**
	 * Synchronized method to get the Singleton instance of TestDatabase
	 * @return databaseInstance
	 */
	public static synchronized EodDatabase getInstance()
	{
		if (databaseInstance == null)
		{
			throw new IllegalStateException(EodDatabase.class.getSimpleName() +" is not initialized, call initializeInstance(..) method first.");
		}

		return databaseInstance;
	}

	/**
	 * Open Database, return the SqliteDatabase Object.
	 * <p>This method increments the counter to track how many threads are accessing the database.
	 *
	 * @return sqliteDatabase - write database object
	 */

	public synchronized SQLiteDatabase openDatabase()
	{
		if(mOpenCounter.incrementAndGet() == 1)
		{
			// Opening new database
			sqliteDatabase = sqliteOpenHelper.getWritableDatabase();

			//Enable Write Ahead Logging
			sqliteDatabase.enableWriteAheadLogging();
		}

		return sqliteDatabase;
	}

	/**
	 * CLose Database,
	 * <p>This method checks If any Thread is Accessing the Database. If no thread is accessing the database then it
	 * <br>close the database.
	 */
	public synchronized void closeDatabase()
	{
		if(mOpenCounter.decrementAndGet() == 0)
		{
			if(sqliteDatabase != null )
			{
				// Closing database
				sqliteDatabase.close();
			}
		}

	}

	//OpenHelper
	private static class OpenHelper extends SQLiteOpenHelper
	{
		OpenHelper(Context context)
		{
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onOpen(SQLiteDatabase sqLiteDatabase)
		{
			super.onOpen(sqLiteDatabase);
		}

		@Override
		public void onCreate(SQLiteDatabase sqLiteDatabase) 
		{
			//Log.e(TAG, "Table created");
			sqLiteDatabase.execSQL(CREATE_TABLE_SQL_EOD_USER_INFO);
			sqLiteDatabase.execSQL(CREATE_TABLE_SQL_EOD_EXPENSE_CATEGORY);
			sqLiteDatabase.execSQL(CREATE_TABLE_SQL_EOD_EXPENSE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) 
		{
			Log.d("test", "onUpgrade");

			switch (oldVersion)
			{
				case EodDatabase.DATABASE_VERSION_1:
					upgradeFromOneToTwo(sqLiteDatabase);
					break;
				default:
					break;
			}
		}

		private void upgradeFromOneToTwo(SQLiteDatabase sqLiteDatabase)
		{
//			sqLiteDatabase.execSQL(ALTER_TABLE_NAME_SOLO_EXPENSE_TABLE_FOR_VOICE_MEMO_PATH);
//			sqLiteDatabase.execSQL(ALTER_TABLE_NAME_SOLO_EXPENSE_TABLE_FOR_MOBILE_EXPENSE_UNIQUE_ID);
		}
	}
}
