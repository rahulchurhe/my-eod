package com.expenseondemand.eod.webservice.model;

public class MasterResponse
{
	private String message; //Message from Server
	private boolean success; //true/false
	private String responseCode;
	private String error;
	private String status;

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public boolean isSuccess() {
		return success = (status != null) && status.equalsIgnoreCase("success");
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getResponseCode()
	{
		return responseCode;
	}

	public void setResponseCode(String responseCode)
	{
		this.responseCode = responseCode;
	}

	public String getError()
	{
		return error;
	}

	public void setError(String error)
	{
		this.error = error;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}
}
