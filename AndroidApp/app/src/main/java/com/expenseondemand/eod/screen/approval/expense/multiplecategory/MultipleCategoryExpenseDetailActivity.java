package com.expenseondemand.eod.screen.approval.expense.multiplecategory;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.activity.BaseActivity;
import com.expenseondemand.eod.dialogFragment.ApplicationAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationErrorAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationNetworkDialog;
import com.expenseondemand.eod.exception.EODBusinessException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.DeviceManager;
import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.model.approval.detail.ExpenseDetail;
import com.expenseondemand.eod.model.approval.detail.attendees.AttendeesItem;
import com.expenseondemand.eod.model.approval.detail.dynamic.DynamicDetail;
import com.expenseondemand.eod.model.approval.detail.mileage.MultipleMileage;
import com.expenseondemand.eod.model.approval.detail.notes.Notes;
import com.expenseondemand.eod.model.approval.detail.project.ProjectDetailsItem;
import com.expenseondemand.eod.model.approval.detail.project.ReceiptItem;
import com.expenseondemand.eod.model.approval.expense.PolicyBreachModel;
import com.expenseondemand.eod.model.approval.expense.multiplecategory.MultiCategoryExpense;
import com.expenseondemand.eod.model.approval.expense.multiplecategory.MultiCategoryExpenseParam;
import com.expenseondemand.eod.screen.approval.detail.DynamicDetailsDialog;
import com.expenseondemand.eod.screen.approval.detail.attendees.AttendeesFragment;
import com.expenseondemand.eod.screen.approval.detail.mileage.MultipleMileageFragment;
import com.expenseondemand.eod.screen.approval.detail.notes.NotesFragment;
import com.expenseondemand.eod.screen.approval.detail.project.ProjectDetailFragment;
import com.expenseondemand.eod.screen.approval.detail.receipt.ReceiptFragment;
import com.expenseondemand.eod.screen.approval.detail.violation.PolicyViolationMutipleExpenseFragment;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.util.UIUtil;
import com.expenseondemand.eod.webservice.model.RejectionReasonModel.RejectionReasonModel;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by ITDIVISION\maximess087 on 18/1/17.
 */

public class MultipleCategoryExpenseDetailActivity extends BaseActivity implements View.OnClickListener, ApplicationAlertDialog.AlertDialogActionListener, ApplicationErrorAlertDialog.ErrorDialogActionListener {
    private ImageView imageViewExpenseCategoryIcon;
    private TextView textViewCategory;
    private TextView textViewUserName;
    private TextView textViewId;
    private TextView textViewDate;
    private TextView textViewAmount;
    private LinearLayout linearLayoutProjectDetail;
    private LinearLayout linearLayoutPolicyViolation;
    private LinearLayout linearLayoutAttendees;
    private LinearLayout linearLayoutNotes;
    private LinearLayout linearLayoutMultipleMileage;
    private ImageView imageViewPolicyViolationBlink;
    private ImageView imageViewRight;
    private ImageView imageViewMore;
    private TextView textViewAttendeesType;

    private MultiCategoryExpense multiCategoryExpense;
    private ExpenseDetail expenseDetail;

    private LinearLayout currentSelected;
    private ArrayList<LinearLayout> middleTabs;
    private int currentSelectedIndex;

    private final String EXPENSE_INFO_KEY = "EXPENSE_INFO_KEY";
    private final String EXPENSE_DETAIL_KEY = "EXPENSE_DETAIL_KEY";
    private final String SELECTED_TAB_INDEX_KEY = "SELECTED_TAB_INDEX_KEY";

    private ArrayList<RejectionReasonModel> rejectionReasonBackupList = new ArrayList<RejectionReasonModel>();
    private String str_rejectionNote, str_rejectionId, str_approveNote = null, str_acounterApproveNote = null;
    private ApplicationNetworkDialog applicationNetworkDialogl;
    private String expenseDate;
    private TextView txt_mileageStatus;


    ApplicationAlertDialog applicationAlertDialog;
    public static final int TYPE_ALERT = 0;
    private int receiptCount;
    private boolean isRejectedStatus;
    private boolean isrejected = false;
    private LinearLayout linearLayoutBottomBar;
    private View viewLineBottom;
    MultiCategoryExpenseParam expenseDetails;
    private PolicyBreachModel policyBreech;
    private LinearLayout linearLayoutReceipt;
    private TextView textViewCount;
    private ApplicationErrorAlertDialog applicationErrorAlertDialog;
    private ImageButton imageViewCloseButton;
    private RelativeLayout relativeOverlayScreenExpense;
    private String claimName;
    private String claimType;

    @Override
    public void onBackPressed() {
        if (relativeOverlayScreenExpense.getVisibility() == View.GONE) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (SharePreference.getBoolean(getApplicationContext(), AppConstant.BACK_NETWORK_SETTING_STAT)) {
            SharePreference.putBoolean(getApplicationContext(), AppConstant.BACK_NETWORK_SETTING_STAT, false);
            loadData();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.approval_expense_detail_activity);
        //Initialize View
        initializeView();
        if (DeviceManager.isOnline()) {
            // populateView1();
            loadData();
        } else {
            enableNetwork();
        }

    }

    //Network enable dialog
    void enableNetwork() {
        applicationNetworkDialogl = new ApplicationNetworkDialog().getInstance(MultipleCategoryExpenseDetailActivity.this);
        applicationNetworkDialogl.show(getSupportFragmentManager(), "dialog");
    }

    private void initializeView() {
        currentSelected = null;
        //get data object from MultipleCategoryList Activity
        Bundle bundle = getIntent().getExtras();
        multiCategoryExpense = bundle.getParcelable(AppConstant.KEY_MULTIPLE_CATEGORY_DETAIL_EXPENSE);
        policyBreech = bundle.getParcelable(AppConstant.KEY_MULTIPLE_CATEGORY_POLICY_DETAILS);
        expenseDetails = bundle.getParcelable(AppConstant.KEY_EXPENSE_DETAIL);
        isrejected = bundle.getBoolean("IS_REJECTED");
        claimName = bundle.getString(AppConstant.KEY_CLAIM_NAME);
        claimType = bundle.getString(AppConstant.KEY_CLAIM_TYPE);
        //Set actionbar
        handleToolBar(getResources().getString(R.string.title_multiple_category_expense_detail), R.color.approval_claim_list_toolbar, R.color.approval_claim_list_status_bar, true);

        imageViewExpenseCategoryIcon = (ImageView) findViewById(R.id.imageViewExpenseCategoryIcon);
        textViewCategory = (TextView) findViewById(R.id.textViewCategory);
        textViewUserName = (TextView) findViewById(R.id.textViewUserName);
        textViewId = (TextView) findViewById(R.id.textViewId);
        textViewDate = (TextView) findViewById(R.id.textViewDate);
        textViewAmount = (TextView) findViewById(R.id.textViewAmount);
        linearLayoutProjectDetail = (LinearLayout) findViewById(R.id.linearLayoutProjectDetail);
        linearLayoutPolicyViolation = (LinearLayout) findViewById(R.id.linearLayoutPolicyViolation);
        linearLayoutAttendees = (LinearLayout) findViewById(R.id.linearLayoutAttendees);
        linearLayoutReceipt = (LinearLayout) findViewById(R.id.linearLayoutReceipt);
        textViewCount = (TextView) findViewById(R.id.textViewCount);
        linearLayoutNotes = (LinearLayout) findViewById(R.id.linearLayoutNotes);
        linearLayoutMultipleMileage = (LinearLayout) findViewById(R.id.linearLayoutMultipleMileage);
        imageViewPolicyViolationBlink = (ImageView) findViewById(R.id.imageViewPolicyViolationBlink);
        imageViewRight = (ImageView) findViewById(R.id.imageViewRight);
        imageViewMore = (ImageView) findViewById(R.id.imageViewMore);
        textViewAttendeesType = (TextView) findViewById(R.id.textViewAttendeesType);
        txt_mileageStatus = (TextView) findViewById(R.id.txt_mileageStatus);
        textViewUserName.setSelected(true);
        //overlay
        imageViewCloseButton = (ImageButton) findViewById(R.id.imageViewCloseButton);
        relativeOverlayScreenExpense = (RelativeLayout) findViewById(R.id.relativeOverlayScreenExpense);

        setPolicyViolationTabAnimation();

        viewLineBottom = (View) findViewById(R.id.viewLineBottom);
        linearLayoutBottomBar = (LinearLayout) findViewById(R.id.linearLayoutBottomBar);
        linearLayoutBottomBar.setVisibility(View.GONE);
        viewLineBottom.setVisibility(View.GONE);

        linearLayoutProjectDetail.setOnClickListener(this);
        linearLayoutPolicyViolation.setOnClickListener(this);
        linearLayoutReceipt.setOnClickListener(this);
        linearLayoutAttendees.setOnClickListener(this);
        linearLayoutNotes.setOnClickListener(this);
        linearLayoutMultipleMileage.setOnClickListener(this);
        imageViewMore.setOnClickListener(this);
        imageViewCloseButton.setOnClickListener(this);
        relativeOverlayScreenExpense.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Get Menu Inflater
        MenuInflater inflater = getMenuInflater();

        //Inflate Menu
        inflater.inflate(R.menu.help_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuItemId = item.getItemId();
        switch (menuItemId) {
            case R.id.menuHelp:
                //Handle OverlayScreen
                handleOverlayScreen();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadData() {
        if (DeviceManager.isOnline()) {
            new ExpenseDetailAsyncTask().execute();
        } else {
            enableNetwork();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //  outState.putParcelable(EXPENSE_INFO_KEY, expenseInfo);
        outState.putParcelable(EXPENSE_DETAIL_KEY, expenseDetail);
        outState.putInt(SELECTED_TAB_INDEX_KEY, currentSelectedIndex);
        super.onSaveInstanceState(outState);
    }

    private void restoreData(Bundle savedInstanceState) {
        //  expenseInfo = savedInstanceState.getParcelable(EXPENSE_INFO_KEY);
        expenseDetail = savedInstanceState.getParcelable(EXPENSE_DETAIL_KEY);
        currentSelectedIndex = savedInstanceState.getInt(SELECTED_TAB_INDEX_KEY);
        populateView();
        currentSelected = middleTabs.get(currentSelectedIndex);
        if (currentSelected != null)
            currentSelected.setSelected(true);
    }

    private void setPolicyViolationTabAnimation() {
        //Setting blink animation on Policy Violation tab
        AlphaAnimation blinkAnimation = new AlphaAnimation(.85f, 0); // Change alpha from .85 visible to invisible
        blinkAnimation.setDuration(300); // duration - half a second
        blinkAnimation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
        blinkAnimation.setRepeatCount(Animation.INFINITE); // Repeat animation infinitely
        blinkAnimation.setRepeatMode(Animation.REVERSE);
        imageViewPolicyViolationBlink.setAnimation(blinkAnimation);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linearLayoutProjectDetail: {
                handleProjectDetailClick();
                break;
            }

            case R.id.linearLayoutPolicyViolation: {
                handlePolicyViolationClick();
                break;
            }

            case R.id.linearLayoutReceipt: {
                handleReceiptClick();
                break;
            }

            case R.id.linearLayoutAttendees: {
                handleAttendeesClick();
                break;
            }

            case R.id.linearLayoutNotes: {
                handleNotesClick();
                break;
            }

            case R.id.linearLayoutMultipleMileage: {
                handleMultipleMileageClick();
                break;
            }

            case R.id.imageViewMore: {
                handleMore();
                break;
            }

            case R.id.imageViewCloseButton:
                handleOverlayScreen();
                break;

            case R.id.relativeOverlayScreenExpense:
                handleOverlayScreen();
                break;

        }
    }

    //OverLay screen
    void handleOverlayScreen() {
        relativeOverlayScreenExpense.setVisibility(relativeOverlayScreenExpense.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
    }


    private void handleProjectDetailClick() {
        if (currentSelected != linearLayoutProjectDetail) {
            unselectSelectedItem();
            currentSelected = linearLayoutProjectDetail;

            Fragment projectDetailFragment = ProjectDetailFragment.getInstance(expenseDetail.getNotes().getPreApprovedDetails(), expenseDetail.getProjectDetails(), expenseDetail.getAdditionalDetails(), expenseDetail.getPaymentType(), expenseDetail.getBusinessPurpose(), expenseDetails.getExpenseDetailID(), expenseDetails.getExpenseHeaderID());
            selectItem(projectDetailFragment);
        }
    }

    private void unselectSelectedItem() {
        if (currentSelected != null)
            currentSelected.setSelected(false);
    }

    private void selectItem(Fragment fragment) {
        currentSelected.setSelected(true);
        currentSelectedIndex = middleTabs.indexOf(currentSelected);
        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutDetail, fragment).commit();
    }

    private void handlePolicyViolationClick() {
        if (currentSelected != linearLayoutPolicyViolation) {
            unselectSelectedItem();
            currentSelected = linearLayoutPolicyViolation;

            Fragment policyViolationFragment = PolicyViolationMutipleExpenseFragment.getInstance(policyBreech, isrejected);
            selectItem(policyViolationFragment);
        }
    }

    private void handleReceiptClick() {
        //ToDO
        if (currentSelected != linearLayoutReceipt) {
            unselectSelectedItem();
            currentSelected = linearLayoutReceipt;
            Fragment projectDetailFragment = ReceiptFragment.getInstance(expenseDetail.getProjectDetails(), expenseDetail.getReceipt(), expenseDetails.getExpenseDetailID(), expenseDetails.getExpenseHeaderID());
            selectItem(projectDetailFragment);
        }
    }

    private void handleAttendeesClick() {
        if (currentSelected != linearLayoutAttendees) {
            unselectSelectedItem();
            currentSelected = linearLayoutAttendees;

            Fragment attendeesFragment = AttendeesFragment.getInstance(expenseDetail.getAttendees());
            selectItem(attendeesFragment);
        }
    }

    private void handleNotesClick() {
        if (currentSelected != linearLayoutNotes) {
            unselectSelectedItem();
            currentSelected = linearLayoutNotes;

            Fragment notesFragment = NotesFragment.getInstance(expenseDetail.getNotes(), policyBreech.getNotes());
            selectItem(notesFragment);
        }
    }

    private void handleMultipleMileageClick() {
        if (currentSelected != linearLayoutMultipleMileage) {
            unselectSelectedItem();
            currentSelected = linearLayoutMultipleMileage;

            MultipleMileage mileage = expenseDetail.getMileage();

            Fragment multipleMileageFragment = MultipleMileageFragment.getInstance(mileage.getMultipleMileageItems(), mileage.getVehicleID());
            selectItem(multipleMileageFragment);
        }
    }

    private void handleMore() {
        if (expenseDetail != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            DynamicDetailsDialog dynamicDetailsDialog = DynamicDetailsDialog.getInstance(expenseDetail.getDynamicDetails());
            dynamicDetailsDialog.show(fragmentTransaction, "Dialog");
        }
    }

    @Override
    public void onRetryButtonClick(int type) {
        if (DeviceManager.isOnline()) {
            loadData();
        } else {
            enableNetwork();
        }
    }

    @Override
    public void onCancelButtonClick() {

    }


    /***
     * MultipleCategoryExpenseDetail AsyncTask
     */
    private class ExpenseDetailAsyncTask extends AsyncTask<Void, Void, ExpenseDetail> {
        private Exception exception = null;
        private Dialog dialog = null;

        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(MultipleCategoryExpenseDetailActivity.this);
        }

        @Override
        protected ExpenseDetail doInBackground(Void... params) {
            ExpenseDetail expenseDetail = null;

            try {
                expenseDetail = MultipleCategoryExpenseDetailManager.getMultipleCategoryExpenseDetail(expenseDetails.getExpenseHeaderID(), expenseDetails.getExpenseDetailID(), multiCategoryExpense.getExpenseItemizationId(), multiCategoryExpense.getDate());
            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return expenseDetail;
        }

        @Override
        protected void onPostExecute(ExpenseDetail expenseDetail) {
            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);

            if (exception == null) {
                //OnSucess
                onSuccessGetExpenseDetail(expenseDetail);
            } else if (exception instanceof EODBusinessException) {
                //OnFail
                onFailGetExpenseDetail((EODException) exception);
            } else if (exception instanceof EODException) {
                //OnError
                onErrorGetExpenseDetail(exception);
            }
        }
    }

    private void onSuccessGetExpenseDetail(ExpenseDetail expenseDetailData) {
        expenseDetail = expenseDetailData;
        if (expenseDetail != null) {
            LinearLayout currentSelected = populateView();
            if (currentSelected != null) {
                onClick(currentSelected);
            }
        }
    }

    private void onFailGetExpenseDetail(EODException exception) {
        // showAlertDialog(getResources().getString(R.string.text_error));
        showErrorAlertDialog(false, null, null, null, ApplicationAlertDialog.TYPE_ERROR);
    }

    private void onErrorGetExpenseDetail(Exception exception) {
        // showAlertDialog(getResources().getString(R.string.text_error));
        showErrorAlertDialog(false, null, null, null, ApplicationAlertDialog.TYPE_ERROR);
    }


    private LinearLayout populateView() {
        imageViewRight.setVisibility(View.GONE);


        if (!AppUtil.isStringEmpty(multiCategoryExpense.getUniqueId())) {
            textViewId.setText(getResources().getString(R.string.approval_expense_detail_unique_id, multiCategoryExpense.getUniqueId()));
        }


        textViewAmount.setText(new DecimalFormat("0.00").format(Double.parseDouble(multiCategoryExpense.getAmount())));

        long date = AppUtil.getLongDate(AppConstant.FORMAT_DATE_WEB_SERVICE, multiCategoryExpense.getDate());
        String formattedDate = AppUtil.getFormattedDateString(AppConstant.FORMAT_DATE_DISPLAY, date);
        textViewDate.setText(formattedDate);

       /* UserInfo userInfo = CachingManager.getUserInfo();
        if (userInfo != null) {
            textViewUserName.setText(userInfo.getDisplayName());
        }*/

        textViewUserName.setText(claimName);

        textViewCategory.setText(multiCategoryExpense.getExpneseCategory());
        int resourceId = CachingManager.getCategoryResourceId(Integer.parseInt(multiCategoryExpense.getBaseCategoryId()));
        imageViewExpenseCategoryIcon.setImageResource(resourceId);


        LinearLayout currentSelected = null;
        middleTabs = new ArrayList<>();

        MultipleMileage multipleMileage = expenseDetail.getMileage();
        if (multipleMileage != null) {
            linearLayoutMultipleMileage.setVisibility(View.VISIBLE);
            currentSelected = linearLayoutMultipleMileage;
            if (multipleMileage.getMultipleMileageItems().size() > 1) {
                txt_mileageStatus.setText(getResources().getString(R.string.approval_expense_detail_multiple_mileage));
            } else {
                txt_mileageStatus.setText(getResources().getString(R.string.approval_expense_detail_mileage));
            }
            middleTabs.add(currentSelected);
        } else
            linearLayoutMultipleMileage.setVisibility(View.GONE);

        Notes notes = expenseDetail.getNotes();

        if (notes != null && (!notes.getCcNotes().equals("") || !notes.getEnteredNotes().equals("") || !notes.getMobileNotes().equals(""))) {
            linearLayoutNotes.setVisibility(View.VISIBLE);
            currentSelected = linearLayoutNotes;
            middleTabs.add(currentSelected);
        } else
            linearLayoutNotes.setVisibility(View.GONE);

        ArrayList<AttendeesItem> attendeesItems = expenseDetail.getAttendees();
        if (!AppUtil.isCollectionEmpty(attendeesItems)) {
            linearLayoutAttendees.setVisibility(View.VISIBLE);
            currentSelected = linearLayoutAttendees;
            middleTabs.add(currentSelected);
            textViewAttendeesType.setText(String.valueOf(attendeesItems.size()));
        } else
            linearLayoutAttendees.setVisibility(View.GONE);


        if (policyBreech != null) {
            if (policyBreech.isDailyCap() || policyBreech.isMaxSpendBreach()) {
                linearLayoutPolicyViolation.setVisibility(View.VISIBLE);
                currentSelected = linearLayoutPolicyViolation;
                middleTabs.add(currentSelected);
            } else
                linearLayoutPolicyViolation.setVisibility(View.GONE);
        } else
            linearLayoutPolicyViolation.setVisibility(View.GONE);

        /*ArrayList<ProjectDetailsItem> projectDetailItems = expenseDetail.getProjectDetails();
        if (!AppUtil.isCollectionEmpty(projectDetailItems)) {
            linearLayoutProjectDetail.setVisibility(View.VISIBLE);
            currentSelected = linearLayoutProjectDetail;
            middleTabs.add(currentSelected);
        } else
            linearLayoutProjectDetail.setVisibility(View.GONE);*/
        ArrayList<ProjectDetailsItem> projectDetailItems = expenseDetail.getProjectDetails();
        if (!AppUtil.isCollectionEmpty(projectDetailItems)) {
            linearLayoutProjectDetail.setVisibility(View.VISIBLE);
            currentSelected = linearLayoutProjectDetail;
            middleTabs.add(currentSelected);
        } else
            linearLayoutProjectDetail.setVisibility(View.GONE);

        /*Receipt Tab*/
        ReceiptItem receipt = expenseDetail.getReceipt();
        if (receipt != null) {
            linearLayoutReceipt.setVisibility(View.VISIBLE);
            if (Integer.parseInt(receipt.getCount()) == 0) {
                textViewCount.setVisibility(View.GONE);
            } else {
                textViewCount.setVisibility(View.VISIBLE);
                textViewCount.setText(receipt.getCount());
            }
        } else {
            linearLayoutReceipt.setVisibility(View.GONE);
        }


        ArrayList<DynamicDetail> dynamicDetails = expenseDetail.getDynamicDetails();
        imageViewMore.setVisibility((!AppUtil.isCollectionEmpty(dynamicDetails)) ? View.VISIBLE : View.GONE);

        return currentSelected;
    }


    private void showAlertDialog(String msg) {
        applicationAlertDialog = new ApplicationAlertDialog().getInstance(this, true, getString(R.string.text_error_processing), msg, getString(R.string.button_ok), TYPE_ALERT);
        applicationAlertDialog.show(getSupportFragmentManager(), "dialog");
    }

    void showErrorAlertDialog(boolean neutralButton, String message, String title, String okButtonTitle, int type) {
        applicationErrorAlertDialog = new ApplicationErrorAlertDialog().getInstance(this, message, title, okButtonTitle, type);
        applicationErrorAlertDialog.show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public void onPositiveButtonClick(int type) {
        switch (type) {
            case 0:
                //
                break;
            case 1:
                //
                break;
        }
    }

    @Override
    public void onNegativeButtonClick() {
    }


}


