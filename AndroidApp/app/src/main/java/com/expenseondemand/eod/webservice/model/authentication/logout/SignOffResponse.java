package com.expenseondemand.eod.webservice.model.authentication.logout;

import com.expenseondemand.eod.webservice.model.MasterResponse;

public class SignOffResponse extends MasterResponse
{
    private Data data;

    public Data getData()
    {
        return data;
    }

    public void setData(Data data)
    {
        this.data = data;
    }
}
