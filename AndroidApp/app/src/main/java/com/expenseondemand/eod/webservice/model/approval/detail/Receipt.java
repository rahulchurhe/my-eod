package com.expenseondemand.eod.webservice.model.approval.detail;

/**
 * Created by ITDIVISION\maximess087 on 22/2/17.
 */

public class Receipt {
    private String LabelName;
    private String Value;
    private String Count;

    public String getLabelName() {
        return LabelName;
    }

    public void setLabelName(String labelName) {
        LabelName = labelName;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String count) {
        Count = count;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
