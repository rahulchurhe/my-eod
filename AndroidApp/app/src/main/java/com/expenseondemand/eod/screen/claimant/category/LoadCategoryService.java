package com.expenseondemand.eod.screen.claimant.category;

import android.app.IntentService;
import android.content.Intent;

import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.model.UserInfo;

/**
 * Created by Ramit Yadav on 12-10-2016.
 */

public class LoadCategoryService extends IntentService
{
    public final static String PASS_SERVICE_STATUS = "Pass_Service_Status";
    public final static String PASS_SERVICE_STATUS_KEY = "Pass_Service_Status_Key";
    public final static String SERVICE_FINISHED = "SERVICE_FINISHED";
    public final static String SERVICE_RUNNING = "SERVICE_RUNNING";
    public final static String SERVICE_ERROR= "SERVICE_ERROR";

    public LoadCategoryService()
    {
        super("LoadCategoryService");
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {

            handleExpenseCategory();

    }



    void handleExpenseCategory()
    {
      //   passCategoryServiceStatusToActivity(SERVICE_RUNNING);

         UserInfo userInfo = null;

             //get User Info
              userInfo = CachingManager.getUserInfo();
				try
				{
					//Save User Information in DB
					if(userInfo != null)
					{
							//Sync Categories
							 GlobalDataManager.syncExpenseCategory();



                        passCategoryServiceStatusToActivity(SERVICE_FINISHED);
					}
				}
				catch(EODException eodException)
				{
                    passCategoryServiceStatusToActivity(SERVICE_ERROR);
				}
    }


    private void passCategoryServiceStatusToActivity(String status)
    {
        Intent intent = new Intent();
        intent.setAction(PASS_SERVICE_STATUS);
        intent.putExtra(PASS_SERVICE_STATUS_KEY,status);
        sendBroadcast(intent);
    }
}
