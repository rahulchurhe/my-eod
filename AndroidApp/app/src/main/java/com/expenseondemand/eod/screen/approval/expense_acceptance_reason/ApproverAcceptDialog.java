package com.expenseondemand.eod.screen.approval.expense_acceptance_reason;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.model.AppRejectionSetting;
import com.expenseondemand.eod.screen.approval.detail.ApproveRejectedInterface;

/**
 * Created by Nandani Gupta on 2016-10-05.
 */
public class ApproverAcceptDialog extends DialogFragment implements View.OnClickListener {
    Dialog dialog;
    TextView textViewCancel;
    TextView textViewOk;
    EditText editTextAcceptNotes;
    TextView textViewNotesCount;
    private static ApproveRejectedInterface listener;
    AppRejectionSetting appRejectionSetting;
    private TextView textViewNoticeForAll;

    @Override
    public void onStart()
    {
        super.onStart();
        getDialog().getWindow().setLayout( ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);

    }
    public static ApproverAcceptDialog newInstance(int title, ApproveRejectedInterface listener1) {
        ApproverAcceptDialog frag = new ApproverAcceptDialog();
        Bundle args = new Bundle();
        args.putInt("title", title);
        frag.setArguments(args);
        listener = listener1;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        initialization();
        setNoteAsPerRejectionSettingRule();
        return dialog;
    }

    private void setNoteAsPerRejectionSettingRule() {
        if(SharePreference.getBoolean(getActivity(),"IsMultiAccept")){
            textViewNoticeForAll.setVisibility(View.VISIBLE);
        }else{
            textViewNoticeForAll.setVisibility(View.GONE);
        }

    }

    //Initialize
    void initialization() {
        //Get App Rejected Setting rules.
        appRejectionSetting= CachingManager.getAppRejectedSetting();

        //initialize dialog
        dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.approver_accept_dialog);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        // set Ok Cancel Button Click
        textViewCancel = (TextView) dialog.findViewById(R.id.textViewCancel);
        textViewCancel.setOnClickListener(this);
        textViewOk = (TextView) dialog.findViewById(R.id.textViewOk);
        textViewOk.setOnClickListener(this);

        // set rejection notes count
        editTextAcceptNotes = (EditText) dialog.findViewById(R.id.editTextAcceptNotes);
        textViewNotesCount = (TextView) dialog.findViewById(R.id.textViewNotesCount);

        textViewNoticeForAll=(TextView)dialog.findViewById(R.id.textViewNoticeForAll);

        editTextAcceptNotes.addTextChangedListener(new AcceptNotesTextWatcher());
    }

    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        switch (viewId) {
            case R.id.textViewCancel:
                dialog.dismiss();
                break;
            case R.id.textViewOk:
                if (editTextAcceptNotes.getText().toString().length() > 1) {
                    listener.approveClickOk(editTextAcceptNotes.getText().toString(), editTextAcceptNotes.getText().toString());
                    dialog.dismiss();
                } else {
                    Toast.makeText(getContext(),getString(R.string.approvalReason), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    /*** Text Watcher*/
    private class AcceptNotesTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence text, int start, int before, int count) {
        }

        @Override
        public void onTextChanged(CharSequence text, int start, int before, int count) {
            updateNotesTextCount(text.length());
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    }
    void updateNotesTextCount(int count) {
        int updatedValue = getResources().getInteger(R.integer.approval_expense_screen_rejection_notes_max_length) - count;
        textViewNotesCount.setText(String.valueOf(updatedValue));
    }
}

