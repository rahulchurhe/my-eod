package com.expenseondemand.eod.webservice.model.approval.detail.expense.receipt;

/**
 * Created by ITDIVISION\maximess087 on 28/1/17.
 */

public class ExpenseDetailsReceipt {
    private String ExpenseSupportDocID;
    private String ReferenceNumber;
    private String Document;
    private String Description;
    private String ExpenseAmount;
    private String CurrencyCode;
    private String Amount;
    private String Varified;
    private String VarifiedBy;
    private String VarificationAmount;

    public String getExpenseSupportDocID() {
        return ExpenseSupportDocID;
    }

    public void setExpenseSupportDocID(String expenseSupportDocID) {
        ExpenseSupportDocID = expenseSupportDocID;
    }

    public String getReferenceNumber() {
        return ReferenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        ReferenceNumber = referenceNumber;
    }

    public String getDocument() {
        return Document;
    }

    public void setDocument(String document) {
        Document = document;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getExpenseAmount() {
        return ExpenseAmount;
    }

    public void setExpenseAmount(String expenseAmount) {
        ExpenseAmount = expenseAmount;
    }

    public String getCurrencyCode() {
        return CurrencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        CurrencyCode = currencyCode;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getVarified() {
        return Varified;
    }

    public void setVarified(String varified) {
        Varified = varified;
    }

    public String getVarifiedBy() {
        return VarifiedBy;
    }

    public void setVarifiedBy(String varifiedBy) {
        VarifiedBy = varifiedBy;
    }

    public String getVarificationAmount() {
        return VarificationAmount;
    }

    public void setVarificationAmount(String varificationAmount) {
        VarificationAmount = varificationAmount;
    }
}
