package com.expenseondemand.eod.model.approval.detail.mileage;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 19-09-2016.
 */
public class MultipleMileageItem
{
    private String sourceAddress;
    private String destinationAddress;
    private String distance;
    private String amount;

    public String getSourceAddress()
    {
        return sourceAddress;
    }

    public void setSourceAddress(String sourceAddress)
    {
        this.sourceAddress = sourceAddress;
    }

    public String getDestinationAddress()
    {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress)
    {
        this.destinationAddress = destinationAddress;
    }

    public String getDistance()
    {
        return distance;
    }

    public void setDistance(String distance)
    {
        this.distance = distance;
    }

    public String getAmount()
    {
        return amount;
    }

    public void setAmount(String amount)
    {
        this.amount = amount;
    }
}
