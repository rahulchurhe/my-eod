package com.expenseondemand.eod.webservice.model.RejectionReasonModel;

import com.expenseondemand.eod.webservice.model.MasterResponse;

import java.util.ArrayList;

/**
 * Created by Nandani Gupta on 2016-10-17.
 */
public class RejectionReasonResponseList extends MasterResponse
{
    ArrayList<RejectionReasonModel> data;

    public ArrayList<RejectionReasonModel> getData()
    {
        return data;
    }

    public void setData(ArrayList<RejectionReasonModel> data)
    {
        this.data = data;
    }
}
