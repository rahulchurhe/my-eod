package com.expenseondemand.eod.model;

/**
 * Created by priya on 15-12-2016.
 */

public class MobileExpense {

    private long mobileUploadExpenseId;
    private String clientItemId;
    private String dateExpense;
    private double amount;
    private String currencyCode;
    private int expenseCategoryId;
    private String dateUpload;
    private String description;
    private int resourceId;
    private String recieptImagePath;
    private String flag;
    private String expenseCategoryName;
    private String conversionRate;
    private String hasReceipt;

    public long getMobileUploadExpenseId() {
        return mobileUploadExpenseId;
    }

    public void setMobileUploadExpenseId(long mobileUploadExpenseId) {
        this.mobileUploadExpenseId = mobileUploadExpenseId;
    }

    public String getClientItemId() {
        return clientItemId;
    }

    public void setClientItemId(String clientItemId) {
        this.clientItemId = clientItemId;
    }

    public String getDateExpense() {
        return dateExpense;
    }

    public void setDateExpense(String dateExpense) {
        this.dateExpense = dateExpense;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public int getExpenseCategoryId() {
        return expenseCategoryId;
    }

    public void setExpenseCategoryId(int expenseCategoryId) {
        this.expenseCategoryId = expenseCategoryId;
    }

    public String getDateUpload() {
        return dateUpload;
    }

    public void setDateUpload(String dateUpload) {
        this.dateUpload = dateUpload;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public String getRecieptImagePath() {
        return recieptImagePath;
    }

    public void setRecieptImagePath(String recieptImagePath) {
        this.recieptImagePath = recieptImagePath;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getExpenseCategoryName() {
        return expenseCategoryName;
    }

    public void setExpenseCategoryName(String expenseCategoryName) {
        this.expenseCategoryName = expenseCategoryName;
    }

    public String getConversionRate() {
        return conversionRate;
    }

    public void setConversionRate(String conversionRate) {
        this.conversionRate = conversionRate;
    }

    public String getHasReceipt() {
        return hasReceipt;
    }

    public void setHasReceipt(String hasReceipt) {
        this.hasReceipt = hasReceipt;
    }
}
