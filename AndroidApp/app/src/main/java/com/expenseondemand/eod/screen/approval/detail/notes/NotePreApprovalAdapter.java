package com.expenseondemand.eod.screen.approval.detail.notes;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.model.approval.detail.project.PreApprovedDetailsItem;
import com.expenseondemand.eod.model.approval.detail.project.ProjectDetailsItem;
import com.expenseondemand.eod.util.AppUtil;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 16-09-2016.
 */
public class NotePreApprovalAdapter extends RecyclerView.Adapter {
    private final ArrayList<PreApprovedDetailsItem> projectDetailData;
    private Context context;


    public NotePreApprovalAdapter(Context context, ArrayList<PreApprovedDetailsItem> projectDetailData) {
        this.context = context;
        this.projectDetailData = projectDetailData;
    }

    @Override
    public int getItemCount() {
        return AppUtil.isCollectionEmpty(projectDetailData) ? 0 : projectDetailData.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.project_detail_item, parent, false);

        return new ProjectDetailItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        PreApprovedDetailsItem projectDetailItem = projectDetailData.get(position);

        ProjectDetailItemHolder projectDetailItemHolder = (ProjectDetailItemHolder) holder;
        projectDetailItemHolder.textViewLeft.setText(projectDetailItem.getLabelName());
        projectDetailItemHolder.textViewRight.setText(projectDetailItem.getValue());
    }

    protected static class ProjectDetailItemHolder extends RecyclerView.ViewHolder {
        TextView textViewLeft;
        TextView textViewRight;

        public ProjectDetailItemHolder(View itemView) {
            super(itemView);

            textViewLeft = (TextView) itemView.findViewById(R.id.textViewLeft);
            textViewRight = (TextView) itemView.findViewById(R.id.textViewRight);

        }
    }

}
