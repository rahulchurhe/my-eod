package com.expenseondemand.eod.model.approval.expense;

import com.expenseondemand.eod.webservice.model.approval.item.DuplicateItemsModel;

import java.util.ArrayList;

/**
 * Created by ITDIVISION\maximess087 on 17/1/17.
 */

public class PolicyDetailsResponse  {
    private String LateSubmission;
    private String NormalSubmissionDays;//
    private String IsResubmitted;
    private String IsEscalated;
    private String IsDailyLimitBreached;
    private String DailyLimitAmount;//
    private String IsMaxSpentBreach;
    private String MaxSpentAmount;
    private String PreApprovalBreach;
    private String PreAprovalAmount;
    private String IsCaRequired;
    private ArrayList<Notes> Notes;

    public ArrayList<com.expenseondemand.eod.model.approval.expense.Notes> getNotes() {
        return Notes;
    }

    public void setNotes(ArrayList<com.expenseondemand.eod.model.approval.expense.Notes> notes) {
        Notes = notes;
    }

    public String getLateSubmission() {
        return LateSubmission;
    }

    public void setLateSubmission(String lateSubmission) {
        LateSubmission = lateSubmission;
    }

    public String getNormalSubmissionDays() {
        return NormalSubmissionDays;
    }

    public void setNormalSubmissionDays(String normalSubmissionDays) {
        NormalSubmissionDays = normalSubmissionDays;
    }

    public String getIsResubmitted() {
        return IsResubmitted;
    }

    public void setIsResubmitted(String isResubmitted) {
        IsResubmitted = isResubmitted;
    }

    public String getIsEscalated() {
        return IsEscalated;
    }

    public void setIsEscalated(String isEscalated) {
        this.IsEscalated = isEscalated;
    }


    public String getIsMaxSpentBreach() {
        return IsMaxSpentBreach;
    }

    public void setIsMaxSpentBreach(String isMaxSpentBreach) {
        IsMaxSpentBreach = isMaxSpentBreach;
    }

    public String getIsDailyLimitBreached() {
        return IsDailyLimitBreached;
    }

    public void setIsDailyLimitBreached(String isDailyLimitBreached) {
        IsDailyLimitBreached = isDailyLimitBreached;
    }

    public String getDailyLimitAmount() {
        return DailyLimitAmount;
    }

    public void setDailyLimitAmount(String dailyLimitAmount) {
        DailyLimitAmount = dailyLimitAmount;
    }

    public String getMaxSpentAmount() {
        return MaxSpentAmount;
    }

    public void setMaxSpentAmount(String maxSpentAmount) {
        MaxSpentAmount = maxSpentAmount;
    }

    public String getPreApprovalBreach() {
        return PreApprovalBreach;
    }

    public void setPreApprovalBreach(String preApprovalBreach) {
        PreApprovalBreach = preApprovalBreach;
    }

    public String getPreAprovalAmount() {
        return PreAprovalAmount;
    }

    public void setPreAprovalAmount(String preAprovalAmount) {
        PreAprovalAmount = preAprovalAmount;
    }

    public String getIsCaRequired() {
        return IsCaRequired;
    }

    public void setIsCaRequired(String isCaRequired) {
        IsCaRequired = isCaRequired;
    }
}
