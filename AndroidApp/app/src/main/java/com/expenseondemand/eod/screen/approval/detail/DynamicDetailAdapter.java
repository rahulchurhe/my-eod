package com.expenseondemand.eod.screen.approval.detail;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.model.approval.detail.dynamic.DynamicDetail;
import com.expenseondemand.eod.util.AppUtil;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 01-10-2016.
 */

public class DynamicDetailAdapter extends RecyclerView.Adapter
{
    private ArrayList<DynamicDetail> dynamicDetailsData;
    private ItemHeightListener itemHeightListener;
    private Context context;

    private int itemHeight;

    public DynamicDetailAdapter(Context context, ItemHeightListener itemHeightListener, ArrayList<DynamicDetail> dynamicDetailsData)
    {
        this.dynamicDetailsData = dynamicDetailsData;
        this.itemHeightListener = itemHeightListener;
        this.context = context;
    }

    public interface ItemHeightListener
    {
        void onMeasureItemHeight(int height);
    }

    @Override
    public int getItemCount()
    {
        return AppUtil.isCollectionEmpty(dynamicDetailsData) ? 0 : dynamicDetailsData.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dynamic_detail_item, parent, false);

        return new DynamicDetailAdapter.DynamicDetailItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        DynamicDetail dynamicDetail = dynamicDetailsData.get(position);

        DynamicDetailItemHolder dynamicDetailItemHolder = (DynamicDetailItemHolder)holder;
        dynamicDetailItemHolder.textViewLeft.setText(dynamicDetail.getLabel());
        dynamicDetailItemHolder.textViewRight.setText(dynamicDetail.getValue());

        if(itemHeight == 0)
        {
            final LinearLayout linearLayout = dynamicDetailItemHolder.linearLayoutDynamicDetail;
            linearLayout.post(new Runnable()
            {
                @Override
                public void run()
                {
                    Log.d("Ramit2 ", "value " + linearLayout.getHeight());
                    itemHeight = linearLayout.getHeight() + (int)context.getResources().getDimension(R.dimen.approval_expense_detail_dynamic_detail_item_vertical_margin);
                    itemHeightListener.onMeasureItemHeight(itemHeight);
                }

            });
        }
    }

    protected static class DynamicDetailItemHolder extends RecyclerView.ViewHolder
    {
        LinearLayout linearLayoutDynamicDetail;
        TextView textViewLeft;
        TextView textViewRight;

        public DynamicDetailItemHolder(View itemView)
        {
            super(itemView);

            linearLayoutDynamicDetail = (LinearLayout) itemView.findViewById(R.id.linearLayoutDynamicDetail);
            textViewLeft = (TextView) itemView.findViewById(R.id.textViewLeft);
            textViewRight = (TextView)itemView.findViewById(R.id.textViewRight);
        }
    }
}
