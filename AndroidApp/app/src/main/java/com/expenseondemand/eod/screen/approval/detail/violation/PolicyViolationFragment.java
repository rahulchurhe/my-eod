package com.expenseondemand.eod.screen.approval.detail.violation;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.expenseondemand.eod.BaseFragment;
import com.expenseondemand.eod.R;
import com.expenseondemand.eod.model.approval.expense.PolicyBreachModel;
import com.expenseondemand.eod.util.AppUtil;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 16-09-2016.
 */
public class PolicyViolationFragment extends BaseFragment implements View.OnClickListener {
    private static boolean isRejected1 = false;
    private final String POLICY_BREACH_KEY = "POLICY_BREACH_KEY";

    private PolicyBreachModel policyBreachModel;
    private boolean isExpandedReason;

    private RelativeLayout relativeLayoutLimit;
    private TextView textViewSpendLabel;
    private TextView textViewSpendValue;
    private TextView textViewCapLabel;
    private TextView textViewCapValue;
    private TextView textViewAmountLabel;
    private TextView textViewAmountValue;

    private LinearLayout linearLayoutMessage;
    private TextView textViewViolationMessageCounterApprover;
    private TextView textViewViolationMessageApproverLimit;
    private TextView textViewViolationMessageOverdue;
    private TextView textViewViolationResubmittedRejected;
    private TextView textViewViolationMessageDailyCap;
    private TextView textViewViolationMessagePreApproved;
    private TextView textViewViolationMessageMaxSpendLimit;

    private LinearLayout linearLayoutReason;
    private ImageView imageViewPlus;
    private LinearLayout linearLayoutExpandable;

    public static PolicyViolationFragment getInstance(PolicyBreachModel policyBreachModel, boolean isRejected) {
        PolicyViolationFragment policyViolationFragment = new PolicyViolationFragment();
        policyViolationFragment.policyBreachModel = policyBreachModel;
        isRejected1 = isRejected;
        return policyViolationFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.policy_violation_fragment, container, false);

        initializeView(rootView);

        if (savedInstanceState != null)
            policyBreachModel = savedInstanceState.getParcelable(POLICY_BREACH_KEY);

        populateView();

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(POLICY_BREACH_KEY, policyBreachModel);
        super.onSaveInstanceState(outState);
    }

    private void initializeView(View rootView) {
        relativeLayoutLimit = (RelativeLayout) rootView.findViewById(R.id.relativeLayoutLimit);
        textViewSpendLabel = (TextView) rootView.findViewById(R.id.textViewSpendLabel);
        textViewSpendValue = (TextView) rootView.findViewById(R.id.textViewSpendValue);
        textViewCapLabel = (TextView) rootView.findViewById(R.id.textViewCapLabel);
        textViewCapValue = (TextView) rootView.findViewById(R.id.textViewCapValue);
        textViewAmountLabel = (TextView) rootView.findViewById(R.id.textViewAmountLabel);
        textViewAmountValue = (TextView) rootView.findViewById(R.id.textViewAmountValue);

        linearLayoutMessage = (LinearLayout) rootView.findViewById(R.id.linearLayoutMessage);
        textViewViolationMessageCounterApprover = (TextView) rootView.findViewById(R.id.textViewViolationMessageCounterApprover);
        textViewViolationMessageApproverLimit = (TextView) rootView.findViewById(R.id.textViewViolationMessageApproverLimit);
        textViewViolationMessageOverdue = (TextView) rootView.findViewById(R.id.textViewViolationMessageOverdue);
        textViewViolationResubmittedRejected = (TextView) rootView.findViewById(R.id.textViewViolationResubmittedRejected);
        textViewViolationMessageDailyCap = (TextView) rootView.findViewById(R.id.textViewViolationMessageDailyCap);
        textViewViolationMessagePreApproved = (TextView) rootView.findViewById(R.id.textViewViolationMessagePreApproved);
        textViewViolationMessageMaxSpendLimit = (TextView) rootView.findViewById(R.id.textViewViolationMessageMaxSpendLimit);

        linearLayoutReason = (LinearLayout) rootView.findViewById(R.id.linearLayoutReason);
        imageViewPlus = (ImageView) rootView.findViewById(R.id.imageViewPlus);
        linearLayoutExpandable = (LinearLayout) rootView.findViewById(R.id.linearLayoutExpandable);

        imageViewPlus.setOnClickListener(this);
    }

    private void populateView() {
        populateAmount();
        populateDescription();
        // populateReason();
    }

    private void populateAmount() {
        if (policyBreachModel.isDailyCap() || policyBreachModel.isMaxSpendBreach()) {
            relativeLayoutLimit.setVisibility(View.VISIBLE);
            textViewAmountValue.setText(new DecimalFormat("0.00").format(Double.parseDouble(policyBreachModel.getAmountBreach())));

            if (policyBreachModel.isDailyCap()) {
                textViewCapLabel.setVisibility(View.VISIBLE);
                textViewCapValue.setVisibility(View.VISIBLE);
                textViewCapValue.setText(new DecimalFormat("0.00").format(Double.parseDouble(policyBreachModel.getDailCapAmt())));
            } else {
                textViewCapLabel.setVisibility(View.GONE);
                textViewCapValue.setVisibility(View.GONE);
            }

            if (policyBreachModel.isMaxSpendBreach()) {
                textViewSpendLabel.setVisibility(View.VISIBLE);
                textViewSpendValue.setVisibility(View.VISIBLE);
                textViewSpendValue.setText(new DecimalFormat("0.00").format(Double.parseDouble(policyBreachModel.getAmountSetLimit())));
            } else {
                textViewSpendLabel.setVisibility(View.GONE);
                textViewSpendValue.setVisibility(View.GONE);
            }
        } else
            relativeLayoutLimit.setVisibility(View.GONE);
    }

    private void populateDescription() {
        boolean showMessage = false;

        if (policyBreachModel.isDailyCap()) {
            showMessage = true;
            textViewViolationMessageDailyCap.setVisibility(View.VISIBLE);
        } else
            textViewViolationMessageDailyCap.setVisibility(View.GONE);


        if (policyBreachModel.isMaxSpendBreach()) {
            showMessage = true;
            textViewViolationMessageMaxSpendLimit.setVisibility(View.VISIBLE);
        } else
            textViewViolationMessageMaxSpendLimit.setVisibility(View.GONE);

        linearLayoutMessage.setVisibility(showMessage ? View.VISIBLE : View.GONE);
    }

    private void populateReason() {
        ArrayList<com.expenseondemand.eod.model.approval.expense.Notes> notes = policyBreachModel.getNotes();

        if (!AppUtil.isCollectionEmpty(notes)) {
            linearLayoutReason.setVisibility(View.VISIBLE);
            LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            for (com.expenseondemand.eod.model.approval.expense.Notes note : notes) {
                View view = layoutInflater.inflate(R.layout.policy_violation_reason_layout, null);
                TextView textViewKey = (TextView) view.findViewById(R.id.textViewKey);
                TextView textViewValue = (TextView) view.findViewById(R.id.textViewValue);

                String reason = getResources().getString(R.string.approval_expense_detail_policy_violation_notes, note.getReason(), note.getName());
                textViewKey.setText(reason);
                textViewValue.setText(note.getNotes());

                linearLayoutExpandable.addView(view);
            }
        } else
            linearLayoutReason.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageViewPlus: {
                handlePlusMinusClick();
                break;
            }
        }
    }

    private void handlePlusMinusClick() {
        if (isExpandedReason) {
            linearLayoutExpandable.setVisibility(View.GONE);
            imageViewPlus.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.approval_expense_detail_policy_violation_plus_selector));
        } else {
            linearLayoutExpandable.setVisibility(View.VISIBLE);
            imageViewPlus.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.approval_expense_detail_policy_violation_minus_selector));
        }
        isExpandedReason = !isExpandedReason;
    }
}
