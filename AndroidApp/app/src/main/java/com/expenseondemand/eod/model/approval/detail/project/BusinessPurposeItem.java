package com.expenseondemand.eod.model.approval.detail.project;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ITDIVISION\maximess087 on 22/2/17.
 */

public class BusinessPurposeItem implements Parcelable {
    private String LabelName;
    private String Value;

    public BusinessPurposeItem(){}
    public BusinessPurposeItem(Parcel in) {
        LabelName = in.readString();
        Value = in.readString();
    }

    public static final Creator<BusinessPurposeItem> CREATOR = new Creator<BusinessPurposeItem>() {
        @Override
        public BusinessPurposeItem createFromParcel(Parcel in) {
            return new BusinessPurposeItem(in);
        }

        @Override
        public BusinessPurposeItem[] newArray(int size) {
            return new BusinessPurposeItem[size];
        }
    };

    public String getLabelName() {
        return LabelName;
    }

    public void setLabelName(String labelName) {
        LabelName = labelName;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(LabelName);
        parcel.writeString(Value);
    }
}
