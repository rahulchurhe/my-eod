package com.expenseondemand.eod.callbackInterface;

import android.view.View;

/**
 * Created by Nandani Gupta on 2016-09-19.
 */
public interface SharedClickListenerInteface
{

    public void ShareClicked(int position);

}
