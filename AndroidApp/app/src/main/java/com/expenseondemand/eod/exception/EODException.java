package com.expenseondemand.eod.exception;

public class EODException extends Exception
{
	private static final long serialVersionUID = 1L;
	
	private int exceptionCode;
	private String exceptionMessage;

	public EODException() 
	{
	}
	
	//Constructor
	public EODException(int exceptionCode)
	{
		this.exceptionCode = exceptionCode;
		this.exceptionMessage = "";
	}
	
	//Constructor
	public EODException(int exceptionCode, String exceptionMessage)
	{
		this.exceptionCode = exceptionCode;
		this.exceptionMessage = exceptionMessage;
	}
	
	public int getExceptionCode()
	{
		return exceptionCode;
	}

	public String getExceptionMessage()
	{
		return exceptionMessage;
	}
}
