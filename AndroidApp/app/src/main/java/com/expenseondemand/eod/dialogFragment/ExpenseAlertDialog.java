package com.expenseondemand.eod.dialogFragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.expenseondemand.eod.R;

/**
 * Created by Ramit Yadav on 04-10-2016.
 */

public class ExpenseAlertDialog extends DialogFragment implements View.OnClickListener
{
    public static final int TYPE_LOGIN = 0;
    public static final int TYPE_ERROR = -1;



    private TextView textViewOk;
    private TextView textViewCancel;
    private TextView textViewMessage;
    private TextView textViewTitle;
    private String message;
    private String title;
    private String okButtonTitle;
    private String cancelButtonTitle;
    private boolean neutralButton;
    private AlertDialogActionListener alertDialogActionListener;

    public interface AlertDialogActionListener
    {
        void onPositiveButtonClick();
        void onNegativeButtonClick();
    }

    public static ExpenseAlertDialog getInstance(AlertDialogActionListener alertDialogActionListener, boolean neutralButton, String message, String title, String okButtonTitle)
    {
        ExpenseAlertDialog applicationAlertDialog = new ExpenseAlertDialog();

        applicationAlertDialog.alertDialogActionListener = alertDialogActionListener;
        applicationAlertDialog.neutralButton = neutralButton;
        applicationAlertDialog.message = message;
        applicationAlertDialog.title = title;
        applicationAlertDialog.okButtonTitle = okButtonTitle;

        return applicationAlertDialog;
    }
    public static ExpenseAlertDialog getInstance(AlertDialogActionListener alertDialogActionListener, boolean neutralButton, String message, String title, String okButtonTitle, String cancelButtonTitle, int type)
    {

        ExpenseAlertDialog applicationAlertDialog = ExpenseAlertDialog.getInstance(alertDialogActionListener,neutralButton,message,title,okButtonTitle);

        applicationAlertDialog.cancelButtonTitle = cancelButtonTitle;
        return applicationAlertDialog;
    }
    @Override
    public void onResume()
    {
        super.onResume();

        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_BACK) && (event.getAction() == KeyEvent.ACTION_UP))
                    handleCancel();
                return false;
            }
        });
    }

    @Override
    public void onStart()
    {
        super.onStart();
        getDialog().getWindow().setLayout( ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);

    }

    @Override
    public void onPause()
    {
        super.onPause();
     //   handleCancel();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setCancelable(false);
        View view = inflater.inflate(R.layout.app_upgrade_dialog_fragment, container, false);

        textViewOk = (TextView) view.findViewById(R.id.textViewOk);
        textViewCancel = (TextView) view.findViewById(R.id.textViewCancel);
        textViewMessage = (TextView) view.findViewById(R.id.textViewMessage);
        textViewTitle = (TextView) view.findViewById(R.id.textViewTitle);

        if(title != null)
        textViewTitle.setText(title);
        else
        {
        textViewTitle.setVisibility(View.GONE);
        }
        textViewMessage.setText(message);

        if(okButtonTitle != null)
        textViewOk.setText(okButtonTitle);

        if(cancelButtonTitle != null)
        textViewCancel.setText(cancelButtonTitle);

        textViewOk.setOnClickListener(this);
        textViewCancel.setOnClickListener(this);


       if(neutralButton)
        textViewCancel.setVisibility(View.GONE);



        return view;
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.textViewOk:

                handleOk();
                break;


            case R.id.textViewCancel:

                handleCancel();
                break;

        }
    }

    private void handleOk()
    {
        dismiss();
        alertDialogActionListener.onPositiveButtonClick();
    }

    private void handleCancel()
    {
        dismiss();
        alertDialogActionListener.onNegativeButtonClick();
    }
}
