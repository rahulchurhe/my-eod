package com.expenseondemand.eod.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;

public class PersistentManager {

    /**
     * Identify if the User has marked to be Remembered at the time of Login
     */


    //Persist Last LoggedIn User's UserId
    public static void persistSyncExpenseCategory(boolean isSyncRunning) {
        //Store isUserRemembered in SharedPref
        writeContentToSharedPreferences(AppConstant.KEY_SYNC_CATEGORY, isSyncRunning);
    }

    //Persist Last LoggedIn User's UserId
    public static void persistIsUserRemembered(boolean isUserRemembered) {
        //Store isUserRemembered in SharedPref
        writeContentToSharedPreferences(AppConstant.KEY_PREF_IS_USER_REMEMBERED, isUserRemembered);
    }

    //Get isUser Remembered
    public static boolean isUserRemembered() {
        //Get isUserRemembered from SharedPref
        boolean isUserRemembered = (Boolean) getContentFromSharedPreferences(AppConstant.KEY_PREF_IS_USER_REMEMBERED, Boolean.class);

        return isUserRemembered;
    }

    //Get Category Sync running status
    public static boolean isCategorySyncRunning() {
        //Get isSyncRunning from SharedPref
        boolean isSyncRunning = (Boolean) getContentFromSharedPreferences(AppConstant.KEY_SYNC_CATEGORY, Boolean.class);

        return isSyncRunning;
    }


    //Persist UserId
    public static void persistUserId(String userId) {
        //Save UserId in SharedPref
        writeContentToSharedPreferences(AppConstant.KEY_PREF_LAST_STORED_USER_ID, userId);
    }

    //Get Last Stored UserId
    public static String getLastStoredUserId() {
        String lastStoredUserId = (String) getContentFromSharedPreferences(AppConstant.KEY_PREF_LAST_STORED_USER_ID, String.class);

        return lastStoredUserId;
    }


    /**
     * Authorization Token
     */

    //Persist Authorization Token
    public static void persistAuthorizationToken(String token) {
        //Save Token in SharedPref
        writeContentToSharedPreferences(AppConstant.KEY_PREF_AUTHORIZATION_TOKEN, token);
    }

    //Get Authorization Token
    public static String getAuthorizationToken() {
        String token = (String) getContentFromSharedPreferences(AppConstant.KEY_PREF_AUTHORIZATION_TOKEN, String.class);

        return token;
    }

    /**
     * Persist Silent Login Status
     *
     * @param status
     */
    public static void persistSilentLoginStatus(int status) {
        writeContentToSharedPreferences(AppConstant.PREF_KEY_SILENT_LOGIN_STATUS, status);
    }

    /**
     * Get Silent Login Status
     *
     * @return int- status
     */
    public static int getSilentLoginStatus() {
        int status = (int) getContentFromSharedPreferences(AppConstant.PREF_KEY_SILENT_LOGIN_STATUS, Integer.class);
        return status;
    }

    //Is Categories Sync Required
    public static boolean isCategoriesSyncRequired() {
        boolean isSyncRequired = false;

        long lastCategoriesSyncDate = getLastCategoriesSyncDate();

        if (lastCategoriesSyncDate > 0) {
            long currentDate = System.currentTimeMillis();
            long noOfDays = AppUtil.getDateDiffences(currentDate, lastCategoriesSyncDate);

            if (noOfDays >= AppConstant.REFRESH_CATEGORIES_NO_OF_DAYS) {
                isSyncRequired = true;
            }
        } else {
            isSyncRequired = true;
        }

        return isSyncRequired;
    }

    //Get Last Categories Sync date
    public static Long getLastCategoriesSyncDate() {
        String lastCategoriesSyncDateKey = null;
        long lastCategoriesSyncDate = 0L;

        //Get User Specific Key
        lastCategoriesSyncDateKey = getUserSpecificPrefrenceKey();

        //Get Date
        lastCategoriesSyncDate = (Long) getContentFromSharedPreferences(lastCategoriesSyncDateKey, Long.class);

        return lastCategoriesSyncDate;
    }

    //Set Categories SyncDate
    public static void persistLastCategoriesSyncDate() {
        long currentDate = System.currentTimeMillis();

        //Get User Specific Key
        String lastCategoriesSyncDateKey = getUserSpecificPrefrenceKey();

        //Save Current Date/Time in SharedPref
        writeContentToSharedPreferences(lastCategoriesSyncDateKey, currentDate);
    }

    private static String getUserSpecificPrefrenceKey() {
        String firstTimeLoginDateKey = null;

        //Get UserInfo from Cache
        UserInfo userInfo = CachingManager.getUserInfo();
        try {
            String userId = userInfo.getUserId();
            firstTimeLoginDateKey = AppConstant.KEY_PREF_LAST_CATEGORIES_SYNC_DATE + userId;
        } catch (Exception e) {
            e.printStackTrace();
        }


        return firstTimeLoginDateKey;
    }

    /**
     * Common Functions
     */

    //Write Content to File
    private static void writeContentToSharedPreferences(String key, Object value) {
        //Get App Context
        Context context = CachingManager.getAppContext();

        //Get SharedPreferences
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        //Get SharedPreferences Editor
        SharedPreferences.Editor editor = sharedPreferences.edit();

        //Write content to File
        if (value instanceof String) //String value
        {
            editor.putString(key, ((String) value).trim());
        } else if (value instanceof Boolean) //Boolean value
        {
            editor.putBoolean(key, ((Boolean) value));
        } else if (value instanceof Float) //Float value
        {
            editor.putFloat(key, ((Float) value));
        } else if (value instanceof Integer) //Integer value
        {
            editor.putInt(key, ((Integer) value));
        } else if (value instanceof Long) //Long value
        {
            editor.putLong(key, ((Long) value));
        }

        //Commit Preferences Changes
        editor.commit();
    }

    //Get Content from File
    private static Object getContentFromSharedPreferences(String key, Class<? extends Object> classType) {
        Object object;

        //Get App Context
        Context context = CachingManager.getAppContext();

        //Get SharedPreferences
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        //Get Content from File
        if (classType.equals(String.class)) //String value
        {
            object = sharedPreferences.getString(key, "");
        } else if (classType.equals(Boolean.class)) //Boolean value
        {
            object = sharedPreferences.getBoolean(key, false);
        } else if (classType.equals(Integer.class)) //Integer value
        {
            object = sharedPreferences.getInt(key, 0);
        } else if (classType.equals(Float.class)) //Float value
        {
            object = sharedPreferences.getFloat(key, 0.00f);
        } else if (classType.equals(Long.class)) //Long value
        {
            object = sharedPreferences.getLong(key, 0L);
        } else {
            object = null;
        }

        return object;
    }
}
