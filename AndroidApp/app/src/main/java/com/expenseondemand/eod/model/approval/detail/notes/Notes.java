package com.expenseondemand.eod.model.approval.detail.notes;

import com.expenseondemand.eod.model.approval.detail.project.PreApprovedDetailsItem;
import com.expenseondemand.eod.model.approval.detail.project.ProjectDetailsItem;
import com.expenseondemand.eod.webservice.model.approval.detail.PreApprovedDetails;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 30-09-2016.
 */

public class Notes
{
    private String EnteredNotes;
    private String CcNotes;
    private String MobileNotes;

    private ArrayList<PreApprovedDetailsItem> PreApprovedDetails;

    public String getEnteredNotes() {
        return EnteredNotes;
    }

    public void setEnteredNotes(String enteredNotes) {
        EnteredNotes = enteredNotes;
    }

    public String getCcNotes() {
        return CcNotes;
    }

    public void setCcNotes(String ccNotes) {
        CcNotes = ccNotes;
    }

    public String getMobileNotes() {
        return MobileNotes;
    }

    public void setMobileNotes(String mobileNotes) {
        MobileNotes = mobileNotes;
    }

    public ArrayList<PreApprovedDetailsItem> getPreApprovedDetails() {
        return PreApprovedDetails;
    }

    public void setPreApprovedDetails(ArrayList<PreApprovedDetailsItem> preApprovedDetails) {
        PreApprovedDetails = preApprovedDetails;
    }

    /* public String getEnteredNotes()
    {
        return enteredNotes;
    }

    public void setEnteredNotes(String enteredNotes)
    {
        this.enteredNotes = enteredNotes;
    }

    public String getMobileNotes()
    {
        return mobileNotes;
    }

    public void setMobileNotes(String mobileNotes)
    {
        this.mobileNotes = mobileNotes;
    }

    public String getCcNotes()
    {
        return ccNotes;
    }

    public void setCcNotes(String ccNotes)
    {
        this.ccNotes = ccNotes;
    }*/
}
