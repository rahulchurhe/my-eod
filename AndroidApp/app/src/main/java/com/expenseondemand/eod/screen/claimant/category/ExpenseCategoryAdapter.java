package com.expenseondemand.eod.screen.claimant.category;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.expenseondemand.eod.R;
import com.expenseondemand.eod.callbackInterface.SharedClickListenerInteface;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.model.ExpenseCategory;
import com.expenseondemand.eod.util.AppUtil;

import java.util.ArrayList;

/**
 * Created by Nandani Gupta on 2016-09-14.
 */
public class ExpenseCategoryAdapter  extends RecyclerView.Adapter<ExpenseCategoryAdapter.MyViewHolder> {

    Context context;
    ArrayList<ExpenseCategory> expenseCategoryNameList;
    SharedClickListenerInteface sharedClickListenerInteface;
    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        RelativeLayout relativeExpenseCategoryItem;
        TextView  textViewExpenseCategory;



        public MyViewHolder(View view)
        {
             super(view);
             textViewExpenseCategory = (TextView) view.findViewById(R.id.textViewExpenseCategory);
             relativeExpenseCategoryItem = (RelativeLayout) view.findViewById(R.id.relativeExpenseCategoryItem);
        }
    }


    public ExpenseCategoryAdapter(Context context,ArrayList<ExpenseCategory> expenseCategoryNameList)
    { 
        this.context = context;
        this.expenseCategoryNameList = expenseCategoryNameList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.expense_category_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder viewHolder, int position)
    {
        ExpenseCategory expenseCategory = expenseCategoryNameList.get(position);
        int resourceId = CachingManager.getCategoryResourceId(expenseCategory.getBaseCategoryId());

          System.out.println("Category id---"+expenseCategory.getBaseCategoryId());
          viewHolder.textViewExpenseCategory.setText(expenseCategory.getCategoryName());
          viewHolder.textViewExpenseCategory.setCompoundDrawablesWithIntrinsicBounds(resourceId, 0, 0, 0);

          viewHolder.relativeExpenseCategoryItem.setTag(position);
          viewHolder.relativeExpenseCategoryItem.setOnClickListener(new ExpenseCategoryOnClickListener());
    }

    @Override
    public int getItemCount()
    {
        return ( (AppUtil.isCollectionEmpty(expenseCategoryNameList)) ? 0 : expenseCategoryNameList.size() );

    }


    private class ExpenseCategoryOnClickListener implements View.OnClickListener
    {

        @Override
        public void onClick(View view) {
            int position = (int)view.getTag();
            //Navigate to Category Activity
            sharedClickListenerInteface.ShareClicked(position);

        }
    }

    //set  clickListener callback instance
    public void setOnShareClickedListener( SharedClickListenerInteface mCallback) {
        sharedClickListenerInteface = mCallback;
    }



}