package com.expenseondemand.eod.webservice.model.approval.claim;

import com.expenseondemand.eod.webservice.model.approval.detail.DynamicDetail;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 19-10-2016.
 */

public class ClaimData
{
    private String recordCount;
    private String employeeId;
    private String resourceName;
    private String expenseHeaderId;
    private String claimDate;
    private String rowId;
    private String resourceId;
    private String title;
    private String amount;
    private String pendingSince;
    private String noOfItems;
    private String rejected;
    private String resubmitted;
    private String breachedItem;
    private String status;
    private String breachedClaim;
    private String dailyLimitBreached;
    private String preApprovalBreach;
    private String preApprovalAmount;
    private String approverNote;
    private String dailyCapAmount;
    private String isMaxSpendBreach;
    private String maxSpendAmount;
    private String approverId;
    private ArrayList<PreApprovedDetails> PreApprovedDetails;

    public ArrayList<com.expenseondemand.eod.webservice.model.approval.claim.PreApprovedDetails> getPreApprovedDetails() {
        return PreApprovedDetails;
    }

    public void setPreApprovedDetails(ArrayList<com.expenseondemand.eod.webservice.model.approval.claim.PreApprovedDetails> preApprovedDetails) {
        PreApprovedDetails = preApprovedDetails;
    }

    public String getRecordCount()
    {
        return recordCount;
    }

    public void setRecordCount(String recordCount)
    {
        this.recordCount = recordCount;
    }

    public String getEmployeeId()
    {
        return employeeId;
    }

    public void setEmployeeId(String employeeId)
    {
        this.employeeId = employeeId;
    }

    public String getResourceName()
    {
        return resourceName;
    }

    public void setResourceName(String resourceName)
    {
        this.resourceName = resourceName;
    }

    public String getExpenseHeaderId()
    {
        return expenseHeaderId;
    }

    public void setExpenseHeaderId(String expenseHeaderId)
    {
        this.expenseHeaderId = expenseHeaderId;
    }

    public String getClaimDate()
    {
        return claimDate;
    }

    public void setClaimDate(String claimDate)
    {
        this.claimDate = claimDate;
    }

    public String getRowId()
    {
        return rowId;
    }

    public void setRowId(String rowId)
    {
        this.rowId = rowId;
    }

    public String getResourceId()
    {
        return resourceId;
    }

    public void setResourceId(String resourceId)
    {
        this.resourceId = resourceId;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getAmount()
    {
        return amount;
    }

    public void setAmount(String amount)
    {
        this.amount = amount;
    }

    public String getPendingSince()
    {
        return pendingSince;
    }

    public void setPendingSince(String pendingSince)
    {
        this.pendingSince = pendingSince;
    }

    public String getNoOfItems()
    {
        return noOfItems;
    }

    public void setNoOfItems(String noOfItems)
    {
        this.noOfItems = noOfItems;
    }

    public String getRejected()
    {
        return rejected;
    }

    public void setRejected(String rejected)
    {
        this.rejected = rejected;
    }

    public String getResubmitted()
    {
        return resubmitted;
    }

    public void setResubmitted(String resubmitted)
    {
        this.resubmitted = resubmitted;
    }

    public String getBreachedItem()
    {
        return breachedItem;
    }

    public void setBreachedItem(String breachedItem)
    {
        this.breachedItem = breachedItem;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getBreachedClaim()
    {
        return breachedClaim;
    }

    public void setBreachedClaim(String breachedClaim)
    {
        this.breachedClaim = breachedClaim;
    }

    public String getDailyLimitBreached()
    {
        return dailyLimitBreached;
    }

    public void setDailyLimitBreached(String dailyLimitBreached)
    {
        this.dailyLimitBreached = dailyLimitBreached;
    }

    public String getPreApprovalBreach()
    {
        return preApprovalBreach;
    }

    public void setPreApprovalBreach(String preApprovalBreach)
    {
        this.preApprovalBreach = preApprovalBreach;
    }

    public String getPreApprovalAmount()
    {
        return preApprovalAmount;
    }

    public void setPreApprovalAmount(String preApprovalAmount)
    {
        this.preApprovalAmount = preApprovalAmount;
    }

    public String getApproverNote()
    {
        return approverNote;
    }

    public void setApproverNote(String approverNote)
    {
        this.approverNote = approverNote;
    }

    public String getDailyCapAmount()
    {
        return dailyCapAmount;
    }

    public void setDailyCapAmount(String dailyCapAmount)
    {
        this.dailyCapAmount = dailyCapAmount;
    }

    public String getIsMaxSpendBreach()
    {
        return isMaxSpendBreach;
    }

    public void setIsMaxSpendBreach(String isMaxSpendBreach)
    {
        this.isMaxSpendBreach = isMaxSpendBreach;
    }

    public String getMaxSpendAmount()
    {
        return maxSpendAmount;
    }

    public void setMaxSpendAmount(String maxSpendAmount)
    {
        this.maxSpendAmount = maxSpendAmount;
    }

    public String getApproverId()
    {
        return approverId;
    }

    public void setApproverId(String approverId)
    {
        this.approverId = approverId;
    }
}
