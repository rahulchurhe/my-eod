package com.expenseondemand.eod.exception;

public class EODBusinessException extends EODException
{
	private static final long serialVersionUID = 1L;

	public EODBusinessException() 
	{
	}
	
	//Constructor
	public EODBusinessException(int exceptionCode)
	{
		super(exceptionCode, "");
	}
	
	//Constructor
	public EODBusinessException(int exceptionCode, String exceptionMessage)
	{
		super(exceptionCode, exceptionMessage);
	}
}
