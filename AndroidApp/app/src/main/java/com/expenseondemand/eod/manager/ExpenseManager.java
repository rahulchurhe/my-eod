package com.expenseondemand.eod.manager;

import android.util.Log;

import java.util.ArrayList;

import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.model.Expense;
import com.expenseondemand.eod.model.ReceiptImage;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.webservice.model.claimant.ExpenseItemRequest;
import com.expenseondemand.eod.webservice.model.claimant.ExpenseItemResponse;
import com.expenseondemand.eod.webservice.model.claimant.Receipt;
import com.expenseondemand.eod.webservice.model.http.Body;
import com.expenseondemand.eod.webservice.model.http.HTTPData;

public class ExpenseManager {
    //Save Expense In Db
    public static boolean saveExpense(Expense expense, boolean isEditExpense) throws EODException {
        //Save Expense in Db
        boolean isExpenseSavedSuccessfully = DbManager.saveExpense(expense, isEditExpense);

        return isExpenseSavedSuccessfully;
    }

    //Get Expense List
    public static ArrayList<Expense> getExpenseList(String userId) throws EODException {
        //Get Expense List form Db
        ArrayList<Expense> expenseList = DbManager.getExpenseList(userId);
        return expenseList;
    }

    //Get Expense List count
    public static int getExpenseListCount(String userId) throws EODException {
        int count = 0;
        //Get Expense List form Db
        count = DbManager.getExpenseListCount(userId);

        return count;
    }

    //Get Distinct Expense Date List
    public static ArrayList<Long> getDistinctExpenseDateList(String userId) throws EODException {
        //Get Distinct Expense Date List
        ArrayList<Long> distinctExpenseDateList = DbManager.getDistinctExpenseDateList(userId);

        return distinctExpenseDateList;
    }

    //Delete Expenses
    public static long deleteExpenses(ArrayList<Integer> expenseIdList) throws EODException {
        long noOfExpensesDeleted = DbManager.deleteExpense(expenseIdList);

        return noOfExpensesDeleted;
    }

    //Get Expense Receipt Image
    public static String getReceiptImage(int expenseId) throws EODException {
        String receiptImageData = DbManager.getReceiptImage(expenseId);

        return receiptImageData;
    }

    //Upload Expense
    public static void uploadExpense(Expense expense) throws EODException {
        //Prepare Upload Expense Item Request
        ExpenseItemRequest request = prepareExpenseItemRequest(expense);

        //Prepare JSON Request String
        String jsonRequestString = ParseManager.prepareJsonRequest(request);

        ArrayList<String> multipartFileName = prepareExpenseItemImages(expense);

        //Prepare http request
        HTTPData httpData = new HTTPData();
        httpData.setType(AppConstant.HTTP_METHOD_POST);
        Body body = new Body();
        body.setJsonRequestString(jsonRequestString);
        body.setMultipartFileName(multipartFileName);
        httpData.setBody(body);

        //Call to Web Service
        String jsonResponseString = (String) WebServiceManager.callWebService(AppConstant.BASE_URL, AppConstant.UPLOAD_EXPENSE_URL, httpData);

        //Prepare Response Object
        ExpenseItemResponse response = (ExpenseItemResponse) ParseManager.prepareWebServiceResponseObject(ExpenseItemResponse.class, jsonResponseString);

        //Process Response Status
        WebServiceManager.processResponseStatus(response);

        //Delete Expense Item from DB
        deleteExpenseItemFromDb(expense);
    }

    //Delete Expense Item from DB
    private static void deleteExpenseItemFromDb(Expense expense) throws EODException {
        //Create ArrayList of Expense Id
        ArrayList<Integer> expenseIdList = new ArrayList<Integer>();

        //Only one
        expenseIdList.add(expense.getExpenseId());

        //Delete From Db
        DbManager.deleteExpense(expenseIdList);
    }

    //Prepare Upload Expense Item Request
    private static ExpenseItemRequest prepareExpenseItemRequest(Expense expense) {
        ExpenseItemRequest request = new ExpenseItemRequest();

        if (expense != null) {
            String currentDateTime = String.valueOf(System.currentTimeMillis());
            String expenseDateString = AppUtil.getFormattedDateString(AppConstant.FORMAT_DATE_WEB_SERVICE, expense.getExpenseDate());
            boolean hasReceipt = expense.isHasReceipt();

            //Get Receipt Image
            ReceiptImage receiptImage = expense.getReceiptImage();

            request.setAmount(Double.parseDouble(expense.getExpenseAmount())); //Amount
            request.setClientRefId(currentDateTime); //Set current date/time as ClientRefId (Unique everytime)
            request.setCurrencyCode(expense.getCurrencyCode()); //Currency Code
            request.setExpenseDate(expenseDateString);
            request.setExpenseCategoryId(expense.getExpenseCategoryId()); //Category Id
            request.setNotes(expense.getExpenseNotes()); //Expense Notes

            if (hasReceipt) {
                request.setHasReceipt(1); //has Receipt
                if (receiptImage != null) {
//					//Create Receipt
//					Receipt receipt = new Receipt();
//					receipt.setFileName(AppConstant.RECEIPT_IMAGE_FILE_NAME_WEBSERVICE);
//					receipt.setMimeType(AppConstant.RECEIPT_IMAGE_MIME_TYPE_WEBSERVICE);
//					receipt.setReceiptData(receiptImage.getReceiptImageFileName());
//
//					request.setReceipt(receipt); //Receipt Image Data
                }
            } else {
                request.setHasReceipt(0); //has Receipt
            }
        }

        return request;
    }

    //Prepare Upload Expense image part Request
    private static ArrayList<String> prepareExpenseItemImages(Expense expense) {
        ArrayList<String> multipartImages = null;
        if (expense != null) {
            boolean hasReceipt = expense.isHasReceipt();

            //Get Receipt Image
            ReceiptImage receiptImage = expense.getReceiptImage();

            if (hasReceipt && receiptImage != null) {
                multipartImages = new ArrayList<>();
                multipartImages.add(receiptImage.getReceiptImageFileName());
            }
        }

        return multipartImages;
    }
}
