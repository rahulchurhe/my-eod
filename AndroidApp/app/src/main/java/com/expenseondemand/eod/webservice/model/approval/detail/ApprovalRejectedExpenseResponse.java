package com.expenseondemand.eod.webservice.model.approval.detail;


import com.expenseondemand.eod.webservice.model.MasterResponse;

/**
 * Created by ITDIVISION\maximess087 on 4/1/17.
 */

public class ApprovalRejectedExpenseResponse extends MasterResponse {
    private ApproveRejectData data;

    public ApproveRejectData getData() {
        return data;
    }

    public void setData(ApproveRejectData data) {
        this.data = data;
    }
}
