package com.expenseondemand.eod.screen.claimant.category;

import java.util.ArrayList;
import java.util.HashMap;

import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.DbManager;
import com.expenseondemand.eod.manager.ExceptionManager;
import com.expenseondemand.eod.manager.ParseManager;
import com.expenseondemand.eod.manager.PersistentManager;
import com.expenseondemand.eod.manager.WebServiceManager;
import com.expenseondemand.eod.model.ExpenseCategory;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.webservice.model.expense_category.CategoriesResponse;
import com.expenseondemand.eod.webservice.model.expense_category.CategoryInfo;
import com.expenseondemand.eod.webservice.model.http.HTTPData;

public class GlobalDataManager {
    //Get Expense Categories
    public static ArrayList<ExpenseCategory> getExpenseCategoryList() throws EODException {
        CategoriesResponse response = null;

        //Prepare http request
        HTTPData httpData = new HTTPData();
        httpData.setType(AppConstant.HTTP_METHOD_GET);

        //Call to Web Service
        String jsonResponseString = (String) WebServiceManager.callWebService(AppConstant.BASE_URL, AppConstant.EXPENSE_CATEGORIES_URL, httpData);

        System.out.println("json Response--" + jsonResponseString);

        //Prepare Response Object
        response = (CategoriesResponse) ParseManager.prepareWebServiceResponseObject(CategoriesResponse.class, jsonResponseString);

        //Process Response Status
        WebServiceManager.processResponseStatus(response);

        //Prepare Categories List
        ArrayList<ExpenseCategory> expenseCategoryList = prepareCategoryList(response.getData().getCategoryList());

        return expenseCategoryList;
    }

    //Prepare Categories List
    private static ArrayList<ExpenseCategory> prepareCategoryList(ArrayList<CategoryInfo> categoryInfoList) {
        ArrayList<ExpenseCategory> expenseCategoryList = null;

        if (!AppUtil.isCollectionEmpty(categoryInfoList)) {
            //New List
            expenseCategoryList = new ArrayList<ExpenseCategory>();

            for (CategoryInfo categoryInfo : categoryInfoList) {
                if (categoryInfo != null) {
                    //New Expense Category
                    ExpenseCategory expenseCategory = new ExpenseCategory();

                    expenseCategory.setCategoryId(categoryInfo.getExpenseCategoryId()); //Category Id
                    expenseCategory.setBaseCategoryId(categoryInfo.getBaseExpenseCategoryId()); //Base Category Id
                    expenseCategory.setCategoryName(categoryInfo.getDisplayName()); //Category Name

                    //Add to List
                    expenseCategoryList.add(expenseCategory);
                }
            }
        }

        return expenseCategoryList;
    }

    //Sync Expense Categories List
    public static ArrayList<ExpenseCategory> syncExpenseCategory() throws EODException {
        ArrayList<ExpenseCategory> alreadyExistExpenseCategoryList = new ArrayList<ExpenseCategory>();
        ArrayList<ExpenseCategory> newExpenseCategoryList = new ArrayList<ExpenseCategory>();
        //Get Expense Categories List from Server
        ArrayList<ExpenseCategory> expenseCategoryServerList = getExpenseCategoryList();

        try {
            //Get Hash Map for Existing Categories
            HashMap<Integer, Integer> expenseCategoryCodeHashMap = DbManager.getExpenseCategoryCodeHashMap();


            if (expenseCategoryServerList.size() < expenseCategoryCodeHashMap.size()) {
                //delete list and insert new
                DbManager.deleteExpenseCategory();

                for (ExpenseCategory serverExpenseCategory : expenseCategoryServerList) {
                    if (expenseCategoryCodeHashMap.containsKey(serverExpenseCategory.getCategoryId())) {
                        alreadyExistExpenseCategoryList.add(serverExpenseCategory);
                    } else {
                        newExpenseCategoryList.add(serverExpenseCategory);
                    }
                }

                //Referesh Categories
                DbManager.refreshExpenseCategoryList(alreadyExistExpenseCategoryList, newExpenseCategoryList);

                //Persist Categories SyncDate
                PersistentManager.persistLastCategoriesSyncDate();
            } else {
                for (ExpenseCategory serverExpenseCategory : expenseCategoryServerList) {
                    if (expenseCategoryCodeHashMap.containsKey(serverExpenseCategory.getCategoryId())) {
                        alreadyExistExpenseCategoryList.add(serverExpenseCategory);
                    } else {
                        newExpenseCategoryList.add(serverExpenseCategory);
                    }
                }

                //Referesh Categories
                DbManager.refreshExpenseCategoryList(alreadyExistExpenseCategoryList, newExpenseCategoryList);

                //Persist Categories SyncDate
                PersistentManager.persistLastCategoriesSyncDate();
            }

        } catch (Exception exception) {
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1021, exception.getMessage(), exception);
        }


        return alreadyExistExpenseCategoryList;
    }

    //Get Sorted Categories List from DB
    public static ArrayList<ExpenseCategory> getSortedExpenseCategoryList() throws EODException {
        //Get Categories List
        ArrayList<ExpenseCategory> expenseCategoriesList = null;

        //Get userInfo from Cache
        UserInfo userInfo = CachingManager.getUserInfo();

        if (userInfo != null) {
            String userId = userInfo.getUserId();

            //Get Categories List
            expenseCategoriesList = DbManager.getExpenseCategoryList(userId);
        }

        return expenseCategoriesList;
    }
}
