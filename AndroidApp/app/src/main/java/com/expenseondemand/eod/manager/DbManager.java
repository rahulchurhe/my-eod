package com.expenseondemand.eod.manager;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.expenseondemand.eod.database.EodDatabase;
import com.expenseondemand.eod.exception.EODAppException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.model.Expense;
import com.expenseondemand.eod.model.ExpenseCategory;
import com.expenseondemand.eod.model.ReceiptImage;
import com.expenseondemand.eod.model.UserCredential;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;

import java.util.ArrayList;
import java.util.HashMap;

public class DbManager {
    //Get User Credentials from UserId
    public static UserCredential getLastStoredUserCredentials(String userId) throws EODException {
        EodDatabase eodDatabase = null;
        SQLiteDatabase sqliteDatabase = null;
        Cursor cursor = null;
        UserCredential userCredential = null;

        try {
            //Get Readable Database
            eodDatabase = EodDatabase.getInstance();
            sqliteDatabase = eodDatabase.openDatabase();

            //Columns to retrive
            String queryColumns[] = {EodDatabase.COLUMN_NAME_LOGIN_ID, EodDatabase.COLUMN_NAME_PASSWORD};

            //Where Clause
            String whereClause = EodDatabase.COLUMN_NAME_USER_ID + " = ?";
            String[] whereClauseArgs = {userId};

            //Execute Query
            cursor = sqliteDatabase.query(EodDatabase.TABLE_NAME_EOD_USERINFO_TABLE, queryColumns, whereClause, whereClauseArgs, null, null, null);

            if (cursor != null && cursor.moveToFirst()) {
                int columnnIndexLoginId = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_LOGIN_ID);
                int columnnIndexPassword = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_PASSWORD);

                //Prepare User Credentials
                userCredential = new UserCredential();
                userCredential.setLoginId(cursor.getString(columnnIndexLoginId));
                userCredential.setPassword(cursor.getString(columnnIndexPassword));
            }
        } catch (Exception exception) {
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1009, exception.getMessage(), exception);
        } finally {
            AppUtil.closeCursor(cursor);
            eodDatabase.closeDatabase();
        }

        return userCredential;
    }

    //Save User Information
    public static long saveUserInformation(UserInfo user) throws EODException {
        SQLiteDatabase sqliteDatabase = null;
        EodDatabase eodDatabase = null;
        long result = 0;

        try {
            //Get Readable Database
            eodDatabase = EodDatabase.getInstance();
            sqliteDatabase = eodDatabase.openDatabase();

            //Begin Transaction
            beginTransaction(sqliteDatabase);

            if (user != null) {
                ContentValues userInfoContentValues = new ContentValues();

                //UserId
                String userId = user.getUserId();

                userInfoContentValues.put(EodDatabase.COLUMN_NAME_USER_ID, userId); //UserId
                userInfoContentValues.put(EodDatabase.COLUMN_NAME_DEFAULT_CATEGORY_ID, user.getDefaultCategoryId()); //DefaultCategoryId
                userInfoContentValues.put(EodDatabase.COLUMN_NAME_DEFAULT_CURRENCY_CODE, AppUtil.setStringNotNull(user.getDefaultCurrencyCode())); //DefaultCurrencyCode
                userInfoContentValues.put(EodDatabase.COLUMN_NAME_RESOURCE_ID, AppUtil.setStringNotNull(user.getResourceId()));
                userInfoContentValues.put(EodDatabase.COLUMN_NAME_COST_CENTER_ID, AppUtil.setStringNotNull(user.getCostCenterId()));
                userInfoContentValues.put(EodDatabase.COLUMN_NAME_COMPANY_LOGO, AppUtil.setStringNotNull(user.getCompanyLogo()));
                userInfoContentValues.put(EodDatabase.COLUMN_NAME_BRANCH_ID, AppUtil.setStringNotNull(user.getBranchId()));
                userInfoContentValues.put(EodDatabase.COLUMN_NAME_COMPANY_ID, AppUtil.setStringNotNull(user.getCompanyId()));
                userInfoContentValues.put(EodDatabase.COLUMN_NAME_DEPARTMENT_ID, AppUtil.setStringNotNull(user.getDepartmentId()));
                userInfoContentValues.put(EodDatabase.COLUMN_NAME_COST_CENTER_NAME, AppUtil.setStringNotNull(user.getCostCenterId()));
                userInfoContentValues.put(EodDatabase.COLUMN_NAME_COMPANY_NAME, AppUtil.setStringNotNull(user.getCompanyName()));
                userInfoContentValues.put(EodDatabase.COLUMN_NAME_DISPLAY_NAME, AppUtil.setStringNotNull(user.getDisplayName()));
                userInfoContentValues.put(EodDatabase.COLUMN_NAME_IS_APPROVER, user.isApprover() ? 1 : 0);
                userInfoContentValues.put(EodDatabase.COLUMN_NAME_IS_FINANCE, user.isFinance() ? 1 : 0);
                userInfoContentValues.put(EodDatabase.COLUMN_NAME_IS_BOSS, user.isBoss() ? 1 : 0);
                userInfoContentValues.put(EodDatabase.COLUMN_NAME_IS_DELEGATE, user.isDelegate() ? 1 : 0);
                userInfoContentValues.put(EodDatabase.COLUMN_NAME_GRADE_ID, AppUtil.setStringNotNull(user.getGradeId()));
                userInfoContentValues.put(EodDatabase.COLUMN_NAME_IS_DEPUTY_ASSIGNED, user.isDeputyAssigned() ? 1 : 0);
                //User Credentials
                UserCredential userCredential = user.getUserCredential();

                if (userCredential != null) {
                    userInfoContentValues.put(EodDatabase.COLUMN_NAME_LOGIN_ID, userCredential.getLoginId()); //LoginId
                    userInfoContentValues.put(EodDatabase.COLUMN_NAME_PASSWORD, userCredential.getPassword()); //Password
                }

                if (doesUserExist(userId, sqliteDatabase, EodDatabase.TABLE_NAME_EOD_USERINFO_TABLE)) {
                    //If this UserId exist, update user information
                    String whereClause = EodDatabase.COLUMN_NAME_USER_ID + " = ?";
                    String[] whereClauseArgs = {userId};

                    result = sqliteDatabase.update(EodDatabase.TABLE_NAME_EOD_USERINFO_TABLE, userInfoContentValues, whereClause, whereClauseArgs);
                } else {
                    result = sqliteDatabase.insert(EodDatabase.TABLE_NAME_EOD_USERINFO_TABLE, null, userInfoContentValues);
                }
            }

            //Set Transaction Successfull
            setTransactionSuccessful(sqliteDatabase);
        } catch (Exception exception) {
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1010, exception.getMessage(), exception);
        } finally {
            //End Transaction
            endTransaction(sqliteDatabase);
            eodDatabase.closeDatabase();
        }

        return result;
    }

    /**
     * Checks if User exists in Database for this User Id
     *
     * @param userId
     * @param sqliteDatabase
     * @return
     * @throws EODException
     */
    public static boolean doesUserExist(String userId, SQLiteDatabase sqliteDatabase, String tableName) throws EODException {
        boolean isUserExist = false;
        int count = 0;
        Cursor cursor = null;

        try {
            cursor = sqliteDatabase.query(tableName, new String[]{EodDatabase.COLUMN_NAME_USER_ID}, EodDatabase.COLUMN_NAME_USER_ID + " = ?", new String[]{userId}, null, null, null);

            if (cursor != null) {
                count = cursor.getCount();

                if (count > 0) {
                    isUserExist = true;
                }
            }
        } catch (Exception exception) {
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1011, exception.getMessage(), exception);
        } finally {
            AppUtil.closeCursor(cursor);
        }

        return isUserExist;
    }

    //Authenticate User Locally
    public static UserInfo getUserInfo(UserCredential userCredential) throws EODException {
        EodDatabase eodDatabase = null;
        SQLiteDatabase sqliteDatabase = null;
        Cursor cursor = null;
        UserInfo userInfo = null;

        try {
            //Get Database
            eodDatabase = EodDatabase.getInstance();
            sqliteDatabase = eodDatabase.openDatabase();

            String loginId = "";
            String password = "";

            if (userCredential != null) {
                loginId = userCredential.getLoginId();
                password = userCredential.getPassword();
            }

            //Where Clause
            String whereClause = EodDatabase.COLUMN_NAME_LOGIN_ID + " = ? AND " + EodDatabase.COLUMN_NAME_PASSWORD + " = ?";
            String[] whereClauseArgs = {loginId, password};

            //Execute Query
            cursor = sqliteDatabase.query(EodDatabase.TABLE_NAME_EOD_USERINFO_TABLE, null, whereClause, whereClauseArgs, null, null, null);

            if (cursor != null && cursor.moveToFirst()) {
                //Prepare UserInfo
                userInfo = prepareUserInfo(cursor);
            }
        } catch (Exception exception) {
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1012, exception.getMessage(), exception);
        } finally {
            AppUtil.closeCursor(cursor);
            eodDatabase.closeDatabase();
        }

        return userInfo;
    }

    //Get User Info from Last Stored UserId
    public static UserInfo getUserInfo(String userId) throws EODException {
        EodDatabase eodDatabase = null;
        SQLiteDatabase sqliteDatabase = null;
        Cursor cursor = null;
        UserInfo userInfo = null;

        try {
            //Get Database
            eodDatabase = EodDatabase.getInstance();
            sqliteDatabase = eodDatabase.openDatabase();

            //Where Clause
            String whereClause = EodDatabase.COLUMN_NAME_USER_ID + " = ?";
            String[] whereClauseArgs = {userId};

            //Execute Query
            cursor = sqliteDatabase.query(EodDatabase.TABLE_NAME_EOD_USERINFO_TABLE, null, whereClause, whereClauseArgs, null, null, null);

            if (cursor != null && cursor.moveToFirst()) {
                //Prepare UserInfo
                userInfo = prepareUserInfo(cursor);
            }
        } catch (Exception exception) {
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1012, exception.getMessage(), exception);
        } finally {
            AppUtil.closeCursor(cursor);
            eodDatabase.closeDatabase();
        }

        return userInfo;
    }

    //Prepare userInfo
    private static UserInfo prepareUserInfo(Cursor cursor) {
        UserInfo userInfo = new UserInfo();

        int columnIndexUserId = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_USER_ID);
        int columnIndexLoginId = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_LOGIN_ID);
        int columnIndexPassword = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_PASSWORD);
        int columnIndexDefaultCategoryId = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_DEFAULT_CATEGORY_ID);
        int columnIndexDefaultCurrencyCode = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_DEFAULT_CURRENCY_CODE);
        int columnIndexResourceId = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_RESOURCE_ID);
        int columnIndexCostCenterId = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_COST_CENTER_ID);
        int columnIndexCompanyLogo = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_COMPANY_LOGO);
        int columnIndexBranchId = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_BRANCH_ID);
        int columnIndexCompanyId = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_COMPANY_ID);
        int columnIndexDepartmentId = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_DEPARTMENT_ID);
        int columnIndexCostCenterName = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_COST_CENTER_NAME);
        int columnIndexCompanyName = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_COMPANY_NAME);
        int columnIndexDisplayName = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_DISPLAY_NAME);
        int columnIndexIsApprover = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_IS_APPROVER);
        int columnIndexIsFinance = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_IS_FINANCE);
        int columnIndexIsBoss = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_IS_BOSS);
        int columnIndexIsDelegate = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_IS_DELEGATE);
        int columnIndexIsGradeId = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_GRADE_ID);
        int columnIndexIsDeputyAssigned = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_IS_DEPUTY_ASSIGNED);

        String loginId = cursor.getString(columnIndexLoginId);
        String password = cursor.getString(columnIndexPassword);

        UserCredential userCredential = new UserCredential();
        userCredential.setLoginId(loginId);
        userCredential.setPassword(password);


        userInfo.setUserCredential(userCredential);

        String userId = cursor.getString(columnIndexUserId);
        userInfo.setUserId(userId);

        int defaultCategoryId = cursor.getInt(columnIndexDefaultCategoryId);
        userInfo.setDefaultCategoryId(defaultCategoryId);

        String defaultCurrencyCode = cursor.getString(columnIndexDefaultCurrencyCode);
        userInfo.setDefaultCurrencyCode(defaultCurrencyCode);

        userInfo.setResourceId(cursor.getString(columnIndexResourceId));
        userInfo.setCostCenterId(cursor.getString(columnIndexCostCenterId));
        userInfo.setCompanyLogo(cursor.getString(columnIndexCompanyLogo));
        userInfo.setBranchId(cursor.getString(columnIndexBranchId));
        userInfo.setCompanyId(cursor.getString(columnIndexCompanyId));
        userInfo.setDepartmentId(cursor.getString(columnIndexDepartmentId));
        userInfo.setCostCenterId(cursor.getString(columnIndexCostCenterName));
        userInfo.setCompanyName(cursor.getString(columnIndexCompanyName));
        userInfo.setDisplayName(cursor.getString(columnIndexDisplayName));
        userInfo.setApprover(cursor.getInt(columnIndexIsApprover) == 1);
        userInfo.setFinance(cursor.getInt(columnIndexIsFinance) == 1);
        userInfo.setBoss(cursor.getInt(columnIndexIsBoss) == 1);
        userInfo.setDelegate(cursor.getInt(columnIndexIsDelegate) == 1);
        userInfo.setDelegate(cursor.getInt(columnIndexIsDeputyAssigned) == 1);
        userInfo.setGradeId(cursor.getString(columnIndexIsGradeId));

        return userInfo;
    }

    //Get Expense Category Code HashMap
    @SuppressLint("UseSparseArrays")
    public static HashMap<Integer, Integer> getExpenseCategoryCodeHashMap() throws EODException {
        EodDatabase eodDatabase = null;
        SQLiteDatabase sqliteDatabase = null;
        Cursor cursor = null;

        String whereClause = null;
        HashMap<Integer, Integer> expenseCategoryCodeHashMap = new HashMap<Integer, Integer>();
        int expenseCategoryId = -1;

        try {
            //Get Database
            eodDatabase = EodDatabase.getInstance();
            sqliteDatabase = eodDatabase.openDatabase();

            //Get userInfo from Cache
            UserInfo userInfo = CachingManager.getUserInfo();

            String userId = userInfo.getUserId();

            if (userId != null) {

                whereClause = EodDatabase.COLUMN_NAME_USER_ID + " = ?";

                String[] whereClauseArgs = {userId};

                //Fire the query
                cursor = sqliteDatabase.query(EodDatabase.TABLE_NAME_EOD_EXPENSE_CATEGORY_TABLE, null, whereClause, whereClauseArgs, null, null, null);

                if (cursor != null && cursor.moveToFirst()) {
                    int columnIndexExpenseCategoryId = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_EXPENSE_CATEGORY_ID);

                    do {
                        expenseCategoryId = cursor.getInt(columnIndexExpenseCategoryId);
                        expenseCategoryCodeHashMap.put(expenseCategoryId, expenseCategoryId);
                    }
                    while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1014, exception.getMessage(), exception);
        } finally {
            AppUtil.closeCursor(cursor);
            eodDatabase.closeDatabase();
        }

        return expenseCategoryCodeHashMap;
    }

    //Refresh Expense Categories
    public static void refreshExpenseCategoryList(ArrayList<ExpenseCategory> updatedCategories, ArrayList<ExpenseCategory> newCategories) throws EODException {
        EodDatabase eodDatabase = null;
        SQLiteDatabase sqliteDatabase = null;

        try
        {
            //Get Database
            eodDatabase = EodDatabase.getInstance();
            sqliteDatabase = eodDatabase.openDatabase();

            beginTransaction(sqliteDatabase); //Begin Transaction

            updateExpenseCategoryList(updatedCategories, sqliteDatabase);

            saveExpenseCategoryList(newCategories, sqliteDatabase);

            setTransactionSuccessful(sqliteDatabase); //Mark Transaction for Success
        }
        catch(Exception exception)
        {
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1015, exception.getMessage(), exception);
        }
        finally
        {
            endTransaction(sqliteDatabase);
            eodDatabase.closeDatabase();
        }
    }

    //Delete Expense Category
    public static long deleteExpenseCategory() throws EODException {
        long result = -1;

        SQLiteDatabase sqliteDatabase = null;
        EodDatabase eodDatabase = null;

        try {
            //Get Database
            eodDatabase = EodDatabase.getInstance();
            sqliteDatabase = eodDatabase.openDatabase();

            //Begin Transaction
            beginTransaction(sqliteDatabase);


            result = sqliteDatabase.delete(EodDatabase.TABLE_NAME_EOD_EXPENSE_CATEGORY_TABLE, null, null);


            //Set Transaction Successfull
            setTransactionSuccessful(sqliteDatabase);

        } catch (Exception exception) {
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1028, exception.getMessage(), exception);
        } finally {
            endTransaction(sqliteDatabase);
            eodDatabase.closeDatabase();
        }

        return result;
    }


    //Update Expense Categories
    private static void updateExpenseCategoryList(ArrayList<ExpenseCategory> expenseCategories, SQLiteDatabase sqLiteDatabase) throws EODException {
        ContentValues values = null;
        long rowId = -1;

        try {
            //Get userInfo from Cache
            UserInfo userInfo = CachingManager.getUserInfo();

            String userId = userInfo.getUserId();

            for (ExpenseCategory expenseCategory : expenseCategories) {
                if (expenseCategory != null) {
                    values = new ContentValues();

                    // Expense Category Id
                    values.put(EodDatabase.COLUMN_NAME_EXPENSE_CATEGORY_ID, AppUtil.setIntegerNotNull(expenseCategory.getCategoryId()));

                    // Expense Base Category Id
                    values.put(EodDatabase.COLUMN_NAME_EXPENSE_BASE_CATEGORY_ID, AppUtil.setIntegerNotNull(expenseCategory.getBaseCategoryId()));

                    // User_Id
                    values.put(EodDatabase.COLUMN_NAME_USER_ID, userId);

                    //Icon Category Name
                    values.put(EodDatabase.COLUMN_NAME_EXPENSE_CATEGORY_NAME, AppUtil.setStringNotNull(expenseCategory.getCategoryName()));

                    //If this currencyName, update user information
                    String whereClause = EodDatabase.COLUMN_NAME_USER_ID + " = ? AND " + EodDatabase.COLUMN_NAME_EXPENSE_CATEGORY_ID + " = ?";
                    String[] whereClauseArgs = {userId, String.valueOf(expenseCategory.getCategoryId())};

                    // update Row
                    rowId = sqLiteDatabase.update(EodDatabase.TABLE_NAME_EOD_EXPENSE_CATEGORY_TABLE, values, whereClause, whereClauseArgs);

                    if (rowId == -1) {
                        throw new EODAppException(AppConstant.ERROR_CODE_1016, "There is some problem in inserting the Expense Categories");
                    }
                }
            }
        } catch (Exception exception) {
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1017, exception.getMessage(), exception);
        }
    }

    //Save Expense Categories
    private static void saveExpenseCategoryList(ArrayList<ExpenseCategory> expenseCategories, SQLiteDatabase sqliteDatabase) throws EODException {
        ContentValues values = null;
        long rowId = -1;

        try {
            String userId = "";

            //Get userInfo from Cache
            UserInfo userInfo = CachingManager.getUserInfo();

            if (userInfo != null) {
                userId = userInfo.getUserId();
            }

            for (ExpenseCategory expenseCategory : expenseCategories) {
                if (expenseCategory != null) {
                    values = new ContentValues();

                    // Expense Category Id
                    values.put(EodDatabase.COLUMN_NAME_EXPENSE_CATEGORY_ID, AppUtil.setIntegerNotNull(expenseCategory.getCategoryId()));

                    // Expense Base Category Id
                    values.put(EodDatabase.COLUMN_NAME_EXPENSE_BASE_CATEGORY_ID, AppUtil.setIntegerNotNull(expenseCategory.getBaseCategoryId()));

                    // User_Id
                    values.put(EodDatabase.COLUMN_NAME_USER_ID, userId);

                    //Icon Category Name
                    values.put(EodDatabase.COLUMN_NAME_EXPENSE_CATEGORY_NAME, AppUtil.setStringNotNull(expenseCategory.getCategoryName()));

                    // Inserting Row
                    rowId = sqliteDatabase.insertOrThrow(EodDatabase.TABLE_NAME_EOD_EXPENSE_CATEGORY_TABLE, null, values);

                    if (rowId == -1) {
                        throw new EODAppException(AppConstant.ERROR_CODE_1018, "There is some problem in inserting the Expense Categories");
                    }
                }
            }
        } catch (Exception exception) {
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1019, exception.getMessage(), exception);
        }
    }

    /**
     * Get the Expense Categories from the List
     *
     * @param userId
     * @return
     * @throws EODException
     */
    public static ArrayList<ExpenseCategory> getExpenseCategoryList(String userId) throws EODException {
        EodDatabase eodDatabase = null;
        SQLiteDatabase sqliteDatabase = null;
        ExpenseCategory expenseCategory = null;
        Cursor cursor = null;
        ArrayList<ExpenseCategory> expenseCategories = null;

        try {
            //Get Database
            eodDatabase = EodDatabase.getInstance();
            sqliteDatabase = eodDatabase.openDatabase();

            //Where Clause
            String whereClause = EodDatabase.COLUMN_NAME_USER_ID + " = ?";
            String[] whereClauseArgs = {String.valueOf(userId)};

            String orderByClause = EodDatabase.COLUMN_NAME_EXPENSE_CATEGORY_NAME + " ASC";

            cursor = sqliteDatabase.query(EodDatabase.TABLE_NAME_EOD_EXPENSE_CATEGORY_TABLE, null, whereClause, whereClauseArgs, null, null, orderByClause, null);

            if (cursor != null && cursor.moveToFirst()) {
                expenseCategories = new ArrayList<ExpenseCategory>();
                do {
                    expenseCategory = prepareExpenseCategory(cursor);
                    expenseCategories.add(expenseCategory);
                }
                while (cursor.moveToNext());
            }
        } catch (Exception exception) {
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1020, exception.getMessage(), exception);
        } finally {
            AppUtil.closeCursor(cursor);
            eodDatabase.closeDatabase();
        }

        return expenseCategories;
    }


    //Prepare Expense Category
    private static ExpenseCategory prepareExpenseCategory(Cursor cursor) {
        //Create Category
        ExpenseCategory expenseCategory = new ExpenseCategory();

        int columnIndexExpenseCategoryId = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_EXPENSE_CATEGORY_ID);
        int columnIndexExpenseCategoryName = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_EXPENSE_CATEGORY_NAME);
        int columnIndexExpenseBaseCategoryId = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_EXPENSE_BASE_CATEGORY_ID);

        int expenseCategoryId = cursor.getInt(columnIndexExpenseCategoryId);
        expenseCategory.setCategoryId(expenseCategoryId);

        int expenseBaseCategoryId = cursor.getInt(columnIndexExpenseBaseCategoryId);
        expenseCategory.setBaseCategoryId(expenseBaseCategoryId);

        String expenseCategoryName = cursor.getString(columnIndexExpenseCategoryName);
        expenseCategory.setCategoryName(expenseCategoryName);


        return expenseCategory;
    }

    //Save Expense
    public static boolean saveExpense(Expense expense, boolean isEditExpense) throws EODException {
        SQLiteDatabase sqliteDatabase = null;
        EodDatabase eodDatabase = null;
        boolean isExpenseSaveSuccessfully = false;

        try {
            //Get Database
            eodDatabase = EodDatabase.getInstance();
            sqliteDatabase = eodDatabase.openDatabase();

            //Begin Transaction
            beginTransaction(sqliteDatabase);

            if (expense != null) {
                if (isEditExpense) //Update
                {
                    updateExpense(expense, sqliteDatabase);
                } else //Create
                {
                    createExpense(expense, sqliteDatabase);
                }
            }

            //Set Transaction Successfull
            setTransactionSuccessful(sqliteDatabase);

            isExpenseSaveSuccessfully = true;
        } catch (Exception exception) {
            isExpenseSaveSuccessfully = false;
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1023, exception.getMessage(), exception);
        } finally {
            endTransaction(sqliteDatabase);
            eodDatabase.closeDatabase();
        }

        return isExpenseSaveSuccessfully;
    }

    private static long createExpense(Expense expense, SQLiteDatabase sqliteDatabase) throws EODException {
        long result = -1;

        try {
            //Get Receipt Image
            ReceiptImage receiptImage = expense.getReceiptImage();

            ContentValues createExpense = new ContentValues();

            createExpense.put(EodDatabase.COLUMN_NAME_USER_ID, expense.getUserId()); //UserId
            createExpense.put(EodDatabase.COLUMN_NAME_EXPENSE_DATE, expense.getExpenseDate()); //Expense Date
            createExpense.put(EodDatabase.COLUMN_NAME_EXPENSE_CATEGORY_ID, expense.getExpenseCategoryId()); //Category Id
            createExpense.put(EodDatabase.COLUMN_NAME_EXPENSE_BASE_CATEGORY_ID, expense.getExpenseBaseCategoryId()); //Base Category Id
            createExpense.put(EodDatabase.COLUMN_NAME_EXPENSE_CATEGORY_NAME, expense.getExpenseCategoryName()); //Category Name
            createExpense.put(EodDatabase.COLUMN_NAME_CURRENCY_CODE, expense.getCurrencyCode()); //Currency Code
            createExpense.put(EodDatabase.COLUMN_NAME_EXPENSE_AMOUNT, expense.getExpenseAmount()); //Amount
            createExpense.put(EodDatabase.COLUMN_NAME_EXPENSE_NOTES, expense.getExpenseNotes()); //Expense Notes
            createExpense.put(EodDatabase.COLUMN_NAME_HAS_RECEIPT, (expense.isHasReceipt() ? 1 : 0)); //has Receipt
            createExpense.put(EodDatabase.COLUMN_NAME_RECEIPT_IMAGE, receiptImage.getReceiptImageFileName()); //Receipt Image Data

            result = sqliteDatabase.insert(EodDatabase.TABLE_NAME_EOD_EXPENSE_TABLE, null, createExpense);
        } catch (Exception exception) {
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1024, exception.getMessage(), exception);
        }

        return result;

    }

    private static long updateExpense(Expense expense, SQLiteDatabase sqliteDatabase) throws EODException {
        long result = -1;

        try {
            //Get Receipt Image
            ReceiptImage receiptImage = expense.getReceiptImage();

            ContentValues updatedExpense = new ContentValues();

            updatedExpense.put(EodDatabase.COLUMN_NAME_USER_ID, expense.getUserId()); //UserId
            updatedExpense.put(EodDatabase.COLUMN_NAME_EXPENSE_DATE, expense.getExpenseDate()); //Expense Date
            updatedExpense.put(EodDatabase.COLUMN_NAME_EXPENSE_CATEGORY_ID, expense.getExpenseCategoryId()); //Category Id
            updatedExpense.put(EodDatabase.COLUMN_NAME_EXPENSE_BASE_CATEGORY_ID, expense.getExpenseBaseCategoryId()); //Base Category Id
            updatedExpense.put(EodDatabase.COLUMN_NAME_EXPENSE_CATEGORY_NAME, expense.getExpenseCategoryName()); //Category Name
            updatedExpense.put(EodDatabase.COLUMN_NAME_CURRENCY_CODE, expense.getCurrencyCode()); //Currency Code
            updatedExpense.put(EodDatabase.COLUMN_NAME_EXPENSE_AMOUNT, expense.getExpenseAmount()); //Amount
            updatedExpense.put(EodDatabase.COLUMN_NAME_EXPENSE_NOTES, expense.getExpenseNotes()); //Expense Notes
            updatedExpense.put(EodDatabase.COLUMN_NAME_HAS_RECEIPT, (expense.isHasReceipt() ? 1 : 0)); //has Receipt
            updatedExpense.put(EodDatabase.COLUMN_NAME_RECEIPT_IMAGE, receiptImage.getReceiptImageFileName()); //Receipt Image Name

            String whereClause = EodDatabase.COLUMN_NAME_EXPENSE_ID + " = ?";
            String[] whereClauseArgs = {String.valueOf(expense.getExpenseId())};

            result = sqliteDatabase.update(EodDatabase.TABLE_NAME_EOD_EXPENSE_TABLE, updatedExpense, whereClause, whereClauseArgs);
        } catch (Exception exception) {
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1025, exception.getMessage(), exception);
        }

        return result;
    }

    public static long deleteExpense(ArrayList<Integer> expenseIdList) throws EODException {
        long result = -1;

        SQLiteDatabase sqliteDatabase = null;
        EodDatabase eodDatabase = null;

        try {
            //Get Database
            eodDatabase = EodDatabase.getInstance();
            sqliteDatabase = eodDatabase.openDatabase();

            //Begin Transaction
            beginTransaction(sqliteDatabase);

            if (!AppUtil.isCollectionEmpty(expenseIdList)) {
                String inClause = TextUtils.join(",", expenseIdList);
                String whereClause = EodDatabase.COLUMN_NAME_EXPENSE_ID + " IN (" + inClause + ")";

                result = sqliteDatabase.delete(EodDatabase.TABLE_NAME_EOD_EXPENSE_TABLE, whereClause, null);
            }

            //Set Transaction Successfull
            setTransactionSuccessful(sqliteDatabase);

        } catch (Exception exception) {
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1028, exception.getMessage(), exception);
        } finally {
            endTransaction(sqliteDatabase);
            eodDatabase.closeDatabase();
        }

        return result;
    }

    //Get Expense List
    public static ArrayList<Expense> getExpenseList(String userId) throws EODException {
        EodDatabase eodDatabase = null;
        SQLiteDatabase sqliteDatabase = null;
        Expense tempExpense = null;
        ArrayList<Expense> expenseList = null;
        Cursor cursor = null;

        try {
            //Get Database
            eodDatabase = EodDatabase.getInstance();
            sqliteDatabase = eodDatabase.openDatabase();

            //Where Clause
            String whereClause = EodDatabase.COLUMN_NAME_USER_ID + " = ?";
            String[] whereClauseArgs = {String.valueOf(userId)};

            //Order By Clause
            String orderByClause = EodDatabase.COLUMN_NAME_EXPENSE_DATE + " ASC";

            cursor = sqliteDatabase.query(EodDatabase.TABLE_NAME_EOD_EXPENSE_TABLE, null, whereClause, whereClauseArgs, null, null, orderByClause, null);

            if (cursor != null && cursor.moveToFirst()) {
                expenseList = new ArrayList<Expense>();

                do {
                    tempExpense = prepareExpenseObject(cursor);
                    expenseList.add(tempExpense);
                }

                while (cursor.moveToNext());
            }
        } catch (Exception exception) {
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1026, exception.getMessage(), exception);
        } finally {
            AppUtil.closeCursor(cursor);
            eodDatabase.closeDatabase();
        }

        return expenseList;
    }


    //Get Expense List count
    public static int getExpenseListCount(String userId) throws EODException {
        EodDatabase eodDatabase = null;
        SQLiteDatabase sqliteDatabase = null;
        int count=0;
        Cursor cursor = null;

        try {
            //Get Database
            eodDatabase = EodDatabase.getInstance();
            sqliteDatabase = eodDatabase.openDatabase();

            //Where Clause
            String whereClause = EodDatabase.COLUMN_NAME_USER_ID + " = ?";
            String[] whereClauseArgs = {String.valueOf(userId)};

            //Order By Clause
            String orderByClause = EodDatabase.COLUMN_NAME_EXPENSE_CATEGORY_NAME + " ASC";

            cursor = sqliteDatabase.query(EodDatabase.TABLE_NAME_EOD_EXPENSE_TABLE, null, whereClause, whereClauseArgs, null, null, orderByClause, null);

            if (cursor != null && cursor.moveToFirst()) {
                count=cursor.getCount();
            }
        } catch (Exception exception) {
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1026, exception.getMessage(), exception);
        } finally {
            AppUtil.closeCursor(cursor);
            eodDatabase.closeDatabase();
        }

        return count;
    }

    //Prepare Expense Object
    private static Expense prepareExpenseObject(Cursor cursor) {
        Expense expense = new Expense();

        int columnIndexExpenseId = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_EXPENSE_ID);
        int columnIndexUserId = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_USER_ID);
        int columnIndexExpenseDate = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_EXPENSE_DATE);
        int columnIndexExpenseCategoryId = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_EXPENSE_CATEGORY_ID);
        int columnIndexExpenseBaseCategoryId = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_EXPENSE_BASE_CATEGORY_ID);
        int columnIndexCategoryName = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_EXPENSE_CATEGORY_NAME);
        int columnIndexCurrencyCode = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_CURRENCY_CODE);
        int columnIndexExpenseAmount = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_EXPENSE_AMOUNT);
        int columnIndexExpenseNotes = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_EXPENSE_NOTES);
        int columnIndexHasReceipt = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_HAS_RECEIPT);
        int columnIndexReceiptImage = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_RECEIPT_IMAGE);

        int expenseId = cursor.getInt(columnIndexExpenseId);
        String userId = cursor.getString(columnIndexUserId);
        long expenseDate = cursor.getLong(columnIndexExpenseDate);
        int expenseCategoryId = cursor.getInt(columnIndexExpenseCategoryId);
        int expenseBaseCategoryId = cursor.getInt(columnIndexExpenseBaseCategoryId);
        String expenseCategoryName = cursor.getString(columnIndexCategoryName);
        String currencyCode = cursor.getString(columnIndexCurrencyCode);
        String expenseAmount = cursor.getString(columnIndexExpenseAmount);
        String expenseNotes = cursor.getString(columnIndexExpenseNotes);
        int hasReceipt = cursor.getInt(columnIndexHasReceipt);
        String receiptImageData = cursor.getString(columnIndexReceiptImage);

        //Populate Expense Object
        expense.setExpenseId(expenseId);
        expense.setUserId(userId);
        expense.setExpenseDate(expenseDate);
        expense.setExpenseCategoryId(expenseCategoryId);
        expense.setExpenseBaseCategoryId(expenseBaseCategoryId);
        expense.setExpenseCategoryName(expenseCategoryName);
        expense.setCurrencyCode(currencyCode);
        expense.setExpenseAmount(expenseAmount);
        expense.setExpenseNotes(expenseNotes);
        expense.setHasReceipt((hasReceipt == 1) ? true : false);

        //Receipt Image
        ReceiptImage receiptImage = new ReceiptImage();
        receiptImage.setReceiptImageFileName(receiptImageData);

        expense.setReceiptImage(receiptImage);

        return expense;
    }

    //Get Distinct Expense Dates
    public static ArrayList<Long> getDistinctExpenseDateList(String userId) throws EODException {
        EodDatabase eodDatabase = null;
        SQLiteDatabase sqliteDatabase = null;
        Cursor cursor = null;
        ArrayList<Long> distinctExpenseDateList = null;

        try {
            //Get Database
            eodDatabase = EodDatabase.getInstance();
            sqliteDatabase = eodDatabase.openDatabase();

            //Create Raw Query
            String rawQuery = "";

            rawQuery += "SELECT DISTINCT " + EodDatabase.COLUMN_NAME_EXPENSE_DATE + " FROM " + EodDatabase.TABLE_NAME_EOD_EXPENSE_TABLE;
            rawQuery += " WHERE " + EodDatabase.COLUMN_NAME_USER_ID + " = '" + userId;
            rawQuery += "' ORDER BY " + EodDatabase.COLUMN_NAME_EXPENSE_DATE + " DESC";

            //Execute Query
            cursor = sqliteDatabase.rawQuery(rawQuery, null);

            if (cursor != null && cursor.moveToFirst()) {
                distinctExpenseDateList = new ArrayList<Long>();

                do {
                    //Prepare distinct date list
                    distinctExpenseDateList.add(cursor.getLong(0));
                }

                while (cursor.moveToNext());
            }
        } catch (Exception exception) {
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1027, exception.getMessage(), exception);
        } finally {
            AppUtil.closeCursor(cursor);
            eodDatabase.closeDatabase();
        }

        return distinctExpenseDateList;
    }

    //Get Receipt Image
    public static String getReceiptImage(int expenseId) throws EODException {
        EodDatabase eodDatabase = null;
        SQLiteDatabase sqliteDatabase = null;
        Cursor cursor = null;
        String receiptImageData = "";

        try {
            //Get Database
            eodDatabase = EodDatabase.getInstance();
            sqliteDatabase = eodDatabase.openDatabase();

            //Columns to retrive
            String queryColumns[] = {EodDatabase.COLUMN_NAME_RECEIPT_IMAGE};

            String whereClause = EodDatabase.COLUMN_NAME_EXPENSE_ID + " = ?";
            String[] whereClauseArgs = {String.valueOf(expenseId)};

            //Execute Query
            cursor = sqliteDatabase.query(EodDatabase.TABLE_NAME_EOD_EXPENSE_TABLE, queryColumns, whereClause, whereClauseArgs, null, null, null);

            if (cursor != null && cursor.moveToFirst()) {
                int columnnIndexReceiptImage = cursor.getColumnIndex(EodDatabase.COLUMN_NAME_RECEIPT_IMAGE);

                receiptImageData = cursor.getString(columnnIndexReceiptImage);
            }
        } catch (Exception exception) {
            ExceptionManager.dispatchExceptionDetails(AppConstant.ERROR_CODE_1029, exception.getMessage(), exception);
        } finally {
            AppUtil.closeCursor(cursor);
            eodDatabase.closeDatabase();
        }

        return receiptImageData;
    }

    /**
     * Sql Transaction Methods
     */
    private static void beginTransaction(SQLiteDatabase sqliteDatabase) {
        try {
            if (sqliteDatabase != null) {
                sqliteDatabase.beginTransactionNonExclusive();
            }
        } catch (Exception exception) {
            //Consume
        }
    }

    private static void setTransactionSuccessful(SQLiteDatabase sqliteDatabase) {
        try {
            if (sqliteDatabase != null) {
                sqliteDatabase.setTransactionSuccessful();
            }
        } catch (Exception exception) {
            //Consume
        }
    }

    private static void endTransaction(SQLiteDatabase sqliteDatabase) {
        try {
            if (sqliteDatabase != null) {
                sqliteDatabase.endTransaction();
            }
        } catch (Exception exception) {
            //Consume
        }
    }
}
