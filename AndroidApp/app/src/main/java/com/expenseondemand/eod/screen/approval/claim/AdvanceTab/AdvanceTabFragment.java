package com.expenseondemand.eod.screen.approval.claim.AdvanceTab;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.expenseondemand.eod.BaseFragment;
import com.expenseondemand.eod.R;

/**
 * Created by Ramit Yadav on 21-10-2016.
 */

public class AdvanceTabFragment extends BaseFragment
{
    private TextView textViewNodataMessage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.approval_claim_advance_tab_fragment, container, false);

        initializeView(rootView);

        return rootView;
    }

    private void initializeView(View rootView)
    {
        textViewNodataMessage=(TextView)rootView.findViewById(R.id.textViewNodataMessage);
        textViewNodataMessage.setText("Coming Soon...");
    }
}
