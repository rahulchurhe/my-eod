package com.expenseondemand.eod.model.approval.expense;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Nandani Gupta on 2016-10-20.
 */
public class ExpenseInfo implements Parcelable
{
    PolicyBreachModel policyBreech;
    String ExpenseHeaderID;
    String ExpenseDetailID;
    String categoryName;


    /**
     * Standard basic constructor for non-parcel
     * object creation
     */
    public ExpenseInfo() {}

    /**
     *
     * Constructor to use when re-constructing object
     * from a parcel
     *
     * @param in a parcel from which to read this object
     */
    public ExpenseInfo(Parcel in) {
        ExpenseItemPolicyBreechInfo(in);
    }
    /**
     *
     * Called from the constructor to create this
     * object from a parcel.
     *
     * @param in parcel from which to re-create object
     */
    private void ExpenseItemPolicyBreechInfo(Parcel in) {
        policyBreech = in.readParcelable(PolicyBreachModel.class.getClassLoader());
        ExpenseHeaderID = in.readString();
        ExpenseDetailID = in.readString();
        categoryName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(policyBreech, flags);
        dest.writeString(ExpenseHeaderID);
        dest.writeString(ExpenseDetailID);
        dest.writeString(categoryName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ExpenseInfo> CREATOR = new Creator<ExpenseInfo>() {
        @Override
        public ExpenseInfo createFromParcel(Parcel in) {
            return new ExpenseInfo(in);
        }

        @Override
        public ExpenseInfo[] newArray(int size) {
            return new ExpenseInfo[size];
        }
    };





    public PolicyBreachModel getPolicyBreech() {
        return policyBreech;
    }

    public void setPolicyBreech(PolicyBreachModel policyBreech) {
        this.policyBreech = policyBreech;
    }

    public String getExpenseHeaderID() {
        return ExpenseHeaderID;
    }

    public void setExpenseHeaderID(String expenseHeaderID) {
        ExpenseHeaderID = expenseHeaderID;
    }

    public String getExpenseDetailID() {
        return ExpenseDetailID;
    }

    public void setExpenseDetailID(String expenseDetailID) {
        ExpenseDetailID = expenseDetailID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
