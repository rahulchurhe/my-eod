package com.expenseondemand.eod.screen.approval.expense.itemlist;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.util.AppConstant;

import java.util.ArrayList;

/**
 * Created by ITDIVISION\maximess087 on 5/3/17.
 */

public class EndlessExpenseListView extends ListView implements AbsListView.OnScrollListener {

    private Context context;
    private View footer;
    private boolean isLoading;
    private EndLessListener listener;
    private ExpenseListAdapter adapter;

    public EndlessExpenseListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setOnScrollListener(this);

        this.context = context;
    }

    public EndlessExpenseListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setOnScrollListener(this);

        this.context = context;
    }

    public EndlessExpenseListView(Context context) {
        super(context);
        this.setOnScrollListener(this);

        this.context = context;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    //	4
    public void addNewData(ArrayList<ExpenseItemsModel> data) {

        this.removeFooterView(footer);
        adapter.addAll(data);
        adapter.notifyDataSetChanged();
        isLoading = false;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        if (getAdapter() == null)
            return;

        if (getAdapter().getCount() == 0)
            return;

        int l = visibleItemCount + firstVisibleItem;

        if (l >= totalItemCount && !isLoading) {

            if (SharePreference.getBoolean(context, AppConstant.LOAD_MORE_EXPENSES)) {
                //	add footer layout
                this.addFooterView(footer);

                //	set progress boolean
                isLoading = true;

                //	call interface method to load new data
                listener.loadData();
            }


        }
    }

    //	Calling order from MainActivity
    //	1
    public void setLoadingView(int resId) {
        footer = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(resId, null);        //		footer = (View)inflater.inflate(resId, null);
        this.addFooterView(footer);
    }

    //	2
    public void setListener(EndLessListener listener) {
        this.listener = listener;
    }

    //	3
    public void setAdapter(ExpenseListAdapter adapter) {
        super.setAdapter(adapter);
        this.adapter = adapter;
        this.removeFooterView(footer);
    }


    public void stopFooter() {
        this.removeFooterView(footer);
    }

    public void LoadMore(){
        listener.loadData();
    }

    //	interface
    public interface EndLessListener {
        public void loadData();
    }
}