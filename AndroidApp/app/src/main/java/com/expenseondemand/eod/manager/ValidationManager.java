package com.expenseondemand.eod.manager;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.model.Expense;
import com.expenseondemand.eod.model.ValidationError;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;


public class ValidationManager
{
	//Validate Expense Details for Save Expense
	public static ValidationError validateExpenseDetails(String expenseCategoryName, String expenseAmount,String expenseNote)
	{
		Context appContext = CachingManager.getAppContext();
		ValidationError validationError = null;

		if(AppUtil.isStringEmpty(expenseCategoryName))
		{
			validationError = new ValidationError(AppConstant.VALIDATION_ERROR_CODE_101, appContext.getString(R.string.message_select_category));
			
			return validationError;
		}
		
		if(AppUtil.isStringEmpty(expenseAmount))
		{
			validationError = new ValidationError(AppConstant.VALIDATION_ERROR_CODE_102, appContext.getString(R.string.message_amount_empty));
			
			return validationError;
		}
		else
		{
			expenseAmount = AppUtil.formatToDouble(expenseAmount);
			double expenseAmountDouble = 0.0;
			
			try
			{
				expenseAmountDouble = Double.parseDouble(expenseAmount);
			}
			catch(NumberFormatException numberFormatException)
			{
				//Consume
			}
			
			if(expenseAmountDouble == 0.0)
			{
				validationError = new ValidationError(AppConstant.VALIDATION_ERROR_CODE_103, appContext.getString(R.string.message_amount_zero));
				
				return validationError;
			}
		}

		if(!AppUtil.isStringEmpty(expenseNote)){
			String newString = expenseNote.replaceAll("[\u0000-\u001f]", "");
			if(!newString.matches("^[a-zA-Z .,0-9\r\n]*")|| newString.startsWith(" ")){
				validationError = new ValidationError(AppConstant.VALIDATION_ERROR_CODE_103, appContext.getString(R.string.message_note_validation));
				return validationError;
			}
		}

		return validationError;
	}

	//Validate Expense List for Edit
	public static ValidationError validateEditExpense(ArrayList<Expense> expenseList)
	{
		Context appContext = CachingManager.getAppContext();
		ValidationError validationError = null;

		int countSelectedExpenses = getSelectedExpenseCount(expenseList);

		if(countSelectedExpenses == 0)
		{
			validationError = new ValidationError(AppConstant.VALIDATION_ERROR_CODE_104, appContext.getString(R.string.message_no_expense_item_to_edit));
		}
		else if(countSelectedExpenses > 1)
		{
			validationError = new ValidationError(AppConstant.VALIDATION_ERROR_CODE_105, appContext.getString(R.string.message_multiple_expense_to_edit));
		}

		return validationError;
	}

	//Validate Expense List for Delete Expense
	public static ValidationError validateDeleteExpense(ArrayList<Expense> expenseList)
	{
		Context appContext = CachingManager.getAppContext();
		ValidationError validationError = null;

		int countSelectedExpenses = getSelectedExpenseCount(expenseList);

		if(countSelectedExpenses == 0)
		{
			validationError = new ValidationError(AppConstant.VALIDATION_ERROR_CODE_106, appContext.getString(R.string.message_please_select_atleast_one_expense));
		}

		return validationError;
	}

	//Validate Upload Expense
	public static ValidationError validateUploadExpense(ArrayList<Expense> expenseList)
	{
		Context appContext = CachingManager.getAppContext();
		ValidationError validationError = null;

		int countSelectedExpenses = getSelectedExpenseCount(expenseList);

		if(countSelectedExpenses == 0)
		{
			validationError = new ValidationError(AppConstant.VALIDATION_ERROR_CODE_107, appContext.getString(R.string.message_please_select_atleast_one_expense));
		}
		else
		{
			validationError = validateFutureExpenseUpload(expenseList);
		}

		return validationError;
	}

	//Validate Future Expense Upload
	public static ValidationError validateFutureExpenseUpload(ArrayList<Expense> expenseList)
	{
		Context appContext = CachingManager.getAppContext();
		boolean isFutureDateExpense = false;
		ValidationError validationError = null;

		long currentDate = System.currentTimeMillis();

		if(!AppUtil.isCollectionEmpty(expenseList))
		{
			//Iterate
			for (Expense expense : expenseList) 
			{
				if(expense != null)
				{
					long expenseDate = expense.getExpenseDate();

					//Get Date Differeces
					long difference = expenseDate - currentDate;

					if(difference > 0 )
					{
						isFutureDateExpense = true;
						break;
					}
				}
			}
		}
		
		if(isFutureDateExpense)
		{
			validationError = new ValidationError(AppConstant.VALIDATION_ERROR_CODE_108, appContext.getString(R.string.message_future_expense_upload));
		}

		return validationError;
	}

	//Validate No Expense selected
	private static int getSelectedExpenseCount(ArrayList<Expense> expenseList)
	{
		int countSelectedExpenses = 0;

		if(!AppUtil.isCollectionEmpty(expenseList))
		{
			//Iterate Expense List
			for (Expense expense : expenseList) 
			{
				if(expense != null)
				{
					//Check if it is Selected
					if(expense.isSelected())
					{
						countSelectedExpenses++;
					}
				}
			}
		}

		return countSelectedExpenses;
	}
}
