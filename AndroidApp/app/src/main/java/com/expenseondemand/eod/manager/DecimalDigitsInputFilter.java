package com.expenseondemand.eod.manager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.text.InputFilter;
import android.text.Spanned;

public class DecimalDigitsInputFilter implements InputFilter
{
	private Pattern patternAllDigits = null;
	private Pattern patternAllDigitsWithDecimal = null;

	private Matcher matcherAllDigits = null;
	private Matcher matcherAllDigitsWithDecimal = null;

	private static final String REG_EXP_PATTERN_ALL_DIGITS = "^\\d{0,10}";
	private static final String REG_EXP_PATTERN_ALL_DIGITS_AND_DECIMALS = "^\\d{0,10}(\\.)\\d{0,2}";

	public DecimalDigitsInputFilter()
	{
		patternAllDigits = Pattern.compile(REG_EXP_PATTERN_ALL_DIGITS);
		patternAllDigitsWithDecimal = Pattern.compile(REG_EXP_PATTERN_ALL_DIGITS_AND_DECIMALS);
	}

	public DecimalDigitsInputFilter(int numberOfNonDecimalDigits, int numberOfDecimalDigits)
	{
		String regExpPatternAllDigits = "^\\d{0,"+numberOfNonDecimalDigits+"}";
		String regExpPatternAllDigitsAndDecimals = regExpPatternAllDigits+"(\\.)\\d{0,"+numberOfDecimalDigits+"}";

		patternAllDigits = Pattern.compile(regExpPatternAllDigits);
		patternAllDigitsWithDecimal = Pattern.compile(regExpPatternAllDigitsAndDecimals);
	}

	/**
	 * source - Character(s) entered by the user. 
	 * 			If one character then start is 0 and end is 1. 
	 * 			If more characters are pasted then it starts from 0 and end at number of characters entered.
	 * 			If characters are removed then both start and end are 0
	 * 
	 * dest - 	Text before change. 
	 * 			If character(s) are added then dstart is number of characters before change and dend is number of characters before change. They will be same.
	 * 			If character(s) are removed then dstart will be number of characters after change and dend will be number of charactes before change.    
	 * 
	 * Ex
	 * OUTPUT - source|start|end|dest|dstart|dend
	 * Entered 5 
	 * OUTPUT - 5|0|1||0|0
	 * 
	 * Entered 8 
	 * OUTPUT - 8|0|1|5|1|1
	 * 
	 * Entered 9
	 * OUTPUT - 9|0|1|58|2|2
	 * 
	 * Entered 5639178 (Characters Pasted)
	 * OUTPUT - 5639178|0|7|589|3|3
	 *  
	 * Removed 8
	 * OUTPUT - |0|0|5895639178|9|10
	 */
	@Override
	public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend)
	{
		CharSequence charSequence = null;
		boolean isMatching = false;
		String destnationText = dest.toString();
		String resultingText = destnationText.substring(0, dstart) + source.subSequence(start, end) + destnationText.substring(dend);

		//Log.i(AppConstants.LOG_TAG, source + "|" + start + "|" + end + "|" + dest + "|" + dstart + "|" + dend);
		//Log.i(AppConstants.LOG_TAG, resultingText);

		matcherAllDigits = patternAllDigits.matcher(resultingText);
		matcherAllDigitsWithDecimal = patternAllDigitsWithDecimal.matcher(resultingText); 

		isMatching = (matcherAllDigits.matches() || matcherAllDigitsWithDecimal.matches());
		if(!isMatching)
		{
			charSequence = "";
		}

		return charSequence;
	}
}