package com.expenseondemand.eod.webservice.model.approval.detail.expense.receipt;

import com.expenseondemand.eod.webservice.model.MasterRequest;

/**
 * Created by ITDIVISION\maximess087 on 28/1/17.
 */

public class ExpenseDetailReceiptRequest extends MasterRequest {
    private String Claimid;//ExpenseHeaderID
    private String ExpenseId;//ExpenseDetailID
    private String CompanyID;

    public String getClaimid() {
        return Claimid;
    }

    public void setClaimid(String claimid) {
        Claimid = claimid;
    }

    public String getExpenseId() {
        return ExpenseId;
    }

    public void setExpenseId(String expenseId) {
        ExpenseId = expenseId;
    }

    public String getCompanyID() {
        return CompanyID;
    }

    public void setCompanyID(String companyID) {
        CompanyID = companyID;
    }
}
