package com.expenseondemand.eod.screen.approval.detail;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.activity.BaseActivityExpenseList;
import com.expenseondemand.eod.dialogFragment.ApplicationAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationErrorAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationNetworkDialog;
import com.expenseondemand.eod.dialogFragment.ExpenseAlertDialog;
import com.expenseondemand.eod.exception.EODBusinessException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.DeviceManager;
import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.model.AppRejectionSetting;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.model.approval.detail.ApproveReject;
import com.expenseondemand.eod.model.approval.detail.ExpenseDetail;
import com.expenseondemand.eod.model.approval.detail.attendees.AttendeesItem;
import com.expenseondemand.eod.model.approval.detail.dynamic.DynamicDetail;
import com.expenseondemand.eod.model.approval.detail.mileage.MultipleMileage;
import com.expenseondemand.eod.model.approval.detail.notes.Notes;
import com.expenseondemand.eod.model.approval.detail.project.LstApproverRejectItem;
import com.expenseondemand.eod.model.approval.detail.project.ProjectDetailsItem;
import com.expenseondemand.eod.model.approval.detail.project.ReceiptItem;
import com.expenseondemand.eod.model.approval.expense.ExpenseInfo;
import com.expenseondemand.eod.model.approval.expense.PolicyBreachModel;
import com.expenseondemand.eod.model.approval.expense.multiplecategory.MultiCategoryExpenseParam;
import com.expenseondemand.eod.screen.approval.detail.attendees.AttendeesFragment;
import com.expenseondemand.eod.screen.approval.detail.mileage.MultipleMileageFragment;
import com.expenseondemand.eod.screen.approval.detail.notes.NotesFragment;
import com.expenseondemand.eod.screen.approval.detail.project.ProjectDetailFragment;
import com.expenseondemand.eod.screen.approval.detail.receipt.ReceiptFragment;
import com.expenseondemand.eod.screen.approval.detail.violation.PolicyViolationFragment;
import com.expenseondemand.eod.screen.approval.expense.multiplecategory.MultipleCategoryListActivity;
import com.expenseondemand.eod.screen.approval.expense_acceptance_reason.ApproverAcceptDialog;
import com.expenseondemand.eod.screen.approval.expense_rejection_reason.RejectionReasonDialog;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.util.UIUtil;
import com.expenseondemand.eod.webservice.model.RejectionReasonModel.RejectionReasonModel;
import com.expenseondemand.eod.webservice.model.approval.detail.ApprovalRejectedExpenseContainer;
import com.expenseondemand.eod.webservice.model.approval.detail.ApprovalRejectedExpenseRequest;
import com.expenseondemand.eod.webservice.model.approval.detail.ApprovalRejectedExpensesRequestData;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 13-09-2016.
 */
public class ExpenseDetailActivity extends BaseActivityExpenseList implements View.OnClickListener, ExpenseAlertDialog.AlertDialogActionListener, ApproveRejectedInterface, ApplicationAlertDialog.AlertDialogActionListener, ApplicationErrorAlertDialog.ErrorDialogActionListener {
    private ImageView imageViewExpenseCategoryIcon;
    private TextView textViewCategory;
    private TextView textViewUserName;
    private TextView textViewId;
    private TextView textViewDate;
    private TextView textViewAmount;
    private LinearLayout linearLayoutProjectDetail;
    private LinearLayout linearLayoutPolicyViolation;
    private LinearLayout linearLayoutAttendees;
    private LinearLayout linearLayoutNotes;
    private LinearLayout linearLayoutMultipleMileage;
    private ImageView imageViewPolicyViolationBlink;
    private ImageView imageViewRight;
    private ImageView imageViewMore;
    private TextView textViewAttendeesType;

    private ExpenseInfo expenseInfo;
    private ExpenseDetail expenseDetail;

    private LinearLayout currentSelected;
    private ArrayList<LinearLayout> middleTabs;
    private int currentSelectedIndex;

    private final String EXPENSE_INFO_KEY = "EXPENSE_INFO_KEY";
    private final String EXPENSE_DETAIL_KEY = "EXPENSE_DETAIL_KEY";
    private final String SELECTED_TAB_INDEX_KEY = "SELECTED_TAB_INDEX_KEY";

    private Button button_approve, button_reject;
    private ArrayList<RejectionReasonModel> rejectionReasonBackupList = new ArrayList<RejectionReasonModel>();
    private String str_rejectionNote, str_rejectionId, str_approveNote = null, str_acounterApproveNote = null;
    private ApplicationNetworkDialog applicationNetworkDialogl;
    private String expenseDate;
    private TextView txt_mileageStatus;

    ApplicationAlertDialog applicationAlertDialog;
    public static final int TYPE_ALERT = 0;
    private int receiptCount;
    private boolean isRejectedStatus;
    private boolean isrejected = false;
    private String claimName;
    private String claimType;
    private LinearLayout linearLayoutReceipt;
    private ExpenseAlertDialog expenseAlertDialog;
    private TextView textViewCount;
    private ApplicationErrorAlertDialog applicationErrorAlertDialog;
    private AppRejectionSetting appRejectionSetting;
    private RelativeLayout relativeOverlayScreenExpense;
    private ImageButton imageViewCloseButton;
    private LinearLayout linear_multipleCategoryList;

    @Override
    protected void onResume() {
        super.onResume();
        /*Back from network setting screen */
        if (SharePreference.getBoolean(getApplicationContext(), AppConstant.BACK_NETWORK_SETTING_STAT)) {
            SharePreference.putBoolean(getApplicationContext(), AppConstant.BACK_NETWORK_SETTING_STAT, false);
            loadData();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.approval_expense_detail_activity);
        //Initialize View
        initializeView();
        if (DeviceManager.isOnline()) {
            if (savedInstanceState == null) {
                linearLayoutProjectDetail.setSelected(true);
                loadData();
            } else {
                restoreData(savedInstanceState);
            }
        } else {
            enableNetwork();
        }
    }


    //Network enable dialog
    void enableNetwork() {
        applicationNetworkDialogl = new ApplicationNetworkDialog().getInstance(ExpenseDetailActivity.this);
        applicationNetworkDialogl.show(getSupportFragmentManager(), "dialog");
    }

    private void initializeView() {
        currentSelected = null;
        Bundle bundle = getIntent().getExtras();
        expenseInfo = bundle.getParcelable(AppConstant.KEY_EXPENSE_ITEM_POLICY_BREECH);
        isrejected = bundle.getBoolean(AppConstant.IS_REJECTED);
        claimName = bundle.getString(AppConstant.KEY_CLAIM_NAME);
        claimType = bundle.getString(AppConstant.KEY_CLAIM_TYPE);

        //Get App Rejected Setting rules.
        appRejectionSetting = CachingManager.getAppRejectedSetting();

        handleToolBar(getResources().getString(R.string.title_expense_detail), R.color.approval_claim_list_toolbar, R.color.approval_claim_list_toolbar, true);

        imageViewExpenseCategoryIcon = (ImageView) findViewById(R.id.imageViewExpenseCategoryIcon);
        textViewCategory = (TextView) findViewById(R.id.textViewCategory);
        textViewUserName = (TextView) findViewById(R.id.textViewUserName);
        textViewId = (TextView) findViewById(R.id.textViewId);
        textViewDate = (TextView) findViewById(R.id.textViewDate);
        textViewAmount = (TextView) findViewById(R.id.textViewAmount);
        linearLayoutProjectDetail = (LinearLayout) findViewById(R.id.linearLayoutProjectDetail);
        linearLayoutPolicyViolation = (LinearLayout) findViewById(R.id.linearLayoutPolicyViolation);
        linearLayoutReceipt = (LinearLayout) findViewById(R.id.linearLayoutReceipt);
        textViewCount = (TextView) findViewById(R.id.textViewCount);
        linearLayoutAttendees = (LinearLayout) findViewById(R.id.linearLayoutAttendees);
        linearLayoutNotes = (LinearLayout) findViewById(R.id.linearLayoutNotes);
        linearLayoutMultipleMileage = (LinearLayout) findViewById(R.id.linearLayoutMultipleMileage);
        imageViewPolicyViolationBlink = (ImageView) findViewById(R.id.imageViewPolicyViolationBlink);
        imageViewRight = (ImageView) findViewById(R.id.imageViewRight);
        imageViewMore = (ImageView) findViewById(R.id.imageViewMore);
        textViewAttendeesType = (TextView) findViewById(R.id.textViewAttendeesType);
        button_approve = (Button) findViewById(R.id.button_approve);
        button_approve.setTextColor(getResources().getColor(R.color.white));
        button_reject = (Button) findViewById(R.id.button_reject);
        button_reject.setTextColor(getResources().getColor(R.color.white));
        txt_mileageStatus = (TextView) findViewById(R.id.txt_mileageStatus);
        //overlay
        imageViewCloseButton = (ImageButton) findViewById(R.id.imageViewCloseButton);
        relativeOverlayScreenExpense = (RelativeLayout) findViewById(R.id.relativeOverlayScreenExpense);
        linear_multipleCategoryList = (LinearLayout) findViewById(R.id.linear_multipleCategoryList);

        setPolicyViolationTabAnimation();

        linearLayoutProjectDetail.setOnClickListener(this);
        linearLayoutPolicyViolation.setOnClickListener(this);
        linearLayoutReceipt.setOnClickListener(this);
        linearLayoutAttendees.setOnClickListener(this);
        linearLayoutNotes.setOnClickListener(this);
        linearLayoutMultipleMileage.setOnClickListener(this);
        imageViewRight.setOnClickListener(this);
        imageViewMore.setOnClickListener(this);
        button_approve.setOnClickListener(this);
        button_reject.setOnClickListener(this);
        imageViewCloseButton.setOnClickListener(this);
        relativeOverlayScreenExpense.setOnClickListener(this);
    }

    private void loadData() {
        ExpenseDetailAsyncTask expenseDetailAsyncTask = new ExpenseDetailAsyncTask();
        expenseDetailAsyncTask.execute();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(EXPENSE_INFO_KEY, expenseInfo);
        outState.putParcelable(EXPENSE_DETAIL_KEY, expenseDetail);
        outState.putInt(SELECTED_TAB_INDEX_KEY, currentSelectedIndex);
        super.onSaveInstanceState(outState);
    }

    private void restoreData(Bundle savedInstanceState) {
        expenseInfo = savedInstanceState.getParcelable(EXPENSE_INFO_KEY);
        expenseDetail = savedInstanceState.getParcelable(EXPENSE_DETAIL_KEY);
        currentSelectedIndex = savedInstanceState.getInt(SELECTED_TAB_INDEX_KEY);
        populateView();
        currentSelected = middleTabs.get(currentSelectedIndex);
        if (currentSelected != null)
            currentSelected.setSelected(true);
    }

    private void setPolicyViolationTabAnimation() {
        //Setting blink animation on Policy Violation tab
        AlphaAnimation blinkAnimation = new AlphaAnimation(.85f, 0); // Change alpha from .85 visible to invisible
        blinkAnimation.setDuration(300); // duration - half a second
        blinkAnimation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
        blinkAnimation.setRepeatCount(Animation.INFINITE); // Repeat animation infinitely
        blinkAnimation.setRepeatMode(Animation.REVERSE);
        imageViewPolicyViolationBlink.setAnimation(blinkAnimation);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linearLayoutProjectDetail: {
                handleProjectDetailClick();
                break;
            }

            case R.id.linearLayoutPolicyViolation: {
                handlePolicyViolationClick();
                break;
            }

            case R.id.linearLayoutReceipt: {
                handleReceiptClick();
                break;
            }

            case R.id.linearLayoutAttendees: {
                handleAttendeesClick();
                break;
            }

            case R.id.linearLayoutNotes: {
                handleNotesClick();
                break;
            }

            case R.id.linearLayoutMultipleMileage: {
                handleMultipleMileageClick();
                break;
            }

            case R.id.imageViewRight: {
                handleMultipleCategoryList();
                break;
            }

            case R.id.imageViewMore: {
                handleMore();
                break;
            }
            case R.id.button_approve: {
                if (DeviceManager.isOnline()) {
                    // setNoteAsPerRejectionSettingRule();
                    showAcceptDialog();
                } else {
                    enableNetwork();
                }

                break;
            }
            case R.id.button_reject: {
                if (DeviceManager.isOnline()) {
                    // setNoteAsPerRejectionSettingRule();
                    showRejectionDialog();
                } else {
                    enableNetwork();
                }

                break;
            }
            case R.id.imageViewCloseButton:
                handleOverlayScreen();
                break;
            case R.id.relativeOverlayScreenExpense:
                handleOverlayScreen();
                break;

        }
    }

    void showAcceptDialog() {
        DialogFragment newFragment = ApproverAcceptDialog.newInstance(R.string.approval_accept_dialog_title, this);
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    void showRejectionDialog() {
        DialogFragment newFragment = RejectionReasonDialog.newInstance(rejectionReasonBackupList, this);
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    private void handleProjectDetailClick() {
        if (currentSelected != linearLayoutProjectDetail) {
            unselectSelectedItem();
            currentSelected = linearLayoutProjectDetail;

            Fragment projectDetailFragment = ProjectDetailFragment.getInstance(expenseDetail.getNotes().getPreApprovedDetails(), expenseDetail.getProjectDetails(), expenseDetail.getAdditionalDetails(), expenseDetail.getPaymentType(), expenseDetail.getBusinessPurpose(), expenseInfo.getExpenseDetailID(), expenseInfo.getExpenseHeaderID());
            selectItem(projectDetailFragment);
        }
    }

    private void unselectSelectedItem() {
        try {
            if (currentSelected != null)
                currentSelected.setSelected(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void selectItem(Fragment fragment) {
        currentSelected.setSelected(true);
        currentSelectedIndex = middleTabs.indexOf(currentSelected);
        //getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutDetail, fragment).commit(); //state loss on android 7.1.1 versions
        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutDetail, fragment).commitAllowingStateLoss();
    }

    private void handlePolicyViolationClick() {
        if (currentSelected != linearLayoutPolicyViolation) {
            unselectSelectedItem();
            currentSelected = linearLayoutPolicyViolation;

            Fragment policyViolationFragment = PolicyViolationFragment.getInstance(expenseInfo.getPolicyBreech(), isrejected);
            selectItem(policyViolationFragment);
        }
    }

    private void handleReceiptClick() {
        if (currentSelected != linearLayoutReceipt) {
            unselectSelectedItem();
            currentSelected = linearLayoutReceipt;

            Fragment receiptFragment = ReceiptFragment.getInstance(expenseDetail.getProjectDetails(), expenseDetail.getReceipt(), expenseInfo.getExpenseDetailID(), expenseInfo.getExpenseHeaderID());
            selectItem(receiptFragment);

        }
    }

    private void handleAttendeesClick() {
        if (currentSelected != linearLayoutAttendees) {
            unselectSelectedItem();
            currentSelected = linearLayoutAttendees;

            Fragment attendeesFragment = AttendeesFragment.getInstance(expenseDetail.getAttendees());
            selectItem(attendeesFragment);
        }
    }

    private void handleNotesClick() {
        if (currentSelected != linearLayoutNotes) {
            unselectSelectedItem();
            currentSelected = linearLayoutNotes;

            Fragment notesFragment = NotesFragment.getInstance(expenseDetail.getNotes(), expenseInfo.getPolicyBreech().getNotes());
            selectItem(notesFragment);

        }
    }

    private void handleMultipleMileageClick() {
        if (currentSelected != linearLayoutMultipleMileage) {
            unselectSelectedItem();
            currentSelected = linearLayoutMultipleMileage;

            MultipleMileage mileage = expenseDetail.getMileage();

            Fragment multipleMileageFragment = MultipleMileageFragment.getInstance(mileage.getMultipleMileageItems(), mileage.getVehicleID());
            selectItem(multipleMileageFragment);
        }
    }

    private void handleMultipleCategoryList() {
        UserInfo userInfo = CachingManager.getUserInfo();
        MultiCategoryExpenseParam expenseRequest = expenseMultipleCategoryParam(expenseInfo.getExpenseHeaderID(), expenseInfo.getExpenseDetailID(), userInfo.getCompanyId(), "0", expenseDetail.getDate());
        Intent intent = new Intent(this, MultipleCategoryListActivity.class);
        intent.putExtra(AppConstant.KEY_EXPENSE_DETAIL, expenseDetail);
        intent.putExtra(AppConstant.KEY_MULTIPLE_CATEGORY_EXPENSE, expenseRequest);
        intent.putExtra(AppConstant.KEY_MULTIPLE_CATEGORY_EXPENSE, expenseRequest);
        intent.putExtra(AppConstant.KEY_CLAIM_NAME, claimName);
        intent.putExtra(AppConstant.KEY_CLAIM_TYPE, claimType);
        startActivity(intent);
    }

    private static MultiCategoryExpenseParam expenseMultipleCategoryParam(String str_expenseHeaderID, String str_expenseDetailID, String str_companyId, String str_isMultiPayment, String str_date) {
        MultiCategoryExpenseParam expenseMultiCategory = new MultiCategoryExpenseParam();
        UserInfo userInfo = CachingManager.getUserInfo();
        if (userInfo != null) {
            expenseMultiCategory.setExpenseHeaderID(str_expenseHeaderID);
            expenseMultiCategory.setExpenseDetailID(str_expenseDetailID);
            expenseMultiCategory.setCompanyId(str_companyId);
            expenseMultiCategory.setIsMultiPayment(str_isMultiPayment);
            expenseMultiCategory.setDate(str_date);
        }
        return expenseMultiCategory;
    }

    private void handleMore() {
        if (expenseDetail != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            DynamicDetailsDialog dynamicDetailsDialog = DynamicDetailsDialog.getInstance(expenseDetail.getDynamicDetails());
            dynamicDetailsDialog.show(fragmentTransaction, "Dialog");
        }
    }

    @Override
    public void approveClickOk(String approveNote, String counterApproveNote) {
        str_rejectionId = "0";
        str_rejectionNote = null;
        str_approveNote = approveNote;
        str_acounterApproveNote = counterApproveNote;

        isRejectedStatus = false;
        if (DeviceManager.isOnline()) {
            approveRejectAsync();
        } else {
            enableNetwork();
        }
    }

    @Override
    public void rejectClickOk(String rejectionNote, String rejectionId) {
        str_rejectionNote = rejectionNote;
        str_rejectionId = rejectionId;
        str_approveNote = null;
        str_acounterApproveNote = null;

        isRejectedStatus = true;
        if (DeviceManager.isOnline()) {
            approveRejectAsync();
        } else {
            enableNetwork();
        }
    }

    @Override
    public void onRetryButtonClick(int type) {
        if (DeviceManager.isOnline()) {
            loadData();
        } else {
            enableNetwork();
        }
    }

    @Override
    public void onCancelButtonClick() {
    }

    /***
     * ExpenseDetail AsyncTask
     */
    private class ExpenseDetailAsyncTask extends AsyncTask<Void, Void, ExpenseDetail> {
        private Exception exception = null;
        private Dialog dialog = null;

        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(ExpenseDetailActivity.this);
        }

        @Override
        protected ExpenseDetail doInBackground(Void... params) {
            ExpenseDetail expenseDetail = null;

            try {
                expenseDetail = ExpenseDetailManager.getExpenseDetail(expenseInfo.getExpenseDetailID(), expenseInfo.getExpenseHeaderID());
            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return expenseDetail;
        }

        @Override
        protected void onPostExecute(ExpenseDetail expenseDetail) {
            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);

            if (exception == null) {
                //OnSucess
                onSuccessGetExpenseDetail(expenseDetail);
            } else if (exception instanceof EODBusinessException) {
                //OnFail
                onFailGetExpenseDetail((EODException) exception);
            } else if (exception instanceof EODException) {
                //OnError
                onErrorGetExpenseDetail(exception);
            }
        }
    }

    private void onSuccessGetExpenseDetail(ExpenseDetail expenseDetailData) {
        expenseDetail = expenseDetailData;
        if (expenseDetail != null) {
            LinearLayout currentSelected = populateView();
            if (currentSelected != null) {
                onClick(currentSelected);
            }
        }
    }

    private void onFailGetExpenseDetail(EODException exception) {
//        showAlertDialog(getResources().getString(R.string.text_error));
        // showResponseErrorAlertDialog(false, null, null, null);
        showErrorAlertDialog(true, null, null, null, 0);
    }

    private void onErrorGetExpenseDetail(Exception exception) {
//        showAlertDialog(getResources().getString(R.string.text_error));
        showErrorAlertDialog(true, null, null, null, 0);
        // showResponseErrorAlertDialog(false, null, null, null);
    }

    void showErrorAlertDialog(boolean neutralButton, String message, String title, String okButtonTitle, int type) {
        applicationErrorAlertDialog = new ApplicationErrorAlertDialog().getInstance(this, message, title, okButtonTitle, type);
        applicationErrorAlertDialog.show(getSupportFragmentManager(), "dialog");
    }

    /***
     * ApproveReject AsyncTask
     */
    private void approveRejectAsync() {
        ApprovalRejectedExpenseContainer approveRejectExpenseObject = getSelectedExpenseList();
        new ApproveRejectedExpensesAsyncTask(approveRejectExpenseObject).execute();
    }

    public class ApproveRejectedExpensesAsyncTask extends AsyncTask<Void, Void, ApproveReject> {
        private final ApprovalRejectedExpenseContainer expenseList;
        private Dialog dialog = null;
        private Exception exception = null;
        ApproveReject response = null;

        public ApproveRejectedExpensesAsyncTask(ApprovalRejectedExpenseContainer expenseList) {
            this.expenseList = expenseList;
        }

        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(ExpenseDetailActivity.this);
        }

        @Override
        protected ApproveReject doInBackground(Void... voids) {
            try {
                response = ExpenseDetailManager.ApprovalRejectedExpenseRequest(expenseList);
            } catch (Exception eodException) {
                this.exception = eodException;
            }

            return response;
        }

        @Override
        protected void onPostExecute(ApproveReject response) {
            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);
            if (exception == null) {
                //OnSucess
                onSuccessGetApprovedRejectExpenses(response);
            } else if (exception instanceof EODBusinessException) {
                //OnFail
                onFailGetApprovedRejectExpenses((EODException) exception);
            } else if (exception instanceof EODException) {
                //OnError
                onErrorGetApprovedRejectExpenses(exception);
            }
        }
    }

    private void onSuccessGetApprovedRejectExpenses(ApproveReject response) {
        if (response != null) {

            boolean alertStatusN = false;
            boolean alertStatusZ = false;
            ArrayList<LstApproverRejectItem> approveRejects = response.getLstApproverReject();
            for (LstApproverRejectItem lstApproverRejectItem : approveRejects) {

                if (lstApproverRejectItem.getStrApprovalAllowed().equalsIgnoreCase("N")) {
                    alertStatusN = true;
                    break;
                } else if (lstApproverRejectItem.getStrApprovalAllowed().equalsIgnoreCase("Z")) {
                    alertStatusZ = true;
                    break;
                }
            }

            if (alertStatusN) {
                showAlertDialog1();
            } else if (alertStatusZ) {
                showAlertDialogZ();
            } else {
                SharePreference.putBoolean(getApplicationContext(), AppConstant.BACK_EXPENSE_DETAIL, true);
                SharePreference.putBoolean(getApplicationContext(), AppConstant.BACK_EXPENSE_DETAIL_LIST, true);
                SharePreference.putBoolean(getApplicationContext(), AppConstant.BACK_CLAIM_LIST, true);
                finish();
            }

        } else {
            // finish();
        }
    }

    private void showAlertDialog1() {
        expenseAlertDialog = new ExpenseAlertDialog().getInstance(this, true, getString(R.string.alert_messageN), getString(R.string.approval_accept_dialog_alert_title), getString(R.string.button_ok));
        expenseAlertDialog.show(getSupportFragmentManager(), "dialog");
    }

    private void showAlertDialogZ() {
        expenseAlertDialog = new ExpenseAlertDialog().getInstance(this, true, getString(R.string.alert_messageZ), getString(R.string.approval_accept_dialog_alert_title), getString(R.string.button_ok));
        expenseAlertDialog.show(getSupportFragmentManager(), "dialog");
    }

    private void onFailGetApprovedRejectExpenses(EODException exception) {
        showAlertDialog(getResources().getString(R.string.text_error));
        //  showResponseErrorAlertDialog(false, null, null, null);
    }

    private void onErrorGetApprovedRejectExpenses(Exception exception) {
        showAlertDialog(getResources().getString(R.string.text_error));
        // showResponseErrorAlertDialog(false, null, null, null);
    }

    private ApprovalRejectedExpenseContainer getSelectedExpenseList() {
        ApprovalRejectedExpenseContainer requestContainer = new ApprovalRejectedExpenseContainer();
        ApprovalRejectedExpenseRequest request = new ApprovalRejectedExpenseRequest();
        ArrayList<ApprovalRejectedExpenseRequest> arrayRequest = new ArrayList<ApprovalRejectedExpenseRequest>();
        ApprovalRejectedExpensesRequestData data = new ApprovalRejectedExpensesRequestData();

        UserInfo userInfo = CachingManager.getUserInfo();
        if (userInfo != null) {
            request.setExpenseDetailId(expenseInfo.getExpenseDetailID());//DetailID
            request.setExpenseHeaderId(expenseInfo.getExpenseHeaderID());//ClaimID
            request.setRejectionReasonID(str_rejectionId);//RejectionReasonID
            request.setRejectionReason(str_rejectionNote);//RejectionReason
            if (isRejectedStatus) {//if rejected
                request.setStatus("R");
            } else {//if approve
                if (userInfo.isApprover()) {
                    request.setStatus("M");//Status
                } else {
                    request.setStatus("B");//Status
                }
            }

            request.setEntryBy(userInfo.getResourceId());//EntryBy
            request.setCompanyID(userInfo.getCompanyId());//CompanyID

            if (expenseInfo.getPolicyBreech().getStatus().toString().equals("O")) {

                if (str_approveNote != null) {
                    request.setApproverNote("Approver – (" + userInfo.getDisplayName() + ")#" + str_approveNote);//ApproverNote
                } else {
                    request.setApproverNote("");//ApproverNote
                }

                request.setCounterApproverNote("");//CounterApproverNote


            } else {
                if (str_acounterApproveNote != null) {
                    request.setCounterApproverNote("Counter Approver – (" + userInfo.getDisplayName() + ")#" + str_acounterApproveNote);//CounterApproverNote
                } else {
                    request.setCounterApproverNote("");//CounterApproverNote
                }
                request.setApproverNote("");//ApproverNote
            }

            try {
                if (expenseDetail != null) {
                    for (ProjectDetailsItem projectDetailItem : expenseDetail.getProjectDetails()) {
                        /*if (projectDetailItem.getReceiptCount() > 0) {
                            request.setReceiptprovided("Y");//Receiptprovided
                        } else {
                            request.setReceiptprovided("N");//Receiptprovided
                        }*/
                    }
                }
            } catch (Exception e) {
                request.setReceiptprovided("N");
            }


        }

        arrayRequest.add(request);
        data.setRequests(arrayRequest);
        requestContainer.setRequestData(data);

        return requestContainer;
    }

    private LinearLayout populateView() {
        /*Multiple expense arrow*/
        try {
            if (expenseDetail.getBaseCategoryId() == 27) {
                imageViewRight.setVisibility(View.VISIBLE);
                linear_multipleCategoryList.setVisibility(View.VISIBLE);
            } else {
                imageViewRight.setVisibility(View.GONE);
                linear_multipleCategoryList.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        /*Set amount*/
        if (!AppUtil.isStringEmpty(expenseDetail.getUniqueId()))
            textViewId.setText(getResources().getString(R.string.approval_expense_detail_unique_id, expenseDetail.getUniqueId()));
        textViewAmount.setText(new DecimalFormat("0.00").format(Double.parseDouble(expenseDetail.getAmount())));

        /*Set date*/
        long date = AppUtil.getLongDate(AppConstant.FORMAT_DATE_WEB_SERVICE, expenseDetail.getDate());
        String formattedDate = AppUtil.getFormattedDateString(AppConstant.FORMAT_DATE_DISPLAY, date);
        textViewDate.setText(formattedDate);

        /*Set userName*/
        textViewUserName.setText(claimName);

        /*Set expense category name*/
        textViewCategory.setText(expenseInfo.getCategoryName());
        int resourceId = CachingManager.getCategoryResourceId(expenseDetail.getBaseCategoryId());
        imageViewExpenseCategoryIcon.setImageResource(resourceId);

        /*More details View dialog*/
        ArrayList<DynamicDetail> dynamicDetails = expenseDetail.getDynamicDetails();
        imageViewMore.setVisibility((!AppUtil.isCollectionEmpty(dynamicDetails)) ? View.VISIBLE : View.GONE);

        /*Mileage Tab*/
        LinearLayout currentSelected = null;
        middleTabs = new ArrayList<>();
        MultipleMileage multipleMileage = expenseDetail.getMileage();
        if (multipleMileage != null) {
            linearLayoutMultipleMileage.setVisibility(View.VISIBLE);
            currentSelected = linearLayoutMultipleMileage;
            if (multipleMileage.getMultipleMileageItems().size() > 1) {
                txt_mileageStatus.setText(getResources().getString(R.string.approval_expense_detail_multiple_mileage));
            } else {
                txt_mileageStatus.setText(getResources().getString(R.string.approval_expense_detail_mileage));
            }
            middleTabs.add(currentSelected);
        } else
            linearLayoutMultipleMileage.setVisibility(View.GONE);

        /*Note Tab*/
        Notes notes = expenseDetail.getNotes();
        if (notes != null && (!notes.getCcNotes().equals("") || !notes.getEnteredNotes().equals("") || !notes.getMobileNotes().equals("") || expenseInfo.getPolicyBreech().getNotes().size() > 0)) {
            linearLayoutNotes.setVisibility(View.VISIBLE);
            currentSelected = linearLayoutNotes;
            middleTabs.add(currentSelected);
        } else
            linearLayoutNotes.setVisibility(View.GONE);

        /*Attendees Tab*/
        ArrayList<AttendeesItem> attendeesItems = expenseDetail.getAttendees();
        if (!AppUtil.isCollectionEmpty(attendeesItems)) {
            linearLayoutAttendees.setVisibility(View.VISIBLE);
            currentSelected = linearLayoutAttendees;
            middleTabs.add(currentSelected);
            textViewAttendeesType.setText(String.valueOf(attendeesItems.size()));
        } else
            linearLayoutAttendees.setVisibility(View.GONE);

        /*PolicyBreach Tab*/
        PolicyBreachModel policyBreachModel = expenseInfo.getPolicyBreech();
        if (policyBreachModel != null) {
            if (policyBreachModel.isDailyCap() || policyBreachModel.isMaxSpendBreach()) {
                linearLayoutPolicyViolation.setVisibility(View.VISIBLE);
                currentSelected = linearLayoutPolicyViolation;
                middleTabs.add(currentSelected);
            } else
                linearLayoutPolicyViolation.setVisibility(View.GONE);
        } else
            linearLayoutPolicyViolation.setVisibility(View.GONE);

        /*Project Details Tab*/
        ArrayList<ProjectDetailsItem> projectDetailItems = expenseDetail.getProjectDetails();
        if (!AppUtil.isCollectionEmpty(projectDetailItems)) {
            linearLayoutProjectDetail.setVisibility(View.VISIBLE);
            currentSelected = linearLayoutProjectDetail;
            middleTabs.add(currentSelected);
        } else
            linearLayoutProjectDetail.setVisibility(View.GONE);

        /*Receipt Tab*/
        ReceiptItem receipt = expenseDetail.getReceipt();
        if (receipt != null) {
            linearLayoutReceipt.setVisibility(View.VISIBLE);
            if (Integer.parseInt(receipt.getCount()) == 0) {
                textViewCount.setVisibility(View.GONE);
            } else {
                textViewCount.setVisibility(View.VISIBLE);
                textViewCount.setText(receipt.getCount());
            }
        } else {
            linearLayoutReceipt.setVisibility(View.GONE);
        }

        return currentSelected;
    }

    private void showAlertDialog(String msg) {
        applicationAlertDialog = new ApplicationAlertDialog().getInstance(this, true, getString(R.string.text_error_processing), msg, getString(R.string.button_ok), TYPE_ALERT);
        applicationAlertDialog.show(getSupportFragmentManager(), "dialog");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Get Menu Inflater
        MenuInflater inflater = getMenuInflater();

        //Inflate Menu
        inflater.inflate(R.menu.help_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuItemId = item.getItemId();
        switch (menuItemId) {
            case R.id.menuHelp:
                //Handle OverlayScreen
                handleOverlayScreen();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    //OverLay screen
    void handleOverlayScreen() {
        relativeOverlayScreenExpense.setVisibility(relativeOverlayScreenExpense.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onPositiveButtonClick(int type) {
        switch (type) {
            case 0:
                //
                break;
            case 1:
                //
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (relativeOverlayScreenExpense.getVisibility() == View.GONE) {
            super.onBackPressed();
        }
        SharePreference.putBoolean(getApplicationContext(), AppConstant.BACK_EXPENSE_DETAIL, true);
    }

    @Override
    public void onPositiveButtonClick() {
        SharePreference.putBoolean(getApplicationContext(), AppConstant.BACK_EXPENSE_DETAIL, true);
        finish();
    }

    @Override
    public void onNegativeButtonClick() {
    }


}
