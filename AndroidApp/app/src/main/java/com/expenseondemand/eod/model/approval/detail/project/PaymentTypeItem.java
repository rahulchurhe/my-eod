package com.expenseondemand.eod.model.approval.detail.project;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ITDIVISION\maximess087 on 22/2/17.
 */

public class PaymentTypeItem implements Parcelable {
    private String LabelName;
    private String Value;

    public PaymentTypeItem(){}
    public PaymentTypeItem(Parcel in) {
        LabelName = in.readString();
        Value = in.readString();
    }

    public static final Creator<PaymentTypeItem> CREATOR = new Creator<PaymentTypeItem>() {
        @Override
        public PaymentTypeItem createFromParcel(Parcel in) {
            return new PaymentTypeItem(in);
        }

        @Override
        public PaymentTypeItem[] newArray(int size) {
            return new PaymentTypeItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(LabelName);
        parcel.writeString(Value);
    }


    public String getLabelName() {
        return LabelName;
    }

    public void setLabelName(String labelName) {
        LabelName = labelName;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
