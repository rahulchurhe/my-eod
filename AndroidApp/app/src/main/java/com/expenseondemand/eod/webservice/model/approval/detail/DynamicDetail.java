package com.expenseondemand.eod.webservice.model.approval.detail;

/**
 * Created by Ramit Yadav on 20-10-2016.
 */

public class DynamicDetail
{
    private String labelName;
    private String value;

    public String getLabelName()
    {
        return labelName;
    }

    public void setLabelName(String labelName)
    {
        this.labelName = labelName;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }
}
