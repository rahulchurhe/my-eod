package com.expenseondemand.eod.webservice.model.approval.detail;

import com.expenseondemand.eod.model.approval.expense.multiplecategory.MultiCategoryExpenseData;
import com.expenseondemand.eod.webservice.model.MasterResponse;

import java.util.ArrayList;

/**
 * Created by ITDIVISION\maximess087 on 16/1/17.
 */

public class ExpenseMultipleCategoryResponse extends MasterResponse {
     ArrayList<MultiCategoryExpenseData> data;

    public ArrayList<MultiCategoryExpenseData> getData() {
        return data;
    }

    public void setData(ArrayList<MultiCategoryExpenseData> data) {
        this.data = data;
    }
}
