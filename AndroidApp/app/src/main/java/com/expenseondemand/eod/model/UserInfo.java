package com.expenseondemand.eod.model;

public class UserInfo 
{
	private UserCredential userCredential;
	private String userId;
	private String token;
	private int defaultCategoryId;
	private String defaultCurrencyCode;
	private String resourceId;
	private String costCenterId;
	private String companyLogo;
	private String branchId;
	private String companyId;
	private String departmentId;
	private String costCenterName;
	private String companyName;
	private String displayName;
	private boolean isApprover;
	private boolean isFinance;
	private boolean isBoss;
	private boolean isDelegate;
	private boolean DeputyAssigned;
	private String gradeId;

	
	public UserCredential getUserCredential() 
	{
		return userCredential;
	}
	
	public void setUserCredential(UserCredential userCredential)
	{
		this.userCredential = userCredential;
	}



	/*public boolean isDelegate()
	{
		return isDelegate;
	}

	public void setDelegate(boolean delegate)
	{
		isDelegate = delegate;
	}*/
	
	public String getUserId()
	{
		return userId;
	}
	
	public void setUserId(String userId) 
	{
		this.userId = userId;
	}
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getDefaultCategoryId() 
	{
		return defaultCategoryId;
	}
	
	public void setDefaultCategoryId(int defaultCategoryId)
	{
		this.defaultCategoryId = defaultCategoryId;
	}
	
	public String getDefaultCurrencyCode() 
	{
		return defaultCurrencyCode;
	}
	
	public void setDefaultCurrencyCode(String defaultCurrencyCode) 
	{
		this.defaultCurrencyCode = defaultCurrencyCode;
	}

	public String getResourceId()
	{
		return resourceId;
	}

	public void setResourceId(String resourceId)
	{
		this.resourceId = resourceId;
	}

	public String getCostCenterId()
	{
		return costCenterId;
	}

	public void setCostCenterId(String costCenterId)
	{
		this.costCenterId = costCenterId;
	}

	public String getCompanyLogo()
	{
		return companyLogo;
	}

	public void setCompanyLogo(String companyLogo)
	{
		this.companyLogo = companyLogo;
	}

	public String getBranchId()
	{
		return branchId;
	}

	public void setBranchId(String branchId)
	{
		this.branchId = branchId;
	}

	public String getCompanyId()
	{
		return companyId;
	}

	public void setCompanyId(String companyId)
	{
		this.companyId = companyId;
	}

	public String getDepartmentId()
	{
		return departmentId;
	}

	public void setDepartmentId(String departmentId)
	{
		this.departmentId = departmentId;
	}

	public String getCostCenterName()
	{
		return costCenterName;
	}

	public void setCostCenterName(String costCenterName)
	{
		this.costCenterName = costCenterName;
	}

	public String getCompanyName()
	{
		return companyName;
	}

	public void setCompanyName(String companyName)
	{
		this.companyName = companyName;
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

	public boolean isApprover()
	{
		return isApprover;
	}

	public void setApprover(boolean approver)
	{
		isApprover = approver;
	}

	public boolean isFinance()
	{
		return isFinance;
	}

	public void setFinance(boolean finance)
	{
		isFinance = finance;
	}

	public boolean isBoss()
	{
		return isBoss;
	}

	public void setBoss(boolean boss)
	{
		isBoss = boss;
	}

	public boolean isDelegate()
	{
		return isDelegate;
	}

	public void setDelegate(boolean delegate)
	{
		isDelegate = delegate;
	}

	public String getGradeId()
	{
		return gradeId;
	}

	public void setGradeId(String gradeId)
	{
		this.gradeId = gradeId;
	}

	public boolean isDeputyAssigned() {
		return DeputyAssigned;
	}

	public void setDeputyAssigned(boolean deputyAssigned) {
		DeputyAssigned = deputyAssigned;
	}


	/*public boolean isApprover()
	{
		return isApprover;
	}

	public void setApprover(boolean approver)
	{
		isApprover = approver;
	}*/
}
