package com.expenseondemand.eod.webservice.model.approval.detail.expense.multiplecategory;

import com.expenseondemand.eod.webservice.model.MasterRequest;
/**
 * Created by ITDIVISION\maximess087 on 18/1/17.
 */

public class MultipleCategoryExpenseDetailRequest extends MasterRequest {
    private String ExpenseHeaderID;
    private String ExpenseDetailID;
    private String ExpenseItemizationID;
    private String Date;

    public String getExpenseHeaderID() {
        return ExpenseHeaderID;
    }

    public void setExpenseHeaderID(String expenseHeaderID) {
        ExpenseHeaderID = expenseHeaderID;
    }

    public String getExpenseDetailID() {
        return ExpenseDetailID;
    }

    public void setExpenseDetailID(String expenseDetailID) {
        ExpenseDetailID = expenseDetailID;
    }

    public String getExpenseItemizationID() {
        return ExpenseItemizationID;
    }

    public void setExpenseItemizationID(String expenseItemizationID) {
        ExpenseItemizationID = expenseItemizationID;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
}
