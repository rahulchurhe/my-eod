package com.expenseondemand.eod.screen.approval.detail.attendees;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.model.approval.detail.attendees.AttendeesItem;
import com.expenseondemand.eod.util.AppUtil;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 18-09-2016.
 */
public class AttendeesAdapter extends RecyclerView.Adapter
{
    private Context context;
    private ArrayList<AttendeesItem> attendeesData;

    public AttendeesAdapter(Context context, ArrayList<AttendeesItem> attendeesData)
    {
        this.context = context;
        this.attendeesData = attendeesData;
    }

    @Override
    public int getItemCount()
    {
        return AppUtil.isCollectionEmpty(attendeesData) ? 0 : attendeesData.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.attendees_item, parent, false);

        return new ProjectDetailItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        AttendeesItem attendeesItem = attendeesData.get(position);

        ProjectDetailItemHolder projectDetailItemHolder = (ProjectDetailItemHolder)holder;
        projectDetailItemHolder.textViewCount.setText((position+1)+".");
        projectDetailItemHolder.textViewCost.setText(new DecimalFormat("0.00").format(Double.parseDouble(attendeesItem.getCost())));
        projectDetailItemHolder.textViewName.setText(attendeesItem.getName());
        projectDetailItemHolder.textViewAttendeesType.setText(attendeesItem.getType());
        String costCenter = context.getResources().getString(R.string.approval_expense_detail_attendees_cost_center, attendeesItem.getLevel(), attendeesItem.getValue());
        projectDetailItemHolder.textViewCostCenter.setText(costCenter);
    }

    protected static class ProjectDetailItemHolder extends RecyclerView.ViewHolder
    {
        TextView textViewCount;
        TextView textViewCost;
        TextView textViewName;
        TextView textViewAttendeesType;
        TextView textViewCostCenter;

        public ProjectDetailItemHolder(View itemView)
        {
            super(itemView);

            textViewCount = (TextView) itemView.findViewById(R.id.textViewCount);
            textViewCost = (TextView) itemView.findViewById(R.id.textViewCost);
            textViewName = (TextView) itemView.findViewById(R.id.textViewName);
            textViewAttendeesType = (TextView) itemView.findViewById(R.id.textViewAttendeesType);
            textViewCostCenter = (TextView) itemView.findViewById(R.id.textViewCostCenter);
        }
    }
}
