package com.expenseondemand.eod.screen.approval.claim;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.activity.BaseActivity;
import com.expenseondemand.eod.dialogFragment.ApplicationNetworkDialog;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.model.Statistics;
import com.expenseondemand.eod.screen.approval.claim.AdvanceTab.AdvanceTabFragment;
import com.expenseondemand.eod.screen.approval.claim.ExpenseTab.ExpenseTabFragment;
import com.expenseondemand.eod.screen.home.HomeManager;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.UIUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ITDIVISION\maximess087 on 8/3/17.
 */


public class ClaimListActivity extends BaseActivity implements View.OnClickListener {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private RelativeLayout relativeOverlayScreenClaims;
    private TextView txt_expenses;

    @Override
    protected void onResume() {
        super.onResume();
        if (SharePreference.getBoolean(getApplicationContext(), AppConstant.BACK_CLAIM_LIST)) {
            SharePreference.putBoolean(getApplicationContext(), AppConstant.BACK_CLAIM_LIST, false);
            GetExpenseBudgetCount();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.approval_claim_list_activity_layout);
        handleToolBar(getResources().getString(R.string.home_item_approvals), R.color.approval_claim_list_toolbar, R.color.approval_claim_list_status_bar, true);

        GetExpenseBudgetCount();
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();


        //overlay close button
        ImageButton imageViewCloseButton = (ImageButton) findViewById(R.id.imageViewCloseButton);

        //overlay layout
        relativeOverlayScreenClaims = (RelativeLayout) findViewById(R.id.relativeOverlayScreenClaims);

        imageViewCloseButton.setOnClickListener(this);
        relativeOverlayScreenClaims.setOnClickListener(this);

    }

    public void GetExpenseBudgetCount() {
        StatisticsAsyncTask statisticsAsyncTask = new StatisticsAsyncTask();
        statisticsAsyncTask.execute();
    }

    //Statistics AsyncTask
    private class StatisticsAsyncTask extends AsyncTask<Void, Void, Statistics> {
        private Exception exception = null;
        private Dialog dialog = null;

        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(ClaimListActivity.this);
        }

        @Override
        protected Statistics doInBackground(Void... params) {
            Statistics statistics = null;

            try {

                statistics = HomeManager.getStatistics();
            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return statistics;
        }

        @Override
        protected void onPostExecute(Statistics statistics) {
            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);
            if (exception == null) {
                if (statistics != null) {
                    if (statistics.getPendingApprovalCount().equals(null) || statistics.getPendingApprovalCount().equals("0")) {
                        txt_expenses.setVisibility(View.GONE);
                    } else {
                        txt_expenses.setVisibility(View.VISIBLE);
                        txt_expenses.setText(statistics.getPendingApprovalCount());
                    }

                } else {
                    txt_expenses.setVisibility(View.GONE);
                }
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Get Menu Inflater
        MenuInflater inflater = getMenuInflater();

        //Inflate Menu
        inflater.inflate(R.menu.help_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuItemId = item.getItemId();

        switch (menuItemId) {
            case R.id.menuHelp:
                //Handle OverlayScreen
                handleOverlayScreen();

                break;

        }

        return super.onOptionsItemSelected(item);
    }

    private void handleOverlayScreen() {
        relativeOverlayScreenClaims.setVisibility(relativeOverlayScreenClaims.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onBackPressed() {
        if (relativeOverlayScreenClaims.getVisibility() == View.GONE) {
            super.onBackPressed();
        }

    }

    // Adding custom view to tab/
    private void setupTabIcons() {

        LinearLayout tabOne = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.custom_expense_tab, null);
        TextView txt_expensesTab = (TextView) tabOne.findViewById(R.id.txt_tab);
        txt_expensesTab.setText(getResources().getString(R.string.approval_claim_expense_tab_title));
        txt_expenses = (TextView) tabOne.findViewById(R.id.textViewCount);

        tabLayout.getTabAt(0).setCustomView(tabOne);

        LinearLayout tabTwo = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.custom_expense_tab, null);
        TextView txt_approvalTab = (TextView) tabTwo.findViewById(R.id.txt_tab);
        txt_approvalTab.setText(getResources().getString(R.string.approval_claim_advances_tab_title));
        TextView txt_approval = (TextView) tabTwo.findViewById(R.id.textViewCount);
        txt_approval.setVisibility(View.GONE);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

    }

    /***
     * Adding fragments to ViewPager
     *
     * @param viewPager
     */
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new ExpenseTabFragment(), "ONE");
        adapter.addFrag(new AdvanceTabFragment(), "TWO");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewCloseButton: {
                handleOverlayScreen();
                break;
            }

            case R.id.relativeOverlayScreenClaims:
                handleOverlayScreen();
                break;
        }
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
