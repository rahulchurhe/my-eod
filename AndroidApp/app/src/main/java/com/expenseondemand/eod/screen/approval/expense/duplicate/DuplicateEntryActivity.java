package com.expenseondemand.eod.screen.approval.expense.duplicate;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.activity.BaseActivity;
import com.expenseondemand.eod.util.AppConstant;

import org.json.JSONArray;
import org.json.JSONObject;


public class DuplicateEntryActivity extends BaseActivity {
    RecyclerView recyclerViewDuplicateEntry;
    DuplicateEntryAdapter duplicateEntryAdapter;
    public JSONArray jsonArrayDuplicateExpense;
    boolean blueStat=false;
    boolean redStat=false;
    private LinearLayout linear_red,linear_blue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_duplicate_entry);

        try {
            Intent intent = getIntent();
            String duplicateExpense = intent.getExtras().getString(AppConstant.KEY_PARENT_DUPLICATE_EXPENSE_ITEM);
            jsonArrayDuplicateExpense = new JSONArray(duplicateExpense);

            linear_red=(LinearLayout)findViewById(R.id.linear_red);
            linear_blue=(LinearLayout)findViewById(R.id.linear_blue);

            for(int i=0;i<jsonArrayDuplicateExpense.length();i++){
                JSONObject jsonObject=jsonArrayDuplicateExpense.getJSONObject(i);
                if(jsonObject.getString("ColourIndicator").equals("Blue")){
                    linear_blue.setVisibility(View.VISIBLE);
                    blueStat=true;
                }else if(jsonObject.getString("ColourIndicator").equals("Red")){
                    linear_red.setVisibility(View.VISIBLE);
                    redStat=true;
                }
            }

            if(blueStat && redStat){
                linear_blue.setVisibility(View.VISIBLE);
                linear_red.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {

        }
        initializeView();


    }

    private void initializeView() {

        //Handle Action Bar
        handleToolBar(getResources().getString(R.string.text_duplicate_entry), R.color.approval_claim_list_toolbar, R.color.approval_claim_list_status_bar, true);

        //Get Adapter
        duplicateEntryAdapter = new DuplicateEntryAdapter(DuplicateEntryActivity.this, jsonArrayDuplicateExpense);

        //Get List View
        recyclerViewDuplicateEntry = (RecyclerView) findViewById(R.id.recyclerViewDuplicateEntry);

        recyclerViewDuplicateEntry.setAdapter(duplicateEntryAdapter);
    }


}
