package com.expenseondemand.eod.model;

import java.util.Date;

import com.expenseondemand.eod.util.AppUtil;

public class SelectedDate
{
	Integer date;
	Integer month;
	Integer year;

	public Integer getDate() {
		return date;
	}

	public void setDate(Integer date) {
		this.date = date;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Date getDateObject()
	{
		Date dateObject = AppUtil.getDate(date, month, year);		
		return dateObject;
	}
}
