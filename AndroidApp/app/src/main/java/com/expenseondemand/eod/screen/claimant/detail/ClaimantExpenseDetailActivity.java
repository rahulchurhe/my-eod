package com.expenseondemand.eod.screen.claimant.detail;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.activity.BaseActivity;
import com.expenseondemand.eod.dialogFragment.ApplicationAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationErrorAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationNetworkDialog;
import com.expenseondemand.eod.exception.EODBusinessException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.AuthenticationManager;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.DbManager;
import com.expenseondemand.eod.manager.DeviceManager;
import com.expenseondemand.eod.manager.ExpenseManager;
import com.expenseondemand.eod.manager.ValidationManager;
import com.expenseondemand.eod.model.Expense;
import com.expenseondemand.eod.model.UserCredential;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.model.ValidationError;
import com.expenseondemand.eod.screen.claimant.create.CreateEditExpenseActivity;
import com.expenseondemand.eod.screen.claimant.upload.UploadExpenseActivity;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.util.UIUtil;

import java.io.File;
import java.util.ArrayList;

public class ClaimantExpenseDetailActivity extends BaseActivity implements OnClickListener, ApplicationAlertDialog.AlertDialogActionListener, ApplicationErrorAlertDialog.ErrorDialogActionListener {

    public static final int TYPE_LOGIN = 0;
    public static final int TYPE_DELETE = 1;
    public static final int TYPE_UPLOAD = 2;

    //Create List of Expense-Ids to Delete
    final ArrayList<Integer> expenseIdList = new ArrayList<Integer>();
    private TextView textViewExpenseAmountValue;
    private TextView textViewExpenseCategoryValue;
    private TextView textViewExpenseDateValue;
    private TextView textViewExpenseNotesValue;
    private ImageView imageViewExpenseReciept;
    private TextView textViewExpenseReceiptTitle;
    private LinearLayout includeBottomActionLayout;

    //set instance alert dialog
    private ApplicationAlertDialog applicationAlertDialog;
    private ApplicationErrorAlertDialog applicationErrorAlertDialog;
    private ApplicationNetworkDialog applicationNetworkDialogl;


    private Expense currentExpense;
    //	private Bitmap selectedReceiptBitmap;
    private boolean isAuthenticationRetried;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expense_detail_activity_layout);

        //Initialize View
        initializeView();

        //Initialize Data
        initializeData();
    }

    //Initialize View
    private void initializeView() {
        //Get View References
        textViewExpenseAmountValue = (TextView) findViewById(R.id.textViewExpenseAmountValue);
        textViewExpenseCategoryValue = (TextView) findViewById(R.id.textViewExpenseCategoryValue);
        textViewExpenseDateValue = (TextView) findViewById(R.id.textViewExpenseDateValue);
        textViewExpenseNotesValue = (TextView) findViewById(R.id.textViewExpenseNotesValue);
        textViewExpenseReceiptTitle = (TextView) findViewById(R.id.textViewExpenseReceiptTitle);
        imageViewExpenseReciept = (ImageView) findViewById(R.id.imageViewExpenseReceipt);
        includeBottomActionLayout = (LinearLayout) findViewById(R.id.includeBottomActionLayout);

        //Delete Button
        ImageView imageViewDelete = (ImageView) findViewById(R.id.imageViewDelete);
        imageViewDelete.setOnClickListener(this);

        //Edit Button
        ImageView imageViewEdit = (ImageView) findViewById(R.id.imageViewEdit);
        imageViewEdit.setOnClickListener(this);

        //Upload Button
        ImageView imageViewUpload = (ImageView) findViewById(R.id.imageViewUpload);
        imageViewUpload.setOnClickListener(this);

        //Image View click
        imageViewExpenseReciept.setOnClickListener(this);

        //Handle toolbar
        handleToolBar(getResources().getString(R.string.text_expense_detail), R.color.approval_claim_list_toolbar, R.color.approval_claim_list_status_bar, true);

    }

    //Initialize Data
    private void initializeData() {
        //Get Intent
        Intent intent = getIntent();

        //Get Expense from Intent
        currentExpense = intent.getParcelableExtra(AppConstant.KEY_EXPENSE_OBJECT);

        if (currentExpense != null) {
            String expenseCategoryName = currentExpense.getExpenseCategoryName();
            String expenseAmount = currentExpense.getExpenseAmount();
            String expenseNotes = currentExpense.getExpenseNotes();
            int resourseId = CachingManager.getCategoryResourceId(currentExpense.getExpenseBaseCategoryId());

            long expenseDate = currentExpense.getExpenseDate();
            String dateString = AppUtil.getFormattedDateString(AppConstant.FORMAT_DATE_DISPLAY, expenseDate);

            int expenseId = currentExpense.getExpenseId();
            boolean hasReceipt = currentExpense.isHasReceipt();

            //Populate Data  on View
            textViewExpenseCategoryValue.setText(expenseCategoryName);
            textViewExpenseCategoryValue.setCompoundDrawablesWithIntrinsicBounds(resourseId, 0, 0, 0);
            textViewExpenseAmountValue.setText(expenseAmount);
            textViewExpenseDateValue.setText(dateString);
            textViewExpenseNotesValue.setText(expenseNotes);

            if (hasReceipt) {
                updateViewWithReceiptImage(currentExpense.getReceiptImage().getReceiptImageFileName());
            }
        }
    }


    @Override
    public void onClick(View view) {
        //Get View Id
        int viewId = view.getId();

        switch (viewId) {
            case R.id.imageViewDelete:
                handleDeleteExpense();
                break;

            case R.id.imageViewEdit:

                handleEditExpense();
                break;

            case R.id.imageViewUpload:

                handleUploadExpense();
                break;

            case R.id.imageViewExpenseReceipt:

                showAttachedReceipt();
                break;


        }
    }

    private void showAttachedReceipt() {
        //UIUtil.showAttachedReceiptDialog(MediaManager.getScaledDownBitmapFromFilePath(currentExpense.getReceiptImage().getReceiptImageFileName()),ClaimantExpenseDetailActivity.this);
        if (!AppUtil.isStringEmpty(currentExpense.getReceiptImage().getReceiptImageFileName())) {
            File imgFile = new File(currentExpense.getReceiptImage().getReceiptImageFileName());
            if (imgFile.exists()) {
                Bitmap selectedBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                UIUtil.showAttachedReceiptDialog(selectedBitmap, ClaimantExpenseDetailActivity.this);
            }
        }
    }

    //Handle Edit Expense
    private void handleEditExpense() {
        //Navigate to Edit Expense
        navigateToEditExpense(currentExpense);
    }

    //Handle Delete Expense
    private void handleDeleteExpense() {
        expenseIdList.add(currentExpense.getExpenseId());
        showAlertDialog(false, getString(R.string.text_delete_expense), null, getString(R.string.button_yes), getString(R.string.button_no), TYPE_DELETE);

    }

    private void showAlertDialog(boolean neutralButton, String message, String title, String okButtonTitle, String cancelButtonTitle, int type) {
        applicationAlertDialog = new ApplicationAlertDialog().getInstance(this, neutralButton, message, title, okButtonTitle, cancelButtonTitle, type);
        applicationAlertDialog.show(getSupportFragmentManager(), "dialog");
    }

    private void deleteExpense(ArrayList<Integer> expenseIdList) {
        //Async task
        DeleteExpenseAsyncTask asyncTask = new DeleteExpenseAsyncTask(expenseIdList);
        asyncTask.execute();
    }

    //Handle Upload Expense
    private void handleUploadExpense() {
        //Get Selected Expense List
        ArrayList<Expense> expenseList = getExpenseList();

        ValidationError validationError = ValidationManager.validateFutureExpenseUpload(expenseList);

        if (validationError != null) {
            //Show Alert Dialog
            showAlertDialog(true, validationError.getValidationMessage(), null, null, TYPE_UPLOAD);

        } else {
            //Check if Network Available
            if (DeviceManager.isOnline()) {
                UploadExpenseAsyncTask asyncTask = new UploadExpenseAsyncTask(expenseList);
                asyncTask.execute();
            } else {
                //Show Network Error Dialog
                enableNetwork();
            }
        }
    }

    private void updateViewWithReceiptImage(String imagePath) {
        if (!AppUtil.isStringEmpty(imagePath)) {

            File imgFile = new File(imagePath);
            if (imgFile.exists()) {
                imageViewExpenseReciept.setVisibility(View.VISIBLE);
                imageViewExpenseReciept.setImageURI(Uri.fromFile(imgFile));
                textViewExpenseReceiptTitle.setVisibility(ImageView.VISIBLE);
            }
        }
    }

    private ArrayList<Expense> getExpenseList() {
        ArrayList<Expense> expenseList = new ArrayList<Expense>();

        if (currentExpense != null) {
            expenseList.add(currentExpense);
        }

        return expenseList;
    }

    //Navigate to Create Expense
    private void navigateToEditExpense(Expense expense) {
        //Create Intent
        Intent intent = new Intent(this, CreateEditExpenseActivity.class);
        intent.putExtra(AppConstant.KEY_IS_EDIT_EXPENSE, true);
        intent.putExtra(AppConstant.KEY_EXPENSE_OBJECT, expense);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_forward, R.anim.slide_out_forward);
    }


    private void showUploadSuccessAlertDialog() {
        //Get Dialog
        showAlertDialog(true, getString(R.string.text_expense_item_uploaded), null, null, TYPE_UPLOAD);

    }


    //Delete Expense AsyncTask
    private class DeleteExpenseAsyncTask extends AsyncTask<Void, Void, Void> {
        private Dialog dialog = null;
        private Exception exception = null;

        ArrayList<Integer> expenseIdList = null;

        public DeleteExpenseAsyncTask(ArrayList<Integer> expenseIdList) {
            this.expenseIdList = expenseIdList;
        }

        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(ClaimantExpenseDetailActivity.this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                //Delete Expense
                ExpenseManager.deleteExpenses(expenseIdList);
            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);

            if (exception == null) {
                //On Success
                onSuccessDeleteExpense();
            } else {
                //On Fail
                onErrorDeleteExpense();
            }
        }
    }

    public void onSuccessDeleteExpense() {
        //Referesh List
        navigateToExpenseList();
    }

    public void onErrorDeleteExpense() {
        //Show Error Alert Dialog
        showErrorAlertDialog(null, null, null, TYPE_DELETE);
        //showResponseErrorAlertDialog(false, null, null, null);

    }

    //Navigate to Expense List
    protected void navigateToExpenseList() {
        Intent intent = new Intent(this, UploadExpenseActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_back, R.anim.slide_out_back);
    }

    //Upload Expense Async Task
    private class UploadExpenseAsyncTask extends AsyncTask<Void, Void, Void> {
        private Dialog dialog = null;
        private Exception exception = null;
        private ArrayList<Expense> uploadExpenseList = null;

        public UploadExpenseAsyncTask(ArrayList<Expense> expenseList) {
            this.uploadExpenseList = expenseList;
        }

        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(ClaimantExpenseDetailActivity.this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                //Iterate List and Upload one by one
                for (Expense expense : uploadExpenseList) {
                    //Upload Expense
                    ExpenseManager.uploadExpense(expense);
                }
            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);

            if (exception == null) {
                //On Success
                onSuccessUploadExpense(uploadExpenseList);
            } else {
                //On Fail
                onErrorUploadExpense();
            }
        }
    }

    public void onSuccessUploadExpense(ArrayList<Expense> uploadExpenseList) {
        //Shown Upload Success Dailog
        showUploadSuccessAlertDialog();
    }

    public void onErrorUploadExpense() {
        if (isAuthenticationRetried) //Already Retried
        {
            //Show Error Alert Dialog
            showErrorAlertDialog(null, null, null, TYPE_UPLOAD);
            //showResponseErrorAlertDialog(false, null, null, null);
        } else //Not yet Retried
        {
            //Authenticate User Again to obtain new Token
            authenticateUser();
        }
    }

    //Authenticate User
    private void authenticateUser() {
        //UserCredentials
        UserCredential userCredential = null;

        //Get UserInfo
        UserInfo userInfo = CachingManager.getUserInfo();

        if (userInfo != null) {
            //Get User Credentials
            userCredential = userInfo.getUserCredential();
        }

        //Autheticate User AsyncTask
        AuthenticateUserAsyncTask asyncTask = new AuthenticateUserAsyncTask();
        asyncTask.execute(userCredential);
    }

    //Authenticate User AsyncTask
    private class AuthenticateUserAsyncTask extends AsyncTask<UserCredential, Void, UserInfo> {
        private Dialog dialog = null;
        private Exception exception = null;

        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(ClaimantExpenseDetailActivity.this);
        }

        @Override
        protected UserInfo doInBackground(UserCredential... params) {
            UserInfo userInfo = null;

            try {
                //Authenticate User
                userInfo = AuthenticationManager.authenticateUser(params[0]);

                //Insert User Information In Database
                DbManager.saveUserInformation(userInfo);
            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return userInfo;
        }

        @Override
        protected void onPostExecute(UserInfo user) {
            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);

            if (exception == null) {
                //OnSucess
                onSuccessAuthenticateUser(user);
            } else if (exception instanceof EODBusinessException) {
                //OnFail
                onFailLogin((EODException) exception);
            } else if (exception instanceof EODException) {
                //OnError
                onErrorLogin(exception);
            }
        }
    }

    //OnSucess
    public void onSuccessAuthenticateUser(UserInfo user) {
        if (user != null) {
            //Retry Upload AsyncTask
            handleUploadExpense();

            //Set Flag True - Async task is retried
            isAuthenticationRetried = true;
        }
    }

    //OnFail
    public void onFailLogin(EODException exception) {
        //Show Error Alert Dialog
        showErrorAlertDialog(null, null, null, TYPE_LOGIN);
        //showResponseErrorAlertDialog(false, null, null, null);
    }

    //OnError
    public void onErrorLogin(Exception exception) {
        //Show Error Alert Dialog
        showErrorAlertDialog(null, null, null, TYPE_LOGIN);
        //showResponseErrorAlertDialog(false, null, null, null);
    }

    void showAlertDialog(boolean neutralButton, String message, String title, String okButtonTitle, int type) {
        applicationAlertDialog = new ApplicationAlertDialog().getInstance(this, neutralButton, message, title, okButtonTitle, type);
        applicationAlertDialog.show(getSupportFragmentManager(), "dialog");
    }

    void showErrorAlertDialog(String message, String title, String okButtonTitle, int type) {
        applicationErrorAlertDialog = new ApplicationErrorAlertDialog().getInstance(this, message, title, okButtonTitle, type);
        applicationErrorAlertDialog.show(getSupportFragmentManager(), "dialog");
    }


    void enableNetwork() {

        applicationNetworkDialogl = new ApplicationNetworkDialog().getInstance(ClaimantExpenseDetailActivity.this);
        applicationNetworkDialogl.show(getSupportFragmentManager(), "dialog");
    }

    // alert dialog action listener
    @Override
    public void onPositiveButtonClick(int type) {
        switch (type) {
            case TYPE_UPLOAD:

                //Navigate To Expense List
                navigateToExpenseList();
                break;
            case TYPE_DELETE:
                //Delete Expense
                deleteExpense(expenseIdList);

                break;


        }
    }

    @Override
    public void onNegativeButtonClick() {

    }


    //error dialog action listener

    @Override
    public void onRetryButtonClick(int type) {
        handleUploadExpense();
    }

    @Override
    public void onCancelButtonClick() {

    }


}
