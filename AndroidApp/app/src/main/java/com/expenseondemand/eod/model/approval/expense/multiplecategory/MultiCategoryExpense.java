package com.expenseondemand.eod.model.approval.expense.multiplecategory;

import android.os.Parcel;
import android.os.Parcelable;

import com.expenseondemand.eod.model.approval.expense.PolicyBreachModel;
import com.expenseondemand.eod.model.approval.expense.PolicyDetails;
import com.expenseondemand.eod.webservice.model.approval.item.DuplicateItemsModel;

import java.util.ArrayList;

/**
 * Created by ITDIVISION\maximess087 on 16/1/17.
 */

public class MultiCategoryExpense implements Parcelable {
    private String UniqueId;
    private String Date;
    private String Amount;
    private String ExpneseCategory;
    private String ExpenseCategoryId;
    private String BaseCategoryId;
    private String ExpenseItemizationId;
    private boolean Rejected;
    private boolean EscalationFlag;
   /* private PolicyDetails PolicyDetails;*/
    private PolicyBreachModel PolicyBreech;
    private boolean IsDuplicate;//
    ArrayList<DuplicateItemsModel> DuplicateItems;

    public MultiCategoryExpense(){}

    protected MultiCategoryExpense(Parcel in) {
        UniqueId = in.readString();
        Date = in.readString();
        Amount = in.readString();
        ExpneseCategory = in.readString();
        ExpenseCategoryId = in.readString();
        BaseCategoryId = in.readString();
        Rejected = in.readByte() != 0;
        EscalationFlag = in.readByte() != 0;
        ExpenseItemizationId = in.readString();
        IsDuplicate = in.readByte() != 0;
       // DuplicateItems = in.readArrayList(DuplicateItemsModel.class.getClassLoader());
    }

    public static final Creator<MultiCategoryExpense> CREATOR = new Creator<MultiCategoryExpense>() {
        @Override
        public MultiCategoryExpense createFromParcel(Parcel in) {
            return new MultiCategoryExpense(in);
        }

        @Override
        public MultiCategoryExpense[] newArray(int size) {
            return new MultiCategoryExpense[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(UniqueId);
        parcel.writeString(Date);
        parcel.writeString(Amount);
        parcel.writeString(ExpneseCategory);
        parcel.writeString(ExpenseCategoryId);
        parcel.writeString(BaseCategoryId);
        parcel.writeByte((byte) (Rejected ? 1 : 0));
        parcel.writeByte((byte) (EscalationFlag ? 1 : 0));
        parcel.writeString(ExpenseItemizationId);
        parcel.writeByte((byte) (IsDuplicate ? 1 : 0));
       // parcel.writeList(DuplicateItems);
    }

    public PolicyBreachModel getPolicyBreech() {
        return PolicyBreech;
    }

    public void setPolicyBreech(PolicyBreachModel policyBreech) {
        PolicyBreech = policyBreech;
    }

    public boolean isRejected() {
        return Rejected;
    }

    public void setRejected(String rejected) {
        this.Rejected = (rejected != null) && (rejected.equals("R"));
    }

    public boolean isEscalationFlag() {
        return EscalationFlag;
    }

    public void setEscalationFlag(String escalationFlag) {
        this.EscalationFlag = (escalationFlag != null) && (escalationFlag.equals("Y"));
    }

    public ArrayList<DuplicateItemsModel> getDuplicateItems() {
        return DuplicateItems;
    }

    public void setDuplicateItems(ArrayList<DuplicateItemsModel> duplicateItems) {
        DuplicateItems = duplicateItems;
    }

    public boolean isDuplicate() {
        return IsDuplicate;
    }

    public void setDuplicate(String duplicate) {
        this.IsDuplicate = (duplicate != null) && (!duplicate.equals("U"));
    }

    public String getUniqueId() {
        return UniqueId;
    }

    public void setUniqueId(String uniqueId) {
        UniqueId = uniqueId;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getExpneseCategory() {
        return ExpneseCategory;
    }

    public void setExpneseCategory(String expneseCategory) {
        ExpneseCategory = expneseCategory;
    }

    public String getExpenseCategoryId() {
        return ExpenseCategoryId;
    }

    public void setExpenseCategoryId(String expenseCategoryId) {
        ExpenseCategoryId = expenseCategoryId;
    }

    public String getBaseCategoryId() {
        return BaseCategoryId;
    }

    public void setBaseCategoryId(String baseCategoryId) {
        BaseCategoryId = baseCategoryId;
    }

    public String getExpenseItemizationId() {
        return ExpenseItemizationId;
    }

    public void setExpenseItemizationId(String expenseItemizationId) {
        ExpenseItemizationId = expenseItemizationId;
    }

    /*public PolicyDetails getPolicyDetails() {
        return PolicyDetails;
    }

    public void setPolicyDetails(PolicyDetails policyDetails) {
        PolicyDetails = policyDetails;
    }*/
}
