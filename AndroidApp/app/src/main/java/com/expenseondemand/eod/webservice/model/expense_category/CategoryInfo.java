package com.expenseondemand.eod.webservice.model.expense_category;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class CategoryInfo
{
	private int expenseCategoryId;
	private int baseExpenseCategoryId;

	private String displayName;

	public int getExpenseCategoryId() {
		return expenseCategoryId;
	}

	public void setExpenseCategoryId(int expenseCategoryId) {
		this.expenseCategoryId = expenseCategoryId;
	}

	public int getBaseExpenseCategoryId() {
		return baseExpenseCategoryId;
	}

	public void setBaseExpenseCategoryId(int baseExpenseCategoryId) {
		this.baseExpenseCategoryId = baseExpenseCategoryId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
