package com.expenseondemand.eod.webservice.model.approval.claim;

import com.expenseondemand.eod.webservice.model.MasterRequest;

/**
 * Created by Ramit Yadav on 19-10-2016.
 */

public class ClaimRequest extends MasterRequest {
    private String companyId;
    private String approverId;
    private int NoOfRecords;
    private int PageNo;

    public int getNoOfRecords() {
        return NoOfRecords;
    }

    public void setNoOfRecords(int noOfRecords) {
        NoOfRecords = noOfRecords;
    }

    public int getPageNo() {
        return PageNo;
    }

    public void setPageNo(int pageNo) {
        PageNo = pageNo;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getApproverId() {
        return approverId;
    }

    public void setApproverId(String approverId) {
        this.approverId = approverId;
    }
}
