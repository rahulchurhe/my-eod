package com.expenseondemand.eod.util;

public interface AlertDialogPermissionBoxClickInterface
{
	void onButtonClicked(boolean isPositiveButtonClicked);
}
