package com.expenseondemand.eod.webservice.model.approval.detail;

import com.expenseondemand.eod.webservice.model.MasterRequest;

/**
 * Created by ITDIVISION\maximess087 on 4/1/17.
 */

public class ExpenseMultipleCategoryRequest extends MasterRequest {
    private String ExpenseHeaderID;
    private String ExpenseDetailID;
    private String CompanyId;
    private String IsMultiPayment;
    private String Date;

    public String getExpenseHeaderID() {
        return ExpenseHeaderID;
    }

    public void setExpenseHeaderID(String expenseHeaderID) {
        ExpenseHeaderID = expenseHeaderID;
    }

    public String getExpenseDetailID() {
        return ExpenseDetailID;
    }

    public void setExpenseDetailID(String expenseDetailID) {
        ExpenseDetailID = expenseDetailID;
    }

    public String getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(String companyId) {
        CompanyId = companyId;
    }

    public String getIsMultiPayment() {
        return IsMultiPayment;
    }

    public void setIsMultiPayment(String isMultiPayment) {
        IsMultiPayment = isMultiPayment;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
}
