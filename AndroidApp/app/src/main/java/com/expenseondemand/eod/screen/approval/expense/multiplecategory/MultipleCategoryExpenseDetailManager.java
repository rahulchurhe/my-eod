package com.expenseondemand.eod.screen.approval.expense.multiplecategory;

import android.util.Log;

import com.expenseondemand.eod.exception.EODAppException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.ParseManager;
import com.expenseondemand.eod.manager.WebServiceManager;
import com.expenseondemand.eod.model.approval.detail.ExpenseDetail;
import com.expenseondemand.eod.model.approval.detail.attendees.AttendeesItem;
import com.expenseondemand.eod.model.approval.detail.mileage.MultipleMileage;
import com.expenseondemand.eod.model.approval.detail.mileage.MultipleMileageItem;
import com.expenseondemand.eod.model.approval.detail.project.AdditionalDetailsItem;
import com.expenseondemand.eod.model.approval.detail.project.BusinessPurposeItem;
import com.expenseondemand.eod.model.approval.detail.project.PaymentTypeItem;
import com.expenseondemand.eod.model.approval.detail.project.PreApprovedDetailsItem;
import com.expenseondemand.eod.model.approval.detail.project.ProjectDetailItem;
import com.expenseondemand.eod.model.approval.detail.project.ProjectDetailsItem;
import com.expenseondemand.eod.model.approval.detail.project.ReceiptItem;
import com.expenseondemand.eod.model.approval.expense.multiplecategory.MultiCategoryExpense;
import com.expenseondemand.eod.model.approval.expense.multiplecategory.MultipleCategoryExpenseDetails;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.webservice.model.approval.detail.AdditionalDetails;
import com.expenseondemand.eod.webservice.model.approval.detail.Attendees;
import com.expenseondemand.eod.webservice.model.approval.detail.BusinessPurpose;
import com.expenseondemand.eod.webservice.model.approval.detail.DynamicDetail;
import com.expenseondemand.eod.webservice.model.approval.detail.ExpenseDetailData;
import com.expenseondemand.eod.webservice.model.approval.detail.ExpenseDetailResponse;
import com.expenseondemand.eod.webservice.model.approval.detail.Notes;
import com.expenseondemand.eod.webservice.model.approval.detail.PaymentType;
import com.expenseondemand.eod.webservice.model.approval.detail.PreApprovedDetails;
import com.expenseondemand.eod.webservice.model.approval.detail.ProjectDetail;
import com.expenseondemand.eod.webservice.model.approval.detail.ProjectDetails;
import com.expenseondemand.eod.webservice.model.approval.detail.Receipt;
import com.expenseondemand.eod.webservice.model.approval.detail.expense.multiplecategory.MultipleCategoryExpenseData;
import com.expenseondemand.eod.webservice.model.approval.detail.expense.multiplecategory.MultipleCategoryExpenseDetailRequest;
import com.expenseondemand.eod.webservice.model.approval.detail.expense.multiplecategory.MultipleCategoryExpenseDetailResponse;
import com.expenseondemand.eod.webservice.model.approval.detail.mileage.Mileage;
import com.expenseondemand.eod.webservice.model.approval.detail.mileage.MileageLeg;
import com.expenseondemand.eod.webservice.model.http.Body;
import com.expenseondemand.eod.webservice.model.http.HTTPData;

import java.util.ArrayList;

/**
 * Created by ITDIVISION\maximess087 on 18/1/17.
 */

public class MultipleCategoryExpenseDetailManager {
    //Multiple category expense
    public static ExpenseDetail getMultipleCategoryExpenseDetail(String expenseHeaderID, String expenseDetailID, String expenseItemizationId, String multiCategoryExpenseDate) throws EODException {
        ExpenseDetailResponse response;

        //Prepare Request Object
        MultipleCategoryExpenseDetailRequest request = prepareMultipleCategoryExpenseDetailRequest(expenseHeaderID, expenseDetailID, expenseItemizationId, multiCategoryExpenseDate);

        //Prepare JSON Request String
        String jsonRequestString = ParseManager.prepareJsonRequest(request);
        Log.e("jsonReqMulCatDetails", ":-" + jsonRequestString);

        //Prepare http request
        HTTPData httpData = new HTTPData();
        httpData.setType(AppConstant.HTTP_METHOD_POST);
        Body body = new Body();
        body.setJsonRequestString(jsonRequestString.toString());
        httpData.setBody(body);

        //Call to Web Service
        String jsonResponseString = (String) WebServiceManager.callWebService(AppConstant.BASE_URL, AppConstant.EXPENSE_MULTIPLE_CATEGORY_DETAILS, httpData);
        Log.e("jsonResponseString", ":-" + jsonResponseString);

        //Prepare Response Object
        response = (ExpenseDetailResponse) ParseManager.prepareWebServiceResponseObject(ExpenseDetailResponse.class, jsonResponseString);

        //Process Response Status
        WebServiceManager.processResponseStatus(response);

        //Process Response Status
        ExpenseDetail expenseDetail = processExpenseDetailResponse(response);

        return expenseDetail;
    }

    private static MultipleCategoryExpenseDetailRequest prepareMultipleCategoryExpenseDetailRequest(String expenseHeaderID, String expenseDetailID, String expenseItemizationId, String multiCategoryExpenseDate) {
        MultipleCategoryExpenseDetailRequest expenseDetailRequest = new MultipleCategoryExpenseDetailRequest();
        expenseDetailRequest.setExpenseHeaderID(expenseHeaderID);
        expenseDetailRequest.setExpenseDetailID(expenseDetailID);
        expenseDetailRequest.setExpenseItemizationID(expenseItemizationId);
        expenseDetailRequest.setDate(multiCategoryExpenseDate);
        return expenseDetailRequest;
    }

    //Check Expense Detail Response Status
    private static ExpenseDetail processExpenseDetailResponse(ExpenseDetailResponse response) throws EODException {
        ExpenseDetail expenseDetail = null;

        if (response != null) {
            expenseDetail = prepareExpenseDetailModel(response);
        } else {
            throw new EODAppException(AppConstant.ERROR_CODE_1008);
        }

        return expenseDetail;
    }

    private static ExpenseDetail prepareExpenseDetailModel(ExpenseDetailResponse response) {
        ExpenseDetail expenseDetail = null;

        ExpenseDetailData data = response.getData();
        if (data != null) {
            expenseDetail = new ExpenseDetail();

            expenseDetail.setUniqueId(data.getUniqueId());
            expenseDetail.setDate(data.getDate());
            expenseDetail.setAmount(data.getAmount());
            expenseDetail.setIsItemised(data.getIsItemised());
            expenseDetail.setBaseCategoryId(data.getBaseCategoryId());

            ArrayList<DynamicDetail> dynamicDetails = data.getDynamicDetails();
            if (!AppUtil.isCollectionEmpty(dynamicDetails)) {
                ArrayList<com.expenseondemand.eod.model.approval.detail.dynamic.DynamicDetail> dynamicDetailList = new ArrayList<>();
                for (DynamicDetail dynamicDetail : dynamicDetails) {
                    com.expenseondemand.eod.model.approval.detail.dynamic.DynamicDetail dynamicDetailItem = new com.expenseondemand.eod.model.approval.detail.dynamic.DynamicDetail();
                    dynamicDetailItem.setValue(dynamicDetail.getValue());
                    dynamicDetailItem.setLabel(dynamicDetail.getLabelName());
                    dynamicDetailList.add(dynamicDetailItem);
                }
                expenseDetail.setDynamicDetails(dynamicDetailList);
            }

           /* ArrayList<ProjectDetail> projectDetails = data.getProjectDetails();
            if (!AppUtil.isCollectionEmpty(projectDetails)) {
                ArrayList<ProjectDetailItem> projectDetailItems = new ArrayList<>();
                for (ProjectDetail projectDetail : projectDetails) {
                    ProjectDetailItem projectDetailItem = new ProjectDetailItem();
                    projectDetailItem.setValue(projectDetail.getValue());
                    projectDetailItem.setKey(projectDetail.getLabelName());
                    projectDetailItem.setReceiptCount(projectDetail.getCount());

                    projectDetailItems.add(projectDetailItem);
                }
                expenseDetail.setProjectDetails(projectDetailItems);
            }*/

            ArrayList<ProjectDetails> projectDetails = data.getProjectDetails();
            if (!AppUtil.isCollectionEmpty(projectDetails)) {
                ArrayList<ProjectDetailsItem> projectDetailItems = new ArrayList<>();
                for (ProjectDetails projectDetail : projectDetails) {
                    ProjectDetailsItem projectDetailItem = new ProjectDetailsItem();
                    projectDetailItem.setLabelName(projectDetail.getLabelName());
                    projectDetailItem.setValue(projectDetail.getValue());

                    projectDetailItems.add(projectDetailItem);
                }
                expenseDetail.setProjectDetails(projectDetailItems);
            }
             /*Payment Type*/
            PaymentType paymentType=data.getPaymentType();
            if(paymentType != null){
                PaymentTypeItem paymentTypeItem=new PaymentTypeItem();
                paymentTypeItem.setLabelName(paymentType.getLabelName());
                paymentTypeItem.setValue(paymentType.getValue());
                expenseDetail.setPaymentType(paymentTypeItem);
            }

            /*Business Purpose*/
            BusinessPurpose businessPurpose=data.getBusinessPurpose();
            if(businessPurpose!=null){
                BusinessPurposeItem businessPurposeItem=new BusinessPurposeItem();
                businessPurposeItem.setLabelName(businessPurpose.getLabelName());
                businessPurposeItem.setValue(businessPurpose.getValue());
                expenseDetail.setBusinessPurpose(businessPurposeItem);
            }

            /*Additional Details*/
            ArrayList<AdditionalDetails> additionalDetailses=data.getAdditionalDetails();
            if (!AppUtil.isCollectionEmpty(additionalDetailses)) {
                ArrayList<AdditionalDetailsItem> additionalDetailsItems=new ArrayList<>();
                for(AdditionalDetails additionalDetails:additionalDetailses){
                    AdditionalDetailsItem additionalDetailsItem=new AdditionalDetailsItem();
                    additionalDetailsItem.setLabelName(additionalDetails.getLabelName());
                    additionalDetailsItem.setValue(additionalDetails.getValue());
                    additionalDetailsItems.add(additionalDetailsItem);
                }
                expenseDetail.setAdditionalDetails(additionalDetailsItems);
            }

            /*Receipt*/
            Receipt receipt=data.getReceipt();
            if(receipt!=null){
                ReceiptItem receiptItem=new ReceiptItem();
                receiptItem.setLabelName(receipt.getLabelName());
                receiptItem.setCount(receipt.getCount());
                receiptItem.setValue(receipt.getValue());
                expenseDetail.setReceipt(receiptItem);
            }


            Notes notes = data.getNotes();
            if (notes != null) {
                com.expenseondemand.eod.model.approval.detail.notes.Notes notesModel = new com.expenseondemand.eod.model.approval.detail.notes.Notes();
                notesModel.setCcNotes(notes.getCcNotes());
                notesModel.setMobileNotes(notes.getMobileNotes());
                notesModel.setEnteredNotes(notes.getEnteredNotes());
                 /*Pre Approval array list*/
                ArrayList<PreApprovedDetails>preApprovedDetailses=notes.getPreApprovedDetails();
                if(!AppUtil.isCollectionEmpty(preApprovedDetailses)){
                    ArrayList<PreApprovedDetailsItem> preApprovedDetailsItems=new ArrayList<>();
                    for(PreApprovedDetails preApprovedDetails:preApprovedDetailses){
                        PreApprovedDetailsItem preApprovedDetailsItem=new PreApprovedDetailsItem();
                        preApprovedDetailsItem.setLabelName(preApprovedDetails.getLabelName());
                        preApprovedDetailsItem.setLabelName(preApprovedDetails.getValue());
                        preApprovedDetailsItems.add(preApprovedDetailsItem);
                    }
                    notesModel.setPreApprovedDetails(preApprovedDetailsItems);
                }

                expenseDetail.setNotes(notesModel);
            }

            Mileage mileage = data.getMileage();
            if (mileage != null) {
                MultipleMileage multipleMileage = new MultipleMileage();
                multipleMileage.setVehicleID(mileage.getVehicleId());

                ArrayList<MileageLeg> mileageLegs = mileage.getMileageLegs();
                if (!AppUtil.isCollectionEmpty(mileageLegs)) {
                    ArrayList<MultipleMileageItem> multipleMileageItems = new ArrayList<>();
                    for (MileageLeg mileageLeg : mileageLegs) {
                        MultipleMileageItem multipleMileageItem = new MultipleMileageItem();
                        multipleMileageItem.setAmount(mileageLeg.getAmount());
                        multipleMileageItem.setDestinationAddress(mileageLeg.getTo());
                        multipleMileageItem.setSourceAddress(mileageLeg.getFrom());
                        multipleMileageItem.setDistance(mileageLeg.getMiles());
                        multipleMileageItems.add(multipleMileageItem);
                    }
                    multipleMileage.setMultipleMileageItems(multipleMileageItems);
                }

                expenseDetail.setMileage(multipleMileage);
            }

            ArrayList<Attendees> attendees = data.getAttendees();
            if (!AppUtil.isCollectionEmpty(attendees)) {
                ArrayList<AttendeesItem> attendeesItems = new ArrayList<>();
                for (Attendees attendeesData : attendees) {
                    AttendeesItem attendeesItem = new AttendeesItem();
                    attendeesItem.setCost(attendeesData.getAmount());
                    attendeesItem.setLevel(attendeesData.getLabel());
                    attendeesItem.setName(attendeesData.getName());
                    attendeesItem.setType(attendeesData.getType());
                    attendeesItem.setValue(attendeesData.getValue());

                    attendeesItems.add(attendeesItem);
                }

                expenseDetail.setAttendees(attendeesItems);
            }
        }
        return expenseDetail;
    }




}
