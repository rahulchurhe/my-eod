package com.expenseondemand.eod.manager;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.util.AlertDialogPermissionBoxClickInterface;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.util.UIUtil;

public class PermissionManager 
{
	/**
	 * handleRequestForPermission
	 * <br>Firstly, it will check for permission status, if denied then it will check whether there is need to show permission rationale (i.e. Why we need this permission) dialog.
	 * <br><code>ActivityCompat.shouldShowRequestPermissionRationale(activityContext, permission);</code> This method returns true if the app has requested this permission previously and the user denied the request.
	 * That indicates that you should probably explain to the user why you need the permission.
	 * @param activityContext
	 * @param permission
	 * @param requestCode
	 * @param msgOnPermissionCancel
	 * @param msgForPermissionRationale
	 * @return boolean
	 */
	public static boolean handleRequestForPermission(Activity activityContext, String permission, int requestCode, String msgOnPermissionCancel, String msgForPermissionRationale)
	{
		boolean checkPermission = false;
		if(AppUtil.isLollipopAndAbove())
		{
			int permissionStatus = ContextCompat.checkSelfPermission(activityContext, permission);

			if(permissionStatus == PackageManager.PERMISSION_DENIED) //Permission Denied.
			{
				boolean shouldShowRequestPermissionRationale = ActivityCompat.shouldShowRequestPermissionRationale(activityContext, permission);
				if(shouldShowRequestPermissionRationale)	//if the app has requested this permission previously and the user denied the request without checking "Never ask again". 
				{
					//Show the message that why we need this permission.
					OnPermissionRationaleClickListener alertDialogBoxClickListener = new OnPermissionRationaleClickListener(activityContext, msgOnPermissionCancel, requestCode, permission);
					UIUtil.showCustomConfirmDialog(activityContext, msgForPermissionRationale, activityContext.getString(R.string.retry), activityContext.getString(R.string.text_cancel), alertDialogBoxClickListener);
				}
				else	//First time asking for the permission
				{
					requestForPermission(activityContext, requestCode, permission);
				}
			}
			else	//Permission Granted.
			{
				checkPermission = true;
			}
		}
		else
		{
			checkPermission = true;
		}
		return checkPermission; 
	}

	private static class OnPermissionRationaleClickListener implements AlertDialogPermissionBoxClickInterface
	{
		Activity activityContext;
		String cancellationMessage;
		int requestCode;
		String permission;

		public OnPermissionRationaleClickListener(Activity activityContext, String cancellationMessage, int requestCode, String permission) 
		{
			this.activityContext =activityContext;
			this.cancellationMessage = cancellationMessage;
			this.requestCode = requestCode;
			this.permission = permission;
		}

		@Override
		public void onButtonClicked(boolean isPositiveButtonClicked)
		{
			if(isPositiveButtonClicked)
			{
				requestForPermission(activityContext, requestCode, permission);
			}
			else
			{
				UIUtil.showToastNotification(activityContext, cancellationMessage, null, true);
			}
		}
	}

	/**
	 * requestForPermission 
	 * This method is requesting for particular given permission.
	 * @param activityContext
	 * @param requestCode
	 * @param permission
	 */
	private static void requestForPermission(Activity activityContext, int requestCode, String permission)
	{
		String[] permissionStringArray = {permission};
		ActivityCompat.requestPermissions(activityContext, permissionStringArray, requestCode);
	}
}
