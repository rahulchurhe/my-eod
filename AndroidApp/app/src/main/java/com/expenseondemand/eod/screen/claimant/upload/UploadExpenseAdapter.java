package com.expenseondemand.eod.screen.claimant.upload;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.model.Expense;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;

import java.util.ArrayList;

/**
 * Created by Nandani Gupta on 2016-09-13.
 */
public class UploadExpenseAdapter extends RecyclerView.Adapter<UploadExpenseAdapter.MyViewHolder> {

    private Context activityContext;
    private ArrayList<Expense> expenses;
    UploadExpenseRowClickListener uploadExpenseRowClickListener;


    public interface UploadExpenseRowClickListener
    {
        void handleCheckBoxClick(CheckBox checkBox);

        void handleExpenseRowClick(int position);

    }



    public class MyViewHolder extends RecyclerView.ViewHolder
    {
           RelativeLayout relativeClaimantExpenseItem;
           CheckBox checkBoxSelectExpenses;
           ImageView imageViewExpenseCategoryIcon;
           ImageView imageViewAttachmentIcon;
           TextView  textViewExpenseCategory;
           TextView  textViewExpenseCreationDate;
           TextView  textViewExpenseAmount;
           ImageView imageViewRightArrow;



        public MyViewHolder(View view)
        {
            super(view);

            relativeClaimantExpenseItem = (RelativeLayout) view.findViewById(R.id.relativeClaimantExpenseItem);
            checkBoxSelectExpenses = (CheckBox) view.findViewById(R.id.checkBoxSelectExpenses);
            imageViewExpenseCategoryIcon = (ImageView) view.findViewById(R.id.imageViewExpenseCategoryIcon);
            imageViewAttachmentIcon = (ImageView) view.findViewById(R.id.imageViewAttachmentIcon);
            textViewExpenseCategory = (TextView) view.findViewById(R.id.textViewExpenseCategory);
            textViewExpenseCreationDate = (TextView) view.findViewById(R.id.textViewExpenseCreationDate);
            textViewExpenseAmount = (TextView) view.findViewById(R.id.textViewExpenseAmount);
            imageViewRightArrow = (ImageView) view.findViewById(R.id.imageViewRightArrow);

            textViewExpenseCategory.setSelected(true);

        }
    }


    public UploadExpenseAdapter(Context activityContext, ArrayList<Expense> expenses, UploadExpenseRowClickListener uploadExpenseRowClickListener)
    {
      this.uploadExpenseRowClickListener =  uploadExpenseRowClickListener;
      this.activityContext = activityContext;
      this.expenses = expenses;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.claimant_expense_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder viewHolder, int position)
    {
        //set click listener to row
        viewHolder.relativeClaimantExpenseItem.setTag(position);
        viewHolder.relativeClaimantExpenseItem.setOnClickListener(new UploadExpenseClickListener());

        Expense expense = expenses.get(position);

        //set checked listener to checkbox
        viewHolder.checkBoxSelectExpenses.setTag(expense);
        viewHolder.checkBoxSelectExpenses.setOnClickListener(new UploadExpenseClickListener());

        boolean hasReceipt = expense.isHasReceipt();

        //get expense date & convert into formatted string
        Long date =  expense.getExpenseDate();
        String expenseDate = AppUtil.getFormattedDateString(AppConstant.FORMAT_DATE_DISPLAY,date);

        //Set Expense Details

        viewHolder.textViewExpenseCategory.setText(expense.getExpenseCategoryName());

        int resourceId = CachingManager.getCategoryResourceId(expense.getExpenseBaseCategoryId());
        viewHolder.imageViewExpenseCategoryIcon.setImageResource(resourceId);

        viewHolder.textViewExpenseCreationDate.setText(expenseDate);
        viewHolder.textViewExpenseAmount.setText(expense.getExpenseAmount());
        viewHolder.imageViewAttachmentIcon.setVisibility((hasReceipt) ? ImageView.VISIBLE : ImageView.INVISIBLE);
        viewHolder.checkBoxSelectExpenses.setChecked(expense.isSelected());
    }

    @Override
    public int getItemCount()
    {
        return ((AppUtil.isCollectionEmpty(expenses)) ? 0 : expenses.size());
    }


    private class UploadExpenseClickListener implements View.OnClickListener
    {

        @Override
        public void onClick(View view)
        {
            int viewId = view.getId();

            switch (viewId)
            {
                case R.id.relativeClaimantExpenseItem:
                    int clickedPos = (int)view.getTag();
                    uploadExpenseRowClickListener.handleExpenseRowClick(clickedPos);

                break;

                case R.id.checkBoxSelectExpenses:

                    //Cast
                    CheckBox checkBox = (CheckBox) view;
                    uploadExpenseRowClickListener.handleCheckBoxClick(checkBox);

                break;
            }




        }
    }





}