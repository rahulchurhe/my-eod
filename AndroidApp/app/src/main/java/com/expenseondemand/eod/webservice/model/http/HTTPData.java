package com.expenseondemand.eod.webservice.model.http;

/**
 * Created by Ramit Yadav on 26-09-2016.
 */
public class HTTPData
{
    private int type;
    private Body body;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }
}
