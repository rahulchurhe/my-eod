package com.expenseondemand.eod.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ReceiptImage implements Parcelable
{
	private String receiptImageFileName;
//	private String receiptImageData;
//	private String mimeType;

	public ReceiptImage()
	{
	}
	
	public String getReceiptImageFileName() 
	{
		return receiptImageFileName;
	}
	
	public void setReceiptImageFileName(String receiptImageFileName) 
	{
		this.receiptImageFileName = receiptImageFileName;
	}
	
//	public String getReceiptImageData()
//	{
//		return receiptImageData;
//	}
//
//	public void setReceiptImageData(String receiptImageData)
//	{
//		this.receiptImageData = receiptImageData;
//	}
//
//	public String getMimeType()
//	{
//		return mimeType;
//	}
//
//	public void setMimeType(String mimeType)
//	{
//		this.mimeType = mimeType;
//	}


	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeString(receiptImageFileName);
	}

	public ReceiptImage(Parcel source)
	{
		this.receiptImageFileName = source.readString();
	}

	public static final Parcelable.Creator<ReceiptImage> CREATOR = new  Creator<ReceiptImage>() {

		@Override
		public ReceiptImage[] newArray(int size)
		{
			//Do Nothing...

			return null;
		}

		@Override
		public ReceiptImage createFromParcel(Parcel source)
		{
			return new ReceiptImage(source);
		}
	};
}
