package com.expenseondemand.eod.model.approval.detail.mileage;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 20-10-2016.
 */

public class MultipleMileage
{
    private String vehicleID;
    private ArrayList<MultipleMileageItem> multipleMileageItems;

    public String getVehicleID()
    {
        return vehicleID;
    }

    public void setVehicleID(String vehicleID)
    {
        this.vehicleID = vehicleID;
    }

    public ArrayList<MultipleMileageItem> getMultipleMileageItems()
    {
        return multipleMileageItems;
    }

    public void setMultipleMileageItems(ArrayList<MultipleMileageItem> multipleMileageItems)
    {
        this.multipleMileageItems = multipleMileageItems;
    }
}
