package com.expenseondemand.eod.application;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.expenseondemand.eod.database.EodDatabase;
import com.expenseondemand.eod.manager.ApplicationManager;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.PersistentManager;
import io.fabric.sdk.android.Fabric;

public class EODApplication extends Application
{
	@Override
	public void onCreate()
	{
		super.onCreate();
		Fabric.with(this, new Crashlytics());
		//Initialize Database
		EodDatabase.initializeInstance(this);
		
		//Prepare Application
		ApplicationManager.prepareApplication(this);
	}
	
	@Override
	public void onTerminate()
	{
		super.onTerminate();
		PersistentManager.persistIsUserRemembered(false);
		//Remove Cache
		CachingManager.removeApplicationCache();
	}

}
