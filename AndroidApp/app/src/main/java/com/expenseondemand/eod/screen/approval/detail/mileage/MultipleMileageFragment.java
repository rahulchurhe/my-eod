package com.expenseondemand.eod.screen.approval.detail.mileage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.expenseondemand.eod.BaseFragment;
import com.expenseondemand.eod.R;
import com.expenseondemand.eod.model.approval.detail.mileage.MultipleMileageItem;
import com.expenseondemand.eod.model.approval.detail.project.ProjectDetailItem;
import com.expenseondemand.eod.screen.approval.detail.project.ProjectDetailAdapter;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 16-09-2016.
 */
public class MultipleMileageFragment extends BaseFragment
{
    private static String str_vehicleID;
    private RecyclerView recyclerViewMultipleMileage;
    private ProjectDetailAdapter MultipleMileageAdapter;
    private ArrayList<MultipleMileageItem> multipleMileageItems;
    private TextView txt_wayPoint;

    public static MultipleMileageFragment getInstance(ArrayList<MultipleMileageItem> multipleMileageItems, String vehicleID)
    {
        MultipleMileageFragment projectDetailFragment = new MultipleMileageFragment();
        projectDetailFragment.multipleMileageItems = multipleMileageItems;
        str_vehicleID=vehicleID;

        return projectDetailFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.multiple_mileage_fragment, container, false);

        initializeView(rootView);

        return rootView;
    }

    private void initializeView(View rootView)
    {
        recyclerViewMultipleMileage = (RecyclerView)rootView.findViewById(R.id.recyclerViewMultipleMileage);
        txt_wayPoint=(TextView)rootView.findViewById(R.id.txt_wayPoint);
        txt_wayPoint.setText(str_vehicleID);
        txt_wayPoint.setSelected(true);
        final LinearLayoutManager projectDetailItemLinerLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerViewMultipleMileage.setLayoutManager(projectDetailItemLinerLayoutManager);

        MultipleMileageAdapter multipleMileageAdapter = new MultipleMileageAdapter(getActivity(), multipleMileageItems);
        recyclerViewMultipleMileage.setAdapter(multipleMileageAdapter);
    }
}
