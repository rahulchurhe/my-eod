package com.expenseondemand.eod.webservice.model.approval.detail.expense.multiplecategory;

import com.expenseondemand.eod.webservice.model.MasterResponse;

/**
 * Created by ITDIVISION\maximess087 on 18/1/17.
 */

public class MultipleCategoryExpenseDetailResponse extends MasterResponse {
    private MultipleCategoryExpenseData data;

    public MultipleCategoryExpenseData getData() {
        return data;
    }

    public void setData(MultipleCategoryExpenseData data) {
        this.data = data;
    }
}
