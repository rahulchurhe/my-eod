package com.expenseondemand.eod.dialogFragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.util.AppConstant;

/**
 * Created by Ramit Yadav on 04-10-2016.
 */

public class ApplicationNetworkDialog extends DialogFragment implements View.OnClickListener
{
    private TextView textViewOk;
    private TextView textViewCancel;
    private TextView textViewMessage;
    private TextView textViewTitle;
    private Context activityContext;

    public static ApplicationNetworkDialog getInstance(Context activityContext)
    {
        ApplicationNetworkDialog applicationAlertDialog = new ApplicationNetworkDialog();
        applicationAlertDialog.activityContext =  activityContext;

        return applicationAlertDialog;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_BACK) && (event.getAction() == KeyEvent.ACTION_UP))
                    handleDismiss();
                return false;
            }
        });
    }

    @Override
    public void onStart()
    {
        super.onStart();
        getDialog().getWindow().setLayout( ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);

    }

    @Override
    public void onPause()
    {
        super.onPause();
        handleDismiss();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
       // View view = inflater.inflate(R.layout.app_upgrade_dialog_fragment, container, false);
        View view = inflater.inflate(R.layout.application_error_alert_dialog, container, false);
        textViewOk = (TextView) view.findViewById(R.id.textViewOk);
        textViewCancel = (TextView) view.findViewById(R.id.textViewCancel);
        textViewMessage = (TextView) view.findViewById(R.id.textViewMessage);
        textViewTitle = (TextView) view.findViewById(R.id.textViewTitle);

        textViewTitle.setText(R.string.text_network);
        textViewMessage.setText(R.string.text_network_not_available);
        textViewOk.setText(getResources().getString(R.string.button_settings));
        textViewCancel.setText(getResources().getString(R.string.button_dismiss));

        textViewOk.setOnClickListener(this);
        textViewCancel.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.textViewOk:
            {
                handleSetting();
                break;
            }

            case R.id.textViewCancel:
            {
                handleDismiss();
                break;
            }
        }
    }

    private void handleSetting()
    {
        dismiss();

        //Goto WiFi Settings
        Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
        activityContext.startActivity(intent);
        SharePreference.putBoolean(getActivity(), AppConstant.BACK_NETWORK_SETTING_STAT,true);
    }

    private void handleDismiss()
    {
        dismiss();

    }
}
