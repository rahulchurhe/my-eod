package com.expenseondemand.eod.model;

/**
 * Created by Ramit Yadav on 08-09-2016.
 */
public class HomeItem
{
    public static final int TYPE_HEADER = 0;
    public static final int TYPE_ITEM = 1;

    public static final int NO_ACTION = 0;
    public static final int CREATE_EXPENSE_ITEM_ACTION = 1;
    public static final int CAMERA_ITEM_ACTION = 2;
    public static final int DUTY_OF_CARE_EXPENSE_ITEM_ACTION = 3;
    public static final int UPLOAD_EXPENSE_ITEM_ACTION = 4;
    public static final int OPEN_ITEM_ACTION = 5;
    public static final int VIEW_CLAIM_STATUS_ITEM_ACTION = 6;
    public static final int APPROVALS_ITEM_ACTION = 7;

    private int type;
    private String name;
    private int imageBackgroundColor;
    private int imageBackgroundSelectedColor;
    private int imageResourceId;
    private int count;
    private int action;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageBackgroundColor() {
        return imageBackgroundColor;
    }

    public void setImageBackgroundColor(int imageBackgroundColor) {
        this.imageBackgroundColor = imageBackgroundColor;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

    public void setImageResourceId(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getImageBackgroundSelectedColor()
    {
        return imageBackgroundSelectedColor;
    }

    public void setImageBackgroundSelectedColor(int imageBackgroundSelectedColor)
    {
        this.imageBackgroundSelectedColor = imageBackgroundSelectedColor;
    }

    public int getAction()
    {
        return action;
    }

    public void setAction(int action)
    {
        this.action = action;
    }
}
