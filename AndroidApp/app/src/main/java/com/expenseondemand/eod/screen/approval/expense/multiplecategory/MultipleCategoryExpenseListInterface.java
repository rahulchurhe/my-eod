package com.expenseondemand.eod.screen.approval.expense.multiplecategory;

import com.expenseondemand.eod.model.approval.expense.ExpenseInfo;

/**
 * Created by android004 on 20/1/17.
 */

public interface MultipleCategoryExpenseListInterface {
    void handleDuplicateClick(int position);
    void handleExpenseRowClick(int position);
   // void handleExpenseRowClick(ExpenseInfo expenseInfo);
}
