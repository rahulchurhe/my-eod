package com.expenseondemand.eod.screen.approval.detail;

import android.util.Log;

import com.expenseondemand.eod.exception.EODAppException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.ParseManager;
import com.expenseondemand.eod.manager.WebServiceManager;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.model.approval.detail.ApproveReject;
import com.expenseondemand.eod.model.approval.detail.ExpenseDetail;
import com.expenseondemand.eod.model.approval.detail.attendees.AttendeesItem;
import com.expenseondemand.eod.model.approval.detail.mileage.MultipleMileage;
import com.expenseondemand.eod.model.approval.detail.mileage.MultipleMileageItem;
import com.expenseondemand.eod.model.approval.detail.project.AdditionalDetailsItem;
import com.expenseondemand.eod.model.approval.detail.project.BusinessPurposeItem;
import com.expenseondemand.eod.model.approval.detail.project.LstApproverRejectItem;
import com.expenseondemand.eod.model.approval.detail.project.PaymentTypeItem;
import com.expenseondemand.eod.model.approval.detail.project.PreApprovedDetailsItem;
import com.expenseondemand.eod.model.approval.detail.project.ProjectDetailItem;
import com.expenseondemand.eod.model.approval.detail.project.ProjectDetailsItem;
import com.expenseondemand.eod.model.approval.detail.project.ReceiptItem;
import com.expenseondemand.eod.model.approval.expense.PolicyBreachModel;
import com.expenseondemand.eod.model.approval.expense.multiplecategory.MultiCategoryExpense;
import com.expenseondemand.eod.model.approval.expense.multiplecategory.MultiCategoryExpenseData;
import com.expenseondemand.eod.model.approval.expense.multiplecategory.MultiCategoryExpenseParam;
import com.expenseondemand.eod.model.approval.expense.PolicyDetails;
import com.expenseondemand.eod.model.approval.expense.PolicyDetailsResponse;
import com.expenseondemand.eod.model.approval.expense.multiplecategory.MultipleCategoryExpenseDetails;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.webservice.model.approval.detail.AdditionalDetails;
import com.expenseondemand.eod.webservice.model.approval.detail.ApprovalRejectedExpenseContainer;
import com.expenseondemand.eod.webservice.model.approval.detail.ApprovalRejectedExpenseResponse;
import com.expenseondemand.eod.webservice.model.approval.detail.ApproveRejectData;
import com.expenseondemand.eod.webservice.model.approval.detail.Attendees;
import com.expenseondemand.eod.webservice.model.approval.detail.BusinessPurpose;
import com.expenseondemand.eod.webservice.model.approval.detail.DynamicDetail;
import com.expenseondemand.eod.webservice.model.approval.detail.ExpenseDetailData;
import com.expenseondemand.eod.webservice.model.approval.detail.ExpenseDetailRequest;
import com.expenseondemand.eod.webservice.model.approval.detail.ExpenseDetailResponse;
import com.expenseondemand.eod.webservice.model.approval.detail.ExpenseMultipleCategoryRequest;
import com.expenseondemand.eod.webservice.model.approval.detail.ExpenseMultipleCategoryResponse;
import com.expenseondemand.eod.webservice.model.approval.detail.LstApproverReject;
import com.expenseondemand.eod.webservice.model.approval.detail.Notes;
import com.expenseondemand.eod.webservice.model.approval.detail.PaymentType;
import com.expenseondemand.eod.webservice.model.approval.detail.PreApprovedDetails;
import com.expenseondemand.eod.webservice.model.approval.detail.ProjectDetail;
import com.expenseondemand.eod.webservice.model.approval.detail.ProjectDetails;
import com.expenseondemand.eod.webservice.model.approval.detail.Receipt;
import com.expenseondemand.eod.webservice.model.approval.detail.expense.multiplecategory.MultipleCategoryExpenseDetailRequest;
import com.expenseondemand.eod.webservice.model.approval.detail.expense.multiplecategory.MultipleCategoryExpenseDetailResponse;
import com.expenseondemand.eod.webservice.model.approval.detail.expense.receipt.ExpenseDetailReceiptRequest;
import com.expenseondemand.eod.webservice.model.approval.detail.expense.receipt.ExpenseDetailReceiptResponse;
import com.expenseondemand.eod.webservice.model.approval.detail.expense.receipt.ExpenseDetailsReceipt;
import com.expenseondemand.eod.webservice.model.approval.detail.expense.receipt.ExpenseDetailsReceiptData;
import com.expenseondemand.eod.webservice.model.approval.detail.mileage.Mileage;
import com.expenseondemand.eod.webservice.model.approval.detail.mileage.MileageLeg;
import com.expenseondemand.eod.webservice.model.approval.item.PolicyBreechResponseModel;
import com.expenseondemand.eod.webservice.model.http.Body;
import com.expenseondemand.eod.webservice.model.http.HTTPData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 20-10-2016.
 */

public class ExpenseDetailManager {


    //get Expense Detail
    public static ExpenseDetail getExpenseDetail(String expenseDetailId, String expenseHeaderId) throws EODException {
        ExpenseDetailResponse response;

        //Prepare Request Object
        ExpenseDetailRequest request = prepareExpenseDetailRequest(expenseDetailId, expenseHeaderId);

        //Prepare JSON Request String
        String jsonRequestString = ParseManager.prepareJsonRequest(request);
        //Prepare http request
        HTTPData httpData = new HTTPData();
        httpData.setType(AppConstant.HTTP_METHOD_POST);
        Body body = new Body();
        body.setJsonRequestString(jsonRequestString);
        httpData.setBody(body);

        //Call to Web Service
        String jsonResponseString = (String) WebServiceManager.callWebService(AppConstant.BASE_URL, AppConstant.EXPENSE_DETAIL, httpData);

        //Prepare Response Object
        response = (ExpenseDetailResponse) ParseManager.prepareWebServiceResponseObject(ExpenseDetailResponse.class, jsonResponseString);

        //Process Response Status
        WebServiceManager.processResponseStatus(response);

        //Process Response Status
        ExpenseDetail expenseDetail = processExpenseDetailResponse(response);

        return expenseDetail;
    }

    //Prepare Expense Detail Request
    private static ExpenseDetailRequest prepareExpenseDetailRequest(String expenseDetailId, String expenseHeaderId) {
        ExpenseDetailRequest expenseDetailRequest = new ExpenseDetailRequest();

        UserInfo userInfo = CachingManager.getUserInfo();

        if (userInfo != null) {
            expenseDetailRequest.setApproverId(userInfo.getResourceId());
            expenseDetailRequest.setApproverCompanyId(userInfo.getCompanyId());
            expenseDetailRequest.setExpenseDetailID(expenseDetailId);
            expenseDetailRequest.setExpenseHeaderID(expenseHeaderId);
            expenseDetailRequest.setIsApprover(AppConstant.IS_APPROVER_FLAG);

        }

        return expenseDetailRequest;
    }

    //get ApprovalRejection
    public static ApproveReject ApprovalRejectedExpenseRequest(ApprovalRejectedExpenseContainer expenseList) {
        ApprovalRejectedExpenseResponse response = null;
        ApproveReject approveReject = null;
        try {
            String request = ParseManager.prepareJsonRequest(expenseList.getRequestData());
            JSONObject jsonArray = new JSONObject(request);
            JSONArray jsonRequestString = jsonArray.getJSONArray("Requests");
            HTTPData httpData = new HTTPData();
            httpData.setType(AppConstant.HTTP_METHOD_POST);
            Body body = new Body();
            body.setJsonRequestString(jsonRequestString.toString());
            httpData.setBody(body);

            //Call to Web Service
            String jsonResponseString = (String) WebServiceManager.callWebService(AppConstant.BASE_URL, AppConstant.APPROVE_REJECTED_EXPENSE, httpData);

            //Prepare Response Object
            response = (ApprovalRejectedExpenseResponse) ParseManager.prepareWebServiceResponseObject(ApprovalRejectedExpenseResponse.class, jsonResponseString);

            //Process Response Status
            WebServiceManager.processResponseStatus(response);

            //Prepare Response
            approveReject = prepareApproverejectResponse(response);

        } catch (Exception e) {
        }
        return approveReject;
    }

    private static ApproveReject prepareApproverejectResponse(ApprovalRejectedExpenseResponse response) throws EODException {
        ApproveReject approveReject = null;
        if (response != null) {
            approveReject = prepareApproveRejectModel(response);
        } else {
            throw new EODAppException(AppConstant.ERROR_CODE_1008);
        }

        return approveReject;
    }

    private static ApproveReject prepareApproveRejectModel(ApprovalRejectedExpenseResponse response) {
        ApproveReject approveReject = null;
        ApproveRejectData data = response.getData();
        if (data != null) {
            approveReject = new ApproveReject();

            //LstApproverReject
            ArrayList<LstApproverReject> lstApproverRejects = data.getLstApproverReject();
            if (!AppUtil.isCollectionEmpty(lstApproverRejects)) {
                ArrayList<LstApproverRejectItem> lstApproverRejectItems = new ArrayList<>();
                for (LstApproverReject lstApproverReject : lstApproverRejects) {
                    LstApproverRejectItem lstApproverRejectItem = new LstApproverRejectItem();
                    lstApproverRejectItem.setDetailId(lstApproverReject.getDetailId());
                    lstApproverRejectItem.setStrApprovalAllowed(lstApproverReject.getStrApprovalAllowed());

                    lstApproverRejectItems.add(lstApproverRejectItem);
                }
                approveReject.setLstApproverReject(lstApproverRejectItems);
            }

        }
        return approveReject;
    }

    //Check Expense Detail Response Status
    private static ExpenseDetail processExpenseDetailResponse(ExpenseDetailResponse response) throws EODException {
        ExpenseDetail expenseDetail = null;

        if (response != null) {
            expenseDetail = prepareExpenseDetailModel(response);
        } else {
            throw new EODAppException(AppConstant.ERROR_CODE_1008);
        }

        return expenseDetail;
    }

    private static ExpenseDetail prepareExpenseDetailModel(ExpenseDetailResponse response) {
        ExpenseDetail expenseDetail = null;

        ExpenseDetailData data = response.getData();
        if (data != null) {
            expenseDetail = new ExpenseDetail();

            expenseDetail.setUniqueId(data.getUniqueId());
            expenseDetail.setDate(data.getDate());
            expenseDetail.setAmount(data.getAmount());
            expenseDetail.setIsItemised(data.getIsItemised());
            expenseDetail.setBaseCategoryId(data.getBaseCategoryId());

            /*Dynamic Details*/
            ArrayList<DynamicDetail> dynamicDetails = data.getDynamicDetails();
            if (!AppUtil.isCollectionEmpty(dynamicDetails)) {
                ArrayList<com.expenseondemand.eod.model.approval.detail.dynamic.DynamicDetail> dynamicDetailList = new ArrayList<>();
                for (DynamicDetail dynamicDetail : dynamicDetails) {
                    com.expenseondemand.eod.model.approval.detail.dynamic.DynamicDetail dynamicDetailItem = new com.expenseondemand.eod.model.approval.detail.dynamic.DynamicDetail();
                    dynamicDetailItem.setValue(dynamicDetail.getValue());
                    dynamicDetailItem.setLabel(dynamicDetail.getLabelName());
                    dynamicDetailList.add(dynamicDetailItem);
                }
                expenseDetail.setDynamicDetails(dynamicDetailList);
            }

            /*Project Details*/
            ArrayList<ProjectDetails> projectDetails = data.getProjectDetails();
            if (!AppUtil.isCollectionEmpty(projectDetails)) {
                ArrayList<ProjectDetailsItem> projectDetailItems = new ArrayList<>();
                for (ProjectDetails projectDetail : projectDetails) {
                    ProjectDetailsItem projectDetailItem = new ProjectDetailsItem();
                    projectDetailItem.setLabelName(projectDetail.getLabelName());
                    projectDetailItem.setValue(projectDetail.getValue());

                    projectDetailItems.add(projectDetailItem);
                }
                expenseDetail.setProjectDetails(projectDetailItems);
            }

            /*Payment Type*/
            PaymentType paymentType = data.getPaymentType();
            if (paymentType != null) {
                PaymentTypeItem paymentTypeItem = new PaymentTypeItem();
                paymentTypeItem.setLabelName(paymentType.getLabelName());
                paymentTypeItem.setValue(paymentType.getValue());
                expenseDetail.setPaymentType(paymentTypeItem);
            }

            /*Business Purpose*/
            BusinessPurpose businessPurpose = data.getBusinessPurpose();
            if (businessPurpose != null) {
                BusinessPurposeItem businessPurposeItem = new BusinessPurposeItem();
                businessPurposeItem.setLabelName(businessPurpose.getLabelName());
                businessPurposeItem.setValue(businessPurpose.getValue());
                expenseDetail.setBusinessPurpose(businessPurposeItem);
            }

            /*Additional Details*/
            ArrayList<AdditionalDetails> additionalDetailses = data.getAdditionalDetails();
            if (!AppUtil.isCollectionEmpty(additionalDetailses)) {
                ArrayList<AdditionalDetailsItem> additionalDetailsItems = new ArrayList<>();
                for (AdditionalDetails additionalDetails : additionalDetailses) {
                    AdditionalDetailsItem additionalDetailsItem = new AdditionalDetailsItem();
                    additionalDetailsItem.setLabelName(additionalDetails.getLabelName());
                    additionalDetailsItem.setValue(additionalDetails.getValue());
                    additionalDetailsItems.add(additionalDetailsItem);
                }
                expenseDetail.setAdditionalDetails(additionalDetailsItems);
            }

            /*Receipt*/
            Receipt receipt = data.getReceipt();
            if (receipt != null) {
                ReceiptItem receiptItem = new ReceiptItem();
                receiptItem.setLabelName(receipt.getLabelName());
                receiptItem.setCount(receipt.getCount());
                receiptItem.setValue(receipt.getValue());
                expenseDetail.setReceipt(receiptItem);
            }

            /*Notes*/
            Notes notes = data.getNotes();
            if (notes != null) {
                com.expenseondemand.eod.model.approval.detail.notes.Notes notesModel = new com.expenseondemand.eod.model.approval.detail.notes.Notes();
                notesModel.setCcNotes(notes.getCcNotes());
                notesModel.setMobileNotes(notes.getMobileNotes());
                notesModel.setEnteredNotes(notes.getEnteredNotes());
                /*Pre Approval array list*/
                ArrayList<PreApprovedDetails> preApprovedDetailses = notes.getPreApprovedDetails();
                if (!AppUtil.isCollectionEmpty(preApprovedDetailses)) {
                    ArrayList<PreApprovedDetailsItem> preApprovedDetailsItems = new ArrayList<>();
                    for (PreApprovedDetails preApprovedDetails : preApprovedDetailses) {
                        PreApprovedDetailsItem preApprovedDetailsItem = new PreApprovedDetailsItem();
                        preApprovedDetailsItem.setLabelName(preApprovedDetails.getLabelName());
                        preApprovedDetailsItem.setValue(preApprovedDetails.getValue());
                        preApprovedDetailsItems.add(preApprovedDetailsItem);
                    }
                    notesModel.setPreApprovedDetails(preApprovedDetailsItems);
                }

                expenseDetail.setNotes(notesModel);
            }

            /*Mileage*/
            Mileage mileage = data.getMileage();
            if (mileage != null) {
                MultipleMileage multipleMileage = new MultipleMileage();
                multipleMileage.setVehicleID(mileage.getVehicleId());

                ArrayList<MileageLeg> mileageLegs = mileage.getMileageLegs();
                if (!AppUtil.isCollectionEmpty(mileageLegs)) {
                    ArrayList<MultipleMileageItem> multipleMileageItems = new ArrayList<>();
                    for (MileageLeg mileageLeg : mileageLegs) {
                        MultipleMileageItem multipleMileageItem = new MultipleMileageItem();
                        multipleMileageItem.setAmount(mileageLeg.getAmount());
                        multipleMileageItem.setDestinationAddress(mileageLeg.getTo());
                        multipleMileageItem.setSourceAddress(mileageLeg.getFrom());
                        multipleMileageItem.setDistance(mileageLeg.getMiles());
                        multipleMileageItems.add(multipleMileageItem);
                    }
                    multipleMileage.setMultipleMileageItems(multipleMileageItems);
                }

                expenseDetail.setMileage(multipleMileage);
            }

            /*Attendees*/
            ArrayList<Attendees> attendees = data.getAttendees();
            if (!AppUtil.isCollectionEmpty(attendees)) {
                ArrayList<AttendeesItem> attendeesItems = new ArrayList<>();
                for (Attendees attendeesData : attendees) {
                    AttendeesItem attendeesItem = new AttendeesItem();
                    attendeesItem.setCost(attendeesData.getAmount());
                    attendeesItem.setLevel(attendeesData.getLabel());
                    attendeesItem.setName(attendeesData.getName());
                    attendeesItem.setType(attendeesData.getType());
                    attendeesItem.setValue(attendeesData.getValue());

                    attendeesItems.add(attendeesItem);
                }

                expenseDetail.setAttendees(attendeesItems);
            }
        }
        return expenseDetail;
    }

    //get multiple category exepense
    public static ArrayList<MultiCategoryExpense> getExpenseMultipleCategory(MultiCategoryExpenseParam multiCategoryExpenseParam) throws EODException {
        ExpenseMultipleCategoryResponse response = null;
        //Prepare Request Object
        ExpenseMultipleCategoryRequest request = prepareExpenseMultipleCategoryRequest(multiCategoryExpenseParam);

        //Prepare JSON Request String
        String jsonRequestString = null;
        jsonRequestString = ParseManager.prepareJsonRequest(request);

        //Prepare http request
        HTTPData httpData = new HTTPData();
        httpData.setType(AppConstant.HTTP_METHOD_POST);
        Body body = new Body();
        body.setJsonRequestString(jsonRequestString);
        httpData.setBody(body);

        //Call to Web Service
        String jsonResponseString = (String) WebServiceManager.callWebService(AppConstant.BASE_URL, AppConstant.EXPENSE_MULTIPLE_CATEGORY_LIST, httpData);

        //Prepare Response Object
        response = (ExpenseMultipleCategoryResponse) ParseManager.prepareWebServiceResponseObject(ExpenseMultipleCategoryResponse.class, jsonResponseString);

        //Process Response Status
        WebServiceManager.processResponseStatus(response);

        //Process Response Status
        ArrayList<MultiCategoryExpense> expenseMultiCategoryList = processExpenseMultipleCategoryResponse(response.getData());

        return expenseMultiCategoryList;
    }

    private static ArrayList<MultiCategoryExpense> processExpenseMultipleCategoryResponse(ArrayList<MultiCategoryExpenseData> responseExpenseMultiCategoryList) {
        ArrayList<MultiCategoryExpense> expenseMultiCategoryItemsList = null;
        if (!AppUtil.isCollectionEmpty(responseExpenseMultiCategoryList)) {
            //New List
            expenseMultiCategoryItemsList = new ArrayList<MultiCategoryExpense>();

            for (MultiCategoryExpenseData multiCategoryExpenseData : responseExpenseMultiCategoryList) {
                //New Expense Multiple Category list
                MultiCategoryExpense expenseItemsModel = new MultiCategoryExpense();
                expenseItemsModel.setUniqueId(multiCategoryExpenseData.getUniqueId());
                expenseItemsModel.setDate(multiCategoryExpenseData.getDate());
                expenseItemsModel.setAmount(multiCategoryExpenseData.getAmount());
                expenseItemsModel.setExpneseCategory(multiCategoryExpenseData.getExpneseCategory());
                expenseItemsModel.setExpenseCategoryId(multiCategoryExpenseData.getExpenseCategoryId());
                expenseItemsModel.setBaseCategoryId(multiCategoryExpenseData.getBaseCategoryId());
                expenseItemsModel.setExpenseItemizationId(multiCategoryExpenseData.getExpenseItemizationId());
                expenseItemsModel.setPolicyBreech(preparePolicyDetails(multiCategoryExpenseData.getPolicyBreech()));
                expenseItemsModel.setDuplicate(multiCategoryExpenseData.getIsDuplicate());
                expenseItemsModel.setDuplicateItems(multiCategoryExpenseData.getDuplicateItems());

                expenseMultiCategoryItemsList.add(expenseItemsModel);
            }
        }

        return expenseMultiCategoryItemsList;
    }

    private static PolicyBreachModel preparePolicyDetails(PolicyBreechResponseModel policyBreechResponseModel) {
        //PolicyDetails
        PolicyBreachModel policyBreachModel = new PolicyBreachModel();

        if (policyBreechResponseModel != null) {
            policyBreachModel.setIsExpenseLimit(policyBreechResponseModel.getIsExpenseLimit());
            policyBreachModel.setExpenseDetailId(policyBreechResponseModel.getExpenseDetailId());
            policyBreachModel.setRowId(policyBreechResponseModel.getRowId());
            policyBreachModel.setIsDailyCap(policyBreechResponseModel.getIsDailyCap());
            policyBreachModel.setIsMaxSpendBreach(policyBreechResponseModel.getIsMaxSpendBreach());
            policyBreachModel.setIsPreApproval(policyBreechResponseModel.getIsPreApproval());
            policyBreachModel.setAmountSetLimit(policyBreechResponseModel.getAmountSetLimmit());
            policyBreachModel.setAmountBreach(policyBreechResponseModel.getAmountBreach());
            policyBreachModel.setStatus(policyBreechResponseModel.getStatus());
            policyBreachModel.setExpenseLimit(policyBreechResponseModel.getExpenseLimit());
            policyBreachModel.setMaxSpendamt(policyBreechResponseModel.getMaxSpendamt());
            policyBreachModel.setDailCapAmt(policyBreechResponseModel.getDailCapAmt());
            policyBreachModel.setNotes(policyBreechResponseModel.getNotes());
        }
        return policyBreachModel;
    }

    private static ExpenseMultipleCategoryRequest prepareExpenseMultipleCategoryRequest(MultiCategoryExpenseParam expenseCategoryExpenseList) {
        ExpenseMultipleCategoryRequest expenseDetailRequest = new ExpenseMultipleCategoryRequest();
        UserInfo userInfo = CachingManager.getUserInfo();
        if (userInfo != null) {
            expenseDetailRequest.setExpenseHeaderID(expenseCategoryExpenseList.getExpenseHeaderID());
            expenseDetailRequest.setExpenseDetailID(expenseCategoryExpenseList.getExpenseDetailID());
            expenseDetailRequest.setCompanyId(expenseCategoryExpenseList.getCompanyId());
            expenseDetailRequest.setIsMultiPayment(expenseCategoryExpenseList.getIsMultiPayment());
            expenseDetailRequest.setDate(expenseCategoryExpenseList.getDate());
        }
        return expenseDetailRequest;
    }

    /*Expense details receipt*/
    public static ExpenseDetailsReceipt getExpenseDetailReceipt(String[] param) throws EODException {
        ExpenseDetailReceiptResponse response;
        //Prepare Request Object
        ExpenseDetailReceiptRequest request = prepareExpenseDetailReceiptRequest(param);
        //Prepare JSON Request String
        String jsonReqReceipt = ParseManager.prepareJsonRequest(request);

        //Prepare http request
        HTTPData httpData = new HTTPData();
        httpData.setType(AppConstant.HTTP_METHOD_POST);
        Body body = new Body();
        body.setJsonRequestString(jsonReqReceipt);
        httpData.setBody(body);

        //Call to Web Service
        String jsonResponseString = (String) WebServiceManager.callWebService(AppConstant.BASE_URL, AppConstant.EXPENSE_DETAIL_RECEIPT, httpData);

        //Prepare Response Object
        response = (ExpenseDetailReceiptResponse) ParseManager.prepareWebServiceResponseObject(ExpenseDetailReceiptResponse.class, jsonResponseString);

        //Process Response Status
        WebServiceManager.processResponseStatus(response);

        //Process Response Status
        ExpenseDetailsReceipt receipt = processExpenseDetailReceiptResponse(response);

        return receipt;
    }

    private static ExpenseDetailsReceipt processExpenseDetailReceiptResponse(ExpenseDetailReceiptResponse response) throws EODException {

        ExpenseDetailsReceipt expenseDetailReceipt = null;

        if (response != null) {
            expenseDetailReceipt = prepareExpenseDetailReceiptModel(response);
        } else {
            throw new EODAppException(AppConstant.ERROR_CODE_1008);
        }

        return expenseDetailReceipt;

    }

    private static ExpenseDetailsReceipt prepareExpenseDetailReceiptModel(ExpenseDetailReceiptResponse expenseDetailReceipt) {
        ExpenseDetailsReceipt receipt = null;

        ExpenseDetailsReceiptData data = expenseDetailReceipt.getData();
        if (data != null) {
            receipt = new ExpenseDetailsReceipt();
            receipt.setAmount(data.getAmount());
            receipt.setCurrencyCode(data.getCurrencyCode());
            receipt.setDescription(data.getDescription());
            receipt.setDocument(data.getDocument());
            receipt.setExpenseAmount(data.getExpenseAmount());
            receipt.setExpenseSupportDocID(data.getExpenseSupportDocID());
            receipt.setReferenceNumber(data.getReferenceNumber());
            receipt.setVarificationAmount(data.getVarificationAmount());
            receipt.setVarified(data.getVarified());
            receipt.setVarifiedBy(data.getVarifiedBy());
        }
        return receipt;
    }

    private static ExpenseDetailReceiptRequest prepareExpenseDetailReceiptRequest(String[] param) {
        ExpenseDetailReceiptRequest request = new ExpenseDetailReceiptRequest();
        UserInfo userInfo = CachingManager.getUserInfo();
        if (userInfo != null) {
            request.setExpenseId(param[0]);
            request.setClaimid(param[1]);
            request.setCompanyID(userInfo.getCompanyId());
        }
        return request;
    }
}
