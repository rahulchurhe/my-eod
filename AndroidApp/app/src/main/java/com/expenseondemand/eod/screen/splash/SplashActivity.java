package com.expenseondemand.eod.screen.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.activity.BaseActivity;
import com.expenseondemand.eod.screen.authentication.LoginActivity;
import com.expenseondemand.eod.screen.home.HomeActivity;
import com.expenseondemand.eod.manager.PersistentManager;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.UIUtil;

public class SplashActivity extends BaseActivity
{
	//Handler to finish the activity after some time of delay in displaying Splash screen
	private Handler handler;
	//ImageView for splash screen UI
    private ImageView imageViewSplashHand;
	private ImageView imageViewSplashRedHand;

	Animation splash_top_to_down;
	Animation splash_down_to_top;
	//Runnable - To launch Login Activity
	private Runnable launchActivityRunnable = new ActivityRunnable();

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.splash_activity_layout);

		//InitializeView
		initializeView();
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		handler.postDelayed(launchActivityRunnable, AppConstant.TIME_SPLASH_DELAY_IN_MILLISEC);
	}

	@Override
	protected void onPause()
	{
		super.onPause();

		handler.removeCallbacks(launchActivityRunnable);
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}

	//InitializeView
	private void initializeView() 
	{
		// Initializing Handler
		imageViewSplashHand = (ImageView)findViewById(R.id.imageViewSplashHand);
		imageViewSplashRedHand = (ImageView)findViewById(R.id.imageViewSplashRedHand);

		setAnimation();

		handler = new Handler();
	}

	//Runnable - to Launch Login Activity
	private class ActivityRunnable implements Runnable
	{
		@Override
		public void run() 
		{
			identifyNavigation();
		}
	}

	//set animation for top bottom imageview
	private void setAnimation()
	{

		splash_top_to_down = UIUtil.getAnimationInstanse(R.anim.splash_top_to_down);
		imageViewSplashHand.setAnimation(splash_top_to_down);

		splash_down_to_top = UIUtil.getAnimationInstanse(R.anim.splash_down_to_top);
		imageViewSplashRedHand.setAnimation(splash_down_to_top);
	}


	//  identifyNavigation
	  void identifyNavigation()
	  {

		  if(PersistentManager.isUserRemembered())
		  {
			  navigateToHomeScreen();
		  }
		  else
		  {
			  navigateToLoginScreen();
		  }

		  SplashActivity.this.finish();
	  }

	//navigate to home scree
	 void navigateToHomeScreen()
	 {
		 Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
		 intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		 startActivity(intent);
	 }

	//navigate to Login scree
	void navigateToLoginScreen()
	{
		Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}
}
