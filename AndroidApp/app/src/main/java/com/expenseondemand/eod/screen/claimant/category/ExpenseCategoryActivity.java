package com.expenseondemand.eod.screen.claimant.category;

import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.activity.BaseActivity;
import com.expenseondemand.eod.callbackInterface.SharedClickListenerInteface;
import com.expenseondemand.eod.dialogFragment.ApplicationErrorAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationNetworkDialog;
import com.expenseondemand.eod.exception.EODBusinessException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.AuthenticationManager;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.DbManager;
import com.expenseondemand.eod.manager.DeviceManager;
import com.expenseondemand.eod.manager.PersistentManager;
import com.expenseondemand.eod.model.ExpenseCategory;
import com.expenseondemand.eod.model.UserCredential;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.screen.authentication.LoginActivity;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.util.UIUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class ExpenseCategoryActivity extends BaseActivity implements SharedClickListenerInteface, ApplicationErrorAlertDialog.ErrorDialogActionListener, LoadCategoryReceiver.LoadCategoryListener {
    private static final int CATEGORY_WAIT_TIME = 4000;
    private RecyclerView recyclerViewExpenseCategory;
    private ExpenseCategoryAdapter expenseCategoryAdapter;
    private ArrayList<ExpenseCategory> expenseCategoryList;
    private boolean isAuthenticationRetried;
    private ApplicationErrorAlertDialog applicationErrorAlertDialog;
    private ApplicationNetworkDialog applicationNetworkDialogl;
    private LoadCategoryReceiver loadCategoryReceiver;
    private Dialog dialog = null;
    private Timer timer;
    private TimerTask timerTask;
    private ArrayList<ExpenseCategory> expenseCategoryListLocal1;

    //we are going to use a handler to be able to run in our TimerTask
    final Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expense_category_activity_layout);

        //IntializeView
        intializeView();
    }

    //IntializeView
    private void intializeView() {
        //Initialize expense category list
        expenseCategoryList = new ArrayList<ExpenseCategory>();

        //Handle Action Bar
        handleActionBar(this, 0, getString(R.string.title_select_category));

        //Handle Toolbar Bar
        handleToolBar(getString(R.string.title_select_category), R.color.home_item_create_expense_selected, R.color.home_item_create_expense, true);

        //Create Adapter
        expenseCategoryAdapter = new ExpenseCategoryAdapter(ExpenseCategoryActivity.this, expenseCategoryList);

        //Get RecyclerView
        recyclerViewExpenseCategory = (RecyclerView) findViewById(R.id.recyclerViewExpenseCategory);

        //Set Adapter to RecyclerView
        recyclerViewExpenseCategory.setAdapter(expenseCategoryAdapter);

        //Set Recycer View ListItem Click Listener
        expenseCategoryAdapter.setOnShareClickedListener(this);
        if (DeviceManager.isOnline()) {
            handleRefreshCategory();
        }

    }

    private void startTimer() {
        timer = new Timer();

        initializeTimerTask();

        System.out.println("time start--" + Calendar.getInstance().get(Calendar.MILLISECOND));
        timer.schedule(timerTask, CATEGORY_WAIT_TIME);
    }

    private void stopTimer() {
        //Dismiss progress dialog.
        UIUtil.dismissDialog(dialog);

        if (timer != null) {
            timer.cancel();
            timer = null;
        }

    }

    private void initializeTimerTask() {
        timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        System.out.println("timer stop--" + Calendar.getInstance().get(Calendar.MILLISECOND));

                        getExpenseCategoryList();

                    }
                });
            }
        };

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Get Menu Inflater
        MenuInflater inflater = getMenuInflater();

        //Inflate Menu
        inflater.inflate(R.menu.expense_category_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuItemId = item.getItemId();

        switch (menuItemId) {
            case R.id.ExpenseCategoryRefresh:
                //Handle Refreash
                handleRefreshCategory();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    //override method for recyclerview list item click listener
    @Override
    public void ShareClicked(int position) {
        handleOnClickExpenseCategory(position);
    }

    //Handle Click on Expense Category
    public void handleOnClickExpenseCategory(int position) {
        //Get Expense category
        ExpenseCategory expenseCategory = expenseCategoryList.get(position);

        //Create Intent
        Intent intent = new Intent();
        intent.putExtra(AppConstant.KEY_CATEGORY_NAME, expenseCategory.getCategoryName()); //Category Name
        intent.putExtra(AppConstant.KEY_CATEGORY_CODE, expenseCategory.getCategoryId()); //Category Code
        intent.putExtra(AppConstant.KEY_BASE_CATEGORY_CODE, expenseCategory.getBaseCategoryId()); //Base Category Code

        //Set Result
        setResult(RESULT_OK, intent);

        //Finish This Activity
        this.finish();
        overridePendingTransition(R.anim.slide_in_back, R.anim.slide_out_back);
    }

    //Get Expense Categories
    private void getExpenseCategoryList() {
        //AsyncTask
        GetExpenseCategoryListAsyncTask asyncTask = new GetExpenseCategoryListAsyncTask();
        asyncTask.execute();
    }


    /**
     * Async Task
     */
    private class GetExpenseCategoryListAsyncTask extends AsyncTask<Void, Void, ArrayList<ExpenseCategory>> {
        private Exception exception = null;
        private ArrayList<ExpenseCategory> expenseCategoryListLocal;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected ArrayList<ExpenseCategory> doInBackground(Void... params) {

            //Get Expense Category List from DB
            try {

                expenseCategoryListLocal = GlobalDataManager.getSortedExpenseCategoryList();


            } catch (EODException exception) {

                this.exception = exception;
            }


            return expenseCategoryListLocal;
        }

        @Override
        protected void onPostExecute(ArrayList<ExpenseCategory> expenseCategoryListLocal) {

            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);


            if (exception == null) {
                if (expenseCategoryListLocal != null) {
                    //On Success
                    onSuccessGetExpenseCategoryList(expenseCategoryListLocal);
                } else {
                    //On Fail
                    onFailGetExpenseCategoryList();
                }

            } else {
                //On Fail
                onFailGetExpenseCategoryList();
            }

        }
    }

    //handle category sync first time


    //On Success
    public void onSuccessGetExpenseCategoryList(ArrayList<ExpenseCategory> expenseCategoryListLocal) {

        //Set Category List
        expenseCategoryList.clear();
        expenseCategoryList.addAll(expenseCategoryListLocal);

        //Convert Category Names to CamelCase
        toCamelCase();

        //Notify Adapter
        expenseCategoryAdapter.notifyDataSetChanged();
    }


    //On Fail
    public void onFailGetExpenseCategoryList() {
        //UIUtil.showToast(getString(R.string.text_error_processing));

    }

    //Convert Category Names to Camel Case
    private void toCamelCase() {
        if (!AppUtil.isCollectionEmpty(expenseCategoryList)) {
            for (ExpenseCategory expenseCategory : expenseCategoryList) {
                if (expenseCategory != null) {
                    expenseCategory.setCategoryName(AppUtil.convertToCamelCase(expenseCategory.getCategoryName()));
                }
            }
        }
    }


    //Handle Referesh Category
    private void handleRefreshCategory() {
        //Check if Network Available
        if (DeviceManager.isOnline()) {
            //Sync Expense Category Async Task
            SyncExpenseCategoriesAsyncTask syncExpenseCategoriesAsyncTask = new SyncExpenseCategoriesAsyncTask();
            syncExpenseCategoriesAsyncTask.execute();
        } else {
            //Show Network Error Dialog
            enableNetwork();
        }
    }


    /**
     * Async Task
     */
    private class SyncExpenseCategoriesAsyncTask extends AsyncTask<Void, Void, Void> {
        private Dialog dialog = null;
        private Exception exception = null;

        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(ExpenseCategoryActivity.this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                //Sync Expense Categories
                expenseCategoryListLocal1 = GlobalDataManager.syncExpenseCategory();
            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);

            if (exception == null) {
                //On Success
                onSuccessSyncExpenseCategory();
            } else {
                //On Fail
                onErrorSyncExpenseCategory();
            }
        }
    }

    //On Success
    public void onSuccessSyncExpenseCategory() {
        //Do Nothing...
        //Set Category List
        expenseCategoryList.clear();
        expenseCategoryList.addAll(expenseCategoryListLocal1);

        //Convert Category Names to CamelCase
        toCamelCase();

        //Notify Adapter
        expenseCategoryAdapter.notifyDataSetChanged();
    }

    //On Fail
    public void onErrorSyncExpenseCategory() {
        if (isAuthenticationRetried) //Already Retried
        {
            //Show Error Alert Dialog
            showErrorAlertDialog(true, null, null, null, 0);
        } else //Not yet Retried
        {
            //Authenticate User Again to obtain new Token
            authenticateUser();
        }
    }

    //Authenticate User
    private void authenticateUser() {
        //UserCredentials
        UserCredential userCredential = null;

        //Get UserInfo
        UserInfo userInfo = CachingManager.getUserInfo();

        if (userInfo != null) {
            //Get User Credentials
            userCredential = userInfo.getUserCredential();
        }

        try {
            if (userCredential != null) {
                //Autheticate User AsyncTask
                AuthenticateUserAsyncTask asyncTask = new AuthenticateUserAsyncTask();
                asyncTask.execute(userCredential);
            } else {
                navigateToLogin();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //Navigate to Login Screen
    private void navigateToLogin() {
        //Get user is remembered or not
        boolean isUserRemembered = false;

        //Persist isUserRemembered
        PersistentManager.persistIsUserRemembered(isUserRemembered);
        //Create Intent
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        this.finish();
    }

    //Authenticate User AsyncTask
    private class AuthenticateUserAsyncTask extends AsyncTask<UserCredential, Void, UserInfo> {
        private Dialog dialog = null;
        private Exception exception = null;

        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(ExpenseCategoryActivity.this);
        }

        @Override
        protected UserInfo doInBackground(UserCredential... params) {
            UserInfo userInfo = null;
            try {
                //Authenticate User
                userInfo = AuthenticationManager.authenticateUser(params[0]);

                //Insert User Information In Database
                DbManager.saveUserInformation(userInfo);
            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return userInfo;
        }

        @Override
        protected void onPostExecute(UserInfo user) {
            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);

            if (exception == null) {
                //OnSucess
                onSuccessAuthenticateUser(user);
            } else if (exception instanceof EODBusinessException) {
                //OnFail
                onFailLogin((EODException) exception);
            } else if (exception instanceof EODException) {
                //OnError
                onErrorLogin(exception);
            }
        }
    }

    //OnSucess
    public void onSuccessAuthenticateUser(UserInfo user) {
        if (user != null) {
            //Retry Refresh Categories AsyncTask
            handleRefreshCategory();

            //Set Flag True - Async task is retried
            isAuthenticationRetried = true;
        }
    }

    //OnFail
    public void onFailLogin(EODException exception) {
        //Show Error Alert Dialog
        showErrorAlertDialog(true, null, null, null, 0);
    }

    //OnError
    public void onErrorLogin(Exception exception) {
        //Show Error Alert Dialog
        showErrorAlertDialog(true, null, null, null, 0);
    }

    void showErrorAlertDialog(boolean neutralButton, String message, String title, String okButtonTitle, int type) {
        applicationErrorAlertDialog = new ApplicationErrorAlertDialog().getInstance(this, message, title, okButtonTitle, type);
        applicationErrorAlertDialog.show(getSupportFragmentManager(), "dialog");
    }

    void enableNetwork() {
        applicationNetworkDialogl = new ApplicationNetworkDialog().getInstance(ExpenseCategoryActivity.this);
        applicationNetworkDialogl.show(getSupportFragmentManager(), "dialog");
    }

    //handle action button
    @Override
    public void onRetryButtonClick(int type) {
        // handle if required
        try {
            expenseCategoryListLocal1 = GlobalDataManager.syncExpenseCategory();
        } catch (EODException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCancelButtonClick() {

    }


    @Override
    protected void onResume() {
        super.onResume();

        // initialize dialog
        dialog = UIUtil.showProgressDialog(ExpenseCategoryActivity.this);

        //Check if timer required
        if (PersistentManager.getLastCategoriesSyncDate() <= 0) {
            registerReceiver();
            startTimer();
        } else
            //Get Expense Categories
            getExpenseCategoryList();
    }

    @Override
    protected void onPause() {
        if (loadCategoryReceiver != null)
            unregisterReceiver(loadCategoryReceiver);
        super.onPause();
    }

    //function to register receiver :3
    private void registerReceiver() {
        //Register BroadcastReceiver
        //to receive event from our service
        loadCategoryReceiver = new LoadCategoryReceiver(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(LoadCategoryService.PASS_SERVICE_STATUS);
        registerReceiver(loadCategoryReceiver, intentFilter);
    }

    @Override
    public void handleCategoryDataReceived(String resultStatus) {
        //Log.d(LoadCategoryService.PASS_SERVICE_STATUS_KEY,resultStatus);
        System.out.println("LoadCategoryService.PASS_SERVICE_STATUS_KEY--" + resultStatus);

        //Dismiss progress dialog.
        UIUtil.dismissDialog(dialog);

        // stop timer
        stopTimer();

        if (resultStatus.equals(LoadCategoryService.SERVICE_FINISHED)) {

            getExpenseCategoryList();
        } else {
            // get error status from intent service
            onFailGetExpenseCategoryList();

        }
    }


}
