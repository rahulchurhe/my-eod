package com.expenseondemand.eod.model.approval.detail.dynamic;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ramit Yadav on 01-10-2016.
 */

public class DynamicDetail implements Parcelable
{
    private String label;
    private String value;

    public DynamicDetail(Parcel in)
    {
        label = in.readString();
        value = in.readString();
    }

    public DynamicDetail()
    {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(label);
        dest.writeString(value);
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    public static final Creator<DynamicDetail> CREATOR = new Creator<DynamicDetail>() {
        @Override
        public DynamicDetail createFromParcel(Parcel in) {
            return new DynamicDetail(in);
        }

        @Override
        public DynamicDetail[] newArray(int size) {
            return new DynamicDetail[size];
        }
    };

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }
}
