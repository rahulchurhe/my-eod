package com.expenseondemand.eod.screen.approval.expense.itemlist;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.activity.BaseActivity;
import com.expenseondemand.eod.dialogFragment.ApplicationAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationErrorAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationNetworkDialog;
import com.expenseondemand.eod.dialogFragment.ExpenseAlertDialog;
import com.expenseondemand.eod.exception.EODBusinessException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.DeviceManager;
import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.model.AppRejectionSetting;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.model.approval.detail.ApproveReject;
import com.expenseondemand.eod.model.approval.detail.project.LstApproverRejectItem;
import com.expenseondemand.eod.model.approval.expense.ExpenseInfo;
import com.expenseondemand.eod.screen.approval.detail.ApproveRejectedInterface;
import com.expenseondemand.eod.screen.approval.detail.ExpenseDetailActivity;
import com.expenseondemand.eod.screen.approval.detail.ExpenseDetailManager;
import com.expenseondemand.eod.screen.approval.expense.duplicate.DuplicateEntryActivity;
import com.expenseondemand.eod.screen.approval.expense_acceptance_reason.ApproverAcceptDialog;
import com.expenseondemand.eod.screen.approval.expense_rejection_reason.RejectionReasonDialog;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.util.UIUtil;
import com.expenseondemand.eod.webservice.model.RejectionReasonModel.RejectionReasonModel;
import com.expenseondemand.eod.webservice.model.approval.detail.ApprovalRejectedExpenseContainer;
import com.expenseondemand.eod.webservice.model.approval.detail.ApprovalRejectedExpenseRequest;
import com.expenseondemand.eod.webservice.model.approval.detail.ApprovalRejectedExpensesRequestData;
import com.expenseondemand.eod.webservice.model.approval.item.DuplicateItemsModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ExpenseListActivity extends BaseActivity implements ExpenseListAdapter.ApproverExpenseListClickListener, EndlessExpenseListView.EndLessListener, View.OnClickListener, ExpenseAlertDialog.AlertDialogActionListener, ApplicationAlertDialog.AlertDialogActionListener, ApproveRejectedInterface, ApplicationErrorAlertDialog.ErrorDialogActionListener {

    public static final int TYPE_ACCEPT = 0;
    public static final int TYPE_ALERT = 1;
    private RecyclerView recyclerViewExpenseList;
    private Button button_approve;
    private Button button_reject;
    private CheckBox checkBoxSelectAll;
    RelativeLayout relativeOverlayScreenExpense;
    ApplicationAlertDialog applicationAlertDialog;
    ArrayList<RejectionReasonModel> rejectionReasonBackupList;
    ArrayList<ExpenseItemsModel> approverExpenseItemsList;
    private String claimId;

    private String claimType, claimName;
    private String userName;

    ArrayList<ExpenseItemsModel> arrayListCheckData = new ArrayList<ExpenseItemsModel>();
    private String str_rejectionNote, str_rejectionId, str_approveNote = null, str_acounterApproveNote = null;
    private ApplicationNetworkDialog applicationNetworkDialogl;
    private boolean isPolicyBreech = false;
    private boolean isNonPolicyBreech = false;
    private boolean isRejectedStatus;
    private boolean isRejected = false;
    private int isReceptCount;
    AppRejectionSetting appRejectionSetting;
    private boolean isShowRejectionPopup;
    private boolean isMultiExpenseReject;
    private ExpenseAlertDialog expenseAlertDialog;
    private ApplicationErrorAlertDialog applicationErrorAlertDialog;
    private int TYPE_LOADLIST = 0;


    private View footerView;
    private boolean isLoadingMoreExpenses;
    int counter = 10;
    int pageNo = 1;
    private ExpenseListAdapter adapter;
    private EndlessExpenseListView eLView;
    private Dialog dialog1 = null;
    private Dialog dialog = null;

    @Override
    public void onBackPressed() {
        if (relativeOverlayScreenExpense.getVisibility() == View.GONE) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (SharePreference.getBoolean(getApplicationContext(), AppConstant.BACK_EXPENSE_DETAIL)) {
            SharePreference.putBoolean(getApplicationContext(), AppConstant.BACK_EXPENSE_DETAIL, false);
            arrayListCheckData.clear();
            checkBoxSelectAll.setChecked(false);
            setButtonEnableFalse();
            pageNo = 1;
            approverExpenseItemsList.clear();
            adapter.notifyDataSetChanged();
            getExpenseItemsList();
        }

        /*Back from network setting screen */
        if (SharePreference.getBoolean(getApplicationContext(), AppConstant.BACK_NETWORK_SETTING_STAT)) {
            SharePreference.putBoolean(getApplicationContext(), AppConstant.BACK_NETWORK_SETTING_STAT, false);
            pageNo = 1;
            approverExpenseItemsList.clear();
            adapter.notifyDataSetChanged();
            getExpenseItemsList();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approver_expense_list);
        populateData();
        initializeView();
    }
    //Get key values from bundle
    private void populateData() {
        UserInfo userInfo = CachingManager.getUserInfo();
        if (userInfo != null) {
            userName = userInfo.getDisplayName();
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            claimId = bundle.getString(AppConstant.KEY_CLAIM_ID);
            claimType = bundle.getString(AppConstant.KEY_CLAIM_TYPE);
            claimName = bundle.getString(AppConstant.KEY_CLAIM_NAME);
        }

        //Get App Rejected Setting rules.
        appRejectionSetting = CachingManager.getAppRejectedSetting();

    }

    private void initializeView() {
        TextView textViewClaimantName = (TextView) findViewById(R.id.textViewClaimantName);
        TextView textViewClaimTitle = (TextView) findViewById(R.id.textViewClaimTitle);

        //Set claim name and type
        textViewClaimantName.setText(claimName);
        textViewClaimTitle.setText(claimType);
        textViewClaimantName.setSelected(true);

        approverExpenseItemsList = new ArrayList<ExpenseItemsModel>();
        rejectionReasonBackupList = new ArrayList<RejectionReasonModel>();

        //Handle Action Bar
        handleToolBar(getResources().getString(R.string.text_expense_list), R.color.approval_claim_list_toolbar, R.color.approval_claim_list_status_bar, true);

        //Get CheckBox
        checkBoxSelectAll = (CheckBox) findViewById(R.id.checkBoxSelectAll);
        checkBoxSelectAll.setOnClickListener(this);

        //Set approve  button
        button_approve = (Button) findViewById(R.id.button_approve);
        button_approve.setTextColor(getResources().getColor(R.color.white));
        button_approve.setEnabled(false);
        button_approve.setOnClickListener(this);

        //Set reject  button
        button_reject = (Button) findViewById(R.id.button_reject);
        button_reject.setTextColor(getResources().getColor(R.color.white));
        button_reject.setEnabled(false);
        button_reject.setOnClickListener(this);

        button_approve.setBackgroundResource(R.drawable.expense_approve_button_disable_selector);
        button_reject.setBackgroundResource(R.drawable.expense_reject_button_disable_selector);

        //overlay close button
        ImageButton imageViewCloseButton = (ImageButton) findViewById(R.id.imageViewCloseButton);
        imageViewCloseButton.setOnClickListener(this);

        //overlay layout
        relativeOverlayScreenExpense = (RelativeLayout) findViewById(R.id.relativeOverlayScreenExpense);
        relativeOverlayScreenExpense.setOnClickListener(this);

        // get Expense items list
        getExpenseItemsList();
    }

    //Get Expense Categories
    private void getExpenseItemsList() {
        if (DeviceManager.isOnline()) {
            ExpenseItemsAsyncTask asyncTask = new ExpenseItemsAsyncTask();
            asyncTask.execute(claimId);
        } else {
            enableNetwork();
        }

        adapter = new ExpenseListAdapter(getApplicationContext(), approverExpenseItemsList, this);
        eLView = (EndlessExpenseListView) findViewById(R.id.myListView);
        eLView.setLoadingView(R.layout.footer_loader);
        eLView.setListener(this);
        eLView.setAdapter(adapter);
    }

    private void setButtonEnableTrue() {
        button_approve.setBackgroundResource(R.drawable.expense_approve_button_selector);
        button_reject.setBackgroundResource(R.drawable.expense_reject_button_selector);
        button_approve.setEnabled(true);
        button_reject.setEnabled(true);
    }

    private void setButtonEnableFalse() {
        button_approve.setBackgroundResource(R.drawable.expense_approve_button_disable_selector);
        button_reject.setBackgroundResource(R.drawable.expense_reject_button_disable_selector);
        button_approve.setEnabled(false);
        button_reject.setEnabled(false);
    }


    @Override
    public void loadData() {
        pageNo += 1;
        new LazyLoaderAsyncTask().execute(claimId);
    }

    @Override
    public void approveClickOk(String approveNote, String counterApproveNote) {
        str_rejectionId = "0";
        str_rejectionNote = null;
        str_approveNote = approveNote;
        str_acounterApproveNote = counterApproveNote;
        if (DeviceManager.isOnline()) {
            approveRejectAsync();
        } else {
            enableNetwork();
        }
    }

    @Override
    public void rejectClickOk(String rejectionNote, String rejectionId) {
        str_rejectionNote = rejectionNote;
        str_rejectionId = rejectionId;
        str_approveNote = null;
        str_acounterApproveNote = null;
        if (DeviceManager.isOnline()) {
            approveRejectAsync();
        } else {
            enableNetwork();
        }
    }

    @Override
    public void onRetryButtonClick(int type) {
        if (DeviceManager.isOnline()) {
            arrayListCheckData.clear();
            checkBoxSelectAll.setChecked(false);
            setButtonEnableFalse();
            pageNo = 0;
            approverExpenseItemsList.clear();
            adapter.notifyDataSetChanged();
            eLView.LoadMore();
            dialog1 = UIUtil.showProgressDialog(ExpenseListActivity.this);
        } else {
            enableNetwork();
        }

    }

    @Override
    public void onCancelButtonClick() {
        UIUtil.dismissDialog(dialog);
        UIUtil.dismissDialog(dialog1);
    }

    @Override
    public void onPositiveButtonClick(int type) {
        switch (type) {
            case 0:
                //
                break;
            case 1:
                //
                break;
        }
    }

    @Override
    public void onPositiveButtonClick() {
        arrayListCheckData.clear();
        checkBoxSelectAll.setChecked(false);
        setButtonEnableFalse();
        pageNo = 1;
        approverExpenseItemsList.clear();
        adapter.notifyDataSetChanged();
        getExpenseItemsList();
    }

    @Override
    public void onNegativeButtonClick() {}

    @Override
    public void handleDuplicateClick(int position) {
        //Navigate to Expense Detail Activity
        navigateToDuplicateEntryActivity(approverExpenseItemsList.get(position).getDuplicateItems());
    }

    @Override
    public void handleExpenseRowClick(ExpenseInfo expenseInfo) {
        //Clear all checkbox
        arrayListCheckData.clear();
        checkBoxSelectAll.setChecked(false);

        for (ExpenseItemsModel expenseItemsModel : approverExpenseItemsList) {
            expenseItemsModel.setSelected(false);
            isRejected = expenseItemsModel.isRejected();
        }

        //Notify Adapter
        adapter.notifyDataSetChanged();

        //Navigate to Expense Detail Activity
        navigateToExpenseDetailActivity(expenseInfo, isRejected);
    }

    @Override
    public void handleCheckBoxClick(CheckBox checkBox) {
        handleSelectExpense(checkBox);
    }

    //LazyLoader Async task
    private class LazyLoaderAsyncTask extends AsyncTask<String, Void, ArrayList<ExpenseItemsModel>> {
        private Exception exception = null;

        @Override
        protected ArrayList<ExpenseItemsModel> doInBackground(String... params) {
            ArrayList<ExpenseItemsModel> approverExpenseItemsServerList = null;

            try {
                approverExpenseItemsServerList = ExpenseItemsManger.getExpenseItems(params[0], counter, pageNo);
            } catch (EODException eodException) {
                this.exception = eodException;
            }
            return approverExpenseItemsServerList;
        }

        @Override
        protected void onPostExecute(ArrayList<ExpenseItemsModel> result) {
            super.onPostExecute(result);
            if (exception == null) {
                if (dialog1 != null) {
                    UIUtil.dismissDialog(dialog1);
                }
                eLView.addNewData(result);
            } else if (exception instanceof EODBusinessException) {
                //OnFail
                onFailGetExpenseItemsList((EODException) exception);
            } else if (exception instanceof EODException) {
                //OnError
                onErrorExpenseItemsList(exception);
            }
        }
    }

    // ApproveReject AsyncTask
    private void approveRejectAsync() {
        ApprovalRejectedExpenseContainer approveRejectExpenseObject = getSelectedExpenseList(arrayListCheckData);
        new ApproveRejectedExpensesAsyncTask(approveRejectExpenseObject).execute();
    }
    private ApprovalRejectedExpenseContainer getSelectedExpenseList(ArrayList<ExpenseItemsModel> expense) {
        ApprovalRejectedExpenseContainer requestContainer = new ApprovalRejectedExpenseContainer();
        ArrayList<ApprovalRejectedExpenseRequest> arrayRequest = new ArrayList<ApprovalRejectedExpenseRequest>();
        ApprovalRejectedExpensesRequestData data = new ApprovalRejectedExpensesRequestData();
        for (int i = 0; i < expense.size(); i++) {
            ApprovalRejectedExpenseRequest request = new ApprovalRejectedExpenseRequest();
            UserInfo userInfo = CachingManager.getUserInfo();
            if (userInfo != null) {
                request.setExpenseDetailId(expense.get(i).getExpenseDetailID());//DetailID
                request.setExpenseHeaderId(expense.get(i).getExpenseHeaderID());//ClaimID
                request.setRejectionReasonID(str_rejectionId);//RejectionReasonID
                request.setRejectionReason(str_rejectionNote);//RejectionReason

                if (isRejectedStatus) {
                    request.setStatus("R");//Status
                } else {
                    if (userInfo.isApprover()) {
                        request.setStatus("M");//Status
                    } else {
                        request.setStatus("B");//Status
                    }
                }

                request.setEntryBy(userInfo.getResourceId());//EntryBy
                request.setCompanyID(userInfo.getCompanyId());//CompanyID
                if (expense.get(i).getPolicyBreech().getStatus().toString().equals("O")) {
                    if (str_approveNote != null) {
                        request.setApproverNote("Approver – (" + userInfo.getDisplayName() + ")#" + str_approveNote);//ApproverNote
                    } else {
                        request.setApproverNote("");//ApproverNote
                    }

                    request.setCounterApproverNote("");//CounterApproverNote
                } else {
                    if (str_acounterApproveNote != null) {
                        request.setCounterApproverNote("Counter Approver – (" + userInfo.getDisplayName() + ")#" + str_acounterApproveNote);//CounterApproverNote
                    } else {
                        request.setCounterApproverNote("");//CounterApproverNote
                    }
                    request.setApproverNote("");//ApproverNote

                }
                if (expense.get(i).getReceiptCount() > 0) {
                    request.setReceiptprovided("Y");//Receiptprovided
                } else {
                    request.setReceiptprovided("N");//Receiptprovided
                }
            }
            arrayRequest.add(request);
        }
        data.setRequests(arrayRequest);
        requestContainer.setRequestData(data);

        return requestContainer;
    }
    public class ApproveRejectedExpensesAsyncTask extends AsyncTask<Void, Void, ApproveReject> {
        private final ApprovalRejectedExpenseContainer expenseList;
        private Dialog dialog = null;
        private Exception exception = null;
        private ApproveReject response;

        public ApproveRejectedExpensesAsyncTask(ApprovalRejectedExpenseContainer expenseList) {
            this.expenseList = expenseList;
        }

        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(ExpenseListActivity.this);
        }

        @Override
        protected ApproveReject doInBackground(Void... voids) {
            try {
                response = ExpenseDetailManager.ApprovalRejectedExpenseRequest(expenseList);
            } catch (Exception eodException) {
                this.exception = eodException;
            }

            return response;
        }

        @Override
        protected void onPostExecute(ApproveReject response) {
            //Dismiss progress dialog.
            UIUtil.dismissDialog(dialog);
            if (exception == null) {
                //OnSucess
                onSuccessGetApprovedRejectExpenses(response);
            } else if (exception instanceof EODBusinessException) {
                //OnFail
                onFailGetExpenseDetail((EODException) exception);
            } else if (exception instanceof EODException) {
                //OnError
                onErrorGetExpenseDetail(exception);
            }
        }
    }
    private void onSuccessGetApprovedRejectExpenses(ApproveReject response) {
        if (response != null) {
            boolean alertStatusN = false;
            boolean alertStatusZ = false;
            ArrayList<LstApproverRejectItem> approveRejects = response.getLstApproverReject();
            for (LstApproverRejectItem lstApproverRejectItem : approveRejects) {
                Log.e("getDetailId", "Approval>>" + lstApproverRejectItem.getDetailId());
                Log.e("getStrApprovalAllowed", "Approval>>" + lstApproverRejectItem.getStrApprovalAllowed());

                if (lstApproverRejectItem.getStrApprovalAllowed().equalsIgnoreCase("N")) {
                    alertStatusN = true;
                    break;
                } else if (lstApproverRejectItem.getStrApprovalAllowed().equalsIgnoreCase("Z")) {
                    alertStatusZ = true;
                    break;
                }
            }

            if (alertStatusN) {
                showAlertDialog();
            } else if (alertStatusZ) {
                showAlertDialog1();
            } else {
                SharePreference.putBoolean(getApplicationContext(), AppConstant.BACK_EXPENSE_DETAIL_LIST, true);
                SharePreference.putBoolean(getApplicationContext(), AppConstant.BACK_CLAIM_LIST, true);
                arrayListCheckData.clear();
                checkBoxSelectAll.setChecked(false);
                setButtonEnableFalse();
                pageNo = 1;
                approverExpenseItemsList.clear();
                adapter.notifyDataSetChanged();
                getExpenseItemsList();
            }
            /*if (response.getStatus().equals("Success")) {
                getExpenseItemsList();
            }*/
        }
    }

    //Async Task Expense Item Async
    private class ExpenseItemsAsyncTask extends AsyncTask<String, Void, ArrayList<ExpenseItemsModel>> {
        private Exception exception = null;
        private ArrayList<ExpenseItemsModel> approverExpenseItemsServerList;

        @Override
        protected void onPreExecute() {
            isLoadingMoreExpenses = true;
            dialog = UIUtil.showProgressDialog(ExpenseListActivity.this);
        }

        @Override
        protected ArrayList<ExpenseItemsModel> doInBackground(String... params) {
            try {
                approverExpenseItemsServerList = ExpenseItemsManger.getExpenseItems(params[0], counter, pageNo);
            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return approverExpenseItemsServerList;
        }

        @Override
        protected void onPostExecute(ArrayList<ExpenseItemsModel> result) {
            //Dismiss progress dialog.
            isLoadingMoreExpenses = false;
            UIUtil.dismissDialog(dialog);

            if (exception == null) {
                //OnSucess
                onSuccessExpenseItemsList(approverExpenseItemsServerList);
            } else if (exception instanceof EODBusinessException) {
                //OnFail
                onFailGetExpenseItemsList((EODException) exception);
            } else if (exception instanceof EODException) {
                //OnError
                onErrorExpenseItemsList(exception);
            }
        }
    }
    public void onSuccessExpenseItemsList(ArrayList<ExpenseItemsModel> approverExpenseItemsSuccessList) {
        if (!AppUtil.isCollectionEmpty(approverExpenseItemsSuccessList)) {
            approverExpenseItemsList.addAll(approverExpenseItemsSuccessList);
            adapter.notifyDataSetChanged();
        } else {
            SharePreference.putBoolean(getApplicationContext(), AppConstant.BACK_EXPENSE_DETAIL_LIST, true);
            SharePreference.putBoolean(getApplicationContext(), AppConstant.BACK_CLAIM_LIST, true);
            finish();
        }
    }
    private void onFailGetExpenseItemsList(EODException exception) {
        showErrorAlertDialog(false, null, null, null, TYPE_LOADLIST);
    }
    private void onErrorExpenseItemsList(Exception exception) {
        eLView.stopFooter();
        showErrorAlertDialog(false, null, null, null, TYPE_LOADLIST);
    }

    //Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.help_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuItemId = item.getItemId();
        switch (menuItemId) {
            case R.id.menuHelp:
                //Handle OverlayScreen
                handleOverlayScreen();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //Click Listener
    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        switch (viewId) {
            case R.id.button_reject:
                if (DeviceManager.isOnline()) {
                    isRejectedStatus = true;//for isRejection button click status
                    setNoteAsPerRejectionSettingRule();
                } else {
                    enableNetwork();
                }

                break;

            case R.id.button_approve:
                if (DeviceManager.isOnline()) {
                    isRejectedStatus = false;//for Approve button click status
                    if (SharePreference.getBoolean(ExpenseListActivity.this, AppConstant.isCounterApprover)) {
                        showAcceptDialog();
                    } else {
                        setNoteAsPerRejectionSettingRule();
                    }
                    // setNoteAsPerRejectionSettingRule();
                } else {
                    enableNetwork();
                }

                break;

            case R.id.imageViewCloseButton:
                handleOverlayScreen();
                break;

            case R.id.relativeOverlayScreenExpense:
                handleOverlayScreen();
                break;

            case R.id.checkBoxSelectAll:
                handleSelectAll(checkBoxSelectAll);
                break;
        }
    }

    //Approve and rejection rules
    private void setNoteAsPerRejectionSettingRule() {
        //Rule 1 And Rule 2 And Rule 3
        if (appRejectionSetting.isAppRejectSettingsRule1() && appRejectionSetting.isAppRejectSettingsRule2() && appRejectionSetting.isAppRejectSettingsRule3()) {
            Log.e("Status", "RejectionNote:-" + "rule1 and rule2 and rule3");
            /*1)For multiple approval, rule 2 should be taken into consideration.
              2)For rejection, Rule 3 should be considered.*/

            if (!isRejectedStatus) {
                //Approve button click
                /*Show Accept Dialog popup when multiple expense are approve*/
                if (arrayListCheckData.size() > 1) {
                    //Multiple expense are select for approve
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                    }
                    showAcceptDialog();
                } else {
                    //Single Expense are select fro approve
                    if (DeviceManager.isOnline()) {
                        if (arrayListCheckData.get(0).getPolicyBreech().isDailyCap() || arrayListCheckData.get(0).getPolicyBreech().isExpenseLimit() || arrayListCheckData.get(0).getPolicyBreech().isMaxSpendBreach() || arrayListCheckData.get(0).getPolicyBreech().isPreApproval()) {
                            // Policy breech expense selection
                            SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                            showAcceptDialog();
                        } else {
                            // Non policy breech expense selection
                            approveRejectAsync();
                        }
                    } else {
                        enableNetwork();
                    }
                   /* if (DeviceManager.isOnline()) {
                        approveRejectAsync();
                    } else {
                        enableNetwork();
                    }*/
                }
            } else {
                //Rejection button click
                if (arrayListCheckData.size() > 1) {
                    SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", true);
                } else {
                    SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", false);
                }
                showRejectionDialog();
            }

        }

        //Rule 1 And Rule 2 And Rule 4
        else if (appRejectionSetting.isAppRejectSettingsRule1() && appRejectionSetting.isAppRejectSettingsRule2() && appRejectionSetting.isAppRejectSettingsRule4()) {
            Log.e("Status", "RejectionNote:-" + "rule1 and rule2 and rule4");
            /*1)For multiple approval, rule 2 should be taken into consideration.
              2)For rejection, Rule 1 should be considered.*/
            if (!isRejectedStatus) {
                //Approve button click
                /*Show Accept Dialog popup when multiple expense are approve*/
                if (arrayListCheckData.size() > 1) {
                    //Multiple expense are select for approve
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                    }
                    showAcceptDialog();
                } else {
                    //Single Expense are select fro approve
                    if (DeviceManager.isOnline()) {
                        if (arrayListCheckData.get(0).getPolicyBreech().isDailyCap() || arrayListCheckData.get(0).getPolicyBreech().isExpenseLimit() || arrayListCheckData.get(0).getPolicyBreech().isMaxSpendBreach() || arrayListCheckData.get(0).getPolicyBreech().isPreApproval()) {
                            // Policy breech expense selection
                            SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                            showAcceptDialog();
                        } else {
                            // Non policy breech expense selection
                            approveRejectAsync();
                        }
                    } else {
                        enableNetwork();
                    }
                    /*if (DeviceManager.isOnline()) {
                        approveRejectAsync();
                    } else {
                        enableNetwork();
                    }*/
                }
            } else {
                //Rejection button click
                isMultiExpenseReject = false;//when multiple expense selected for rejection
                for (int i = 0; i < arrayListCheckData.size(); i++) {
                    if (arrayListCheckData.size() > 1) {
                        showMulExpRejectionAlertDialog();
                        isMultiExpenseReject = true;
                        break;
                    }
                }

                //Multiple Expense Reject alert.
                if (isMultiExpenseReject) {
                    isMultiExpenseReject = false;
                } else {
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", false);
                    }
                    showRejectionDialog();
                }
            }
        }

        //Rule 1 And Rule 3 And Rule 4
        else if (appRejectionSetting.isAppRejectSettingsRule1() && appRejectionSetting.isAppRejectSettingsRule3() && appRejectionSetting.isAppRejectSettingsRule4()) {
            Log.e("Status", "RejectionNote:-" + "rule1 and rule3 and rule3");
            /*1)For approval, rule 1 should be taken into consideration.
              2)For rejection, Rule 3 should be considered.*/
            if (!isRejectedStatus) {
                //Approve button click
                isPolicyBreech = false;//when policy breech expense select more than one them show alert
                isNonPolicyBreech = false;//when non policy breech expense select more than one them show alert
                for (int i = 0; i < arrayListCheckData.size(); i++) {
                    if (arrayListCheckData.size() > 1) {
                        if (arrayListCheckData.get(i).getPolicyBreech().isDailyCap() || arrayListCheckData.get(i).getPolicyBreech().isExpenseLimit() || arrayListCheckData.get(i).getPolicyBreech().isMaxSpendBreach() || arrayListCheckData.get(i).getPolicyBreech().isPreApproval()) {
                            //Multiple Policy breech expense selection
                            showMulExpPolicyBreechSelectAlertDialog();
                            isPolicyBreech = true;
                            break;
                        } else {
                            //Multiple Non policy breech expense selection
                            showMulExpApproveAlertDialog();
                            isNonPolicyBreech = true;
                            break;
                        }
                    }
                }

                //Policy Breech alert.
                if (isPolicyBreech || isNonPolicyBreech) {
                    isPolicyBreech = false;
                    isNonPolicyBreech = false;
                } else {
                    //Only one expense selection
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                    }
                    showAcceptDialog();
                }
            } else {
                //Rejected button click
                if (arrayListCheckData.size() > 1) {
                    SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", true);
                } else {
                    SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", false);
                }
                showRejectionDialog();
            }
        }

        //Rule 2 And Rule 3 And Rule 4
        else if (appRejectionSetting.isAppRejectSettingsRule2() && appRejectionSetting.isAppRejectSettingsRule3() && appRejectionSetting.isAppRejectSettingsRule4()) {
            Log.e("Status", "RejectionNote:-" + "rule2 and rule3 and rule4");
            /*1)For multiple approval, rule 2 should be taken into consideration.
            * 2)For rejection, Rule 3 should be considered.*/
            if (!isRejectedStatus) {
                //Approve button click
                /*Show Accept Dialog popup when multiple expense are approve*/
                if (arrayListCheckData.size() > 1) {
                    //Multiple expense are select for approve
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                    }
                    showAcceptDialog();
                } else {
                    //Single Expense are select fro approve
                    if (DeviceManager.isOnline()) {
                        if (arrayListCheckData.get(0).getPolicyBreech().isDailyCap() || arrayListCheckData.get(0).getPolicyBreech().isExpenseLimit() || arrayListCheckData.get(0).getPolicyBreech().isMaxSpendBreach() || arrayListCheckData.get(0).getPolicyBreech().isPreApproval()) {
                            // Policy breech expense selection
                            SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                            showAcceptDialog();
                        } else {
                            // Non policy breech expense selection
                            approveRejectAsync();
                        }
                        // approveRejectAsync();
                    } else {
                        enableNetwork();
                    }
                    /*if (DeviceManager.isOnline()) {
                        approveRejectAsync();
                    } else {
                        enableNetwork();
                    }*/
                }
            } else {
                //Rejected button click
                if (arrayListCheckData.size() > 1) {
                    SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", true);
                } else {
                    SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", false);
                }
                showRejectionDialog();
            }
        }

        //Rule 1 And Rule 2
        else if (appRejectionSetting.isAppRejectSettingsRule1() && appRejectionSetting.isAppRejectSettingsRule2()) {
            Log.e("Status", "RejectionNote:-" + "rule1 and rule2");
            /*1)For multiple approval, rule 2 should be taken into consideration.
              2)Reject will act as in Rule 1.*/
            if (!isRejectedStatus) {
                //Approve button click
                /*Show Accept Dialog popup when multiple expense are approve*/
                if (arrayListCheckData.size() > 1) {
                    //Multiple expense are select for approve
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                    }
                    showAcceptDialog();
                } else {
                    //Single Expense are select fro approve
                    if (DeviceManager.isOnline()) {
                        if (arrayListCheckData.get(0).getPolicyBreech().isDailyCap() || arrayListCheckData.get(0).getPolicyBreech().isExpenseLimit() || arrayListCheckData.get(0).getPolicyBreech().isMaxSpendBreach() || arrayListCheckData.get(0).getPolicyBreech().isPreApproval()) {
                            // Policy breech expense selection
                            SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                            showAcceptDialog();
                        } else {
                            // Non policy breech expense selection
                            approveRejectAsync();
                        }
                    } else {
                        enableNetwork();
                    }
                   /* if (DeviceManager.isOnline()) {
                        approveRejectAsync();
                    } else {
                        enableNetwork();
                    }*/
                }
            } else {
                //Rejected button click
                isMultiExpenseReject = false;//when multiple expense selected for rejection
                for (int i = 0; i < arrayListCheckData.size(); i++) {
                    if (arrayListCheckData.size() > 1) {
                        showMulExpRejectionAlertDialog();
                        isMultiExpenseReject = true;
                        break;
                    }
                }

                //Multiple Expense Reject alert.
                if (isMultiExpenseReject) {
                    isMultiExpenseReject = false;
                } else {
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", false);
                    }
                    showRejectionDialog();
                }
            }
        }

        //Rule 1 And Rule 3
        else if (appRejectionSetting.isAppRejectSettingsRule1() && appRejectionSetting.isAppRejectSettingsRule3()) {
            Log.e("Status", "RejectionNote:-" + "rule1 and rule3");
            /*1)For approval Rule 1 will be considered.
              2)For rejection Rule 3 will be considered.*/
            if (!isRejectedStatus) {
                //Approve button click
                isPolicyBreech = false;//when policy breech expense select more than one them show alert
                isNonPolicyBreech = false;//when non policy breech expense select more than one them show alert
                for (int i = 0; i < arrayListCheckData.size(); i++) {
                    if (arrayListCheckData.size() > 1) {
                        if (arrayListCheckData.get(i).getPolicyBreech().isDailyCap() || arrayListCheckData.get(i).getPolicyBreech().isExpenseLimit() || arrayListCheckData.get(i).getPolicyBreech().isMaxSpendBreach() || arrayListCheckData.get(i).getPolicyBreech().isPreApproval()) {
                            //Multiple Policy breech expense selection
                            showMulExpPolicyBreechSelectAlertDialog();
                            isPolicyBreech = true;
                            break;
                        } else {
                            //Multiple Non policy breech expense selection
                            showMulExpApproveAlertDialog();
                            isNonPolicyBreech = true;
                            break;
                        }
                    }
                }

                //Policy Breech alert.
                if (isPolicyBreech || isNonPolicyBreech) {
                    isPolicyBreech = false;
                    isNonPolicyBreech = false;
                } else {
                    //Only one expense selection
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                    }
                    showAcceptDialog();
                }
            } else {
                //Rejection button click
                if (arrayListCheckData.size() > 1) {
                    SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", true);
                } else {
                    SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", false);
                }
                showRejectionDialog();
            }
        }

        //Rule 1 And Rule 4
        else if (appRejectionSetting.isAppRejectSettingsRule1() && appRejectionSetting.isAppRejectSettingsRule4()) {
            Log.e("Status", "RejectionNote:-" + "rule1 and rule4");
            /*1)Rule 1 will be considered for both approval and rejection*/
            if (!isRejectedStatus) {
                //Approve button click
                isPolicyBreech = false;//when policy breech expense select more than one them show alert
                isNonPolicyBreech = false;//when non policy breech expense select more than one them show alert
                for (int i = 0; i < arrayListCheckData.size(); i++) {
                    if (arrayListCheckData.size() > 1) {
                        if (arrayListCheckData.get(i).getPolicyBreech().isDailyCap() || arrayListCheckData.get(i).getPolicyBreech().isExpenseLimit() || arrayListCheckData.get(i).getPolicyBreech().isMaxSpendBreach() || arrayListCheckData.get(i).getPolicyBreech().isPreApproval()) {
                            //Multiple Policy breech expense selection
                            showMulExpPolicyBreechSelectAlertDialog();
                            isPolicyBreech = true;
                            break;
                        } else {
                            //Multiple Non policy breech expense selection
                            showMulExpApproveAlertDialog();
                            isNonPolicyBreech = true;
                            break;
                        }
                    }
                }

                //Policy Breech alert.
                if (isPolicyBreech || isNonPolicyBreech) {
                    isPolicyBreech = false;
                    isNonPolicyBreech = false;
                } else {
                    //Only one expense selection
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                    }
                    showAcceptDialog();
                }

            } else {
                //Rejected button click
                isMultiExpenseReject = false;//when multiple expense selected for rejection
                for (int i = 0; i < arrayListCheckData.size(); i++) {
                    if (arrayListCheckData.size() > 1) {
                        showMulExpRejectionAlertDialog();
                        isMultiExpenseReject = true;
                        break;
                    }
                }

                //Multiple Expense Reject alert.
                if (isMultiExpenseReject) {
                    isMultiExpenseReject = false;
                } else {
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", false);
                    }
                    showRejectionDialog();
                }
            }
        }

        //Rule 2 And Rule 3
        else if (appRejectionSetting.isAppRejectSettingsRule2() && appRejectionSetting.isAppRejectSettingsRule3()) {
            Log.e("Status", "RejectionNote:-" + "rule2 and rule3");
            /*1)For approval Rule 2 will be considered.
               2)For rejection Rule 3 will be considered.*/
            if (!isRejectedStatus) {
                //Approve button click
                /*Show Accept Dialog popup when multiple expense are approve*/
                if (arrayListCheckData.size() > 1) {
                    //Multiple expense are select for approve
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                    }
                    showAcceptDialog();
                } else {
                    //Single Expense are select fro approve
                    if (DeviceManager.isOnline()) {
                        if (arrayListCheckData.get(0).getPolicyBreech().isDailyCap() || arrayListCheckData.get(0).getPolicyBreech().isExpenseLimit() || arrayListCheckData.get(0).getPolicyBreech().isMaxSpendBreach() || arrayListCheckData.get(0).getPolicyBreech().isPreApproval()) {
                            // Policy breech expense selection
                            SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                            showAcceptDialog();
                        } else {
                            // Non policy breech expense selection
                            approveRejectAsync();
                        }
                    } else {
                        enableNetwork();
                    }
                   /* if (DeviceManager.isOnline()) {
                        approveRejectAsync();
                    } else {
                        enableNetwork();
                    }*/
                }

            } else {
                //Rejection button click
                if (arrayListCheckData.size() > 1) {
                    SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", true);
                } else {
                    SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", false);
                }
                showRejectionDialog();
            }
        }

        //Rule 2 And Rule 4
        else if (appRejectionSetting.isAppRejectSettingsRule2() && appRejectionSetting.isAppRejectSettingsRule4()) {
            Log.e("Status", "RejectionNote:-" + "rule2 and rule4");
            /*1)For multiple approval, rule 2 should be taken into consideration*/
            if (!isRejectedStatus) {
                //Approve button click
                /*Show Accept Dialog popup when multiple expense are approve*/
                if (arrayListCheckData.size() > 1) {
                    //Multiple expense are select for approve
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                    }
                    showAcceptDialog();
                } else {
                    //Single Expense are select fro approve
                    if (DeviceManager.isOnline()) {
                        if (arrayListCheckData.get(0).getPolicyBreech().isDailyCap() || arrayListCheckData.get(0).getPolicyBreech().isExpenseLimit() || arrayListCheckData.get(0).getPolicyBreech().isMaxSpendBreach() || arrayListCheckData.get(0).getPolicyBreech().isPreApproval()) {
                            // Policy breech expense selection
                            SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                            showAcceptDialog();
                        } else {
                            // Non policy breech expense selection
                            approveRejectAsync();
                        }
                    } else {
                        enableNetwork();
                    }
                   /* if (DeviceManager.isOnline()) {
                        approveRejectAsync();
                    } else {
                        enableNetwork();
                    }*/
                }
            } else {
                //Rejection button click
                isMultiExpenseReject = false;//when multiple expense selected for rejection
                for (int i = 0; i < arrayListCheckData.size(); i++) {
                    if (arrayListCheckData.size() > 1) {
                        showMulExpRejectionAlertDialog();
                        isMultiExpenseReject = true;
                        break;
                    }
                }

                //Multiple Expense Reject alert.
                if (isMultiExpenseReject) {
                    isMultiExpenseReject = false;
                } else {
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", false);
                    }
                    showRejectionDialog();
                }
            }
        }

        //Rule 3 And Rule 4
        else if (appRejectionSetting.isAppRejectSettingsRule3() && appRejectionSetting.isAppRejectSettingsRule4()) {
            Log.e("Status", "RejectionNote:-" + "rule3 and rule4");

            /*1)For approval Rule 1 should be considered.
               2)For rejection Rule 3 should be considered.*/
            if (!isRejectedStatus) {
                //Approve button click
                isPolicyBreech = false;//when policy breech expense select more than one them show alert
                isNonPolicyBreech = false;//when non policy breech expense select more than one them show alert
                for (int i = 0; i < arrayListCheckData.size(); i++) {
                    if (arrayListCheckData.size() > 1) {
                        if (arrayListCheckData.get(i).getPolicyBreech().isDailyCap() || arrayListCheckData.get(i).getPolicyBreech().isExpenseLimit() || arrayListCheckData.get(i).getPolicyBreech().isMaxSpendBreach() || arrayListCheckData.get(i).getPolicyBreech().isPreApproval()) {
                            //Multiple Policy breech expense selection
                            showMulExpPolicyBreechSelectAlertDialog();
                            isPolicyBreech = true;
                            break;
                        } else {
                            //Multiple Non policy breech expense selection
                            showMulExpApproveAlertDialog();
                            isNonPolicyBreech = true;
                            break;
                        }
                    }
                }

                //Policy Breech alert.
                if (isPolicyBreech || isNonPolicyBreech) {
                    isPolicyBreech = false;
                    isNonPolicyBreech = false;
                } else {
                    //Only one expense selection
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                    }
                    showAcceptDialog();
                }
            } else {
                //Rejected button click
                if (arrayListCheckData.size() > 1) {
                    SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", true);
                } else {
                    SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", false);
                }
                showRejectionDialog();
            }
        }
        //Rule 1
        else if (appRejectionSetting.isAppRejectSettingsRule1()) {
            Log.e("Status", "RejectionNote:-" + "rule1");
            /*Approve Rule*/
            /*  1)Policy breech:-Only one expense can approve,Multiple expense can not approve,comment exit text show,note message not show
                2)Non breech:-Only One expense can approve,Multiple expense can not approve*/
            /*Rejection Rule*/
            /*1)Only one expense reject at a time not a multiple*/


            if (!isRejectedStatus) {
                //Approve button click
                isPolicyBreech = false;//when policy breech expense select more than one them show alert
                isNonPolicyBreech = false;//when non policy breech expense select more than one them show alert
                for (int i = 0; i < arrayListCheckData.size(); i++) {
                    if (arrayListCheckData.size() > 1) {
                        if (arrayListCheckData.get(i).getPolicyBreech().isDailyCap() || arrayListCheckData.get(i).getPolicyBreech().isExpenseLimit() || arrayListCheckData.get(i).getPolicyBreech().isMaxSpendBreach() || arrayListCheckData.get(i).getPolicyBreech().isPreApproval()) {
                            //Multiple Policy breech expense selection
                            showMulExpPolicyBreechSelectAlertDialog();
                            isPolicyBreech = true;
                            break;
                        } else {
                            //Multiple Non policy breech expense selection
                            showMulExpApproveAlertDialog();
                            isNonPolicyBreech = true;
                            break;
                        }
                    }
                }

                //Policy Breech alert.
                if (isPolicyBreech || isNonPolicyBreech) {
                    isPolicyBreech = false;
                    isNonPolicyBreech = false;
                } else {
                    //Only one expense selection
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                    }
                    showAcceptDialog();
                }

            } else {
                //Rejected button click
                isMultiExpenseReject = false;//when multiple expense selected for rejection
                for (int i = 0; i < arrayListCheckData.size(); i++) {
                    if (arrayListCheckData.size() > 1) {
                        showMulExpRejectionAlertDialog();
                        isMultiExpenseReject = true;
                        break;
                    }
                }

                //Multiple Expense Reject alert.
                if (isMultiExpenseReject) {
                    isMultiExpenseReject = false;
                } else {
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", false);
                    }
                    showRejectionDialog();
                }
            }


        }

        //Rule 2
        else if (appRejectionSetting.isAppRejectSettingsRule2()) {
            Log.e("Status", "RejectionNote:-" + "rule2");
            /*Approve Rule*/
            /*Policy breech expense and Non breech expenses:-
               1)multiple should be approve
               2)Comment box required in case of multiple expense
               3)single comment copy into the all expenses
               4)note show and copy on all expenses
               5)No comment is required individual item approval. (No approver comment popup should come up)*/
            /*Reject Rule*/
            /*1)Only one expense can be Reject
            * 2)Multiple not rejected
            * 3)*/

            if (!isRejectedStatus) {
                //Approve button click
                /*Show Accept Dialog popup when multiple expense are approve*/
                if (arrayListCheckData.size() > 1) {
                    //Multiple expense are select for approve
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                    }
                    showAcceptDialog();
                } else {
                    //Single Expense are select fro approve
                    if (DeviceManager.isOnline()) {
                        if (arrayListCheckData.get(0).getPolicyBreech().isDailyCap() || arrayListCheckData.get(0).getPolicyBreech().isExpenseLimit() || arrayListCheckData.get(0).getPolicyBreech().isMaxSpendBreach() || arrayListCheckData.get(0).getPolicyBreech().isPreApproval()) {
                            // Policy breech expense selection
                            SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                            showAcceptDialog();
                        } else {
                            // Non policy breech expense selection
                            approveRejectAsync();
                        }
                    } else {
                        enableNetwork();
                    }
                    /*if (DeviceManager.isOnline()) {
                        approveRejectAsync();
                    } else {
                        enableNetwork();
                    }*/
                }

            } else {
                //Rejection button click
                isMultiExpenseReject = false;//when multiple expense selected for rejection
                for (int i = 0; i < arrayListCheckData.size(); i++) {
                    if (arrayListCheckData.size() > 1) {
                        showMulExpRejectionAlertDialog();
                        isMultiExpenseReject = true;
                        break;
                    }
                }

                //Multiple Expense Reject alert.
                if (isMultiExpenseReject) {
                    isMultiExpenseReject = false;
                } else {
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", false);
                    }
                    showRejectionDialog();
                }
            }

        }

        //Rule 3
        else if (appRejectionSetting.isAppRejectSettingsRule3()) {
            Log.e("Status", "RejectionNote:-" + "rule3");
             /*Approve Rule*/
            /*Policy breech expense :-
               1)multiple can be select but show pop up for approve item contains expenses that has breached policy.
               2)comment box show.
              Non breech expenses:-
              1)multiple can select
              2)no approval popup show
               */
            /*Reject Rule*/
            /*
             1)Multiple expense can rejected
             2)Same reason applied on all expense */

            if (!isRejectedStatus) {
                int breechCount = 0;
                int nonBreechCount = 0;
                for (int i = 0; i < arrayListCheckData.size(); i++) {
                    if (arrayListCheckData.size() > 1) {
                        if (arrayListCheckData.get(i).getPolicyBreech().isDailyCap() || arrayListCheckData.get(i).getPolicyBreech().isExpenseLimit() || arrayListCheckData.get(i).getPolicyBreech().isMaxSpendBreach() || arrayListCheckData.get(i).getPolicyBreech().isPreApproval()) {
                            breechCount++;
                        } else {
                            nonBreechCount++;
                        }
                    } else {
                        if (arrayListCheckData.get(i).getPolicyBreech().isDailyCap() || arrayListCheckData.get(i).getPolicyBreech().isExpenseLimit() || arrayListCheckData.get(i).getPolicyBreech().isMaxSpendBreach() || arrayListCheckData.get(i).getPolicyBreech().isPreApproval()) {
                            breechCount++;
                        } else {
                            nonBreechCount++;
                        }
                    }
                }

                if (breechCount > 1) {
                    showMulExpPolicyBreechSelectAlertDialog();
                } else {
                    if (nonBreechCount >= 1) {
                        if (breechCount == 1) {
                            showMulExpPolicyBreechSelectAlertDialog();
                        } else {
                            if (DeviceManager.isOnline()) {
                                if (arrayListCheckData.get(0).getPolicyBreech().isDailyCap() || arrayListCheckData.get(0).getPolicyBreech().isExpenseLimit() || arrayListCheckData.get(0).getPolicyBreech().isMaxSpendBreach() || arrayListCheckData.get(0).getPolicyBreech().isPreApproval()) {
                                    // Policy breech expense selection
                                    SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                                    showAcceptDialog();
                                } else {
                                    // Non policy breech expense selection
                                    approveRejectAsync();
                                }
                            } else {
                                enableNetwork();
                            }
                           /* if (DeviceManager.isOnline()) {
                                approveRejectAsync();
                            } else {
                                enableNetwork();
                            }*/
                        }
                    } else {
                        if (arrayListCheckData.size() > 1) {
                            SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", true);
                        } else {
                            SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                        }
                        showAcceptDialog();
                    }

                }

            } else {
                //Rejected button click
                if (arrayListCheckData.size() > 1) {
                    SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", true);
                } else {
                    SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", false);
                }
                showRejectionDialog();
            }
        }

        //Rule 4
        else if (appRejectionSetting.isAppRejectSettingsRule4()) {
            Log.e("Status", "RejectionNote:-" + "rule4");
            /*Policy breech:-Same behave like rule 1
            * Non Policy breech:-same behave like rule1
            * Rejection:Same Behave like rule1*/

            if (!isRejectedStatus) {
                //Approve button click
                isPolicyBreech = false;//when policy breech expense select more than one them show alert
                isNonPolicyBreech = false;//when non policy breech expense select more than one them show alert
                for (int i = 0; i < arrayListCheckData.size(); i++) {
                    if (arrayListCheckData.size() > 1) {
                        if (arrayListCheckData.get(i).getPolicyBreech().isDailyCap() || arrayListCheckData.get(i).getPolicyBreech().isExpenseLimit() || arrayListCheckData.get(i).getPolicyBreech().isMaxSpendBreach() || arrayListCheckData.get(i).getPolicyBreech().isPreApproval()) {
                            //Multiple Policy breech expense selection
                            showMulExpPolicyBreechSelectAlertDialog();
                            isPolicyBreech = true;
                            break;
                        } else {
                            //Multiple Non policy breech expense selection
                            showMulExpApproveAlertDialog();
                            isNonPolicyBreech = true;
                            break;
                        }
                    }
                }

                //Policy Breech alert.
                if (isPolicyBreech || isNonPolicyBreech) {
                    isPolicyBreech = false;
                    isNonPolicyBreech = false;
                } else {
                    //Only one expense selection
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                    }
                    showAcceptDialog();
                }

            } else {
                //Rejected button click
                isMultiExpenseReject = false;//when multiple expense selected for rejection
                for (int i = 0; i < arrayListCheckData.size(); i++) {
                    if (arrayListCheckData.size() > 1) {
                        showMulExpRejectionAlertDialog();
                        isMultiExpenseReject = true;
                        break;
                    }
                }

                //Multiple Expense Reject alert.
                if (isMultiExpenseReject) {
                    isMultiExpenseReject = false;
                } else {
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", false);
                    }
                    showRejectionDialog();
                }
            }
        } else {

            if (!appRejectionSetting.isAppRejectSettingsRule1() && !appRejectionSetting.isAppRejectSettingsRule2() && !appRejectionSetting.isAppRejectSettingsRule3() && !appRejectionSetting.isAppRejectSettingsRule4()) {
                Log.e("Status", "RejectionNote:-" + "rule1=N,rule3=N,rule3=N,rule3=N");
                Log.e("Status", "RejectionNote:-" + "rule3");
             /*Approve Rule*/
            /*Policy breech expense :-
               1)multiple can be select but show pop up for approve item contains expenses that has breached policy.
               2)comment box show.
              Non breech expenses:-
              1)multiple can select
              2)no approval popup show
               */
            /*Reject Rule*/
            /*
             1)Multiple expense can rejected
             2)Same reason applied on all expense */

                if (!isRejectedStatus) {
                    int breechCount = 0;
                    int nonBreechCount = 0;
                    for (int i = 0; i < arrayListCheckData.size(); i++) {
                        if (arrayListCheckData.size() > 1) {
                            if (arrayListCheckData.get(i).getPolicyBreech().isDailyCap() || arrayListCheckData.get(i).getPolicyBreech().isExpenseLimit() || arrayListCheckData.get(i).getPolicyBreech().isMaxSpendBreach() || arrayListCheckData.get(i).getPolicyBreech().isPreApproval()) {
                                breechCount++;
                            } else {
                                nonBreechCount++;
                            }
                        } else {
                            if (arrayListCheckData.get(i).getPolicyBreech().isDailyCap() || arrayListCheckData.get(i).getPolicyBreech().isExpenseLimit() || arrayListCheckData.get(i).getPolicyBreech().isMaxSpendBreach() || arrayListCheckData.get(i).getPolicyBreech().isPreApproval()) {
                                breechCount++;
                            } else {
                                nonBreechCount++;
                            }
                        }
                    }

                    if (breechCount > 1) {
                        showMulExpPolicyBreechSelectAlertDialog();
                    } else {
                        if (nonBreechCount >= 1) {
                            if (breechCount == 1) {
                                showMulExpPolicyBreechSelectAlertDialog();
                            } else {
                                if (DeviceManager.isOnline()) {
                                    if (arrayListCheckData.get(0).getPolicyBreech().isDailyCap() || arrayListCheckData.get(0).getPolicyBreech().isExpenseLimit() || arrayListCheckData.get(0).getPolicyBreech().isMaxSpendBreach() || arrayListCheckData.get(0).getPolicyBreech().isPreApproval()) {
                                        // Policy breech expense selection
                                        SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                                        showAcceptDialog();
                                    } else {
                                        // Non policy breech expense selection
                                        approveRejectAsync();
                                    }
                                } else {
                                    enableNetwork();
                                }
                               /* if (DeviceManager.isOnline()) {
                                    approveRejectAsync();
                                } else {
                                    enableNetwork();
                                }*/
                            }
                        } else {
                            if (arrayListCheckData.size() > 1) {
                                SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", true);
                            } else {
                                SharePreference.putBoolean(getApplicationContext(), "IsMultiAccept", false);
                            }
                            showAcceptDialog();
                        }

                    }

                } else {
                    //Rejected button click
                    if (arrayListCheckData.size() > 1) {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", true);
                    } else {
                        SharePreference.putBoolean(getApplicationContext(), "IsMultiRejection", false);
                    }
                    showRejectionDialog();
                }
            }
        }

    }

    //Select All Expenses
    public void handleSelectAll(CheckBox checkBox) {
        //Get Checkbox state
        boolean isChecked = checkBox.isChecked();
        arrayListCheckData.clear();
        if (!AppUtil.isCollectionEmpty(approverExpenseItemsList)) {
            //Iterate Display Expense List
            int count = 0;
            for (ExpenseItemsModel expenseItemsModel : approverExpenseItemsList) {
                expenseItemsModel.setSelected(isChecked);
                if (isChecked) {
                    arrayListCheckData.add(expenseItemsModel);
                    setButtonEnableTrue();
                } else {
                    arrayListCheckData.clear();
                    setButtonEnableFalse();
                }
                count = count + 1;
            }
            //Notify Adapter
            adapter.notifyDataSetChanged();
        } else {
            checkBox.setChecked(false);
        }
    }

    //Select Single Expense
    public void handleSelectExpense(CheckBox checkBox) {
        //Get Expense from Tag
        ExpenseItemsModel expense = (ExpenseItemsModel) checkBox.getTag();
        boolean isChecked = checkBox.isChecked();
        expense.setSelected(isChecked);

        if (isChecked) {
            arrayListCheckData.add(expense);
            if (arrayListCheckData.size() > 1) {
            } else {
                setButtonEnableTrue();
            }
        } else {
            arrayListCheckData.remove(expense);
            checkBoxSelectAll.setChecked(false);
            if (arrayListCheckData.size() == 0) {
                setButtonEnableFalse();
            } else {
                if (arrayListCheckData.size() > 1) {
                } else {
                    setButtonEnableTrue();
                }
            }
        }
    }

    //OverLay screen
    void handleOverlayScreen() {
        relativeOverlayScreenExpense.setVisibility(relativeOverlayScreenExpense.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
    }

    //Navigate to Expense Detail Activity
    private void navigateToExpenseDetailActivity(ExpenseInfo expenseInfo, boolean isRejected) {
        Intent intent = new Intent(this, ExpenseDetailActivity.class);
        intent.putExtra(AppConstant.KEY_EXPENSE_ITEM_POLICY_BREECH, expenseInfo);
        intent.putExtra(AppConstant.KEY_EXPENSE_ITEM_HEADER_ID, expenseInfo.getExpenseHeaderID());
        intent.putExtra(AppConstant.KEY_EXPENSE_ITEM_DETAIL_ID, expenseInfo.getExpenseDetailID());
        intent.putExtra(AppConstant.KEY_CLAIM_NAME, claimName);
        intent.putExtra(AppConstant.KEY_CLAIM_TYPE, claimType);
        intent.putExtra(AppConstant.IS_REJECTED, isRejected);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_forward, R.anim.slide_out_forward);
    }

    //Navigate to Duplicate entry Activity
    private void navigateToDuplicateEntryActivity(ArrayList<DuplicateItemsModel> parentExpenseItem) {
        try {
            JSONArray jsonArray = new JSONArray();
            for (DuplicateItemsModel duplicateItemsModel : parentExpenseItem) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("ColourIndicator", duplicateItemsModel.getColourIndicator());

                jsonObject.put("FieldTitle1", duplicateItemsModel.getDuplicateItem().getFieldTitle1());
                jsonObject.put("FieldValue1", duplicateItemsModel.getDuplicateItem().getFieldValue1());

                jsonObject.put("FieldTitle2", duplicateItemsModel.getDuplicateItem().getFieldTitle2());
                jsonObject.put("FieldValue2", duplicateItemsModel.getDuplicateItem().getFieldValue2());

                jsonObject.put("FieldTitle3", duplicateItemsModel.getDuplicateItem().getFieldTitle3());
                jsonObject.put("FieldValue3", duplicateItemsModel.getDuplicateItem().getFieldValue3());

                jsonObject.put("FieldTitle4", duplicateItemsModel.getDuplicateItem().getFieldTitle4());
                jsonObject.put("FieldValue4", duplicateItemsModel.getDuplicateItem().getFieldValue4());

                jsonArray.put(jsonObject);
            }


            Intent intent = new Intent(this, DuplicateEntryActivity.class);
            intent.putExtra(AppConstant.KEY_PARENT_DUPLICATE_EXPENSE_ITEM, jsonArray.toString());
            startActivity(intent);

            overridePendingTransition(R.anim.slide_in_forward, R.anim.slide_out_forward);
        } catch (Exception e) {

        }
    }

    //  Alert dialog action click listener
    private void showMulExpPolicyBreechSelectAlertDialog() {
        applicationAlertDialog = new ApplicationAlertDialog().getInstance(this, true, getString(R.string.approval_accept_dialog_alert_message), getString(R.string.approval_accept_dialog_alert_title), getString(R.string.button_ok), TYPE_ACCEPT);
        applicationAlertDialog.show(getSupportFragmentManager(), "dialog");
    }
    private void showMulExpApproveAlertDialog() {
        applicationAlertDialog = new ApplicationAlertDialog().getInstance(this, true, getString(R.string.multiple_approve_dialog_alert_message), getString(R.string.approval_accept_dialog_alert_title), getString(R.string.button_ok), TYPE_ACCEPT);
        applicationAlertDialog.show(getSupportFragmentManager(), "dialog");
    }
    private void showMulExpRejectionAlertDialog() {
        applicationAlertDialog = new ApplicationAlertDialog().getInstance(this, true, getString(R.string.multiple_rejection_dialog_alert_message), getString(R.string.approval_accept_dialog_alert_title), getString(R.string.button_ok), TYPE_ACCEPT);
        applicationAlertDialog.show(getSupportFragmentManager(), "dialog");
    }
    private void showAlertDialog(String msg) {
        applicationAlertDialog = new ApplicationAlertDialog().getInstance(this, true, getString(R.string.text_error_processing), msg, getString(R.string.button_ok), TYPE_ALERT);
        applicationAlertDialog.show(getSupportFragmentManager(), "dialog");
    }
    void showErrorAlertDialog(boolean neutralButton, String message, String title, String okButtonTitle, int type) {
        applicationErrorAlertDialog = new ApplicationErrorAlertDialog().getInstance(this, message, title, okButtonTitle, type);
        applicationErrorAlertDialog.show(getSupportFragmentManager(), "dialog");
    }
    private void showAlertDialog() {
        expenseAlertDialog = new ExpenseAlertDialog().getInstance(this, true, getString(R.string.alert_messageN), getString(R.string.approval_accept_dialog_alert_title), getString(R.string.button_ok));
        expenseAlertDialog.show(getSupportFragmentManager(), "dialog");
    }
    private void showAlertDialog1() {
        expenseAlertDialog = new ExpenseAlertDialog().getInstance(this, true, getString(R.string.alert_messageZ), getString(R.string.approval_accept_dialog_alert_title), getString(R.string.button_ok));
        expenseAlertDialog.show(getSupportFragmentManager(), "dialog");
    }
    private void onFailGetExpenseDetail(EODException exception) {
        showAlertDialog("Fail");
    }
    private void onErrorGetExpenseDetail(Exception exception) {
        showAlertDialog("Error!!");
    }
    private void enableNetwork() {
        applicationNetworkDialogl = new ApplicationNetworkDialog().getInstance(ExpenseListActivity.this);
        applicationNetworkDialogl.show(getSupportFragmentManager(), "dialog");
    }
    void showAcceptDialog() {
        DialogFragment newFragment = ApproverAcceptDialog.newInstance(R.string.approval_accept_dialog_title, this);
        newFragment.show(getSupportFragmentManager(), "dialog");
    }
    void showRejectionDialog() {

        DialogFragment newFragment = RejectionReasonDialog.newInstance(rejectionReasonBackupList, this);
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

}
