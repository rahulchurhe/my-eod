package com.expenseondemand.eod.screen.approval.detail.project;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.expenseondemand.eod.BaseFragment;
import com.expenseondemand.eod.R;
import com.expenseondemand.eod.model.approval.detail.project.ProjectDetailItem;

import java.util.ArrayList;

/**
 * Created by ITDIVISION\maximess087 on 13/1/17.
 */

public class DuplicateExpenseFragment extends BaseFragment {
    public static DuplicateExpenseFragment getInstance(ArrayList<ProjectDetailItem> projectDetailData) {
        return null;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.activity_duplicate_entry, container, false);

        return rootView;
    }
}
