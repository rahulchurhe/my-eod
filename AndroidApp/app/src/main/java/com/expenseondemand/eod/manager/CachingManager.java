package com.expenseondemand.eod.manager;

import com.expenseondemand.eod.model.AppRejectionSetting;
import com.expenseondemand.eod.model.DeviceInfo;
import com.expenseondemand.eod.model.UserInfo;
import com.expenseondemand.eod.model.approval.claim.ApprovalClaimListExpenseItem;

import android.app.Application;
import android.content.Context;

public class CachingManager
{
	//Save Application Context
	public static void saveAppContext(Application context)
	{
		//Get ApplicationCache
		ApplicationCache applicationCache = ApplicationCache.getInstance();

		//Save Application Context
		applicationCache.setAppContext(context);
	}

	//Get Application Context
	public static Context getAppContext()
	{
		//Get ApplicationCache
		ApplicationCache applicationCache = ApplicationCache.getInstance();

		//Get Application Context
		return (applicationCache.getAppContext());
	}
	
	//Remove Cache
	public static void removeApplicationCache()
	{
		//Get ApplicationCache
		ApplicationCache applicationCache = ApplicationCache.getInstance();

		//Remove Application Cache
		applicationCache.removeApplicationCache();
	}
	
	//Save UserInfo
	public static void saveUserInfo(UserInfo userInfo)
	{
		//Get ApplicationCache
		ApplicationCache applicationCache = ApplicationCache.getInstance();
		
		applicationCache.setUserInfo(userInfo);
	}
	
	//Get UserInfo
	public static UserInfo getUserInfo()
	{
		//Get ApplicationCache
		ApplicationCache applicationCache = ApplicationCache.getInstance();
		
		return (applicationCache.getUserInfo());
	}



	//Save DeviceId
	public static void saveDeviceInfo(DeviceInfo deviceInfo)
	{
		//Get ApplicationCache
		ApplicationCache applicationCache = ApplicationCache.getInstance();

		applicationCache.setDeviceInfo(deviceInfo);
	}

	//Get DeviceId
	public static DeviceInfo getDeviceInfo()
	{
		//Get ApplicationCache
		ApplicationCache applicationCache = ApplicationCache.getInstance();

		return (applicationCache.getDeviceInfo());
	}

	public static int getCategoryResourceId(int baseCategoryId)
	{
		ApplicationCache applicationCache = ApplicationCache.getInstance();

		return applicationCache.getCategoryResourceId(baseCategoryId);
	}

	//Save App Rejected Setting
	public static void saveAppRejectedSetting(AppRejectionSetting appRejSettings) {
		//Get ApplicationCache
		ApplicationCache applicationCache = ApplicationCache.getInstance();
		applicationCache.setAppRejSettings(appRejSettings);
	}

	//Get App Rejected Setting
	public static AppRejectionSetting getAppRejectedSetting(){
		//Get ApplicationCache
		ApplicationCache applicationCache = ApplicationCache.getInstance();
		return applicationCache.getAppRejSettings();
	}
}
