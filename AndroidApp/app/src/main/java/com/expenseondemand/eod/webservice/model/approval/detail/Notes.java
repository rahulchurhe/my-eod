package com.expenseondemand.eod.webservice.model.approval.detail;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 20-10-2016.
 */

public class Notes
{
    private String EnteredNotes;
    private String CcNotes;
    private String MobileNotes;

    private ArrayList<PreApprovedDetails> PreApprovedDetails;

    public String getEnteredNotes() {
        return EnteredNotes;
    }

    public void setEnteredNotes(String enteredNotes) {
        EnteredNotes = enteredNotes;
    }

    public String getCcNotes() {
        return CcNotes;
    }

    public void setCcNotes(String ccNotes) {
        CcNotes = ccNotes;
    }

    public String getMobileNotes() {
        return MobileNotes;
    }

    public void setMobileNotes(String mobileNotes) {
        MobileNotes = mobileNotes;
    }

    public ArrayList<PreApprovedDetails> getPreApprovedDetails() {
        return PreApprovedDetails;
    }

    public void setPreApprovedDetails(ArrayList<PreApprovedDetails> preApprovedDetails) {
        PreApprovedDetails = preApprovedDetails;
    }

    /*public String getEnteredNotes()
    {
        return enteredNotes;
    }

    public void setEnteredNotes(String enteredNotes)
    {
        this.enteredNotes = enteredNotes;
    }

    public String getcCNotes()
    {
        return cCNotes;
    }

    public void setcCNotes(String cCNotes)
    {
        this.cCNotes = cCNotes;
    }

    public String getMobileNotes()
    {
        return mobileNotes;
    }

    public void setMobileNotes(String mobileNotes)
    {
        this.mobileNotes = mobileNotes;
    }*/
}
