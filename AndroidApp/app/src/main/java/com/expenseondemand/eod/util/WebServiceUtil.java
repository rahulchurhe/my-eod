package com.expenseondemand.eod.util;

import android.os.Build;
import android.util.Log;

import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.manager.PersistentManager;
import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.model.DeviceInfo;
import com.expenseondemand.eod.model.UserCredential;
import com.expenseondemand.eod.model.UserInfo;

import java.net.HttpURLConnection;

public class WebServiceUtil 
{
	public static void initializeHeaderInRequest(HttpURLConnection httpURLConnection, String url, int httpVerb)
	{
		String loginId = null;
		String verb = (httpVerb == AppConstant.HTTP_METHOD_GET) ? "GET" : "POST";
		String timeStamp = AppUtil.getCurrentFormattedDate(AppConstant.FORMAT_DATE_NETWORK);
		DeviceInfo deviceInfo = CachingManager.getDeviceInfo();
		String osVersion = Build.VERSION.RELEASE;

		//Get userInfo from Cache
		UserInfo userInfo = CachingManager.getUserInfo();

		if(userInfo != null)
		{
			//Get UserCredential
			UserCredential userCredential = userInfo.getUserCredential();

			if(userCredential != null)
			{
				loginId = userCredential.getLoginId();
			}
		}

		String authorization = deviceInfo.getDeviceId()+verb+timeStamp+url;
		String md5Authorization = AppUtil.getMD5String(authorization);

		//Get Authorization Token from Persistent Manager
		String token = PersistentManager.getAuthorizationToken();

		httpURLConnection.setRequestProperty(AppConstant.REQUEST_HEADER_CONTENT_TYPE_KEY, AppConstant.CONTENT_TYPE_JSON);
		httpURLConnection.setRequestProperty(AppConstant.REQUEST_HEADER_DEVICE_ID, deviceInfo.getDeviceId());
		httpURLConnection.setRequestProperty(AppConstant.REQUEST_HEADER_HTTP_VERB, verb);
		httpURLConnection.setRequestProperty(AppConstant.REQUEST_HEADER_DATE_TIME_STAMP, timeStamp);
		httpURLConnection.setRequestProperty(AppConstant.REQUEST_HEADER_URL, url);
		httpURLConnection.setRequestProperty(AppConstant.REQUEST_HEADER_AUTHORIZATION_KEY, md5Authorization);
		httpURLConnection.setRequestProperty(AppConstant.REQUEST_HEADER_USER_ID_KEY, loginId);
		httpURLConnection.setRequestProperty(AppConstant.REQUEST_HEADER_ACCEPT_LANGUAGE, AppConstant.CONTENT_DEFAULT_LANGUAGE);
		httpURLConnection.setRequestProperty(AppConstant.REQUEST_HEADER_OS_VERSION, osVersion);
		httpURLConnection.setRequestProperty(AppConstant.REQUEST_HEADER_OS_TYPE, AppConstant.CONTENT_OS_TYPE);
		httpURLConnection.setRequestProperty(AppConstant.REQUEST_HEADER_APP_VERSION, deviceInfo.getAppVersion());

		if(SharePreference.getBoolean(CachingManager.getAppContext(),"IS_LOGIN")){
			httpURLConnection.setRequestProperty(AppConstant.REQUEST_HEADER_TOKEN, null);
			SharePreference.putBoolean(CachingManager.getAppContext(),"IS_LOGIN",false);
		}else{
			httpURLConnection.setRequestProperty(AppConstant.REQUEST_HEADER_TOKEN, token);
		}


		Log.v(""+AppConstant.REQUEST_HEADER_CONTENT_TYPE_KEY,"MasterData:-"+AppConstant.CONTENT_TYPE_JSON);
		Log.v(""+AppConstant.REQUEST_HEADER_DEVICE_ID,"MasterData:-"+deviceInfo.getDeviceId());
		Log.v(""+AppConstant.REQUEST_HEADER_HTTP_VERB,"MasterData:-"+verb);
		Log.v(""+AppConstant.REQUEST_HEADER_DATE_TIME_STAMP,"MasterData:-"+timeStamp);
		Log.v(""+AppConstant.REQUEST_HEADER_URL,"MasterData:-"+url);
		Log.v(""+AppConstant.REQUEST_HEADER_AUTHORIZATION_KEY,"MasterData:-"+md5Authorization);
		Log.v(""+AppConstant.REQUEST_HEADER_USER_ID_KEY,"MasterData:-"+loginId);
		Log.v(""+AppConstant.REQUEST_HEADER_ACCEPT_LANGUAGE,"MasterData:-"+AppConstant.CONTENT_DEFAULT_LANGUAGE);
		Log.v(""+AppConstant.REQUEST_HEADER_OS_VERSION,"MasterData:-"+osVersion);
		Log.v(""+AppConstant.REQUEST_HEADER_OS_TYPE,"MasterData:-"+AppConstant.CONTENT_OS_TYPE);
		Log.v(""+AppConstant.REQUEST_HEADER_APP_VERSION,"MasterData:-"+deviceInfo.getAppVersion());
		Log.v(""+AppConstant.REQUEST_HEADER_TOKEN,"MasterData:-"+token);

	}
}
