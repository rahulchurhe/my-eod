package com.expenseondemand.eod.model;

public class ExpenseCategory 
{
	private int categoryId;
	private int baseCategoryId;
	private String categoryName;

	public int getBaseCategoryId() {
		return baseCategoryId;
	}

	public void setBaseCategoryId(int baseCategoryId) {
		this.baseCategoryId = baseCategoryId;
	}

	public int getCategoryId()
	{
		return categoryId;
	}
	
	public void setCategoryId(int categoryId) 
	{
		this.categoryId = categoryId;
	}
	
	public String getCategoryName() 
	{
		return categoryName;
	}
	
	public void setCategoryName(String categoryName) 
	{
		this.categoryName = categoryName;
	}
}
