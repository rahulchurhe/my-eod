package com.expenseondemand.eod.webservice.model.approval.item;

import java.util.ArrayList;

/**
 * Created by ITDIVISION\maximess087 on 2/3/17.
 */

public class ItemResponseData {
    ArrayList<ItemsResponseModel> data;
    private String HasMorePage;

    public String getHasMorePage() {
        return HasMorePage;
    }

    public void setHasMorePage(String hasMorePage) {
        HasMorePage = hasMorePage;
    }

    public ArrayList<ItemsResponseModel> getData() {
        return data;
    }

    public void setData(ArrayList<ItemsResponseModel> data) {
        this.data = data;
    }
}
