package com.expenseondemand.eod.screen.approval.expense.itemlist;

import com.expenseondemand.eod.model.approval.expense.DuplicateItemModel;
import com.expenseondemand.eod.model.approval.expense.PolicyBreachModel;
import com.expenseondemand.eod.webservice.model.approval.item.DuplicateItemsModel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Nandani Gupta on 2016-10-20.
 */
public class ExpenseItemsModel
{
    String rowId;
    String BaseCategoryId;
    String  referenceNumber;
    String  dateExpense;
    String  customerName;
    String  projectName;
    String  expenseCategory;
    String  billable;
    String  paymentTypeName;
    String  currencyCode;
    String  expenseAmount;
    String  amount;
    String  expenseDetailID;
    String  expenseHeaderID;
    String  itemizationID;
    boolean  rejected;
    PolicyBreachModel policyBreech;
    String  notes;
    boolean  escalationflag;
    boolean  duplicateFlag;
    ArrayList<DuplicateItemsModel> duplicateItems;
    boolean  receipt;
    int receiptCount;

    public String getBaseCategoryId() {
        return BaseCategoryId;
    }

    public void setBaseCategoryId(String baseCategoryId) {
        BaseCategoryId = baseCategoryId;
    }

    public ArrayList<DuplicateItemsModel> getDuplicateItems() {
        return duplicateItems;
    }

    public void setDuplicateItems(ArrayList<DuplicateItemsModel> duplicateItems) {
        this.duplicateItems = duplicateItems;
    }

    public int getReceiptCount() {
        return receiptCount;
    }

    public void setReceiptCount(int receiptCount) {
        this.receiptCount = receiptCount;
    }

    private boolean isSelected; //Only used in ExpenseListActivity for Tracking Selection of Expenses

    public String getRowId() {
        return rowId;
    }



    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }



    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getDateExpense() {
        return dateExpense;
    }

    public void setDateExpense(String dateExpense) {
        this.dateExpense = dateExpense;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getExpenseCategory() {
        return expenseCategory;
    }

    public void setExpenseCategory(String expenseCategory) {
        this.expenseCategory = expenseCategory;
    }

    public String getBillable() {
        return billable;
    }

    public void setBillable(String billable) {
        this.billable = billable;
    }

    public String getPaymentTypeName() {
        return paymentTypeName;
    }

    public void setPaymentTypeName(String paymentTypeName) {
        this.paymentTypeName = paymentTypeName;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getExpenseAmount() {
        return expenseAmount;
    }

    public void setExpenseAmount(String expenseAmount) {
        this.expenseAmount = expenseAmount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getExpenseDetailID() {
        return expenseDetailID;
    }

    public void setExpenseDetailID(String expenseDetailID) {
        this.expenseDetailID = expenseDetailID;
    }

    public String getExpenseHeaderID() {
        return expenseHeaderID;
    }

    public void setExpenseHeaderID(String expenseHeaderID) {
        this.expenseHeaderID = expenseHeaderID;
    }

    public String getItemizationID() {
        return itemizationID;
    }

    public void setItemizationID(String itemizationID) {
        this.itemizationID = itemizationID;
    }

    public boolean isRejected() {
        return rejected;
    }

    public void setRejected(String rejected) {
        this.rejected =  (rejected != null) && (rejected.equals("R"));
    }

    public PolicyBreachModel getPolicyBreech() {
        return policyBreech;
    }

    public void setPolicyBreech(PolicyBreachModel policyBreech) {
        this.policyBreech = policyBreech;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public boolean isEscalationflag() {
        return escalationflag;
    }

    public void setEscalationflag(String escalationflag) {
        this.escalationflag = (escalationflag != null) && (escalationflag.equals("Y"));
    }

    public boolean isDuplicateFlag() {
        return duplicateFlag;
    }

    public void setDuplicateFlag(String duplicateFlag) {
        this.duplicateFlag = (duplicateFlag != null) && (!duplicateFlag.equals("U"));
    }



    public boolean hasReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = (receipt != null) && (!receipt.equals("Y"))||(!receipt.equals("V"));
    }


}
