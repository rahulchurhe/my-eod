package com.expenseondemand.eod.webservice.model.expense_category;

import com.expenseondemand.eod.webservice.model.MasterResponse;

public class CategoriesResponse extends MasterResponse
{
	private CategoriesListResponse data;

	public CategoriesListResponse getData() {
		return data;
	}

	public void setData(CategoriesListResponse data) {
		this.data = data;
	}
}
