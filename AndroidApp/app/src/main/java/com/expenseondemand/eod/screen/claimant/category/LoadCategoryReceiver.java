package com.expenseondemand.eod.screen.claimant.category;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Ramit Yadav on 12-10-2016.
 */

public class LoadCategoryReceiver extends BroadcastReceiver
{
    private LoadCategoryListener loadCategoryListener;
    public interface LoadCategoryListener
    {
        void handleCategoryDataReceived(String text);
    }

    public LoadCategoryReceiver(LoadCategoryListener loadCategoryListener)
    {
        this.loadCategoryListener = loadCategoryListener;
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {
        //if var exist only print or do some stuff

         if (intent != null)
         {
            String category_service_status = intent.getStringExtra(LoadCategoryService.PASS_SERVICE_STATUS_KEY);

            Log.d(LoadCategoryService.PASS_SERVICE_STATUS_KEY,category_service_status);

            loadCategoryListener.handleCategoryDataReceived(category_service_status);
        }
    }
}
