package com.expenseondemand.eod.model.approval.detail.project;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ITDIVISION\maximess087 on 22/2/17.
 */

public class ProjectDetailsItem implements Parcelable {
    private String LabelName;
    private String Value;

    public  ProjectDetailsItem(){}
    public ProjectDetailsItem(Parcel in) {
        LabelName = in.readString();
        Value = in.readString();
    }

    public static final Creator<ProjectDetailsItem> CREATOR = new Creator<ProjectDetailsItem>() {
        @Override
        public ProjectDetailsItem createFromParcel(Parcel in) {
            return new ProjectDetailsItem(in);
        }

        @Override
        public ProjectDetailsItem[] newArray(int size) {
            return new ProjectDetailsItem[size];
        }
    };

    public String getLabelName() {
        return LabelName;
    }

    public void setLabelName(String labelName) {
        LabelName = labelName;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(LabelName);
        parcel.writeString(Value);
    }
}
