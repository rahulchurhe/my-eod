package com.expenseondemand.eod.webservice.model.approval.detail;

import java.util.ArrayList;

/**
 * Created by ITDIVISION\maximess087 on 23/2/17.
 */

public class ApproveRejectData {
    private ArrayList<LstApproverReject> LstApproverReject;

    public ArrayList<LstApproverReject> getLstApproverReject() {
        return LstApproverReject;
    }

    public void setLstApproverReject(ArrayList<LstApproverReject> lstApproverReject) {
        LstApproverReject = lstApproverReject;
    }
}
