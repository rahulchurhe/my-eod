package com.expenseondemand.eod.manager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;
import android.widget.Toast;

import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;

import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;

public class DeviceManager
{
	public static boolean isOnline()
	{
		//Get Application Context
		Context appContext = CachingManager.getAppContext();

		//Get ConnectivityManager
		ConnectivityManager connectivityManager = (ConnectivityManager)appContext.getSystemService(Context.CONNECTIVITY_SERVICE);

		//Get NetworkInfo
		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

		return ((networkInfo == null || !networkInfo.isConnected()) ? false : true);
	}
	public static boolean isInternetWorking() {
		boolean success = false;
		try {
			URL url = new URL("https://google.com");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setConnectTimeout(10000);
			connection.connect();
			success = connection.getResponseCode() == 200;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return success;
	}
	//Check Network Connection
	public static boolean isNetworkAvailable()
	{

		boolean isConnected = false;

		try
		{
			Context appContext = CachingManager.getAppContext();
			ConnectivityManager connectivity = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);

			if (connectivity != null)
			{
				int numberOfNetworkInfo = 0;
				NetworkInfo[] networkInfo = connectivity.getAllNetworkInfo();

				if (networkInfo != null)
				{
					numberOfNetworkInfo = networkInfo.length;
					for (int i = 0; i < numberOfNetworkInfo; i++)
					{
						if (networkInfo[i].getState() == NetworkInfo.State.CONNECTED)
						{
							isConnected = true;
							break;
						}
					}
				}
			}
		}
		catch (Exception exception)
		{
			// Consume it
		}

		return isConnected;
	}
	public static File getImageFile(String directoryName, String fileName)
	{
		File fileObject = null;
		File folderFileObject = getFolderOnExternalDirectory(directoryName);

		if (folderFileObject != null)
		{
			fileObject = new File(folderFileObject, fileName);
		}
		return fileObject;
	}

	public static File getFolderOnExternalDirectory(String folderName)
	{
		//Folder
		boolean doesFolderExists = false;
		File folderFileObject = null;

		Context context = CachingManager.getAppContext();

		//Get External Directory
		File externalFilesDirectory = context.getExternalFilesDir(null);

		if (!AppUtil.isStringEmpty(folderName))
		{
			//Get App Directory
			folderFileObject = new File(externalFilesDirectory, folderName);

			//Check if App Directory exits
			doesFolderExists = folderFileObject.exists();

			//If Doesn't exist then create one (Will happen First time only)
			if (!doesFolderExists)
			{
				doesFolderExists = folderFileObject.mkdir();

				if(!doesFolderExists)
				{
					folderFileObject = externalFilesDirectory;
				}
			}
		}
		else
		{
			folderFileObject = externalFilesDirectory;
		}

		return folderFileObject;
	}

	/**
	 * openSettings
	 * redirecting to app settings
	 * @param activityContext
	 * @param requestCode
	 */
	public static void openSettings(Activity activityContext, int requestCode)
	{
		Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
		Uri uri = Uri.fromParts("package", activityContext.getPackageName(), null);
		intent.setData(uri);
		activityContext.startActivityForResult(intent, requestCode);
	}
}
