package com.expenseondemand.eod.screen.approval.claim.ExpenseTab;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.model.approval.claim.PreApprovedDetails;
import com.expenseondemand.eod.model.approval.detail.dynamic.DynamicDetail;
import com.expenseondemand.eod.screen.approval.detail.DynamicDetailAdapter;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 01-10-2016.
 */


public class PreApprovalDynamicDetailsDialog extends DialogFragment implements DynamicDetailAdapter.ItemHeightListener {
    private static String strTitle;
    private RecyclerView recyclerViewDynamicDetail;
    private TextView textViewOk;

    private ArrayList<PreApprovedDetails> dynamicDetailData;
    private TextView textViewTitle;

    public static PreApprovalDynamicDetailsDialog getInstance(ArrayList<PreApprovedDetails> dynamicDetailData, String title) {
        PreApprovalDynamicDetailsDialog dynamicDetailsDialog = new PreApprovalDynamicDetailsDialog();
        dynamicDetailsDialog.dynamicDetailData = dynamicDetailData;
        strTitle = title;
        return dynamicDetailsDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onPause() {
        super.onPause();
        dismiss();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.dynamic_details_dialog_fragment, container, false);

        textViewTitle = (TextView) view.findViewById(R.id.textViewTitle);
        textViewTitle.setText(strTitle);
        textViewOk = (TextView) view.findViewById(R.id.textViewOk);
        recyclerViewDynamicDetail = (RecyclerView) view.findViewById(R.id.recyclerViewDynamicDetail);
        final LinearLayoutManager homeItemLinerLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerViewDynamicDetail.setLayoutManager(homeItemLinerLayoutManager);

        textViewOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        PreApprovalDynamicDetailAdapter dynamicDetailAdapter = new PreApprovalDynamicDetailAdapter(getActivity(), this, dynamicDetailData);
        recyclerViewDynamicDetail.setAdapter(dynamicDetailAdapter);

        return view;
    }

    @Override
    public void onMeasureItemHeight(int height) {
        ViewGroup.LayoutParams params = recyclerViewDynamicDetail.getLayoutParams();
        params.height = height * recyclerViewDynamicDetail.getChildCount();
        recyclerViewDynamicDetail.setLayoutParams(params);
    }
}
