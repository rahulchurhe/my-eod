package com.expenseondemand.eod.screen.claimant.create;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.activity.BaseActivity;
import com.expenseondemand.eod.model.SelectedDate;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;

public class CalendarActivity extends BaseActivity
{
	private Calendar currentCalendar = Calendar.getInstance();
	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(AppConstant.FORMAT_DATE_CALENDER, Locale.getDefault());

	private Resources resources;
	private TextView textViewMonth = null;
	private CalendarAdapter calendarAdapter = null;

	@SuppressWarnings("unused")
	private int cellWidth = 0;
	private int cellHeight = 0;

	//Selected Calendar Dates
	private ArrayList<Date> selectedCalendarDates = null;

	//Flag - Whether Multiple Selection is allowed or not.
	private boolean areMultipleDatesAllowed = false;

	//Displayed Month and Year
	private int currentDisplayedMonth = -1;
	private int currentDisplayedYear = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.calendar_screen_layout);

		initializeView(savedInstanceState);
	}
	@SuppressWarnings("unchecked")
	private void initializeView(Bundle savedInstanceState)
	{
		//Get resources
		resources = getResources();

		//Get calling Intent
		Intent intent = getIntent();


		//Prepare Action Bar
		//handleActionBar(this, 0, getString(R.string.title_calendar));
		handleToolBar(getString(R.string.title_calendar), R.color.home_item_create_expense_selected, R.color.home_item_create_expense, true);

		//Prepare the Position 
		prepareCalendarPosition();

		//Get Dates from Calling Component
		selectedCalendarDates = (ArrayList<Date>) intent.getSerializableExtra(AppConstant.KEY_SELECTED_CALENDAR_DATES);
		selectedCalendarDates = (selectedCalendarDates != null) ? selectedCalendarDates : new ArrayList<Date>();

		//Get Flag (Multiple Dates Allowed) from Calling Component
		areMultipleDatesAllowed = intent.getBooleanExtra(AppConstant.KEY_MULTIPLE_DATES_ALLOWED, false);

		if(areMultipleDatesAllowed)
		{
			View viewCalendarBottomBorder = (View) findViewById(R.id.viewCalendarBottomBorder);
			viewCalendarBottomBorder.setVisibility(View.VISIBLE);

			TextView textViewHintForMultipleSelection = (TextView) findViewById(R.id.textViewHintForMultipleSelection);
			textViewHintForMultipleSelection.setVisibility(View.VISIBLE);
		}
		//Get views references
		textViewMonth = (TextView) findViewById(R.id.textViewMonth);
		textViewMonth.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				//calendarAdapter.nextYear();
			}
		});

		restorePreviousCalendarSelections(savedInstanceState);

		//Preparing Grid View, Adapter and On Item Listener
		GridView gridViewCalendar = (GridView) findViewById(R.id.gridViewCalendar);

		calendarAdapter = new CalendarAdapter();
		gridViewCalendar.setAdapter(calendarAdapter);

		GridViewItemClickListener gridViewItemClickListener = new GridViewItemClickListener();
		gridViewCalendar.setOnItemClickListener(gridViewItemClickListener);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		int itemId = item.getItemId();

		switch(itemId)
		{
		case android.R.id.home:
			handleBackNavigation();
			break;
		case R.id.menu_item_done:
			handleDateSelectionDone();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void handleBackNavigation()
	{
		CalendarActivity.this.finish();
		overridePendingTransition(R.anim.slide_in_back, R.anim.slide_out_back);
	}

	@SuppressWarnings("unchecked")
	private void restorePreviousCalendarSelections(Bundle savedInstanceState)
	{
		// restore last saves instance
		if (savedInstanceState != null) 
		{
			selectedCalendarDates = (ArrayList<Date>) savedInstanceState.getSerializable(AppConstant.KEY_SAVED_SELECTED_DATES);
			currentDisplayedMonth = savedInstanceState.getInt(AppConstant.KEY_CALENDER_MONTH);
			currentDisplayedYear = savedInstanceState.getInt(AppConstant.KEY_CALENDER_YEAR);
		} 
		else 
		{
			currentDisplayedMonth = currentCalendar.get(Calendar.MONTH);
			currentDisplayedYear = currentCalendar.get(Calendar.YEAR);
		}
	}

	private void prepareCalendarPosition()
	{
		// get dimension of screen
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int screenHeight = displaymetrics.heightPixels;
		int screenWidth = displaymetrics.widthPixels;
		int actionBarHeight = 0;

		cellWidth = 0;
		cellHeight = 0;

		//TODO
		TypedValue tv = new TypedValue();
		if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) 
		{
			actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
		}

		// get Drawable of gradient
		currentCalendar.setFirstDayOfWeek(Calendar.SUNDAY);

		if (screenWidth < screenHeight)// Portrait mode
		{
			int numberOfPixels = resources.getDimensionPixelSize(R.dimen.calendar_grid_border);
			int screenWidthForGrid = screenWidth - 8* numberOfPixels;
			// set cell height
			cellHeight = cellWidth = screenWidthForGrid / 7;
		} 
		else// landscape mode
		{
			RelativeLayout relativeLayoutCalendar = (RelativeLayout) findViewById(R.id.relativeLayoutCalendar);

			// get layout params to set height and width
			RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) relativeLayoutCalendar.getLayoutParams();
			layoutParams.width = screenHeight;
			relativeLayoutCalendar.setLayoutParams(layoutParams);

			// set cell height
			cellHeight = cellWidth = (screenHeight - actionBarHeight) / 8;
		}
	}

	// Updates months and year on splash_top_to_down of calendar
	private void updateCalendarHeader(Calendar currentCalendar) 
	{
		String monthText = simpleDateFormat.format(currentCalendar.getTime());
		textViewMonth.setText(monthText);
	}

	private class GridViewItemClickListener implements OnItemClickListener 
	{
		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int position, long id)
		{
			//Get the Day of Month
			TextView textView = (TextView) view.findViewById(R.id.textViewGrid);
			String dayOfMonthString = textView.getText().toString();
			int dayOfMonth = AppUtil.convertToInt(dayOfMonthString);

			//Get the Date object from Day of Month, Month and Year
			Date date = AppUtil.getDate(dayOfMonth, currentDisplayedMonth, currentDisplayedYear);

			//Multiple Dates Selection is allowed
			if (areMultipleDatesAllowed)
			{
				boolean isCalendarCellDateSelected = isCalendarCellDateSelected(date);
				if (isCalendarCellDateSelected)
				{
					selectedCalendarDates.remove(date);

					if(isCurrentDateCell(dayOfMonth, currentDisplayedMonth, currentDisplayedYear))
					{
						prepareCurrentDateCell(view);
					}
					else
					{
						prepareNormalDateCell(view);
					}
				}
				else
				{
					selectedCalendarDates.add(date);
					prepareSelectedDateCell(view);
				}
			}
			else
			{
				selectedCalendarDates.clear();
				selectedCalendarDates.add(date);

				prepareSelectedDateCell(view);

				handleDateSelectionDone();
			}
		}

		private boolean isCurrentDateCell(int dayOfMonth, int currentDisplayedMonth, int currentDisplayedYear) 
		{
			boolean isCurrentDateCell = false;

			Calendar defaultCalender = Calendar.getInstance();
			int currentDate = defaultCalender.get(Calendar.DAY_OF_MONTH);
			int currentMonth = defaultCalender.get(Calendar.MONTH);
			int currentYear = defaultCalender.get(Calendar.YEAR);

			if(dayOfMonth == currentDate && currentDisplayedMonth == currentMonth && currentDisplayedYear == currentYear)
			{
				isCurrentDateCell = true;
			}

			return isCurrentDateCell;
		}

	}

	private void handleDateSelectionDone()
	{
		Intent intent = getIntent();
		intent.putExtra(AppConstant.KEY_SELECTED_CALENDAR_DATES, selectedCalendarDates);
		setResult(RESULT_OK, intent);

		CalendarActivity.this.finish();
		overridePendingTransition(R.anim.slide_in_back, R.anim.slide_out_back);
	}

	// adapter class to fill data in grid
	class CalendarAdapter extends BaseAdapter 
	{
		private Calendar calendar = (Calendar) currentCalendar.clone();
		int length;
		boolean isCurrentDate;

		CalendarAdapter() 
		{
			prepareCalendarData();
		}

		@Override
		public boolean areAllItemsEnabled() 
		{
			return false;
		}

		@Override
		public boolean isEnabled(int position) 
		{
			return isDateCellEnabled(position);
		}

		private boolean isDateCellEnabled(int position)
		{
			boolean isDateCellEnabled = true;
			SelectedDate selectedDate = (SelectedDate) getItem(position);
			Integer date = selectedDate.getDate();

			//Disables tap
			if ((position < 7 && date >= 20) || (date < 7 && position > 20)) 
			{
				isDateCellEnabled =  false;
			}

			return isDateCellEnabled;
		}

		private boolean isCurrentDateCell(SelectedDate selectedDate)
		{
			boolean isCurrentDateCell = false;

			Calendar defaultCalender = Calendar.getInstance();
			int currentDate = defaultCalender.get(Calendar.DAY_OF_MONTH);
			int currentMonth = defaultCalender.get(Calendar.MONTH);
			int currentYear = defaultCalender.get(Calendar.YEAR);

			Integer date = selectedDate.getDate();
			Integer month = selectedDate.getMonth();
			Integer year = selectedDate.getYear();

			if(date == currentDate && month == currentMonth && year == currentYear)
			{
				isCurrentDateCell = true;
			}

			return isCurrentDateCell;
		}

		private void prepareCalendarData() 
		{
			int firstDay = 0;

			calendar.set(Calendar.MONTH, currentDisplayedMonth);
			calendar.set(Calendar.YEAR, currentDisplayedYear);

			//Show name and year of month
			updateCalendarHeader(calendar);

			//Move to first date
			calendar.set(Calendar.DAY_OF_MONTH, 1);

			int startWeek = calendar.get(Calendar.WEEK_OF_YEAR);
			if (calendar.get(Calendar.MONTH) == 0 && startWeek != 1) 
			{
				startWeek = 0;
			}

			calendar.add(Calendar.MONTH, 1);
			calendar.add(Calendar.DAY_OF_MONTH, -1);

			int endWeek = calendar.get(Calendar.WEEK_OF_YEAR);

			if (endWeek > startWeek) 
			{
				length = (endWeek - startWeek + 1) * 7;
			} 
			else 
			{
				calendar.add(Calendar.WEEK_OF_YEAR, -1);
				endWeek = calendar.get(Calendar.WEEK_OF_YEAR);
				length = (endWeek - startWeek + 2) * 7;
			}

			// calendar.add(Calendar.MONTH, -1);
			calendar.set(Calendar.DAY_OF_MONTH, 1);

			firstDay = calendar.get(Calendar.DAY_OF_WEEK);

			calendar.add(Calendar.DAY_OF_MONTH, -firstDay + 1);
		}

		// change month
		public void nextMonth() 
		{
			int date = calendar.get(Calendar.DATE);

			if (date != 1) 
			{
				calendar.set(Calendar.DATE, 1);
				calendar.add(Calendar.MONTH, 1);
			}
			calendar.add(Calendar.MONTH, 1);

			updateCalendar();
		}

		// change month
		public void previousMonth() 
		{
			int date = calendar.get(Calendar.DATE);

			if (date == 1) 
			{
				calendar.add(Calendar.MONTH, -1);
			}

			updateCalendar();
		}

		//Change Year
		public void nextYear()
		{
			int date = calendar.get(Calendar.DATE);

			if (date != 1)
			{
				calendar.set(Calendar.DATE, 1);
				calendar.add(Calendar.MONTH, 3);
			}
			calendar.add(Calendar.MONTH, 1);

			updateCalendar();
		}

		private void updateCalendar()
		{
			currentDisplayedMonth = calendar.get(Calendar.MONTH);
			currentDisplayedYear = calendar.get(Calendar.YEAR);

			//Update the Header of Calendar
			updateCalendarHeader(calendar);

			//Prepare Calendar Data (Number of Cells, etc.)
			prepareCalendarData();

			//Notify Adapter
			notifyDataSetChanged();
		}

		@Override
		public int getCount() 
		{
			return length;
		}

		@Override
		public Object getItem(int position) 
		{
			if (position != 0)
			{
				calendar.add(Calendar.DAY_OF_MONTH, position);
			}

			//Get date at desired location
			int date = calendar.get(Calendar.DATE);
			int month = calendar.get(Calendar.MONTH);
			int year = calendar.get(Calendar.YEAR);

			SelectedDate selectedDate = new SelectedDate();
			selectedDate.setDate(date);
			selectedDate.setMonth(month);
			selectedDate.setYear(year);

			// set current date flag
			isCurrentDate  = (calendar.equals(currentCalendar)) ? true : false; 

			if (position != 0)
			{
				calendar.add(Calendar.DAY_OF_MONTH, -position);
			}

			return selectedDate;
		}

		@Override
		public long getItemId(int position)
		{
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) 
		{

			TextView textView = null;

			SelectedDate selectedDate = (SelectedDate) getItem(position);
			Integer date = selectedDate.getDate();

			if (convertView == null) 
			{
				convertView = getLayoutInflater().inflate(R.layout.calender_grid_item, null);
				textView = (TextView) convertView.findViewById(R.id.textViewGrid);
				textView.setHeight(cellHeight);
			} 
			else 
			{
				textView = (TextView) convertView.findViewById(R.id.textViewGrid);
			}

			if (isDateCellEnabled(position)) 
			{
				//If the date of this cell is present in the Container of selected dates then prepare the cell view 
				boolean isCalendarCellDateSelected = isCalendarCellDateSelected(selectedDate.getDateObject());
				if (isCalendarCellDateSelected)
				{
					prepareSelectedDateCell(convertView);
				}
				else if(isCurrentDateCell(selectedDate))
				{
					prepareCurrentDateCell(convertView);
				}
				else
				{
					prepareNormalDateCell(convertView);
				}
			} 
			else 
			{
				prepareDisabledDateCell(convertView);		
			}

			// set date in text grid cell
			textView.setText(String.valueOf(date));

			return convertView;
		}
	}

	public void showNextMonth(View view) 
	{
		calendarAdapter.nextMonth();
	}

	public void showPreviousMonth(View view) 
	{
		calendarAdapter.previousMonth();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) 
	{
		if (outState == null) 
		{
			outState = new Bundle();
		}

		outState.putInt(AppConstant.KEY_CALENDER_MONTH, currentDisplayedMonth);
		outState.putInt(AppConstant.KEY_CALENDER_YEAR, currentDisplayedYear);

		if(selectedCalendarDates != null)
		{
			outState.putSerializable(AppConstant.KEY_SAVED_SELECTED_DATES, selectedCalendarDates);
		}
		super.onSaveInstanceState(outState);
	}

	private void prepareSelectedDateCell(View view)
	{
		prepareCalendarCell(view, R.drawable.calendar_selected_date_gradient, R.color.calendar_selected_cell_font_color);
	}

	private void prepareCurrentDateCell(View view)
	{
		prepareCalendarCell(view, R.drawable.current_date_gradient, R.color.calendar_selected_cell_font_color);
	}

	private void prepareDisabledDateCell(View view)
	{
		prepareCalendarCell(view, R.drawable.calendar_cell_background_shape, R.color.disable_font_color);
	}

	private void prepareNormalDateCell(View view)
	{
		prepareCalendarCell(view, R.drawable.calendar_cell_background_shape, R.color.enable_font_color);
	}

	private void prepareCalendarCell(View view, int drawableResId, int colorResId)
	{
		Drawable drawable = resources.getDrawable(drawableResId);

		LinearLayout linearLayoutView = (LinearLayout)view; 
		linearLayoutView.setBackgroundDrawable(drawable);

		TextView textView = (TextView) view.findViewById(R.id.textViewGrid);
		if (textView != null)
		{
			textView.setTextColor(resources.getColor(colorResId));
		}
	}

	private boolean isCalendarCellDateSelected(Date date)
	{
		boolean isCalendarCellDateSelected = false;
		if (selectedCalendarDates != null)
		{
			isCalendarCellDateSelected = selectedCalendarDates.contains(date);
		}

		return isCalendarCellDateSelected;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		if(areMultipleDatesAllowed)
		{
			MenuInflater inflater = getMenuInflater();
			//menu for save the expense
			inflater.inflate(R.menu.calendar_menu, menu);
		}
		return true;
	}

	@Override
	public void onBackPressed()
	{
		CalendarActivity.this.finish();
		overridePendingTransition(R.anim.slide_in_back, R.anim.slide_out_back);

		super.onBackPressed();
	}	
}