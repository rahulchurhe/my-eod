package com.expenseondemand.eod.webservice.model.approval.detail.mileage;

/**
 * Created by Ramit Yadav on 20-10-2016.
 */

public class MileageLeg
{
    private String from;
    private String to;
    private String miles;
    private String amount;

    public String getFrom()
    {
        return from;
    }

    public void setFrom(String from)
    {
        this.from = from;
    }

    public String getTo()
    {
        return to;
    }

    public void setTo(String to)
    {
        this.to = to;
    }

    public String getMiles()
    {
        return miles;
    }

    public void setMiles(String miles)
    {
        this.miles = miles;
    }

    public String getAmount()
    {
        return amount;
    }

    public void setAmount(String amount)
    {
        this.amount = amount;
    }
}
