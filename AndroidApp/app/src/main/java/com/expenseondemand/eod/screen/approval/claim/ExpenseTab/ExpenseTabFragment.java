package com.expenseondemand.eod.screen.approval.claim.ExpenseTab;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.expenseondemand.eod.BaseFragment;
import com.expenseondemand.eod.R;
import com.expenseondemand.eod.dialogFragment.ApplicationAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationErrorAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationNetworkDialog;
import com.expenseondemand.eod.dialogFragment.ExpenseAlertDialog;
import com.expenseondemand.eod.exception.EODBusinessException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.DeviceManager;
import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.model.approval.claim.ApprovalClaimListExpenseItem;
import com.expenseondemand.eod.screen.approval.claim.ClaimManager;
import com.expenseondemand.eod.screen.approval.expense.itemlist.ExpenseListActivity;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;
import com.expenseondemand.eod.util.UIUtil;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 12-09-2016.
 */
public class ExpenseTabFragment extends BaseFragment implements EndlessListView.EndLessListener, ExpenseListTabAdapter.ExpenseItemEventListener, ExpenseAlertDialog.AlertDialogActionListener, ApplicationAlertDialog.AlertDialogActionListener, ApplicationErrorAlertDialog.ErrorDialogActionListener {
    public static final int TYPE_LOGOUT = 0;
    public static final int TYPE_ERROR = -1;
    private ArrayList<ApprovalClaimListExpenseItem> claimData;
    private ApplicationNetworkDialog applicationNetworkDialogl;
    LinearLayout linearNodataScreen;
    private TextView textViewNodataMessage;
    private ApplicationAlertDialog applicationAlertDialog;
    private ExpenseAlertDialog expenseAlertDialog;
    private ApplicationErrorAlertDialog applicationErrorAlertDialog;
    private ListView listView_approvalClaimList;
    private View footerView;
    private boolean isLoadingMoreExpenses;
    int counter = 10;
    int pageNo = 1;
    private ExpenseListTabAdapter adapter;
    private EndlessListView eLView;
    private View rootView;
    private Dialog dialog1 = null;
    private Dialog dialog = null;

    @Override
    public void onResume() {
        super.onResume();
        /*Back from expense list screen*/
        if (SharePreference.getBoolean(getActivity(), AppConstant.BACK_EXPENSE_DETAIL_LIST)) {
            SharePreference.putBoolean(getActivity(), AppConstant.BACK_EXPENSE_DETAIL_LIST, false);
            pageNo = 1;
            claimData.clear();
            adapter.notifyDataSetChanged();
            getClaimList();

        }

        /*Back from network setting screen */
        if (SharePreference.getBoolean(getActivity(), AppConstant.BACK_NETWORK_SETTING_STAT)) {
            SharePreference.putBoolean(getActivity(), AppConstant.BACK_NETWORK_SETTING_STAT, false);
            pageNo = 1;
            claimData.clear();
            adapter.notifyDataSetChanged();
            getClaimList();
        }

        /*For Lazy loading loader visibility after back from ExpenseList activity*/
        adapter = new ExpenseListTabAdapter(getActivity(), claimData, this);
        eLView = (EndlessListView) rootView.findViewById(R.id.myListView);
        eLView.setLoadingView(R.layout.footer_loader);
        eLView.setListener(this);
        eLView.setAdapter(adapter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.approval_claim_expense_tab_fragment, container, false);
        initializeView(rootView);

        /*Deputy assign alert dialog*/
        if (!SharePreference.getBoolean(getActivity(), AppConstant.IS_DEPUTY_ASSIGNED)) {
            showMulExpApproveAlertDialog();
        }

        return rootView;
    }

    //Initialize View
    private void initializeView(View rootView) {
        listView_approvalClaimList = (ListView) rootView.findViewById(R.id.listView_approvalClaimList);

        linearNodataScreen = (LinearLayout) rootView.findViewById(R.id.linearNodataScreen);
        textViewNodataMessage = (TextView) rootView.findViewById(R.id.textViewNodataMessage);
        textViewNodataMessage.setText(getResources().getString(R.string.approver_text_empty_expense_list));

        claimData = new ArrayList<>();
        claimData.clear();

        getClaimList();
    }

    private void getClaimList() {
        if (DeviceManager.isOnline()) {
            ClaimAsyncTask claimAsyncTask = new ClaimAsyncTask();
            claimAsyncTask.execute();
        } else {
            enableNetwork();
        }

        adapter = new ExpenseListTabAdapter(getActivity(), claimData, this);
        eLView = (EndlessListView) rootView.findViewById(R.id.myListView);
        eLView.setLoadingView(R.layout.footer_loader);
        eLView.setListener(this);
        eLView.setAdapter(adapter);
    }

    @Override
    public void loadData() {
        pageNo += 1;
        new LazyLoaderAsyncTask().execute();
    }

    @Override
    public void onRetryButtonClick(int type) {
        if (DeviceManager.isOnline()) {
            pageNo = 0;
            claimData.clear();
            adapter.notifyDataSetChanged();
            eLView.LoadMore();
            dialog1 = UIUtil.showProgressDialog(getActivity());
        } else {
            enableNetwork();
        }
    }

    @Override
    public void onCancelButtonClick() {
        UIUtil.dismissDialog(dialog);
        UIUtil.dismissDialog(dialog1);
    }

    @Override
    public void handleExpenseItemClick(int position) {
        ApprovalClaimListExpenseItem approvalClaimListExpenseItem = claimData.get(position);

        if (approvalClaimListExpenseItem.isBreachedClaim() && !approvalClaimListExpenseItem.isStatus()) {
            SharePreference.putBoolean(getActivity(), AppConstant.isCounterApprover, true);
        } else if (approvalClaimListExpenseItem.isStatus()) {
            SharePreference.putBoolean(getActivity(), AppConstant.isCounterApprover, true);
        } else {
            SharePreference.putBoolean(getActivity(), AppConstant.isCounterApprover, false);
        }

        navigateToApproverExpenseList(approvalClaimListExpenseItem.getClaimId(), approvalClaimListExpenseItem.getClaimType(), approvalClaimListExpenseItem.getClaimantName());
    }

    @Override
    public void handlePreApprovedDetails(int position) {
        if (claimData != null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            PreApprovalDynamicDetailsDialog dynamicDetailsDialog = PreApprovalDynamicDetailsDialog.getInstance(claimData.get(position).getPreApprovedDetails(), "Pre-approval details");
            dynamicDetailsDialog.show(fragmentTransaction, "Dialog");
        }
    }

    @Override
    public void onPositiveButtonClick(int type) {}

    @Override
    public void onPositiveButtonClick() {
        getActivity().finish();
    }

    @Override
    public void onNegativeButtonClick() {}

    //LazyLoader Async task
    private class LazyLoaderAsyncTask extends AsyncTask<Void, Void, ArrayList<ApprovalClaimListExpenseItem>> {
        private Exception exception = null;

        @Override
        protected ArrayList<ApprovalClaimListExpenseItem> doInBackground(Void... params) {
            ArrayList<ApprovalClaimListExpenseItem> claimList = null;

            try {
                claimList = ClaimManager.getClaims(counter, pageNo);
            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return claimList;
        }

        @Override
        protected void onPostExecute(ArrayList<ApprovalClaimListExpenseItem> result) {
            super.onPostExecute(result);
            if (exception == null) {
                if (dialog1 != null) {
                    UIUtil.dismissDialog(dialog1);
                }
                eLView.addNewData(result);
            } else if (exception instanceof EODBusinessException) {
                onFailGetClaimList((EODException) exception);
            } else if (exception instanceof EODException) {
                onErrorGetClaimList(exception);
            }
        }
    }

    //Navigate to Expense List
    private void navigateToApproverExpenseList(String claimId, String claimType, String claimName) {
        Intent intent = new Intent(getActivity(), ExpenseListActivity.class);
        intent.putExtra(AppConstant.KEY_CLAIM_ID, claimId);
        intent.putExtra(AppConstant.KEY_CLAIM_TYPE, claimType);
        intent.putExtra(AppConstant.KEY_CLAIM_NAME, claimName);
        startActivity(intent);
    }

    //Claim AsyncTask
    private class ClaimAsyncTask extends AsyncTask<Void, Void, ArrayList<ApprovalClaimListExpenseItem>> {
        private Exception exception = null;

        @Override
        protected void onPreExecute() {
            isLoadingMoreExpenses = true;
            dialog = UIUtil.showProgressDialog(getActivity());
        }

        @Override
        protected ArrayList<ApprovalClaimListExpenseItem> doInBackground(Void... params) {
            ArrayList<ApprovalClaimListExpenseItem> claimList = null;

            try {
                claimList = ClaimManager.getClaims(counter, pageNo);
            } catch (EODException eodException) {
                this.exception = eodException;
            }

            return claimList;
        }

        @Override
        protected void onPostExecute(ArrayList<ApprovalClaimListExpenseItem> claimList) {
            //Dismiss progress dialog.
            isLoadingMoreExpenses = false;
            UIUtil.dismissDialog(dialog);

            if (exception == null) {
                //OnSucess
                onSuccessGetStatistics(claimList);
            } else if (exception instanceof EODBusinessException) {
                //OnFail
                onFailGetClaimList((EODException) exception);
            } else if (exception instanceof EODException) {
                //OnError
                onErrorGetClaimList(exception);
            }
        }
    }
    private void onSuccessGetStatistics(ArrayList<ApprovalClaimListExpenseItem> claimList) {
        if (!AppUtil.isCollectionEmpty(claimList)) {
            claimData.addAll(claimList);
            adapter.notifyDataSetChanged();
            linearNodataScreen.setVisibility(View.GONE);
        } else {
            linearNodataScreen.setVisibility(View.VISIBLE);
        }
    }
    private void onFailGetClaimList(EODException exception) {
        showAlertDialog(true, exception.getExceptionMessage(), null, null, ApplicationAlertDialog.TYPE_ERROR);
    }
    private void onErrorGetClaimList(Exception exception) {
        eLView.stopFooter();
        showErrorAlertDialog(null, null, null, TYPE_ERROR);
    }

    /*Alert Dialog*/
    private void showMulExpApproveAlertDialog() {
        expenseAlertDialog = new ExpenseAlertDialog().getInstance(this, true, getString(R.string.label_msg_deputy_assigned), getString(R.string.approval_accept_dialog_alert_title), getString(R.string.button_ok));
        expenseAlertDialog.show(getFragmentManager(), "dialog");
    }
    void showAlertDialog(boolean neutralButton, String message, String title, String okButtonTitle, int type) {
        applicationAlertDialog = new ApplicationAlertDialog().getInstance(this, neutralButton, message, title, okButtonTitle, type);
        applicationAlertDialog.show(getFragmentManager(), "dialog");
    }
    void showErrorAlertDialog(String message, String title, String okButtonTitle, int type) {
        applicationErrorAlertDialog = new ApplicationErrorAlertDialog().getInstance(this, message, title, okButtonTitle, type);
        applicationErrorAlertDialog.show(getFragmentManager(), "dialog");
    }
    //Network enable dialog
    void enableNetwork() {
        applicationNetworkDialogl = new ApplicationNetworkDialog().getInstance(getActivity());
        applicationNetworkDialogl.show(getFragmentManager(), "dialog");
    }
}
