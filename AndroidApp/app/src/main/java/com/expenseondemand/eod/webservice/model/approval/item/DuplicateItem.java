package com.expenseondemand.eod.webservice.model.approval.item;

/**
 * Created by ITDIVISION\maximess087 on 13/1/17.
 */

public class DuplicateItem {
    String FieldTitle1;
    String FieldValue1;

    String FieldTitle2;
    String FieldValue2;

    String FieldTitle3;
    String FieldValue3;

    String FieldTitle4;
    String FieldValue4;

    public String getFieldTitle1() {
        return FieldTitle1;
    }

    public void setFieldTitle1(String fieldTitle1) {
        FieldTitle1 = fieldTitle1;
    }

    public String getFieldValue1() {
        return FieldValue1;
    }

    public void setFieldValue1(String fieldValue1) {
        FieldValue1 = fieldValue1;
    }

    public String getFieldTitle2() {
        return FieldTitle2;
    }

    public void setFieldTitle2(String fieldTitle2) {
        FieldTitle2 = fieldTitle2;
    }

    public String getFieldValue2() {
        return FieldValue2;
    }

    public void setFieldValue2(String fieldValue2) {
        FieldValue2 = fieldValue2;
    }

    public String getFieldTitle3() {
        return FieldTitle3;
    }

    public void setFieldTitle3(String fieldTitle3) {
        FieldTitle3 = fieldTitle3;
    }

    public String getFieldValue3() {
        return FieldValue3;
    }

    public void setFieldValue3(String fieldValue3) {
        FieldValue3 = fieldValue3;
    }

    public String getFieldTitle4() {
        return FieldTitle4;
    }

    public void setFieldTitle4(String fieldTitle4) {
        FieldTitle4 = fieldTitle4;
    }

    public String getFieldValue4() {
        return FieldValue4;
    }

    public void setFieldValue4(String fieldValue4) {
        FieldValue4 = fieldValue4;
    }
}
