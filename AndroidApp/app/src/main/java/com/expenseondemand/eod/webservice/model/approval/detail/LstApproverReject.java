package com.expenseondemand.eod.webservice.model.approval.detail;

/**
 * Created by ITDIVISION\maximess087 on 23/2/17.
 */

public class LstApproverReject {
    private String DetailId;
    private String StrApprovalAllowed;

    public String getDetailId() {
        return DetailId;
    }

    public void setDetailId(String detailId) {
        DetailId = detailId;
    }

    public String getStrApprovalAllowed() {
        return StrApprovalAllowed;
    }

    public void setStrApprovalAllowed(String strApprovalAllowed) {
        StrApprovalAllowed = strApprovalAllowed;
    }
}
