package com.expenseondemand.eod.screen.approval.expense.itemlist;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.manager.CachingManager;
import com.expenseondemand.eod.model.approval.claim.ApprovalClaimListExpenseItem;
import com.expenseondemand.eod.model.approval.expense.ExpenseInfo;
import com.expenseondemand.eod.model.approval.expense.PolicyBreachModel;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.AppUtil;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by ITDIVISION\maximess087 on 5/3/17.
 */

public class ExpenseListAdapter extends ArrayAdapter<ExpenseItemsModel> {
    private ApproverExpenseListClickListener approverExpenseListClickListener;
    private Context context;
    private ArrayList<ExpenseItemsModel> approverExpenseItemsList;
    private PolicyBreachModel policyBreachModel;

    public interface ApproverExpenseListClickListener {
        void handleDuplicateClick(int position);

        void handleExpenseRowClick(ExpenseInfo expenseInfo);

        void handleCheckBoxClick(CheckBox checkBox);
    }

    public ExpenseListAdapter(Context context, ArrayList<ExpenseItemsModel> approverExpenseItemsList, ApproverExpenseListClickListener expenseListActivity1) {
        super(context, 0, approverExpenseItemsList);
        this.approverExpenseItemsList = approverExpenseItemsList;
        this.approverExpenseListClickListener = expenseListActivity1;
        this.context = context;
    }


    private static class ViewHolder {
        RelativeLayout relativeClaimantExpenseItem;
        CheckBox checkBoxSelectExpenses;
        ImageView imageViewExpenseCategoryIcon;
        ImageView imageViewAttachmentIcon;
        ImageView imageViewEscalation;
        ImageView imageViewDuplicate;
        ImageView imageViewViolation;
        TextView textViewExpenseCategory;
        TextView textViewExpenseCreationDate;
        TextView textViewExpenseAmount;
        ImageView imageViewRightArrow;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {

        View view = convertView;
        final ViewHolder v;
        if (view == null) {
            LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(R.layout.approver_expense_list_item, null);

        } else {
            view = convertView;
        }


        v = new ViewHolder();
        v.relativeClaimantExpenseItem = (RelativeLayout) view.findViewById(R.id.relativeClaimantExpenseItem);
        v.checkBoxSelectExpenses = (CheckBox) view.findViewById(R.id.checkBoxSelectExpenses);
        v.imageViewExpenseCategoryIcon = (ImageView) view.findViewById(R.id.imageViewExpenseCategoryIcon);
        v.imageViewAttachmentIcon = (ImageView) view.findViewById(R.id.imageViewAttachmentIcon);
        v.imageViewViolation = (ImageView) view.findViewById(R.id.imageViewViolation);
        v.imageViewEscalation = (ImageView) view.findViewById(R.id.imageViewEscalation);
        v.imageViewDuplicate = (ImageView) view.findViewById(R.id.imageViewDuplicate);
        v.textViewExpenseCategory = (TextView) view.findViewById(R.id.textViewExpenseCategory);
        v.textViewExpenseCreationDate = (TextView) view.findViewById(R.id.textViewExpenseCreationDate);
        v.textViewExpenseAmount = (TextView) view.findViewById(R.id.textViewExpenseAmount);
        v.imageViewRightArrow = (ImageView) view.findViewById(R.id.imageViewRightArrow);
        v.textViewExpenseCategory.setSelected(true);

        ExpenseItemsModel expenseItemsModel = approverExpenseItemsList.get(i);

        v.textViewExpenseCategory.setText(expenseItemsModel.getExpenseCategory());

        long date = AppUtil.getLongDate(AppConstant.FORMAT_DATE_WEB_SERVICE, expenseItemsModel.getDateExpense());
        String formattedDate = AppUtil.getFormattedDateString(AppConstant.FORMAT_DATE_DISPLAY, date);
        v.textViewExpenseCreationDate.setText(formattedDate);

        v.textViewExpenseAmount.setText(new DecimalFormat("0.00").format(Double.parseDouble(expenseItemsModel.getAmount())));

        //Row item click
        v.relativeClaimantExpenseItem.setTag(i);
        v.relativeClaimantExpenseItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                approverExpenseListClickListener.handleExpenseRowClick(prepareExpenseItemPolicyBreechInfo(i));
            }
        });

        //Duplicate list click
        v.imageViewDuplicate.setTag(i);
        v.imageViewDuplicate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                approverExpenseListClickListener.handleDuplicateClick(i);
            }
        });

        //set checked listener to checkbox click
        v.checkBoxSelectExpenses.setTag(expenseItemsModel);
        v.checkBoxSelectExpenses.setChecked(expenseItemsModel.isSelected());
        v.checkBoxSelectExpenses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox checkBox = (CheckBox) view;
                approverExpenseListClickListener.handleCheckBoxClick(checkBox);
            }
        });

        if (expenseItemsModel.getReceiptCount() > 0) {
            v.imageViewAttachmentIcon.setVisibility(View.VISIBLE);
        } else {
            v.imageViewAttachmentIcon.setVisibility(View.INVISIBLE);
        }

        //Expense Category Icon
        int resourceId = CachingManager.getCategoryResourceId(Integer.parseInt(expenseItemsModel.getBaseCategoryId()));
        v.imageViewExpenseCategoryIcon.setImageResource(resourceId);

        setPolicyBreechIcons(v, expenseItemsModel);
        setPolicyBreechTextColor(v, expenseItemsModel);
        setPolicyBreechBackground(v, expenseItemsModel);


        view.setTag(v);
        return view;
    }

    private void setPolicyBreechIcons(ViewHolder v, ExpenseItemsModel expenseItemsModel) {
        if (expenseItemsModel.isEscalationflag()) {
            v.imageViewEscalation.setVisibility(View.VISIBLE);
        }else{
            v.imageViewEscalation.setVisibility(View.GONE);
        }
        if (expenseItemsModel.isDuplicateFlag()) {
            v.imageViewDuplicate.setVisibility(View.VISIBLE);
        }else{
            v.imageViewDuplicate.setVisibility(View.GONE);
        }
        if (expenseItemsModel.getPolicyBreech() != null) {
            try {
                policyBreachModel = expenseItemsModel.getPolicyBreech();
                if (policyBreachModel.isDailyCap() || policyBreachModel.isMaxSpendBreach()) {
                    v.imageViewViolation.setVisibility(View.VISIBLE);
                }else{
                    v.imageViewViolation.setVisibility(View.GONE);
                }
            } catch (Exception ex) {
                ex.printStackTrace();

            }

        }
    }

    void setPolicyBreechTextColor(ViewHolder viewHolder, ExpenseItemsModel expenseItemsModel) {
        if (expenseItemsModel.isDuplicateFlag()) {
            viewHolder.textViewExpenseCategory.setTextColor(context.getResources().getColor(R.color.text_color_violet));
            viewHolder.textViewExpenseCreationDate.setTextColor(context.getResources().getColor(R.color.text_color_violet));
            viewHolder.textViewExpenseAmount.setTextColor(context.getResources().getColor(R.color.text_color_violet));
        }else{
            viewHolder.textViewExpenseCategory.setTextColor(context.getResources().getColor(R.color.text_color_black));
            viewHolder.textViewExpenseCreationDate.setTextColor(context.getResources().getColor(R.color.text_color_light_grey));
            viewHolder.textViewExpenseAmount.setTextColor(context.getResources().getColor(R.color.text_color_expense_amount));
        }
    }

    void setPolicyBreechBackground(ViewHolder viewHolder, ExpenseItemsModel expenseItemsModel) {
        PolicyBreachModel policyBreachModel = expenseItemsModel.getPolicyBreech();
        if (policyBreachModel.isExpenseLimit()) {
            viewHolder.relativeClaimantExpenseItem.setBackgroundResource(R.drawable.cream_backbgrund_selector);
        }else{
            viewHolder.relativeClaimantExpenseItem.setBackgroundResource(R.drawable.default_listitem_background_selector);
        }

        if (policyBreachModel.isPreApproval() && expenseItemsModel.isRejected()) {
            viewHolder.relativeClaimantExpenseItem.setBackgroundResource(R.drawable.light_green_background_selector);
        } else if (policyBreachModel.isPreApproval()) {
            viewHolder.relativeClaimantExpenseItem.setBackgroundResource(R.drawable.wheat_background_selector);
        } else if (expenseItemsModel.isRejected()) {
            viewHolder.relativeClaimantExpenseItem.setBackgroundResource(R.drawable.light_green_background_selector);
        }
    }


    private ExpenseInfo prepareExpenseItemPolicyBreechInfo(int position) {
        ExpenseInfo expenseInfo = new ExpenseInfo();
        ExpenseItemsModel expenseItemsModel = approverExpenseItemsList.get(position);
        expenseInfo.setExpenseDetailID(expenseItemsModel.getExpenseDetailID());
        expenseInfo.setExpenseHeaderID(expenseItemsModel.getExpenseHeaderID());
        expenseInfo.setPolicyBreech(expenseItemsModel.getPolicyBreech());
        expenseInfo.setCategoryName(expenseItemsModel.getExpenseCategory());

        return expenseInfo;
    }
}