package com.expenseondemand.eod.exception;

public class EODServiceException extends EODAppException
{
	private static final long serialVersionUID = 1L;

	//Constructor
	public EODServiceException(int exceptionCode)
	{
		super(exceptionCode, "");
	}
	
	//Constructor
	public EODServiceException(int exceptionCode, String exceptionMessage)
	{
		super(exceptionCode, exceptionMessage);
	}
}
