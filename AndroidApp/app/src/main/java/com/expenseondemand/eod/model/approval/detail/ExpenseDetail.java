package com.expenseondemand.eod.model.approval.detail;

import android.os.Parcel;
import android.os.Parcelable;

import com.expenseondemand.eod.model.approval.detail.attendees.AttendeesItem;
import com.expenseondemand.eod.model.approval.detail.dynamic.DynamicDetail;
import com.expenseondemand.eod.model.approval.detail.mileage.MultipleMileage;
import com.expenseondemand.eod.model.approval.detail.notes.Notes;
import com.expenseondemand.eod.model.approval.detail.project.AdditionalDetailsItem;
import com.expenseondemand.eod.model.approval.detail.project.BusinessPurposeItem;
import com.expenseondemand.eod.model.approval.detail.project.PaymentTypeItem;
import com.expenseondemand.eod.model.approval.detail.project.ProjectDetailItem;
import com.expenseondemand.eod.model.approval.detail.project.ProjectDetailsItem;
import com.expenseondemand.eod.model.approval.detail.project.ReceiptItem;

import java.util.ArrayList;

/**
 * Created by Ramit Yadav on 20-10-2016.
 */

public class ExpenseDetail implements Parcelable
{
    private String uniqueId;
    private String date;
    private String amount;
    private String isItemised;
    private int baseCategoryId;
    private ArrayList<DynamicDetail> dynamicDetails;
    //  private ArrayList<ProjectDetailItem> projectDetails;
    private ArrayList<ProjectDetailsItem> ProjectDetails;
    private PaymentTypeItem PaymentType;
    private BusinessPurposeItem BusinessPurpose;
    private ArrayList<AdditionalDetailsItem> AdditionalDetails;
    private ReceiptItem Receipt;
    private Notes notes;
    private MultipleMileage mileage;
    private ArrayList<AttendeesItem> attendees;


    public ExpenseDetail(Parcel in)
    {
        uniqueId = in.readString();
        date = in.readString();
        amount = in.readString();
        isItemised = in.readString();
        baseCategoryId = in.readInt();
        dynamicDetails = in.readArrayList(DynamicDetail.class.getClassLoader());
        ProjectDetails = in.readArrayList(ProjectDetailsItem.class.getClassLoader());
    }

    public ExpenseDetail()
    {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(uniqueId);
        dest.writeString(date);
        dest.writeString(amount);
        dest.writeString(isItemised);
        dest.writeInt(baseCategoryId);
        dest.writeList(dynamicDetails);
        dest.writeList(ProjectDetails);
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    public static final Creator<ExpenseDetail> CREATOR = new Creator<ExpenseDetail>() {
        @Override
        public ExpenseDetail createFromParcel(Parcel in) {
            return new ExpenseDetail(in);
        }

        @Override
        public ExpenseDetail[] newArray(int size) {
            return new ExpenseDetail[size];
        }
    };

    public int getBaseCategoryId() {
        return baseCategoryId;
    }

    public void setBaseCategoryId(int baseCategoryId) {
        this.baseCategoryId = baseCategoryId;
    }

    public String getUniqueId()
    {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId)
    {
        this.uniqueId = uniqueId;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public String getAmount()
    {
        return amount;
    }

    public void setAmount(String amount)
    {
        this.amount = amount;
    }

    public String getIsItemised()
    {
        return isItemised;
    }

    public void setIsItemised(String isItemised)
    {
        this.isItemised = isItemised;
    }

    public ArrayList<DynamicDetail> getDynamicDetails()
    {
        return dynamicDetails;
    }

    public void setDynamicDetails(ArrayList<DynamicDetail> dynamicDetails)
    {
        this.dynamicDetails = dynamicDetails;
    }

    /*public ArrayList<ProjectDetailItem> getProjectDetails()
    {
        return projectDetails;
    }

    public void setProjectDetails(ArrayList<ProjectDetailItem> projectDetails)
    {
        this.projectDetails = projectDetails;
    }*/

    public ArrayList<ProjectDetailsItem> getProjectDetails() {
        return ProjectDetails;
    }

    public void setProjectDetails(ArrayList<ProjectDetailsItem> projectDetails) {
        ProjectDetails = projectDetails;
    }

    public PaymentTypeItem getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(PaymentTypeItem paymentType) {
        PaymentType = paymentType;
    }

    public BusinessPurposeItem getBusinessPurpose() {
        return BusinessPurpose;
    }

    public void setBusinessPurpose(BusinessPurposeItem businessPurpose) {
        BusinessPurpose = businessPurpose;
    }

    public ArrayList<AdditionalDetailsItem> getAdditionalDetails() {
        return AdditionalDetails;
    }

    public void setAdditionalDetails(ArrayList<AdditionalDetailsItem> additionalDetails) {
        AdditionalDetails = additionalDetails;
    }

    public ReceiptItem getReceipt() {
        return Receipt;
    }

    public void setReceipt(ReceiptItem receipt) {
        Receipt = receipt;
    }

    public Notes getNotes()
    {
        return notes;
    }

    public void setNotes(Notes notes)
    {
        this.notes = notes;
    }

    public MultipleMileage getMileage()
    {
        return mileage;
    }

    public void setMileage(MultipleMileage mileage)
    {
        this.mileage = mileage;
    }

    public ArrayList<AttendeesItem> getAttendees()
    {
        return attendees;
    }

    public void setAttendees(ArrayList<AttendeesItem> attendees)
    {
        this.attendees = attendees;
    }
}
