package com.expenseondemand.eod.model;

public class ExpenseDate implements Expenseable
{
	private String expenseDate;

	public String getExpenseDate() {
		return expenseDate;
	}

	public void setExpenseDate(String expenseDate) {
		this.expenseDate = expenseDate;
	}
	

}
