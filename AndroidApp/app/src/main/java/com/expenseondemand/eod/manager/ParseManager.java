package com.expenseondemand.eod.manager;

import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.webservice.model.MasterRequest;
import com.expenseondemand.eod.webservice.model.MasterResponse;


public class ParseManager
{
	//Prepare JSON request string
	public static String prepareJsonRequest(MasterRequest requestObject) throws EODException
	{
		//Create JSON String
		String jsonRequestString = JacksonHandler.createJson(requestObject);

		return jsonRequestString;
	}

	//Prepare Web Service Object Model
	public static MasterResponse prepareWebServiceResponseObject(Class<? extends Object> classInstance, String jsonResponseString) throws EODException
	{
		//Get Response Object
		MasterResponse masterResponse = JacksonHandler.createResponse(jsonResponseString, classInstance);

		return masterResponse;
	}
}
