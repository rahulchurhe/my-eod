package com.expenseondemand.eod.webservice.model.approval.item;

import java.util.ArrayList;

/**
 * Created by Nandani Gupta on 2016-10-20.
 */
public class ItemsResponseModel

{

    String rowId;
    String BaseCategoryId;
    String  referenceNumber;
    String  dateExpense;
    String  customerName;
    String  projectName;
    String  expenseCategory;
    String  billable;
    String  paymentTypeName;
    String  currencyCode;
    String  expenseAmount;
    String  amount;
    String  expenseDetailID;
    String  expenseHeaderID;
    String  itemizationID;
    String  rejected;
    PolicyBreechResponseModel policyBreech;
    String  notes;
    String  escalationflag;
    String  duplicateFlag;
    ArrayList<DuplicateItemsModel> DuplicateItems;
    String  receipt;
    int receiptCount;

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getBaseCategoryId() {
        return BaseCategoryId;
    }

    public void setBaseCategoryId(String baseCategoryId) {
        BaseCategoryId = baseCategoryId;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getDateExpense() {
        return dateExpense;
    }

    public void setDateExpense(String dateExpense) {
        this.dateExpense = dateExpense;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getExpenseCategory() {
        return expenseCategory;
    }

    public void setExpenseCategory(String expenseCategory) {
        this.expenseCategory = expenseCategory;
    }

    public String getBillable() {
        return billable;
    }

    public void setBillable(String billable) {
        this.billable = billable;
    }

    public String getPaymentTypeName() {
        return paymentTypeName;
    }

    public void setPaymentTypeName(String paymentTypeName) {
        this.paymentTypeName = paymentTypeName;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getExpenseAmount() {
        return expenseAmount;
    }

    public void setExpenseAmount(String expenseAmount) {
        this.expenseAmount = expenseAmount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getExpenseDetailID() {
        return expenseDetailID;
    }

    public void setExpenseDetailID(String expenseDetailID) {
        this.expenseDetailID = expenseDetailID;
    }

    public String getExpenseHeaderID() {
        return expenseHeaderID;
    }

    public void setExpenseHeaderID(String expenseHeaderID) {
        this.expenseHeaderID = expenseHeaderID;
    }

    public String getItemizationID() {
        return itemizationID;
    }

    public void setItemizationID(String itemizationID) {
        this.itemizationID = itemizationID;
    }

    public String getRejected() {
        return rejected;
    }

    public void setRejected(String rejected) {
        this.rejected = rejected;
    }

    public PolicyBreechResponseModel getPolicyBreech() {
        return policyBreech;
    }

    public void setPolicyBreech(PolicyBreechResponseModel policyBreech) {
        this.policyBreech = policyBreech;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getEscalationflag() {
        return escalationflag;
    }

    public void setEscalationflag(String escalationflag) {
        this.escalationflag = escalationflag;
    }

    public String getDuplicateFlag() {
        return duplicateFlag;
    }

    public void setDuplicateFlag(String duplicateFlag) {
        this.duplicateFlag = duplicateFlag;
    }

    public ArrayList<DuplicateItemsModel> getDuplicateItems() {
        return DuplicateItems;
    }

    public void setDuplicateItems(ArrayList<DuplicateItemsModel> duplicateItems) {
        DuplicateItems = duplicateItems;
    }

    public String getReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public int getReceiptCount() {
        return receiptCount;
    }

    public void setReceiptCount(int receiptCount) {
        this.receiptCount = receiptCount;
    }
}
