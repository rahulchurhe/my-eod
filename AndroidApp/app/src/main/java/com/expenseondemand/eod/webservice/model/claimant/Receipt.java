package com.expenseondemand.eod.webservice.model.claimant;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Receipt 
{
	private String fileName;
	private String mimeType;
	private String receiptData;
	
	public String getFileName() 
	{
		return fileName;
	}
	
	public void setFileName(String fileName) 
	{
		this.fileName = fileName;
	}
	
	@JsonProperty("MIMEType")
	public String getMimeType() 
	{
		return mimeType;
	}
	
	public void setMimeType(String mimeType) 
	{
		this.mimeType = mimeType;
	}
	
	public String getReceiptData() 
	{
		return receiptData;
	}
	
	public void setReceiptData(String receiptData) 
	{
		this.receiptData = receiptData;
	}
}
