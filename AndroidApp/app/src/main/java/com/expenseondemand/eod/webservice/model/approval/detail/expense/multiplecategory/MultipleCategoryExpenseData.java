package com.expenseondemand.eod.webservice.model.approval.detail.expense.multiplecategory;

import com.expenseondemand.eod.webservice.model.approval.detail.Attendees;
import com.expenseondemand.eod.webservice.model.approval.detail.DynamicDetail;
import com.expenseondemand.eod.webservice.model.approval.detail.Notes;
import com.expenseondemand.eod.webservice.model.approval.detail.ProjectDetail;
import com.expenseondemand.eod.webservice.model.approval.detail.mileage.Mileage;

import java.util.ArrayList;

/**
 * Created by ITDIVISION\maximess087 on 18/1/17.
 */

public class MultipleCategoryExpenseData {
    private String uniqueId;
    private String date;
    private String amount;
    private String isItemised;
    private int baseCategoryId;
    private ArrayList<DynamicDetail> dynamicDetails;
    private ArrayList<ProjectDetail> projectDetails;
    private Notes notes;
    private Mileage mileage;
    private ArrayList<Attendees> attendees;

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getIsItemised() {
        return isItemised;
    }

    public void setIsItemised(String isItemised) {
        this.isItemised = isItemised;
    }

    public int getBaseCategoryId() {
        return baseCategoryId;
    }

    public void setBaseCategoryId(int baseCategoryId) {
        this.baseCategoryId = baseCategoryId;
    }

    public ArrayList<DynamicDetail> getDynamicDetails() {
        return dynamicDetails;
    }

    public void setDynamicDetails(ArrayList<DynamicDetail> dynamicDetails) {
        this.dynamicDetails = dynamicDetails;
    }

    public ArrayList<ProjectDetail> getProjectDetails() {
        return projectDetails;
    }

    public void setProjectDetails(ArrayList<ProjectDetail> projectDetails) {
        this.projectDetails = projectDetails;
    }

    public Notes getNotes() {
        return notes;
    }

    public void setNotes(Notes notes) {
        this.notes = notes;
    }

    public Mileage getMileage() {
        return mileage;
    }

    public void setMileage(Mileage mileage) {
        this.mileage = mileage;
    }

    public ArrayList<Attendees> getAttendees() {
        return attendees;
    }

    public void setAttendees(ArrayList<Attendees> attendees) {
        this.attendees = attendees;
    }
}
