package com.expenseondemand.eod.screen.approval.expense.multiplecategory;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.expenseondemand.eod.R;
import com.expenseondemand.eod.activity.BaseActivity;
import com.expenseondemand.eod.dialogFragment.ApplicationAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationErrorAlertDialog;
import com.expenseondemand.eod.dialogFragment.ApplicationNetworkDialog;
import com.expenseondemand.eod.exception.EODBusinessException;
import com.expenseondemand.eod.exception.EODException;
import com.expenseondemand.eod.manager.DeviceManager;
import com.expenseondemand.eod.manager.SharePreference;
import com.expenseondemand.eod.model.approval.detail.ExpenseDetail;
import com.expenseondemand.eod.model.approval.expense.PolicyBreachModel;
import com.expenseondemand.eod.model.approval.expense.multiplecategory.MultiCategoryExpense;
import com.expenseondemand.eod.model.approval.expense.multiplecategory.MultiCategoryExpenseParam;
import com.expenseondemand.eod.screen.approval.detail.ExpenseDetailManager;
import com.expenseondemand.eod.screen.approval.expense.duplicate.DuplicateEntryActivity;
import com.expenseondemand.eod.util.AppConstant;
import com.expenseondemand.eod.util.UIUtil;
import com.expenseondemand.eod.webservice.model.RejectionReasonModel.RejectionReasonModel;
import com.expenseondemand.eod.webservice.model.approval.item.DuplicateItemsModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MultipleCategoryListActivity extends BaseActivity implements View.OnClickListener, MultipleCategoryExpenseListInterface, ApplicationAlertDialog.AlertDialogActionListener, ApplicationErrorAlertDialog.ErrorDialogActionListener {

    private RecyclerView recyclerViewExpenseList;
    MultipleCategoryListAdapter multipleCategoryListAdapter;
    private MultiCategoryExpenseParam multiCategoryExpenseParam;
    private ApplicationAlertDialog applicationAlertDialog;
    public static final int TYPE_ALERT = 0;
    private TextView textViewSelectAll;
    private TextView textViewClaimantName;
    private ExpenseDetail expenseDetail;
    private ArrayList<MultiCategoryExpense> approverExpenseItemsList = new ArrayList<MultiCategoryExpense>();
    private String claimName;
    private TextView textViewExpenseTitle;
    private String claimType;
    ArrayList<RejectionReasonModel> rejectionReasonBackupList;
    private ApplicationNetworkDialog applicationNetworkDialogl;
    private ApplicationErrorAlertDialog applicationErrorAlertDialog;
    private RelativeLayout relativeOverlayScreenExpense;

    @Override
    public void onBackPressed() {
        if (relativeOverlayScreenExpense.getVisibility() == View.GONE) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*Back from network setting screen */
        if (SharePreference.getBoolean(getApplicationContext(), AppConstant.BACK_NETWORK_SETTING_STAT)) {
            SharePreference.putBoolean(getApplicationContext(), AppConstant.BACK_NETWORK_SETTING_STAT, false);
            multipleCategoryListAsyncTask();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_category_list);

        //Initialize View
        initializeView();
    }

    private void initializeView() {
        //get Multiple Category Expense param
        Bundle bundle = getIntent().getExtras();
        Intent i = getIntent();
        multiCategoryExpenseParam = (MultiCategoryExpenseParam) i.getParcelableExtra(AppConstant.KEY_MULTIPLE_CATEGORY_EXPENSE);
        expenseDetail = (ExpenseDetail) i.getParcelableExtra(AppConstant.KEY_EXPENSE_DETAIL);
        claimName = bundle.getString(AppConstant.KEY_CLAIM_NAME);
        claimType = bundle.getString(AppConstant.KEY_CLAIM_TYPE);

        //Handle Action Bar
        handleToolBar(getResources().getString(R.string.text_multiplae_category), R.color.approval_claim_list_toolbar, R.color.approval_claim_list_status_bar, true);

        //Get List View
        recyclerViewExpenseList = (RecyclerView) findViewById(R.id.recyclerViewMultipleCategory);

        textViewClaimantName = (TextView) findViewById(R.id.textViewClaimantName);
        textViewSelectAll = (TextView) findViewById(R.id.textViewSelectAll);

        // UserInfo userInfo = CachingManager.getUserInfo();
        textViewClaimantName.setSelected(true);
        textViewClaimantName.setText(claimName);
        textViewSelectAll.setText(expenseDetail.getUniqueId());

        textViewExpenseTitle = (TextView) findViewById(R.id.textViewExpenseTitle);
        textViewExpenseTitle.setText(claimType);

        //overlay close button
        ImageButton imageViewCloseButton = (ImageButton) findViewById(R.id.imageViewCloseButton);
        imageViewCloseButton.setOnClickListener(this);

        //overlay layout
        relativeOverlayScreenExpense = (RelativeLayout) findViewById(R.id.relativeOverlayScreenExpense);
        relativeOverlayScreenExpense.setOnClickListener(this);

        multipleCategoryListAsyncTask();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Get Menu Inflater
        MenuInflater inflater = getMenuInflater();

        //Inflate Menu
        inflater.inflate(R.menu.help_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuItemId = item.getItemId();
        switch (menuItemId) {
            case R.id.menuHelp:
                //Handle OverlayScreen
                handleOverlayScreen();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    //OverLay screen
    void handleOverlayScreen() {
        relativeOverlayScreenExpense.setVisibility(relativeOverlayScreenExpense.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
    }

    public void handleHomeUpNavigation() {
        this.finish();
        overridePendingTransition(R.anim.slide_in_back, R.anim.slide_out_back);
    }

    /***
     * Navigate to Duplicate Expenses Activity
     */
    @Override
    public void handleDuplicateClick(int position) {
        navigateToDuplicateEntryActivity(approverExpenseItemsList.get(position).getDuplicateItems());
    }

    private void navigateToDuplicateEntryActivity(ArrayList<DuplicateItemsModel> duplicateItems) {
        try {
            JSONArray jsonArray = new JSONArray();
            for (DuplicateItemsModel duplicateItemsModel : duplicateItems) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("ColourIndicator", duplicateItemsModel.getColourIndicator());

                jsonObject.put("FieldTitle1", duplicateItemsModel.getDuplicateItem().getFieldTitle1());
                jsonObject.put("FieldValue1", duplicateItemsModel.getDuplicateItem().getFieldValue1());

                jsonObject.put("FieldTitle2", duplicateItemsModel.getDuplicateItem().getFieldTitle2());
                jsonObject.put("FieldValue2", duplicateItemsModel.getDuplicateItem().getFieldValue2());

                jsonObject.put("FieldTitle3", duplicateItemsModel.getDuplicateItem().getFieldTitle3());
                jsonObject.put("FieldValue3", duplicateItemsModel.getDuplicateItem().getFieldValue3());

                jsonObject.put("FieldTitle4", duplicateItemsModel.getDuplicateItem().getFieldTitle4());
                jsonObject.put("FieldValue4", duplicateItemsModel.getDuplicateItem().getFieldValue4());

                jsonArray.put(jsonObject);
            }

            Log.e("jsonArray", ">>>" + jsonArray.toString());

            Intent intent = new Intent(this, DuplicateEntryActivity.class);
            intent.putExtra(AppConstant.KEY_PARENT_DUPLICATE_EXPENSE_ITEM, jsonArray.toString());
            startActivity(intent);

            overridePendingTransition(R.anim.slide_in_forward, R.anim.slide_out_forward);
        } catch (Exception e) {

        }
    }

    /***
     * Navigate to Expense Detail Activity
     */
    @Override
    public void handleExpenseRowClick(int position) {
        navigateToExpenseDetailActivity(approverExpenseItemsList.get(position), approverExpenseItemsList.get(position).getPolicyBreech(), approverExpenseItemsList.get(position).isRejected());

    }

    private void navigateToExpenseDetailActivity(MultiCategoryExpense multiCategoryExpense, PolicyBreachModel policyDetails, boolean isRejected) {
        //Create Intent
        Intent intent = new Intent(this, MultipleCategoryExpenseDetailActivity.class);
        intent.putExtra(AppConstant.KEY_MULTIPLE_CATEGORY_DETAIL_EXPENSE, multiCategoryExpense);
        intent.putExtra(AppConstant.KEY_MULTIPLE_CATEGORY_POLICY_DETAILS, policyDetails);
        intent.putExtra(AppConstant.KEY_EXPENSE_DETAIL, multiCategoryExpenseParam);
        intent.putExtra("IS_REJECTED", isRejected);
        intent.putExtra(AppConstant.KEY_CLAIM_NAME, claimName);
        intent.putExtra(AppConstant.KEY_CLAIM_TYPE, claimType);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_forward, R.anim.slide_out_forward);
    }

    /***
     * Multiple category Expenses AsyncTask
     **/
    private void multipleCategoryListAsyncTask() {
        if (DeviceManager.isOnline()) {
            new MultipleCategoryExpenseListAsyncTask(multiCategoryExpenseParam).execute();
        } else {
            enableNetwork();
        }

    }

    @Override
    public void onRetryButtonClick(int type) {
        multipleCategoryListAsyncTask();
    }

    @Override
    public void onCancelButtonClick() {

    }

    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        switch (viewId) {
            case R.id.imageViewCloseButton:
                handleOverlayScreen();
                break;
            case R.id.relativeOverlayScreenExpense:
                handleOverlayScreen();
                break;
        }
    }


    private class MultipleCategoryExpenseListAsyncTask extends AsyncTask<Void, Void, ArrayList<MultiCategoryExpense>> {
        private MultiCategoryExpenseParam multiCategoryExpenseParam;
        private Dialog dialog = null;
        private Exception exception = null;
        private ArrayList<MultiCategoryExpense> multiCategoryExpense = null;

        public MultipleCategoryExpenseListAsyncTask(MultiCategoryExpenseParam multiCategoryExpenseParam) {
            this.multiCategoryExpenseParam = multiCategoryExpenseParam;
        }


        @Override
        protected void onPreExecute() {
            dialog = UIUtil.showProgressDialog(MultipleCategoryListActivity.this);
        }

        @Override
        protected ArrayList<MultiCategoryExpense> doInBackground(Void... voids) {
            try {
                multiCategoryExpense = ExpenseDetailManager.getExpenseMultipleCategory(multiCategoryExpenseParam);
            } catch (EODException eodException) {
                this.exception = eodException;
            }
            return multiCategoryExpense;
        }

        @Override
        protected void onPostExecute(ArrayList<MultiCategoryExpense> multiCategoryExpense) {
            UIUtil.dismissDialog(dialog);
            if (exception == null) {
                //OnSuccess
                onSuccessExpenseItemsList(multiCategoryExpense);
            } else if (exception instanceof EODBusinessException) {
                //OnFail
                onFailGetExpenseItemsList((EODException) exception);
            } else if (exception instanceof EODException) {
                //OnError
                onErrorExpenseItemsList(exception);
            }
        }


    }

    public void onSuccessExpenseItemsList(ArrayList<MultiCategoryExpense> multiCategoryExpense) {
        if (multiCategoryExpense != null) {
            approverExpenseItemsList.clear();
            approverExpenseItemsList.addAll(multiCategoryExpense);

            multipleCategoryListAdapter = new MultipleCategoryListAdapter(MultipleCategoryListActivity.this, approverExpenseItemsList, this);
            recyclerViewExpenseList.setAdapter(multipleCategoryListAdapter);
        } else {

        }
    }

    private void onErrorExpenseItemsList(Exception exception) {
        //showAlertDialog(getResources().getString(R.string.text_error));
        showErrorAlertDialog(false, null, null, null, ApplicationAlertDialog.TYPE_ERROR);
    }

    void showErrorAlertDialog(boolean neutralButton, String message, String title, String okButtonTitle, int type) {
        applicationErrorAlertDialog = new ApplicationErrorAlertDialog().getInstance(this, message, title, okButtonTitle, type);
        applicationErrorAlertDialog.show(getSupportFragmentManager(), "dialog");
    }

    private void onFailGetExpenseItemsList(EODException exception) {
        // showAlertDialog(getResources().getString(R.string.text_error));
        showErrorAlertDialog(false, null, null, null, ApplicationAlertDialog.TYPE_ERROR);
    }

    /****
     * Show Alert Dialog
     */
    private void showAlertDialog(String msg) {
        applicationAlertDialog = new ApplicationAlertDialog().getInstance(this, true, getString(R.string.text_error_processing), msg, getString(R.string.button_ok), TYPE_ALERT);
        applicationAlertDialog.show(getSupportFragmentManager(), "dialog");
    }

    /*Network enable dialog*/
    void enableNetwork() {
        applicationNetworkDialogl = new ApplicationNetworkDialog().getInstance(MultipleCategoryListActivity.this);
        applicationNetworkDialogl.show(getSupportFragmentManager(), "dialog");
    }


    @Override
    public void onPositiveButtonClick(int type) {
    }

    @Override
    public void onNegativeButtonClick() {
    }


}
